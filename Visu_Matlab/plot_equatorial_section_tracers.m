function [] = plot_equatorial_section_tracers(tra,epsilon,npt_r,npt_p,rmin,rmax,pmin,pmax,TYPE)
% Plots equatorial section using all tracers within a small distance from
% equator

tra_mat = ones(length(tra)/5,5);

% Starts by redimensionalize the array
length(tra);
for i=1:length(tra)/5-5
    for j=1:5
        tra_mat(i,j) = tra((i-1)*5+j);
    end
end

length(tra)

% First select all corresponding tracers and project them on equatorial
% plane
%nr = length(r);
ntr = length(tra_mat(:,1));

n=0;
%for i=1:ntr
%    rad   = tra_mat(i,1);
%    theta = tra_mat(i,2);
%    phi   = tra_mat(i,3);
%    if (abs(rad*cos(theta)) <= epsilon && rmin <= rad && rad <= rmax && pmin <= phi && phi <= pmax) 
%        n = n + 1;
%    end
%end
%n
%proj = ones(n,3);

for i=1:ntr
    rad   = tra_mat(i,1);
    theta = tra_mat(i,2);
    phi   = tra_mat(i,3);
    if (abs(rad*cos(theta)) <= epsilon && rmin <= rad && rad <= rmax && pmin <= phi && phi <= pmax)
%        rad
%        theta
%        phi
        n = n+1;
        proj(n,1) = rad*cos(phi); % in cartesian geometry
        proj(n,2) = rad*sin(phi); % in cartesian geometry
        proj(n,3) = tra_mat(i,5);
    end
end

n
%tra_mat(:,1:4)

%Now interpolate to a regular grid
%Define finer grid
if (TYPE == 1)

r_rep = linspace(rmin,rmax,npt_r);
p_rep = linspace(pmin,pmax,npt_p);
[R, T] = meshgrid(r_rep,p_rep);
[X,Y] = pol2cart(T, R);

%proj
%X
%Y

V = griddata(proj(:,1),proj(:,2),proj(:,3),X,Y,'cubic');


%Finally plot the results
figure(1)
pcolor(X,Y,V); shading interp;
axis equal;
axis off;
hold on;
%circle(0,0,rmin);
%circle(0,0,rmax);
colorbar;
hold off;

end 
    
if (TYPE == 2) 

figure(2)
scatter(proj(:,1),proj(:,2),1,proj(:,3),'filled')
hold on;
circle(0,0,r(1));
circle(0,0,r(nr));
colorbar;
hold off;

end
end

