function plot_kappa (kappa_surf,dCdr)

% Portion dCdr < 0
subplot(1,2,1)
plot(dCdr(10:50),1./kappa_surf(10:50),'k');
grid on;
xlabel('\partial\xi/\partialr');
ylabel('\kappa^-^1^/^2');
xlim([-inf 0])

% Portion dCdr > 0
subplot(1,2,2)
plot(dCdr(50:147),1./sqrt(kappa_surf(50:147)),'k');
grid on;
xlabel('\partial\xi/\partialr');
ylabel('\kappa^-^1^/^2');
xlim([0 inf])

end