function [moy] = calc_moyenne_glissante (F,n)

npts = length(F);
for i = n+1:npts-n
    moy(i) = mean(F(i-n:i+n));
end
for i = 1:n
    moy(i) = mean(F(1:i+n));
end
for i = npts-n+1:npts
    moy(i) = mean(F(i-n:npts));
end

end