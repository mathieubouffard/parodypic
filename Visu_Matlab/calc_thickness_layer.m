function [thickness] = calc_thickness_layer (Cmean,Tmean,Br,r)

nr = length(r);
%NAIF
%i=nr/2;
%thickness = 0;
%while C(i) < 0.5 && i<nr
%    thickness = r(nr)-r(i);
%    i=i+1;
%end

% INTEGRAL EVALUATION code � l'arrache

%Cmax = max(C(nr_min:nr));
%int = 0;
%int_var=0;

%ir_max=find(C==Cmax);
%Cmin = min(C(nr_min:ir_max));
%ir_min=find(C==Cmin);
%for i = ir_min:nr
%    C(i) = C(i) - Cmin; %need positive C
%end
%C;
%ir_max;
%for i = ir_min:ir_max
%    dr(i) = 0.5*(r(min(i+1,nr))-r(i-1));
%    int_var = int_var + C(i)*(r(ir_max)-r(i))*4*pi*r(i)^2*dr(i);
%    int     = int + C(i)*4*pi*r(i)^2*dr(i);
%end
%int;
%int_var;

%thickness = (r(nr)-r(ir_max)) + int_var/int;

%USING THE BRUNT VAISALA FREQUENCY

%BV = plot_Brunt_Vaisala_frequency (Tmean,Cmean,Br,r);
%BV = max(BV,0);

i = nr-5; % REVOIR LE DEPART 
%BV(i);
%while (BV(i) > 0) 
%    i = i-1;
%end
while (Cmean(i)-0.0429 > 0)
    i = i-1;
end
thickness = r(nr) - r(i);


end