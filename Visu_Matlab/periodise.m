function [C_periodised] = periodise (C,p)

%When periodicity is assumed in longitude, creates a 3D array that contains
%the values of the field in the whole sphere
%p is the periodicity (p=1 : whole sphere, p=2 : half a sphere etc.)

if (ndims(C) == 3)
    
Nlong = length(C(:,1,1));

for i=0:(p-1)
    C_periodised(1+i*Nlong:(i+1)*Nlong,:,:) = C(:,:,:);
end

% use only for phi !
else
    
    dphi = C(2) - C(1);
    Nlong = length(C);
    C_periodised(1:p*(Nlong-1)) = C(1);

    for i=2:(p*Nlong)
    C_periodised(i) = C_periodised(i-1) + dphi;
    end
    
end
    

end