function plot_Emag_time (n)

%plot the (n-1)th column of e_kin.bm1 as a function of the first column
%(time)
% n = 2 : energVp + energVt
% n = 3 : energVp
% n = 4 : energVt
% n = 5 : energVpa
% n = 6 : energVta

%first open the file
fid = fopen('e_mag.bm1','r');
e_mag = fscanf(fid,'%g %g %g %g %g',[6 Inf]);

e_mag = e_mag';

%then plot

% Create figure
%figure1 = figure('XVisual',...
%    '0xc2 (TrueColor, depth 24, RGB mask 0xff0000 0xff00 0x00ff)');

% Create axes
%axes1 = axes('Parent',figure1,'YGrid','on','XGrid','on','FontWeight','demi',...
%    'FontSize',16);
%box(axes1,'on');
%hold(axes1,'all');

% Create plot
plot(e_mag(:,1),e_mag(:,n),'LineWidth',2);

% Create xlabel
xlabel('time');

% Create ylabel
if (n == 2)
    ylabel('EnergBp+EnergBt');
elseif (n == 3)
    ylabel('EnergBp');
elseif (n == 4)
    ylabel('EnergBt');
elseif (n == 5)
    ylabel('EnergBpa');
elseif (n == 6)
    ylabel('EnergBta');
end

end