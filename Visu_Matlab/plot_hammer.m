function plot_hammer (theta,phi,Field,ir)

nt = length(theta);
np = length(phi);

latitude = -(theta-pi/2);
longitude = -(phi-pi);

f = [Field(np/2:np,1:nt/2-1,ir)' Field(1:np/2-1,1:nt/2-1,ir)'
    Field(np/2:np,nt/2:nt,ir)' Field(1:np/2-1,nt/2:nt,ir)'];

[L,T] = meshgrid(longitude,latitude);

[HX,HY] = sph2hammer(L,T);
clf;
pcolor(HX,HY,f);
colorbar, shading interp , daspect([1 1 1]);

end