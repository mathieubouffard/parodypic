function equatorial_section (r,phi,C)

% plot an equatorial section of the field C
% don't forget to periodise phi !

Nlat = length(C(1,:,1));
Nlong = length(phi);

%add an extra phi to make a complete disk
phi(Nlong+1) = phi(1);
C(Nlong+1,:,:) = C(1,:,:);

[R, T] = meshgrid(r, phi);
[Y, X] = pol2cart(T, R);
Z = squeeze(C(:,floor(Nlat/2),:));

size(X)
size(Y)
size(Z)

figure(1)
pcolor(X,Y,Z); shading interp;
axis equal;
axis off;
hold on;
colorbar;

end