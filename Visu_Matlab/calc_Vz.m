function Vz = calc_Vz (Vr,Vt,theta)

Nlat = length(theta);

Vz = 0 * Vr;

for j=1:Nlat
    Vz(:,j,:) = Vr(:,j,:)*cos(theta(j)) + Vt(:,j,:)*sin(theta(j));
end

end
            