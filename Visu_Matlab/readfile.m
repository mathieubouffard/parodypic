function matrix = readfile(name,type,Nrad,Nlat,Nlong)
%Open and make a matrix of file to the right dimensions

fid = fopen(name)
vector = fread(fid,type);

matrix = make_matrix (vector,Nrad,Nlat,Nlong);

end

