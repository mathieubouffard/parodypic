function [S_r,S_t,S_p] = calc_threshold (r,theta,phi,p)

nr = length(r);
nt = length(theta);
np = length(phi);
dphi = 2.*(phi(2)-phi(1)); %regular spacing

S_r(1:np,1:nt,1) = p/(r(2)-r(1));
for i = 2:nr-1
    S_r(1:np,1:nt,i) = 2*p/(r(i+1)-r(i-1));   
end
S_r(1:np,1:nt,nr) = p/(r(2)-r(1));

for i = 1:nr
    S_t(1:np,1,i) = p/(r(i)*theta(1));
    for j = 2:nt-1
        S_t(1:np,j,i) = 2*p/(r(i)*(theta(j+1)-theta(j)));
    end
    S_t(1:np,nt,i) = p/(r(i)*theta(1));
end

for i=1:nr
    for j = 1:nt
        for k = 1:np
            S_p(k,j,i) = 2*p/(r(i)*sin(theta(j))*dphi);
        end
    end
end


end
