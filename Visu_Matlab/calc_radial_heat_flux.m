function [hf_r] = calc_radial_heat_flux (r,theta,phi,T)

nr = length(r);
nt = length(theta);
np = length(phi);

for i=1:nr-1
    for j=1:nt
        for k=1:np
            hf_r(k,j,i) = (T(k,j,i+1)-T(k,j,i))/(r(i+1)-r(i));
        end
    end
end

hf_r(:,:,nr) = hf_r(:,:,nr-1);

end