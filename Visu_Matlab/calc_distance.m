function [distances] = calc_distance (maptrth)
%UNTITLED3 Summary of this function goes here
% Arguments: (input)
%  mapxy - coordinates of the tracers with time in spherical coordinates
%            (ouput)
%  distances - array containing the distances of the tracers to the curve 

% problem constants
r0 = 0.5385; %icb
r1 = 1.5385; %cmb
rmean = (r0+r1)/2.;
rmean0 = (r0*2+r1)/3;
rmean1 = (r0+r1*2)/3;

npts = 1000;
dr = 0.009;

% Vr(r,th) = sin(a*r+b) * cos(th)
a = pi/(r1-r0);
b = -pi*r0/(r1-r0);


%extract the coordinates
r  = maptrth(2,:);
th = maptrth(3,:);
npts = length(r);

% Determine (rmin,rmax) 
A = real(r(1)*sqrt(sin(a*r(1)+b))*sin(th(1)));

%f = 'A/(x*sqrt(sin(a*x+b)))-1';
%df = '-A/(x^2*sin(a*x+b))*(sqrt(sin(a*x+b))+x/(2*sqrt(sin(a*x+b)))*a*cos(a*x+b))';

f = @(x)(A/(x*sqrt(sin(a*x+b)))-1);

 rmin = fzero(f,[r0+0.00001 rmean]);
 rmax = fzero(f,[rmean r1-0.00001]);

%[rmin,ex] = newton(f,df,rmean0,1e-4,100,A,a,b)
%[rmax,ex] = newton(f,df,rmean1,1e-4,100,A,a,b)

%syms s;
%rmin = vpasolve(A/(s*sqrt(sin(a*s+b))) == 1,s,[r0 rmean]);
%rmax = vpasolve(A/(s*sqrt(sin(a*s+b))) == 1,s,[rmean r1]);

rmin = double(rmin);
rmax = double(rmax);

%construct flow line of reference

r_fl1_deb = linspace(rmin,rmin+dr,npts);
r_fl1_mid = linspace(rmin+dr,rmax-dr,npts); %flow line for 0 < th <= pi/2
r_fl1_fin = linspace(rmax-dr,rmax,npts);

r_fl1 = [r_fl1_deb r_fl1_mid r_fl1_fin];

r_fl2_deb = linspace(rmax,rmax-dr,npts);
r_fl2_mid = linspace(rmax-dr,rmin+dr,npts); %flow line for 0 < th <= pi/2
r_fl2_fin = linspace(rmin+dr,rmin,npts);

r_fl2 = [r_fl2_deb r_fl2_mid r_fl2_fin]; %flow line for pi/2 <= th < pi 


for i = 1:length(r_fl1)
    th_fl1(i) = asin(A/(r_fl1(i)*sqrt(sin(a*r_fl1(i)+b)))); %0 < th <= pi/2
    th_fl2(i) = pi - asin(A/(r_fl2(i)*sqrt(sin(a*r_fl2(i)+b)))); %pi/2 <= th < pi
end

for i = 1:length(r_fl1)
    x_fl(i)      = real(r_fl1(i) * sin(th_fl1(i)));
    x_fl(i+length(r_fl1)) = real(r_fl2(i) * sin(th_fl2(i)));
    
    y_fl(i)      = real(r_fl1(i) * cos(th_fl1(i)));
    y_fl(i+length(r_fl1)) = real(r_fl2(i) * cos(th_fl2(i)));
end

% Compute tracer's trajectory in cartesian coordinates
for i = 1:length(r)
    x(i) = r(i) * sin(th(i));
    y(i) = r(i) * cos(th(i));
end



%plot
figure(1);
circle(0,0,1.5385);
hold on
circle(0,0,0.5385);
plot(x_fl,y_fl,'b'); % reference flow line in blue
plot(x,y,'r+'); % tracer's trajectory in red
axis([-1.55 1.55 -1.55 1.55]);
axis equal
hold off



% Compute the distance to the curve for each point
curvexy = [x_fl' y_fl'];
mapxy = [x' y'];

[xy,distances,t_a] = distance2curve(curvexy,mapxy,'linear');

%plot error = f(t)
figure(2)
plot(maptrth(1,:),distances)
xlabel('Time','Fontsize',20)
ylabel('Error','Fontsize',20)

end


function circle(x,y,r)
%x and y are the coordinates of the center of the circle
%r is the radius of the circle
%0.01 is the angle step, bigger values will draw the circle faster but
%you might notice imperfections (not very smooth)
ang=0:0.01:2*pi; 
xp=r*cos(ang);
yp=r*sin(ang);
plot(x+xp,y+yp,'k');
end






