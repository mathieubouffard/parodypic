function plot_Elsasser_time ()

%first open the file
fid = fopen('e_mag.bm1','r');
e_mag = fscanf(fid,'%g %g %g %g %g',[6 Inf]);

e_mag = e_mag';

%first calculates Rm
Elsasser = sqrt(2*e_mag(:,2));

%then plot

% Create figure
figure1 = figure('XVisual',...
    '0xc2 (TrueColor, depth 24, RGB mask 0xff0000 0xff00 0x00ff)');

% Create axes
axes1 = axes('Parent',figure1,'YGrid','on','XGrid','on','FontWeight','demi',...
    'FontSize',16);
box(axes1,'on');
hold(axes1,'all');

% Create plot
plot(e_mag(:,1),Elsasser,'LineWidth',2);

% Create xlabel
xlabel('time');

% Create ylabel
ylabel('Elsasser');


end