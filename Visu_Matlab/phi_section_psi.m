function [ output_args ] = phi_section_psi(r,theta,Vr)
% Plot a section phi = cste

[R, T] = meshgrid(r, theta);
[X, Y] = pol2cart(T, R);

Nlat = length(theta);
Nrad = length(r);

for i=1:Nrad
    psi(1,i) = -0.5*r(i)*sin(theta(1))*mean(Vr(:,1,i))*theta(1)
    for j=2:Nlat
        psi(j,i) = psi(j-1,i) - (theta(j)-theta(j-1))*(r(i)*sin(theta(j-1))*mean(Vr(:,j-1,i)));
    end
end 

Z = squeeze(psi);

figure;
surf(Y, X, Z,'EdgeColor', 'None', 'facecolor', 'interp');
view(2);
axis equal; 
axis off;
colorbar;

end