function [mean] = calc_mean (F,r,theta,phi)

nr = length(r);
nt = length(theta);
np = length(phi);

dr(1) = r(2)-r(1);
for i = 2:nr-1
    dr(i) = 0.5*(r(i+1)-r(i-1));
end
dr(nr) = dr(1);

dtheta(1) = 0.5*(theta(1)+theta(2));
for j = 2:nt-1
    dtheta(j) = 0.5*(theta(j+1)-theta(j-1));
end
dtheta(nt) = dtheta(1);

dphi = 2*pi/np;


mean = 0;

for i = 1:nr
    for j = 1:nt
        for k = 1:np
            mean = mean + F(k,j,i)*r(i)^2*sin(theta(j))*dr(i)*dtheta(j)*dphi;
        end
    end
end

vol = 4/3*pi*(r(nr)^3-r(1)^3);
mean = mean/vol;
    


end