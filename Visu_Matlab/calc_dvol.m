function [dvol] = calc_dvol(r,theta,phi)
%Computes elementary volumes 

nr = length(r);
nt = length(theta);
np = length(phi);

dr(1) = 0.5*(r(2)-r(1));
for i=2:nr-1
    dr(i)=0.5*(r(i+1)-r(i-1));
end
dr(nr) = 0.5*(r(nr)-r(nr-1));

dtheta(1) = theta(1)+0.5*(theta(2)-theta(1));
for j=2:nt-1
    dtheta(j) = 0.5*(theta(j+1)-theta(j-1));
end
dtheta(nt) = dtheta(1); %symmetry

dphi = 2*pi/np;

for i=1:nr
    for j=1:nt
        for k=1:np
            dvol(k,j,i) = r(i)^2*sin(theta(j))*dr(i)*dtheta(j)*dphi;
        end
    end
end
        
%sum(sum(sum(dvol)))
%4/3*pi*(r(nr)^3-r(1)^3)    


end

