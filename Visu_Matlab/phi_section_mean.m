function [ output_args ] = phi_section_mean( r,colat,D)
% Plot a section phi = cste

[R, T] = meshgrid(r, colat);
[X, Y] = pol2cart(T, R);
Z = squeeze(mean(D(:,:,:)));


figure;
surf(Y, X, Z,'EdgeColor', 'None', 'facecolor', 'interp');
view(2);
axis equal; 
axis off;
colorbar;

end
