function [w_z_equ] = calc_axial_vorticity_equator(r,theta,phi,Vr,Vp)
%Computes axial vorticity on the equator

nr = length(r);
nt = length(theta);
np = length(phi);

dphi=2*pi/np;

% First computes dVr/dp and d(rVp)/dr
for i=1:nr
    dVrdp(1,i) = (Vr(2,nt/2,i)-Vr(np,nt/2,i))/(2*r(i)*dphi);
    for k=2:np-1
        dVrdp(k,i) = (Vr(k+1,nt/2,i)-Vr(k-1,nt/2,i))/(2*r(i)*dphi);
    end
    dVrdp(np,i) = (Vr(1,nt/2,i)-Vr(np-1,nt/2,i))/(2*r(i)*dphi);

end

for k=1:np
    drVpdr(k,1) = (r(2)*Vp(k,nt/2,2)-r(1)*Vp(k,nt/2,1))/(r(2)-r(1));
    for i=2:nr-1
        drVpdr(k,i) = (r(i+1)*Vp(k,nt/2,i+1)-r(i-1)*Vp(k,nt/2,i-1))/(r(i+1)-r(i-1));
    end
    drVpdr(k,nr) = (r(nr)*Vp(k,nt/2,nr)-r(nr-1)*Vp(k,nt/2,nr-1))/(r(nr)-r(nr-1));
end

%Computes vorticity
for i=1:nr
    for j=1:nt
        for k=1:np
            w_z_equ(k,j,i) = 1/(r(i)*sin(theta(nt/2)))*dVrdp(k,i)-1/r(i)*drVpdr(k,i);
        end
    end
end



