
function radial_map(Y,rad)

Nrad = length(Y(1,1,:));
Nlat = length(Y(1,:,1));
Nlong = length(Y(:,1,1));


l=1;
X(1:Nrad*Nlat*Nlong) = 0;
for i=1:Nrad
    for j=1:Nlat
        for k=1:Nlong
           X(l) = Y(k,j,i);
           l=l+1;
        end
    end
end
X = X';
nstart = (rad-1)*Nlat*Nlong+1;

C = X(nstart:nstart+Nlong)';
for i=2:Nlat
    D = X(nstart+Nlong*(i-1):nstart+Nlong*i);
    B = [C
         D'];
    C = B;
end
    B = [C
         X(nstart:nstart+Nlong)'];
    C = B;
    
[x,y,z] = spherical_map(Nlat,Nlong);

%figure(1)
%hist(X,50)
size(z)
size(C)

figure(1);
surface(x,y,z,C,'EdgeColor','none','FaceColor','interp');
view(45,45);
axis equal;
colormap jet;
colorbar;
view(90,0)

