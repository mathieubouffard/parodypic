function [triquad] = calc_triquad(rot_r,rot_t,rot_p,seuil)

nr = length(rot_r(1,1,:));
nt = length(rot_t(1,:,1));
np = length(rot_p(:,1,1));

triquad(1:np,1:nt,1:nr) = 0;
for i=1:nr
    for j=1:nt
        for k=1:np
            if (abs(rot_r(k,j,i))>=seuil | abs(rot_t(k,j,i))>=seuil | abs(rot_p(k,j,i))>=seuil)
                triquad(k,j,i) = 1;
            end
        end
    end
end

end