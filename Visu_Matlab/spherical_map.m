function [xx,yy,zz] = spherical_map(Nlat,Nlong)

% 0 <= theta <= pi is a column vector.
% 0 <= phi <= 2*pi is a row vector.

theta = (-Nlong:2:Nlong)/Nlong*pi;
phi = (-Nlat:2:Nlat)'/Nlat*pi/2;
cosphi = cos(phi); cosphi(1) = 0; cosphi(Nlat+1) = 0;
sintheta = sin(theta); sintheta(1) = 0; sintheta(Nlong+1) = 0;

x = cosphi*cos(theta);
y = cosphi*sintheta;
z = sin(phi)*ones(1,Nlong+1);


% surf(x,y,z)
    
 xx = x; yy = y; zz = z;
 
end
