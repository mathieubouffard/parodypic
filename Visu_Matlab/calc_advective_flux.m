function [fadv] = calc_advective_flux (r,theta,phi,T,Vr)

nr = length(r);
nt = length(theta);
np = length(phi);

Tmean = calc_mean(T,r,theta,phi);


dtheta(1) = 0.5*(theta(1)+theta(2));
for j = 2:nt-1
    dtheta(j) = 0.5*(theta(j+1)-theta(j-1));
end
dtheta(nt) = dtheta(1);

dphi = 2*pi/np;

for i = 1:nr
    for j = 1:nt
        for k = 1:np
            fadv(k,j,i) = (T(k,j,i)-Tmean)*Vr(k,j,i);%*dphi*sin(theta(j))*dtheta(j)*r(i)^2;
        end
    end
%    fadv(i) = sum(sum(f_adv(:,:,i)))/4/pi/r(i)^2;
end


end