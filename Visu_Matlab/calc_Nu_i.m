function [Nu,Nui] = calc_Nu_i (field,r,theta,phi)
%computes the Nusselt number using a 3rd-order spatial scheme

nt = length(theta);
np = length(phi);

dtheta(1) = theta(1)+0.5*(theta(2)-theta(1));
for j=2:nt-1
    dtheta(j) = 0.5*(theta(j+1)-theta(j-1));
end
dtheta(nt) = dtheta(1); %theta is symmetric with respect to equator

dphi = 2*pi/np;

%coefficients for computing the first derivative
dr1 = r(2)-r(1);
dr2 = r(3)-r(2);

den = (dr1*(dr1+dr2)^2-(dr1+dr2)*dr1*dr1);

c0 = -((dr2+dr1)^2-dr1^2)/den;
c1 = ((dr2+dr1)^2)/den;
c2 = -(dr1^2)/den;
 
Nu = 0.;

for  j=1:nt
    for k=1:np
        %compute cells areas on ICB
        area(k,j) = r(1)^2*sin(theta(j))*dtheta(j)*dphi;
        Nu(k,j) = area(k,j) * (c0*field(k,j,1)+c1*field(k,j,2)+c2*field(k,j,3));
        Nui(k,j,1:90) = -0.35*(c0*field(k,j,1)+c1*field(k,j,2)+c2*field(k,j,3));
        Nu2pts(k,j) = (field(k,j,3)-field(k,j,1))/(r(3)-r(1))*area(k,j);
    end
end


sum(sum(area));
surf_tot = 4*pi*r(1)^2;

-sum(sum(Nu))/surf_tot*0.35
-sum(sum(Nu2pts))/surf_tot*0.35

end

