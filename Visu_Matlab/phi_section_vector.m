function [ output_args ] = phi_section_vector(r,theta,phi,Vr,Vt,Vp,nvect_r,nvect_t,scale,linewidth,arrow_head_size)
% Plot in a section phi = cste

%For phi component (Vp), represents using colors
%-----------------------------------------------
[R, T] = meshgrid(r, theta);
[Y, X] = pol2cart(T, R);
Z = squeeze(Vp(phi,:,:));

%For r and theta components (Vr and Vt), represents as vectors
%-------------------------------------------------------------

%First, choose nvect_r points in radius and nvect_t in theta 
%to plot nvect_r * nvect_t vectors in total

nr = floor(length(r)/nvect_r);
nt = floor(length(theta)/nvect_t);
remr = length(r)-nr*nvect_r;
remt = length(theta)-nt*nvect_t;

for i=1:nvect_r
    r_vect(i) = r(1+floor(remr/2)+(i-1)*nr);
end

for i=1:nvect_t
    theta_vect(i) = theta(1+floor(remt/2)+(i-1)*nt);
end

%Construct the r and theta coordinates of the vectors
l=0;
for i=1:nvect_r
    for j=1:nvect_t
        l=l+1;
        Rvect(l) = r_vect(i);
        Tvect(l) = theta_vect(j);
        ur_vect(l) = Vr(phi,1+floor(remt/2)+(j-1)*nt,1+floor(remr/2)+(i-1)*nr);
        ut_vect(l) = Vt(phi,1+floor(remt/2)+(j-1)*nt,1+floor(remr/2)+(i-1)*nr);
        Zvect(l) = max(max(Z));
        Uz(l) = 0;
        
        %Now convert everything in cartesian coordinates
        Xvect(l) = Rvect(l)*sin(Tvect(l));
        Yvect(l) = Rvect(l)*cos(Tvect(l));
        Ux(l) = ur_vect(l)*sin(Tvect(l))+ut_vect(l)*cos(Tvect(l));
        Uy(l) = ur_vect(l)*cos(Tvect(l))-ut_vect(l)*sin(Tvect(l));
    end
end

%Now plot
%--------------------------------
%figure;

%surf(X, Y, Z,'EdgeColor', 'None', 'facecolor', 'interp');
%view(2);
%axis equal; 
%axis off;
%colorbar;
%hold on;
%quiver3(Xvect,Yvect,Zvect,Ux,Uy,Uz,scale,'k','LineWidth',linewidth);

%cmap = colormap
%hold off
figure(1)
pcolor(X,Y,Z); shading interp;
axis equal;
axis off;
hold on;
h = quiver(Xvect,Yvect,Ux,Uy,scale,'k','LineWidth',linewidth);
%adjust_quiver_arrowhead_size(h, arrow_head_size);
colorbar;
end


