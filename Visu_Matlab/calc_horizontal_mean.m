function [Fmean] = calc_horizontal_mean (r,theta,phi,Field)

nr = length(r);
nt = length(theta);
np = length(phi);

dvol = calc_dvol(r,theta,phi);

for i = 1:nr
    Fmean(i) = 0;
    for j = 1:nt
        for k = 1:np
            Fmean(i) = Fmean(i) + dvol(k,j,i)*Field(k,j,i);
        end
    end
    Fmean(i) = Fmean(i)/sum(sum(dvol(:,:,i)));
end


end