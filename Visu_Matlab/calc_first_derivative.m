function [dFdr] = calc_first_derivative(r,theta,phi,Field)

nr = length(r);
nt = length(theta);
np = length(phi);

for i=1:nr-1
    for j=1:nt
        for k=1:np
            dFdr(k,j,i) = (Field(k,j,i+1)-Field(k,j,i))/(r(i+1)-r(i));
        end
    end
end
dFdr(:,:,nr) = dFdr(:,:,nr-1);


end