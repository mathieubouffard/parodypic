function [vol] = calc_vol (r,theta,phi)

nr = length(r);
nt = length(theta);
np = length(phi);

drad(1) = 0.5*(r(2)-r(1));
for i=2:nr-1
    drad(i) = 0.5*(r(i+1)-r(i-1));
end
drad(nr) = 0.5*(r(nr)-r(nr-1));

dtheta(1) = theta(1)+0.5*(theta(2)-theta(1));
for j=2:nt-1
    dtheta(j) = 0.5*(theta(j+1)-theta(j-1));
end
dtheta(nt) = dtheta(1);

dphi = 2*pi/np;

for i = 1:nr
    for j = 1:nt
        for k=1:np
            vol(k,j,i) = r(i)*r(i)*sin(theta(j))*drad(i)*dtheta(j)*dphi;
        end
    end
end
end