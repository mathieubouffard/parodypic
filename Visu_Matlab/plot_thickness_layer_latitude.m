function [thickness] = plot_thickness_layer_latitude (C,T,Br,r,theta)

nr = length(r);
nt = length(theta);
np = length(C(:,1,1));

for j=1:nt
%    for k=1:np
        for i=1:nr
            Cmean(i) = mean(C(:,j,i));
            Tmean(i) = mean(T(:,j,i));
        end
       % Cmean = calc_moyenne_glissante(Cwork,0);
        thickness(j) = calc_thickness_layer (Cmean,Tmean,Br,r);
%    end
   % thickness(j) = mean(thickness_work(:,j));
end



plot(theta,thickness);

end