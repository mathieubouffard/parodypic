function plot_Rm_time (Pm)

%first open the file
fid = fopen('e_kin.bm1','r');
e_kin = fscanf(fid,'%g %g %g %g %g',[6 Inf]);

e_kin = e_kin';

%first calculates Rm
Re = sqrt(2*e_kin(:,2)); %Re
Rm = Re * Pm;

%then plot

% Create figure
figure1 = figure('XVisual',...
    '0xc2 (TrueColor, depth 24, RGB mask 0xff0000 0xff00 0x00ff)');

% Create axes
axes1 = axes('Parent',figure1,'YGrid','on','XGrid','on','FontWeight','demi',...
    'FontSize',16);
box(axes1,'on');
hold(axes1,'all');

% Create plot
plot(e_kin(:,1),Rm,'LineWidth',2);

% Create xlabel
xlabel('time');

% Create ylabel
ylabel('Rm');


end
