function [dVr1_r,dVr1_t,dVr1_p,dVr2_r,dVr2_t,dVr2_p,dVt1_r,dVt1_t,dVt1_p,dVt2_r,dVt2_t,dVt2_p,dVp1_r,dVp1_t,dVp1_p,dVp2_r,dVp2_t,dVp2_p] = calc_velocity_derivatives(r,theta,phi,Vr,Vt,Vp)
% calculates an approximation of the velocity first and second derivatives

nr = length(r);
nt = length(theta);
np = length(phi);
dphi = 2.*(phi(2)-phi(1)); %phi has regular spacing


%======================FIRST DERIVATIVES dv1=============================

%first derivatives along r
for k = 1:np
    for j = 1:nt
        dVr1_r(k,j,1) = (Vr(k,j,2)-Vr(k,j,1))/(r(2)-r(1));
        dVt1_r(k,j,1) = (Vt(k,j,2)-Vt(k,j,1))/(r(2)-r(1)); 
        dVp1_r(k,j,1) = (Vp(k,j,2)-Vp(k,j,1))/(r(2)-r(1));
        for i = 2:nr-1
            dVr1_r(k,j,i) = (Vr(k,j,i+1)-Vr(k,j,i-1))/(r(i+1)-r(i-1));
            dVt1_r(k,j,i) = (Vt(k,j,i+1)-Vt(k,j,i-1))/(r(i+1)-r(i-1));  
            dVp1_r(k,j,i) = (Vp(k,j,i+1)-Vp(k,j,i-1))/(r(i+1)-r(i-1));
        end
        dVr1_r(k,j,nr) = (Vr(k,j,nr)-Vr(k,j,nr-1))/(r(nr)-r(nr-1));    
        dVt1_r(k,j,nr) = (Vt(k,j,nr)-Vt(k,j,nr-1))/(r(nr)-r(nr-1));   
        dVp1_r(k,j,nr) = (Vp(k,j,nr)-Vp(k,j,nr-1))/(r(nr)-r(nr-1));   
    end
end

%first derivative along theta
for k = 1:np
    for i = 1:nr
        dVr1_t(k,1,i) = (Vr(k,2,i)-Vr(k,1,i))/(r(i)*(theta(2)-theta(1)));
        dVt1_t(k,1,i) = (Vt(k,2,i)-Vt(k,1,i))/(r(i)*(theta(2)-theta(1))); 
        dVp1_t(k,1,i) = (Vp(k,2,i)-Vp(k,1,i))/(r(i)*(theta(2)-theta(1)));
        for j = 2:nt-1        
            dVr1_t(k,j,i) = (Vr(k,j+1,i)-Vr(k,j-1,i))/(r(i)*(theta(j+1)-theta(j-1)));
            dVt1_t(k,j,i) = (Vt(k,j+1,i)-Vt(k,j-1,i))/(r(i)*(theta(j+1)-theta(j-1)));
            dVp1_t(k,j,i) = (Vp(k,j+1,i)-Vp(k,j-1,i))/(r(i)*(theta(j+1)-theta(j-1)));
        end
        dVr1_t(k,nt,i) = (Vr(k,nt,i)-Vr(k,nt-1,i))/(r(i)*(theta(nt)-theta(nt-1)));
        dVt1_t(k,nt,i) = (Vt(k,nt,i)-Vt(k,nt-1,i))/(r(i)*(theta(nt)-theta(nt-1))); 
        dVp1_t(k,nt,i) = (Vp(k,nt,i)-Vp(k,nt-1,i))/(r(i)*(theta(nt)-theta(nt-1)));
    end
end

%first derivative along phi
for j = 1:nt
    for i = 1:nr
        dVr1_p(1,j,i) = (Vr(2,j,i)-Vr(np,j,i))/(r(i)*sin(theta(j))*dphi);
        dVt1_p(1,j,i) = (Vt(2,j,i)-Vt(np,j,i))/(r(i)*sin(theta(j))*dphi); 
        dVp1_p(1,j,i) = (Vp(2,j,i)-Vp(np,j,i))/(r(i)*sin(theta(j))*dphi);
        for k = 2:np-1
            dVr1_p(k,j,i) = (Vr(k+1,j,i)-Vr(k-1,j,i))/(r(i)*sin(theta(j))*dphi);
            dVt1_p(k,j,i) = (Vt(k+1,j,i)-Vt(k-1,j,i))/(r(i)*sin(theta(j))*dphi);
            dVp1_p(k,j,i) = (Vp(k+1,j,i)-Vp(k-1,j,i))/(r(i)*sin(theta(j))*dphi);
        end
        dVr1_p(np,j,i) = (Vr(1,j,i)-Vr(np-1,j,i))/(r(i)*sin(theta(j))*dphi);
        dVt1_p(np,j,i) = (Vt(1,j,i)-Vt(np-1,j,i))/(r(i)*sin(theta(j))*dphi); 
        dVp1_p(np,j,i) = (Vp(1,j,i)-Vp(np-1,j,i))/(r(i)*sin(theta(j))*dphi);
    end
end

%================SECOND DERIVATES dV2====================================

%second derivatives along r
for k = 1:np
    for j = 1:nt
        dVr2_r(k,j,1) = (dVr1_r(k,j,2)-dVr1_r(k,j,1))/(r(2)-r(1));
        dVt2_r(k,j,1) = (dVt1_r(k,j,2)-dVt1_r(k,j,1))/(r(2)-r(1)); 
        dVp2_r(k,j,1) = (dVp1_r(k,j,2)-dVp1_r(k,j,1))/(r(2)-r(1));
        for i = 2:nr-1
            dVr2_r(k,j,i) = (dVr1_r(k,j,i+1)-dVr1_r(k,j,i-1))/(r(i+1)-r(i-1));
            dVt2_r(k,j,i) = (dVt1_r(k,j,i+1)-dVt1_r(k,j,i-1))/(r(i+1)-r(i-1));  
            dVp2_r(k,j,i) = (dVp1_r(k,j,i+1)-dVp1_r(k,j,i-1))/(r(i+1)-r(i-1));
        end
        dVr2_r(k,j,nr) = (dVr1_r(k,j,nr)-dVr1_r(k,j,nr-1))/(r(nr)-r(nr-1));    
        dVt2_r(k,j,nr) = (dVt1_r(k,j,nr)-dVt1_r(k,j,nr-1))/(r(nr)-r(nr-1));   
        dVp2_r(k,j,nr) = (dVp1_r(k,j,nr)-dVp1_r(k,j,nr-1))/(r(nr)-r(nr-1));   
    end
end

%second derivative along theta
for k = 1:np
    for i = 1:nr
        dVr2_t(k,1,i) = (dVr1_t(k,2,i)-dVr1_t(k,1,i))/(r(i)*(theta(2)-theta(1)));
        dVt2_t(k,1,i) = (dVt1_t(k,2,i)-dVt1_t(k,1,i))/(r(i)*(theta(2)-theta(1))); 
        dVp2_t(k,1,i) = (dVp1_t(k,2,i)-dVp1_t(k,1,i))/(r(i)*(theta(2)-theta(1)));
        for j = 2:nt-1        
            dVr2_t(k,j,i) = (dVr1_t(k,j+1,i)-dVr1_t(k,j-1,i))/(r(i)*(theta(j+1)-theta(j-1)));
            dVt2_t(k,j,i) = (dVt1_t(k,j+1,i)-dVt1_t(k,j-1,i))/(r(i)*(theta(j+1)-theta(j-1)));
            dVp2_t(k,j,i) = (dVp1_t(k,j+1,i)-dVp1_t(k,j-1,i))/(r(i)*(theta(j+1)-theta(j-1)));
        end
        dVr2_t(k,nt,i) = (dVr1_t(k,nt,i)-dVr1_t(k,nt-1,i))/(r(i)*(theta(nt)-theta(nt-1)));
        dVt2_t(k,nt,i) = (dVt1_t(k,nt,i)-dVt1_t(k,nt-1,i))/(r(i)*(theta(nt)-theta(nt-1))); 
        dVp2_t(k,nt,i) = (dVp1_t(k,nt,i)-dVp1_t(k,nt-1,i))/(r(i)*(theta(nt)-theta(nt-1)));
    end
end

%second derivative along phi
for j = 1:nt
    for i = 1:nr
        dVr2_p(1,j,i) = (dVr1_p(2,j,i)-dVr1_p(np,j,i))/(r(i)*sin(theta(j))*dphi);
        dVt2_p(1,j,i) = (dVt1_p(2,j,i)-dVt1_p(np,j,i))/(r(i)*sin(theta(j))*dphi); 
        dVp2_p(1,j,i) = (dVp1_p(2,j,i)-dVp1_p(np,j,i))/(r(i)*sin(theta(j))*dphi);
        for k = 2:np-1
            dVr2_p(k,j,i) = (dVr1_p(k+1,j,i)-dVr1_p(k-1,j,i))/(r(i)*sin(theta(j))*dphi);
            dVt2_p(k,j,i) = (dVt1_p(k+1,j,i)-dVt1_p(k-1,j,i))/(r(i)*sin(theta(j))*dphi);
            dVp2_p(k,j,i) = (dVp1_p(k+1,j,i)-dVp1_p(k-1,j,i))/(r(i)*sin(theta(j))*dphi);
        end
        dVr2_p(np,j,i) = (dVr1_p(1,j,i)-dVr1_p(np-1,j,i))/(r(i)*sin(theta(j))*dphi);
        dVt2_p(np,j,i) = (dVt1_p(1,j,i)-dVt1_p(np-1,j,i))/(r(i)*sin(theta(j))*dphi); 
        dVp2_p(np,j,i) = (dVp1_p(1,j,i)-dVp1_p(np-1,j,i))/(r(i)*sin(theta(j))*dphi);
    end
end

end