function plot_dipole_time (n)

%plot the (n-1)th column of e_dipole.bm1 as a function of the first column
%(time)
% n = 2 : dipcolat
% n = 3 : real(yVpr(lm10,NR))
% n = 4 : abs(yBpr(lm11,NR))
% n = 5 : sqrt(EnergBcmb)
% n = 6 : sqrt(EnergBdipcomb)
% n = 7 : sqrt(EnergBcmb12)

%first open the file
fid = fopen('dipole.bm1','r');
dipole = fscanf(fid,'%g %g %g %g %g %g',[7 Inf]);

dipole = dipole';

%then plot

% Create figure
figure1 = figure('XVisual',...
    '0xc2 (TrueColor, depth 24, RGB mask 0xff0000 0xff00 0x00ff)');

% Create axes
axes1 = axes('Parent',figure1,'YGrid','on','XGrid','on','FontWeight','demi',...
    'FontSize',16);
box(axes1,'on');
hold(axes1,'all');

% Create plot
plot(dipole(:,1),dipole(:,n),'LineWidth',2);

% Create xlabel
xlabel('time');

% Create ylabel
if (n == 2)
    ylabel('real(yVpr(lm10,NR))');
elseif (n == 3)
    ylabel('real(yVpr(lm10,NR))');
elseif (n == 4)
    ylabel('abs(yBpr(lm11,NR))');
elseif (n == 5)
    ylabel('sqrt(EnergBcmb)');
elseif (n == 6)
    ylabel('sqrt(EnergBdipcomb)');
elseif (n == 7)
     ylabel('sqrt(EnergBcmb12)');
end


end