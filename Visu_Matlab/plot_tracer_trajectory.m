function plot_tracer_trajectory (rt,tt,pt,r)

npts = length(rt);

% Convert to cartesian coordinates
for i = 1:npts
    xt(i) = rt(i)*sin(tt(i))*sin(pt(i));
    yt(i) = rt(i)*sin(tt(i))*cos(pt(i));
    zt(i) = rt(i)*cos(tt(i));
end

plot3(xt,yt,zt,'LineWidth',3)
hold on
% Add bottom and top boundaries
R = r(1);
phi=linspace(0,pi,30);
theta=linspace(0,2*pi,40);
[phi,theta]=meshgrid(phi,theta);

x=R*sin(phi).*cos(theta);
y=R*sin(phi).*sin(theta);
z=R*cos(phi); 
c = z*0;
surf(x,y,z,c,'LineStyle','-','FaceAlpha',1,'EdgeAlpha',0.5)

R = r(length(r));
phi=linspace(0,pi,30);
theta=linspace(0,2*pi,40);
[phi,theta]=meshgrid(phi,theta);

x=R*sin(phi).*cos(theta);
y=R*sin(phi).*sin(theta);
z=R*cos(phi); 
c = z*0;
surf(x,y,z,c,'LineStyle','-','FaceAlpha',0.2,'EdgeAlpha',0.2)

end
