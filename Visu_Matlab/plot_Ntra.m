%%PLOTS NUMBER OF TRACERS AS A FUNCTION OF EKMAN NUMBER AND ASPECT RATIO

%INPUT: RATIO BETWEEN MEAN TRACERS DISTANCE AND EKMAN LAYER
ratio = 4;

%Computes computational domain's volume for each aspect ratio
aspect_ratio = linspace(0,0.9,100);

for i=1:length(aspect_ratio)
    gvol(i) = 4*pi/3*(1-aspect_ratio(i)^3)/(1-aspect_ratio(i))^3;
end

%Computes number of tracers for different ekman numbers
Ek(1) = 0.001;
Ek(2) = 0.0003;
Ek(3) = 0.0001;
Ek(4) = 0.00003;
Ek(5) = 0.00001;

nEk = length(Ek);

for i=1:nEk
    for j=1:length(gvol)
        Ntra(i,j) = gvol(j)*ratio^3*Ek(i)^-1.5;
    end
end

%Plots in a semi-log figure
figure
semilogy(aspect_ratio(:),squeeze(Ntra(1,:)),'b','LineWidth',2);
hold on
semilogy(aspect_ratio(:),squeeze(Ntra(2,:)),'r','LineWidth',2);
semilogy(aspect_ratio(:),squeeze(Ntra(3,:)),'g','LineWidth',2);
semilogy(aspect_ratio(:),squeeze(Ntra(4,:)),'m','LineWidth',2);
semilogy(aspect_ratio(:),squeeze(Ntra(5,:)),'c','LineWidth',2);
semilogy(aspect_ratio(:),ones(length(aspect_ratio))*10^10,'k--','LineWidth',1);
semilogy(aspect_ratio(:),ones(length(aspect_ratio))*10^9,'k--','LineWidth',1);
semilogy(aspect_ratio(:),ones(length(aspect_ratio))*2.5*10^8,'k--','LineWidth',1);

legend('Ek = 1e-3','Ek = 3e-4','Ek = 1e-4','Ek = 3e-5','Ek = 1e-5','location','northwest')
%title('Estimated number of tracers for different Ekman numbers and aspect ratios','FontSize',24)
xlabel('r_o/r_i','FontSize',11) % x-axis label
ylabel('N_t_r_a','Fontsize',11) % y-axis label
ylim([5e6 1e13])
set(gca,'fontsize',10)
grid on
