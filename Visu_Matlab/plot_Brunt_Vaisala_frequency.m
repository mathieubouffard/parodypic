function [BV] = plot_Brunt_Vaisala_frequency (Tmean,Cmean,Br,r)

nr = length(r);

for i = 2:nr-1
    dTdr(i) = 0.5*(Tmean(i+1)-Tmean(i-1));
    dCdr(i) = 0.5*(Cmean(i+1)-Cmean(i-1));
end
dTdr(1) = dTdr(2);
dTdr(nr) = dTdr(nr-1);
dCdr(1) = dCdr(2);
dCdr(nr) = dCdr(nr-1);

BV = dTdr + Br*dCdr;
    
plot(dTdr+Br*dCdr,r)


end