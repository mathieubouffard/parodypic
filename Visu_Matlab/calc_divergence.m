function [ div ] = calc_divergence(r,theta,phi,Vr,Vt,Vp)
%computes velocity divergence

nr = length(r);
nt = length(theta);
np = length(phi);

dphi = 2*pi/np;

%First computes d(r^2Vr)/dr
for j=1:nt
    for k=1:np
        dr2Vrdr(k,j,1) = (r(2)^2*Vr(k,j,2)-r(1)^2*Vr(k,j,1))/(r(2)-r(1));
        for i=2:nr-1
            dr2Vrdr(k,j,i) = (r(i+1)^2*Vr(k,j,i+1)-r(i-1)^2*Vr(k,j,i-1))/(r(i+1)-r(i-1));
        end
        dr2Vrdr(k,j,nr) = (r(nr)^2*Vr(k,j,nr)-r(nr-1)^2*Vr(k,j,nr-1))/(r(nr)-r(nr-1));
    end
end

%Computes d(sin(theta)Vt)/dtheta
for i=1:nr
    for k=1:np
        dsintVtdt(k,1,i) = (sin(theta(2))*Vt(k,2,i)-sin(theta(1))*Vt(k,1,i))/(theta(2)-theta(1));
        for j=2:nt-1
            dsintVtdt(k,j,i) = (sin(theta(j+1))*Vt(k,j+1,i)-sin(theta(j-1))*Vt(k,j-1,i))/(theta(j+1)-theta(j-1));
        end
        dsintVtdt(k,nt,i) = (sin(theta(nt))*Vt(k,nt,i)-sin(theta(nt-1))*Vt(k,nt-1,i))/(theta(nt)-theta(nt-1));
    end
end

%dVp/dp
for i=1:nr
    for j=1:nt
        dVpdp(1,j,i) = (Vp(2,j,i)-Vp(np,j,i))/(2*dphi);
        for k=2:np-1
            dVpdp(k,j,i) = (Vp(k+1,j,i)-Vp(k-1,j,i))/(2*dphi);
        end
        dVpdp(np,j,i) = (Vp(1,j,i)-Vp(np-1,j,i))/(2*dphi);
    end
end
        


%Finally computes fields divergence
for i=1:nr
    for j=1:nt
        for k=1:np
            div(k,j,i) = dr2Vrdr(k,j,i)/r(i)^2+dsintVtdt(k,j,i)/(sin(theta(j))*r(i))+dVpdp(k,j,i)/(r(i)*sin(theta(j)));
        end
    end
end


end

