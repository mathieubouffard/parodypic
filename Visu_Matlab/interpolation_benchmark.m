function [T0,Bt0,Vp0] = interpolation_benchmark (Nr,Ntheta,Nphi,r,theta,phi,Vr,Vp,Bt,T)

% Calculates the coordinates of the point where the values T, Bt, Vp must be calculated for the benchmark. Then calculates T, Bt, Vp at \
%      these points using trilinear interpolation.
% Note that this only works with Nr EVEN

%-------------------------------------------------------------------------------
%First, localise the point, which correspond to Vr = 0 and dVr/dphi > 0.
r0 =(r(Nr)+r(1))/2;
theta0 = pi/2;

for k = 1:Nphi
    Vr_mid(k) = mean(mean(Vr(k,Ntheta/2:Ntheta/2+1,Nr/2:Nr/2+1)));
end


for i=1:Nphi/4 %periodicity 4 for the benchmark
    if (Vr_mid(i) <= Vr_mid(i+1)) && (Vr_mid(i)*Vr_mid(i+1) <= 0); % the point we look for is in between
        a = [Vr_mid(i+1)-Vr_mid(i)]/[phi(i+1)-phi(i)]; % slope
        b = Vr_mid(i) - a*phi(i);                      % ordinate at origin
        phi0 = -b/a;
        ipm = i;
    end
end

% coordinates are (r0,theta0,phi0)


%------------------------------------------------------------------------------------
%Then, calculates the values of T, Bt and Vp at (r0,theta0,phi0) using trilinear interpolation

irm = Nr/2;
irp = Nr/2+1;

itm = Ntheta/2;
itp = Ntheta/2+1;

ipp = ipm+1;


dphi = 2*pi/Nphi;
dtheta = theta(Ntheta/2+1)-theta(Ntheta/2);
dr = r(Nr/2+1)-r(Nr/2);

dist_r = (r0-r(Nr/2))/dr;
dist_t = (theta0-theta(Ntheta/2))/dtheta;
dist_p = (phi0 - phi(ipm))/dphi;

% weights
w1 = (1-dist_r)*(1-dist_t)*(1-dist_p);
w2 = (1-dist_r)*(1-dist_t)*  dist_p  ;
w3 = (1-dist_r)*  dist_t  *(1-dist_p);
w4 = (1-dist_r)*  dist_t  *  dist_p  ;
w5 =   dist_r  *(1-dist_t)*(1-dist_p);
w6 =   dist_r  *(1-dist_t)*  dist_p  ;
w7 =   dist_r  *  dist_t  *(1-dist_p);
w8 =   dist_r  *  dist_t  *  dist_p  ;

% interpolation for each variable
T0 = w1 * T(ipm,itm,irm) + ...
     w2 * T(ipp,itm,irm) + ...
     w3 * T(ipm,itp,irm) + ...
     w4 * T(ipp,itp,irm) + ...
     w5 * T(ipm,itm,irp) + ...
     w6 * T(ipp,itm,irp) + ...
     w7 * T(ipm,itp,irp) + ...
     w8 * T(ipp,itp,irp) ;

Bt0 = w1 * Bt(ipm,itm,irm) + ...
      w2 * Bt(ipp,itm,irm) + ...
      w3 * Bt(ipm,itp,irm) + ...
      w4 * Bt(ipp,itp,irm) + ...
      w5 * Bt(ipm,itm,irp) + ...
      w6 * Bt(ipp,itm,irp) + ...
      w7 * Bt(ipm,itp,irp) + ...
      w8 * Bt(ipp,itp,irp) ;

Vp0 = w1 * Vp(ipm,itm,irm) + ...
      w2 * Vp(ipp,itm,irm) + ...
      w3 * Vp(ipm,itp,irm) + ...
      w4 * Vp(ipp,itp,irm) + ...
      w5 * Vp(ipm,itm,irp) + ...
      w6 * Vp(ipp,itm,irp) + ...
      w7 * Vp(ipm,itp,irp) + ...
      w8 * Vp(ipp,itp,irp) ;


end 
