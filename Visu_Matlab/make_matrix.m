function mat = make_matrix( A,Nrad,Nlat,Nlong )
%ntra, compo are printed in column in Parody
%makes a matrix rightly ordered so as to use the visualisation tool
%directly

mat = zeros(Nlong,Nlat,Nrad);

for i=1:Nlong
    for j=1:Nlat
        for k=1:Nrad
            mat(i,j,k) = A((k-1)*Nlat*Nlong+(j-1)*Nlong+i);
        end
    end
end

end

