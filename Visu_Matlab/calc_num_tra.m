function [N] = calc_num_tra (Ek,AR,n)
% computes the adequate number of tracers for a given grid aspect ratio and 
% Ekman number
% Computation is based on a mean distance between tracers that is 1/n of
% the Ekman layer's thickness

Ek_layer = sqrt(Ek);
mean_dist = Ek_layer/n;

Vol = 4*pi/3/(1-AR)^3*(1-AR^3);

N = Vol/mean_dist^3;



end