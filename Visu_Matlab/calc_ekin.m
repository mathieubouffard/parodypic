function [ekin] = calc_ekin (Vr,Vt,Vp,r,theta,phi)

nr = length(r);
nt = length(theta);
np = length(phi);

for i = 1:nr
    for j = 1:nt
        for k = 1:np
%            Vy = 
%            Vz = 
%            ekin(k,j,i) = sqrt(Vx^2+Vy^2+Vz^2);
            V1 = Vr(k,j,i);
            V2 = r(i)*Vt(k,j,i);
            V3 = r(i)*sin(theta(j))*Vp(k,j,i);
            ekin(k,j,i) = V1^2+V2^2+V3^2;
        end
    end
end

end