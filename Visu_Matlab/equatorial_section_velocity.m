function equatorial_section_velocity (r,phi,Vr,Vt,Vp,nvect_r,nvect_p,scale,linewidth,arrow_head_size)

%First plot field Vt in colors
Nlat = length(Vt(1,:,1));
Nlong = length(phi);

%add an extra phi to make a complete disk
phi(Nlong+1) = phi(1);
Vt(Nlong+1,:,:) = Vt(1,:,:);

[R, T] = meshgrid(r, phi);
[Y, X] = pol2cart(T, R);
Z = squeeze(Vt(:,floor(Nlat/2),:));

size(X)
size(Y)
size(Z)

figure(1)
pcolor(X,Y,Z); shading interp;
axis equal;
axis off;
hold on;
colorbar;

% Then add arrows for the Vr Vp velocities

%First, choose nvect_r points in radius and nvect_p in phi
%to plot nvect_r * nvect_p vectors in total

nr = floor(length(r)/nvect_r);
np = floor(length(phi)/nvect_p);
remr = length(r)-nr*nvect_r;
remp = length(phi)-np*nvect_p;

for i=1:nvect_r
    r_vect(i) = r(1+floor(remr/2)+(i-1)*nr);
end

for i=1:nvect_p
    phi_vect(i) = phi(1+floor(remp/2)+(i-1)*np);
end

%Construct the r and phi coordinates of the vectors
l=0;
for i=1:nvect_r
    for j=1:nvect_p
        l=l+1;
        Rvect(l) = r_vect(i);
        Pvect(l) = phi_vect(j);
        ur_vect(l) = Vr(1+floor(remp/2)+(j-1)*np,Nlat/2,1+floor(remr/2)+(i-1)*nr);
        up_vect(l) = Vp(1+floor(remp/2)+(j-1)*np,Nlat/2,1+floor(remr/2)+(i-1)*nr);
        Zvect(l) = max(max(Z));
        Uz(l) = 0;
        
        %Now convert everything in cartesian coordinates
        Xvect(l) = Rvect(l)*sin(Pvect(l));
        Yvect(l) = Rvect(l)*cos(Pvect(l));
        Ux(l) = ur_vect(l)*sin(Pvect(l))+up_vect(l)*cos(Pvect(l));
        Uy(l) = ur_vect(l)*cos(Pvect(l))-up_vect(l)*sin(Pvect(l));
    end
end

%Now plot
%--------------------------------
%figure;

%surf(X, Y, Z,'EdgeColor', 'None', 'facecolor', 'interp');
%view(2);
%axis equal; 
%axis off;
%colorbar;
%hold on;
%quiver3(Xvect,Yvect,Zvect,Ux,Uy,Uz,scale,'k','LineWidth',linewidth);

%cmap = colormap
%hold off
%pcolor(X,Y,Z); shading interp;
%axis equal;
%axis off;
%hold on;
h = quiver(Xvect,Yvect,Ux,Uy,scale,'k','MaxHeadSize',10.);
%adjust_quiver_arrowhead_size(h, 1.5);
colorbar;

end