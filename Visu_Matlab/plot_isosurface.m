function [] = plot_isosurface (r,theta,phi,field,isovalue)

nr = length(r);
nt = length(theta);
np = length(phi);

for i=1:nt
    elevation(i) = pi/2-theta(i);
end

l=0;
nrmax=nr/2;

for i=1:nrmax
    for j=1:nt
        for k=3*np/4:np
            l = l+1;
            [x(l),y(l),z(l)] = sph2cart(phi(k),elevation(j),r(i));
            field_ext(l) = field(k,j,i);
        end
    end
end

% Then create a regular grid on which to interpolate the previous data
lim = r(nrmax);
[xgrid,ygrid,zgrid]=meshgrid(0:0.0025:lim,-lim:0.0025:0,-lim:0.0025:lim);

% Interpolate scattered spherical data to regular cartesian data 

v = griddata3(x,y,z,field_ext,xgrid,ygrid,zgrid);

color = v*0+1;

size(v)

isosurface(xgrid,ygrid,zgrid,v,isovalue,color);
%isonormals(xgrid,ygrid,zgrid,v,p)
%p.FaceColor = 'blue';
%p.EdgeColor = 'none';
%daspect([1,1,1])
view(3); axis tight
camlight 
colormap winter
lighting gouraud
grid on



end