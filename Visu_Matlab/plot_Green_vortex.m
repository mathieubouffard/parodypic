function [] = plot_Green_vortex (tra,ntr,N)

% Plots the velocity field as well as the particles 

% First plot the velocity field
dx = 1/N;
dy = dx;
[x,y] = meshgrid(-1:dx:1,-1:dy:1);


Vx = sin(pi*x).*cos(pi*y);
Vy = -cos(pi*x).*sin(pi*y);
vort = pi*sin(pi*x).*sin(pi*y);

xt = tra(:,1);
yt = tra(:,2);
vt = tra(:,3);

v = griddata(xt,yt,vt,x,y);

num_tra(1:2*N+1,1:2*N+1) = 0;
for i=1:ntr
    ix = N+1+floor(tra(i,1)/dx);
    iy = N+1+floor(tra(i,2)/dx);
    num_tra(iy,ix) = num_tra(iy,ix)+1;
end

figure
%quiver(x,y,Vx,Vy)
axis on
box on
hold on
scatter(tra(:,1),tra(:,2),15,tra(:,3),'filled')

figure
pcolor(x,y,vort); shading interp;
axis equal;
axis off;
hold on;
colorbar;

figure
pcolor(x,y,num_tra); shading interp;
axis equal;
axis off;
hold on;
colorbar;

figure
pcolor(x,y,v); shading interp;
axis equal;
axis off;
hold on;
colorbar;

mean(reshape(num_tra,1,(2*N+1)*(2*N+1)))
std(reshape(num_tra,1,(2*N+1)*(2*N+1)))


figure
hist(reshape(num_tra,1,(2*N+1)*(2*N+1)),max(max(num_tra)))
end