function [ output_args ] = phi_section( r,colat,phi,D)
% Plot a section phi = cste

[R, T] = meshgrid(r, colat);
[X, Y] = pol2cart(T, R);
Z = squeeze(D(phi,:,:));


figure;
surf(Y, X, Z,'EdgeColor', 'None', 'facecolor', 'interp');
view(2);
axis equal; 
axis off;
colorbar;

end

