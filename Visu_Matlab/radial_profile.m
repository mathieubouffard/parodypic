function C_mean_rad = radial_profile (r,theta,phi,C)

C_mean_rad = calc_horizontal_mean(r,theta,phi,C);

%then plot

% Create figure
%figure1 = figure('XVisual',...
%    '0xc2 (TrueColor, depth 24, RGB mask 0xff0000 0xff00 0x00ff)');

% Create axes
%axes1 = axes('Parent',figure1,'YGrid','on','XGrid','on','FontWeight','demi',...
%    'FontSize',16);
%box(axes1,'on');
%hold(axes1,'all');

% Create plot
plot(C_mean_rad,r,'LineWidth',2);

% Create xlabel
xlabel('Mean');

% Create ylabel
ylabel('r');

end