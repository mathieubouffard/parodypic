function [quad] = calc_mean_quadratic_error(Ekin,Emag,T,Up,Bt,w)
%Computes mean quadratic error compared to standard solution of 
%benchmark case 1 (Christensen et al)

Ekin_ref = 30.773;
Emag_ref = 626.41;
T_ref = 0.37338;
Up_ref = -7.6250;
Bt_ref = -4.9289;
w_ref = -3.1017;

quad =  ((Ekin-Ekin_ref)/Ekin_ref)^2 ...
      + ((Emag-Emag_ref)/Emag_ref)^2 ...
      + ((T   -T_ref   )/T_ref   )^2 ...
      + ((Up  -Up_ref  )/Up_ref  )^2 ...
      + ((Bt  -Bt_ref  )/Bt_ref  )^2 ...
      + ((w   -w_ref   )/w_ref   )^2 ;

end

