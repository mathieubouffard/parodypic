!========================================!
module tracerstuff
!
! Scalar, vectors and arrays for tracers !
!========================================!
implicit none
SAVE


  logical :: use_tracers

  ! Size of OpenMP packs for dynamic distribution of iterations. To be adapted to a given machine.
  integer :: size_packs=100


  ! Parameters (non-dimensional numbers and factors in the equations)
  Real :: Ra_T,Ra_C,Pr_c,Buoyancy_c,DeltaC,delta,Buoyancy_T,Pr_T,delta_TC,Buoyancy_ratio
  

  ! Grid stuff for tracers
  logical :: rebalance_domains ! MPI load balance in favor of the tracers
  real :: dphi,dphi_1,dtheta_reg_1,dtheta_reg, domain_long, theta_poles
  real :: theta_Nlat !useful in find_closest_lateral_grid_point
  real,allocatable:: long(:),dlat1(:),dlat2(:),drad1(:),drad2(:),r_local(:),drad1_1(:),dlat1_1(:)
  real :: dlat_mean_1
  integer :: nb_irreg,nb_reg,ilat_N,ilat_S,ideb_Ctra,ifin_Ctra,ideb_veltra,ifin_veltra,ideb_Ttra,ifin_Ttra
  real,allocatable  ::    costheta(:),sintheta_1(:),sintheta_2(:)
  real,allocatable  ::    sinphi(:)
  real,allocatable  ::    cosphi(:)
  real :: tmp2_find,tmp1_find,tmp1_find2,q1,q2,deltaR,coeffr
  

  ! Tracers array
  integer :: nCompFields ! not presently used, the array comp(:,:,:) contains only one compositional field
  integer :: COMPPOS, TEMPPOS, NCELLPOS
  Integer*8 :: ntr, ntr_tot, ntr_max, ntr_init
  integer*8 :: maxcomtr ! Define the maximum number of tracers that can be exchange between 2 MPI procs
  integer :: ntracervar
  real,allocatable :: tra(:,:) !dimensions are ntracervar,ntr
  real :: rdeb_tra,rfin_tra
  integer :: initial_distribution
  integer,parameter :: MAXNTRACERVAR=6
  real,parameter :: BLANK=-999.0
  character*16 :: tracervarname(MAXNTRACERVAR) 
  character*20 :: last_tra_file, Entree_tra
  logical :: reordering
  integer :: Modulo_reord
  

  ! Tracers counting
  integer,allocatable :: tra_per_cell(:,:,:),ntr_all(:)
  real,allocatable :: dV1(:,:,:), dV2(:,:,:), Dens_tra(:,:,:) !dimensions: that of the grid, two different estimations of dV
  real :: Vtot, Vtot1, Vtot2,volmax,volmin

  ! T,C,V arrays
  logical :: composition_tracers, temperature_tracers, diffusion_composition 
  real,allocatable :: comp(:,:,:),temp(:,:,:),temp_stat(:,:,:), C_cond(:)
  real,allocatable :: vel_spat(:,:,:,:),vel_cart1(:,:,:,:),vel_cart2(:,:,:,:),vel_cart_poles (:,:,:,:) !arrays for velocity; 1: North, 2: South
  real,allocatable :: Psc_N(:,:,:),Psc_S(:,:,:) !matrix for changing coordinates at North and South poles
  Complex,allocatable ::  yNLT(:,:) 
  Complex,allocatable :: yTts(:,:)
  integer ::  lc_init, mc_init,init_comp_mode
  integer :: modulo_comp


  ! Interpolations
  real,allocatable :: weight_interp(:,:)
  integer,allocatable :: closest_point_coord(:,:,:)
  real,allocatable :: irad_min_emptyn(:),irad_max_emptyn(:),ilat_min_emptyn(:,:),ilat_max_emptyn(:,:)
  real,allocatable :: irad_min_emptyn0(:),irad_max_emptyn0(:),ilat_min_emptyn0(:,:),ilat_max_emptyn0(:,:)
  real ::  Vmax_empty, dmax_empty, dmax_empty0

  ! Arrays for semi-quadratic interpolation
  integer,allocatable :: direction_2ndord_Interp(:,:,:,:)
  real,allocatable :: coeff_second_deriv_r(:,:),coeff_second_deriv_t(:),coeff_second_deriv_p(:,:)
  real,allocatable :: Mat_r(:,:,:),Mat_t(:,:,:),Mat_p(:,:)

  ! Triquadratic interpolation stuff
  logical :: triquadratic_interpolation
  real, allocatable :: mat3Q_1(:,:,:,:),coeffs_3Q(:,:,:,:)


  ! Tracers advection
  integer :: tracers_advord
  logical :: tracers_noadvection, tracers_interp2ord


  ! Tracers diffusion
  real :: numerical_diffusion
  real,allocatable :: t_diff_T(:,:,:),t_diff_C(:,:,:)


  ! Boundary conditions stuff
  logical :: volumic_sources
  real,allocatable :: flux_icb(:,:,:) !dimension nlat,nlong,nCompFields
  real :: cfb1,cfb2,cfb3,cfb4,surf_icb
  real,allocatable:: icb_dS(:,:)
  real :: C_bottom, Lewis, Stefan=10., Nu_C, dist_flux_cmb=0.035, dist_flux_icb=0.035 
  real, allocatable :: C_flux(:,:)
  integer ::  TC_bcs_bot
  real :: sum_tra_source
  real :: icb_cflux,comp_source


  ! Matrices for composition (when solving diffusion)
  Real,allocatable :: A6a_2d_bcs(:,:), diag6_bcs(:,:), A6c_2d_bcs(:,:)
  Real,allocatable :: A6a_bcs(:),A6b_bcs(:,:),A6c_bcs(:)
  Real,allocatable :: A6bp_bcs(:)
  Real,allocatable :: m6_l_bcs(:,:),   m6_u_bcs(:,:)
  integer :: ir_botbcs=10


  ! Coarse grid stuff
  integer :: nlong_cart
  logical :: grid_fusion
  integer :: ndiv_lat,ndiv_long
  integer,allocatable :: div_lat(:),div_long(:),grid_fusion_lat(:),grid_fusion_long(:,:)
  real,allocatable :: comp_poleN(:),comp_poleS(:),temp_poleN(:),temp_poleS(:)
  real :: comp_center, temp_center
  
  ! Tracers tracking
  integer :: ntracked
  real,allocatable :: tracked_tracers(:,:)
  logical :: track_tracers

  ! Measure time of each routine
  logical :: measure_time
  integer :: rank_time
  
contains
  


!============================
end module tracerstuff
!============================




