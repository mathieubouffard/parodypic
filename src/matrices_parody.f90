  module matrices_parody
  implicit none
  public :: define_fw_matrices_parody
  private
  contains
subroutine define_fw_matrices_parody
  use data_hyper, only : hyperdiff
  hyperd: if (.not. hyperdiff) then
     call compute_matrices
  else
     call compute_matrices_hyp
  end if hyperd
end subroutine define_fw_matrices_parody
subroutine compute_matrices
use globall
use tracerstuff
  integer :: l, lm, i, ir
  real :: tmp1, tmp2
  integer :: ier, NGG

  if (rank==0) write (6,'("Re-computing matrices at time ",d16.8)') time
  if (rank==0) then
     open (22, file='log.'//trim(runid), STATUS = 'old', position='append')
     write (22,'("Re-computing matrices at time ",d16.8)') time
     close(22)
  endif

  !**        Initialisation de la matrice A1



  ! ATTENTION, LE CODE N'A PAS ENCORE ETE VALIDE AVEC DeltaU 
  ! different de 1

  do ir = NG+1, NR-1

     A1a(ir) = (-0.5 * Lva(ir) - (1.0/r(ir)) * Gra(ir))*DeltaU

     do l = 1, Lmax

        A1b(l, ir) = (1.0/dt) &
             +(- 0.5 * Lvb(ir) &
             - (1.0/r(ir)) * Grb(ir) &
             + (Real(l*(l+1)) &
             /( 2 * (r(ir))**2) ))*DeltaU
     enddo

     A1c(ir) = (-0.5*Lvc(ir) - (1.0/r(ir))*Grc(ir))*DeltaU

  enddo

  do l = 1, Lmax
     if (A1b(l,NG+1) .eq. 0.) then
        if(rank==0) print *,'Pb Resolution A1b :',l
        if(rank==0) print *,'tridiag (TriRes): cf. Num.Rec. p.43'
        call MPI_finalize(ier)
        stop
     endif
  enddo


!!!Inner boundary

   if (NOSLIPIC) then
      A1a(NG)=0.0
      A1a(NG+1) = 0.0
      A1b(:,NG) = 0.0
      A1c(NG)   = 0.0
   else
      tmp1=r(NG+1)-r(NG)
      tmp2=tmp1*tmp1

      A1a(NG)=0.0 
      do l=1, Lmax
         A1b(l,NG) = 1./dt +DeltaU*(1./tmp2   &
              + real(l*(l+1))/(2*r(NG)**2))
      enddo
      A1c(NG)=DeltaU*(-1./tmp2*r(NG)/r(NG+1)-1/r(NG)/r(NG+1))

   endif


!!!Outer boundary

   if (NOSLIPOC) then
      A1c(NR-1) = 0.0
      A1a(NR)   = 0.0
      A1b(:,NR) = 0.0
      A1c(NR)   = 0.0
   else
      tmp1=r(NR)-r(NR-1)
      tmp2=tmp1*tmp1
      A1a(NR)=DeltaU*(-1./tmp2*r(NR)/r(NR-1)-1/r(NR)/r(NR-1))
      do l=1, Lmax
         A1b(l,NR) = 1./dt +DeltaU*(1/tmp2    &
                   + real(l*(l+1))/(2*r(NR)**2))
      enddo
      A1c(NR)=0.0
   endif

  do i=ideb_all,ifin_all
     A1a_2d(:,i)=A1a(i)
     A1c_2d(:,i)=A1c(i)
  enddo

  do i=ideb_all,ifin_all
     do lm = 2, LMmax
        diag1(lm,i)=A1b(li(lm),i)              
     enddo
  enddo

  call factor_3_para(A1a_2d,diag1,A1c_2d,LMmax,ideb_Vt,ifin_Vt,2,LMmax)
  
  call solver_3_init(A1a_2d,diag1,A1c_2d,m1_l,m1_u,LMmax,ideb_Vt,ifin_Vt,2,LMmax)



!!!        Initialisation des matrices A2 et B2

  !
  ! C.L. ICB (r = r_i)
  !
  ! on utilise :
  !      CLi =  -0.5 * Lva(NG+1) - (1.0/r(NG+1)) * Gra(NG+1)

  ir = NG+1



  do l = 1, Lmax

!!!        A2 = A1 * Lap

     A2LU(l, ir, 1) = 0.0
     A2LU(l, ir, 2) = 0.0
     if (Aspect_ratio.ne.0.) then
     if (NOSLIPIC) then
        A2LU(l, ir, 3) = CLi * ( 2. / ((r(ir)-r(ir-1))**2)) &
          + A1b(l,ir) * ( Lb(ir)-real(l*(l+1))*r_2(ir) ) &
          + A1c(ir)   * La(ir+1)
     else
        A2LU(l, ir, 3) = CLi * ( 2. / ( r(NG)*(r(NG+1)-r(NG)) )) &
          + A1b(l,ir) * ( Lb(ir)-real(l*(l+1))*r_2(ir) ) &
          + A1c(ir)   * La(ir+1)
     endif
     else
        A2LU(l, ir, 3) = 0. &
          + A1b(l,ir) * ( Lb(ir)-real(l*(l+1))*r_2(ir) ) &
          + A1c(ir)   * La(ir+1)
     endif
     A2LU(l, ir, 4) = A1b(l,ir) * Lc(ir) &
          + A1c(ir)   * ( Lb(ir+1)-real(l*(l+1))*r_2(ir+1) )
     A2LU(l, ir, 5) = A1c(ir)   * Lc(ir+1)

!!!        B2 = (2/dt) * Lap - A2

     B2(l, ir, 1) =                   - A2LU(l, ir, 1)
     B2(l, ir, 2) = (2./dt) * La(ir)   - A2LU(l, ir, 2)
     B2(l, ir, 3) = (2./dt) * ( Lb(ir) - real(l*(l+1))*r_2(ir) ) &
          - A2LU(l, ir, 3)
     B2(l, ir, 4) = (2./dt) * Lc(ir)   - A2LU(l, ir, 4)
     B2(l, ir, 5) =                   - A2LU(l, ir, 5)
  enddo



  do ir = NG+2, NR-2

     do l = 1, Lmax


!!!        A2 = A1 * Lap

        A2LU(l,ir,1)=A1a(ir)   * La(ir-1)
        A2LU(l,ir,2)=A1a(ir)*(Lb(ir-1)-real(l*(l+1))*r_2(ir-1)) &
             + A1b(l,ir) * La(ir)
        A2LU(l,ir,3)=A1a(ir)   * Lc(ir-1) &
             + A1b(l,ir)*( Lb(ir)-real(l*(l+1))*r_2(ir)) &
             + A1c(ir)   * La(ir+1)
        A2LU(l,ir,4)=A1b(l,ir) * Lc(ir) &
             + A1c(ir)*( Lb(ir+1)-real(l*(l+1))*r_2(ir+1))
        A2LU(l,ir,5)=A1c(ir)   * Lc(ir+1)

!!!        B2 = (2/dt) * Lap - A2

        B2(l, ir, 1) =                   - A2LU(l, ir, 1)
        B2(l, ir, 2) = (2./dt) * La(ir)   - A2LU(l, ir, 2)
        B2(l, ir, 3) = (2./dt) *(Lb(ir) - real(l*(l+1))*r_2(ir)) &
             - A2LU(l, ir, 3)
        B2(l, ir, 4) = (2./dt) * Lc(ir)   - A2LU(l, ir, 4)
        B2(l, ir, 5) =                   - A2LU(l, ir, 5)

     enddo
  enddo


  !
  ! C.L. CMB (r = r_e)
  !
  ! on utilise :
  !      CLe =  -0.5 * Lvc(NR-1) - (1.0/r(NR-1)) * Grc(NR-1)

  ir= NR-1

  do l = 1, Lmax

!!!        A2 = A1 * Lap

     A2LU(l,ir,1) = A1a(ir)* La(ir-1)
     A2LU(l,ir,2) = A1a(ir)*(Lb(ir-1)-real(l*(l+1))*r_2(ir-1) ) &
          + A1b(l,ir)* La(ir)
     if (NOSLIPOC) then
     A2LU(l,ir,3) = A1a(ir)* Lc(ir-1) &
          + A1b(l,ir)*( Lb(ir)-real(l*(l+1))*r_2(ir) ) &
          + CLe * (2./ ((r(ir+1) - r(ir))**2))
     else
     A2LU(l,ir,3) = A1a(ir)* Lc(ir-1) &
          + A1b(l,ir)*( Lb(ir)-real(l*(l+1))*r_2(ir) ) &
          -  Cle * (2./ ( r(NR)*(r(NR) - r(NR-1)))  )
     endif
     A2LU(l,ir,4) = 0.0
     A2LU(l,ir,5) = 0.0

!!!        B2 = (2/dt) * Lap - A2

     B2(l, ir, 1) =                   - A2LU(l, ir, 1)
     B2(l, ir, 2) = (2./dt) * La(ir)   - A2LU(l, ir, 2)
     B2(l, ir, 3) = (2./dt) * ( Lb(ir) - real(l*(l+1))*r_2(ir) ) &
          - A2LU(l, ir, 3)
     B2(l, ir, 4) = (2./dt) * Lc(ir)   - A2LU(l, ir, 4)
     B2(l, ir, 5) =                   - A2LU(l, ir, 5)
  enddo



  do l = 1, Lmax
     A2LU(l, NG, 1)    = 0.0
     A2LU(l, NG, 2)    = 0.0
     A2LU(l, NG, 3)    = 0.0
     A2LU(l, NG, 4)    = 0.0
     A2LU(l, NG, 5)    = 0.0
     A2LU(l, NG+1, 1)  = 0.0
     A2LU(l, NG+1, 2)  = 0.0
     A2LU(l, NG+2, 1)  = 0.0
     A2LU(l, NR-2, 5)  = 0.0
     A2LU(l, NR-1, 4)  = 0.0
     A2LU(l, NR-1, 5)  = 0.0
     A2LU(l, NR, 1)    = 0.0
     A2LU(l, NR, 2)    = 0.0
     A2LU(l, NR, 3)    = 0.0
     A2LU(l, NR, 4)    = 0.0
     A2LU(l, NR, 5)    = 0.0
     B2(l, NG, 1)     = 0.0
     B2(l, NG, 2)     = 0.0
     B2(l, NG, 3)     = 0.0
     B2(l, NG, 4)     = 0.0
     B2(l, NG, 5)     = 0.0
     B2(l, NG+1, 1)   = 0.0
     B2(l, NG+1, 2)   = 0.0
     B2(l, NG+2, 1)   = 0.0
     B2(l, NR-2, 5)   = 0.0
     B2(l, NR-1, 4)   = 0.0
     B2(l, NR-1, 5)   = 0.0
     B2(l, NR, 1)     = 0.0
     B2(l, NR, 2)     = 0.0
     B2(l, NR, 3)     = 0.0
     B2(l, NR, 4)     = 0.0
     B2(l, NR, 5)     = 0.0
  enddo



  !! Produit

  do ir = NG, NR
     do lm = 2, LMmax
        l = li(lm)
        B2a(lm, ir) = B2(l, ir, 1)
        B2b(lm, ir) = B2(l, ir, 2)
        B2c(lm, ir) = B2(l, ir, 3)
        B2d(lm, ir) = B2(l, ir, 4)
        B2e(lm, ir) = B2(l, ir, 5)
     enddo
  enddo

  do i=ideb_all,ifin_all
     do lm = 2, LMmax
        A2U_big(lm,i,:)=A2LU(li(lm),i,:)
     enddo
  enddo

!!!        Decomposition L.U de A2


  call factor_5_para(A2U_big,LMmax,ideb_Vp,ifin_Vp)
  
  call solver_5_init(A2U_big,m51_l,m52_l,m51_u,m52_u,LMmax,ideb_Vp,ifin_Vp)


!!!        Initialisation de A3


  do ir = NG+1, NR-1


     A3a(ir) =  ( -0.5*Lva(ir) - (1.0/r(ir))*Gra(ir) ) *DeltaT

     do l = 0, Lmax
        A3b(l, ir) = ( 1.0/dt ) &
             + ( -0.5 * Lvb(ir) &
             - (1.0/r(ir)) * Grb(ir) &
             + (Real(l*(l+1)) &
             /( 2 * (r(ir))**2) ) ) *DeltaT
     enddo
     A3bp(ir) = ( 1.0/dt ) &
          + ( -0.5 * Lvb(ir) &
          - (1.0/r(ir)) * Grb(ir) ) *DeltaT

     A3c(ir) = ( -0.5*Lvc(ir) - (1.0/r(ir))*Grc(ir) ) *DeltaT

  enddo

  if ((HeatingMode.eq.2).or.(HeatingMode.eq.3).or.(HeatingMode.eq.5)) then

! Secular Cooling or thermochemical
! No ICB heat flux for perturbation

     if (Aspect_ratio.ne.0.) then
        A3a(NG) = 0.
        do l = 0, Lmax
           A3b(l, NG) = ( 1.0/dt ) &
                + ((1.0 / (r(NG+1)-r(NG))**2 ) &
                + (Real(l*(l+1)) /( 2 * (r(NG))**2))) *DeltaT
        enddo
        A3bp(NG) = ( 1.0/dt ) + (1.0 / (r(NG+1)-r(NG))**2) *DeltaT
        A3c(NG) =  ( -1.0 / (r(NG+1)-r(NG))**2 ) *DeltaT
     else
        A3a(NG) = 0.
        do l = 0, Lmax
           A3b(l, NG) = ( 1.0/dt ) 
        enddo
        A3bp(NG) = ( 1.0/dt ) + (1.0 / (r(NG+1)-r(NG))**2) *DeltaT
        A3c(NG) =  0.
     endif
     
  else

! Other driving modes: ICB temperature is fixed

     A3a(NG+1) = 0.0

  endif

  if ((HeatingMode.eq.1).or.(HeatingMode.eq.2).or.(HeatingMode.eq.3) &
     .or.(HeatingMode.eq.4)) then
     
! Chemical convection or secular cooling or thermochemical or ICT/OCflux
! no CMB heat flux for the temperature perturbation

     A3a(NR) =  ( -1.0 / (r(NR)-r(NR-1))**2 ) *DeltaT
     do l = 0, Lmax
        A3b(l, NR) = ( 1.0/dt ) &
             + ((1.0 / (r(NR)-r(NR-1))**2 ) &
             + (Real(l*(l+1)) /( 2 * (r(NR))**2))) *DeltaT
     enddo
     A3bp(NR) = ( 1.0/dt ) + (1.0 / (r(NR)-r(NR-1))**2) *DeltaT
     A3c(NR) = 0.0
  else
! Delta T heating: CMB temperature is fixed
     A3c(NR-1)=0.0
  endif

  do l = 0, Lmax
     if (A3b(l,NG+1) .eq. 0.) then
        if(rank==0) print *,'Pb A3b pour l =',l
        if(rank==0) print *, '2.tridiag (TriRes): cf. Num.Rec. p.43'
        call MPI_finalize(ier)
        stop
     endif
  enddo



  do i=ideb_all,ifin_all
     A3a_2d(:,i)=A3a(i)
     A3c_2d(:,i)=A3c(i)
  enddo

  do i=ideb_all,ifin_all
     do lm = 1, LMmax
        diag3(lm,i)=A3b(li(lm),i)              
     enddo
  enddo

  call factor_3_para(A3a_2d,diag3,A3c_2d,LMmax,ideb_Tt,ifin_Tt,1,LMmax)
  
  call solver_3_init(A3a_2d,diag3,A3c_2d,m3_l,m3_u,LMmax,ideb_Tt,ifin_Tt,1,LMmax)


  !**   Initialisation de la matrice A4 (poloidal de B)

  if (CondIC) then
     NGG=1
  else
     NGG=2
  endif

  if (Aspect_ratio.eq.0.) NGG=1

  do ir = NGG, NR-1

     A4a(ir) = DeltaB*(-0.5 * Lva(ir) - (1.0/r(ir)) * Gra(ir))

     do l = 1, Lmax

        A4b(l, ir) = (1.0/dt)+DeltaB*( &
             - 0.5 * Lvb(ir) &
             - (1.0/r(ir)) * Grb(ir) &
             + (Real(l*(l+1)) &
             /( 2 * (r(ir))**2) ))
     enddo

     A4c(ir) = DeltaB*(-0.5*Lvc(ir) - (1.0/r(ir))*Grc(ir))

  enddo

! Match potential field outside
  tmp1 = r(NR)- r(NR-1)
  tmp2 = tmp1 * tmp1

  A4a(NR) = -(DeltaB)*(1./tmp2)

  do l = 1, Lmax
     A4b(l, NR) = (1.0/dt)+ (DeltaB)*( (1./tmp2) &
          + (Real(l+1)/(r(NR)*tmp1))            &
          + (Real(l+1)/(r(NR)**2))              &
          + Real(l*(l+1))/(2*(r(NR)**2)) )
  enddo

  A4c(NR) = 0.0


  if (InsulIC) then
! Match potential field inside

     if (Aspect_ratio.ne.0.) then

      tmp1 = r(NG+1)- r(NG)
      tmp2 = tmp1 * tmp1

      A4a(NG)= 0.
      do l=1,Lmax
         A4b(l, NG) = (1.0/dt)+ (DeltaB)*( (1./tmp2) &
          + (Real(l)/(r(NG)*tmp1))                  &
          - (Real(l)/(r(NG)**2))                    &
          + Real(l*(l+1))/(2*(r(NG)**2)) )
      enddo
      A4c(NG) =-(DeltaB)*(1./tmp2)

      endif

   endif

  if (CondIC) then
     do l = 1, Lmax
        if (A4b(l,1) .eq. 0.) then
           if(rank==0) print *,'Pb Resolution A4b :',l
           if(rank==0) print *,'tridiag (TriRes): cf. Num.Rec. p.43'
           call MPI_finalize(ier)
           stop
        endif
     enddo
  else
     do l = 1, Lmax
        if (A4b(l,NG+1) .eq. 0.) then
           if(rank==0) print *,'Pb Resolution A4b :',l
           if(rank==0) print *,'tridiag (TriRes): cf. Num.Rec. p.43'
           call MPI_finalize(ier)
           stop
        endif
     enddo
     endif
     
        do i=ideb_allB,ifin_allB
         A4a_2d(:,i)=A4a(i)
         A4c_2d(:,i)=A4c(i)
        enddo

        do i=ideb_allB,ifin_allB
        do lm = 2, LMmax
           diag4(lm,i)=A4b(li(lm),i)              
        enddo
        enddo

         call factor_3_para(A4a_2d,diag4,A4c_2d,LMmax,ideb_Bp,ifin_Bp,2,LMmax)
         call solver_3_init(A4a_2d,diag4,A4c_2d,m4_l,m4_u,LMmax,ideb_Bp,ifin_Bp,2,LMmax)


  !     Initialisation de la matrice A5 pour le champ toroidal

  do ir = 1, NR-1

     A5a(ir) = DeltaB*(-0.5 * Lva(ir) - (1.0/r(ir)) * Gra(ir))
     do l = 1, Lmax
        A5b(l, ir) = (1.0/dt)+DeltaB*( &
             - 0.5 * Lvb(ir) &
             - (1.0/r(ir)) * Grb(ir) &
             + (Real(l*(l+1)) &
             /( 2 * (r(ir))**2) ) &
             )
     enddo
     A5c(ir) = DeltaB*(-0.5*Lvc(ir) - (1.0/r(ir))*Grc(ir))

  enddo

  !     conditions aux limites sur le toroidal de B:
  !     Bt=0, soit les points 0 et NR sont nuls:

  if (Aspect_ratio.ne.0.) then
  A5a(1) = 0.0
  if (InsulIC) A5a(2) =0.0
  else
  A5a(1) = 0.0
  endif

  A5c(NR-1) = 0.0

  do l = 1, Lmax
     if (A5b(l,2) .eq. 0.) then
        if(rank==0) print *,'Pb A5b pour l =',l
        if(rank==0) print *, '2.tridiag (TriRes): cf. Num.Rec. p.43'
        call MPI_finalize(ier)
        stop
     endif
  enddo


  do i=ideb_allB,ifin_allB
     A5a_2d(:,i)=A5a(i)
     A5c_2d(:,i)=A5c(i)
  enddo
  
  do i=ideb_allB,ifin_allB
     do lm = 2, LMmax
        diag5(lm,i)=A5b(li(lm),i)              
     enddo
  enddo
  
  call factor_3_para(A5a_2d,diag5,A5c_2d,LMmax,ideb_Bt,ifin_Bt,2,LMmax)
  call solver_3_init(A5a_2d,diag5,A5c_2d,m5_l,m5_u,LMmax,ideb_Bt,ifin_Bt,2,LMmax)
        

if (dd_convection .and. diffusion_composition) then
  !**   Initialisation de la matrice A6 (composition)
 do ir = NG+1, NR-1

     A6a(ir) =  ( -0.5*Lva(ir) - (1.0/r(ir))*Gra(ir) ) *DeltaC

     do l = 0, Lmax
        A6b(l, ir) = ( 1.0/dt ) &
             + ( -0.5 * Lvb(ir) &
             - (1.0/r(ir)) * Grb(ir) &
             + (Real(l*(l+1)) &
             /( 2 * (r(ir))**2) ) ) *DeltaC
     enddo
     A6bp(ir) = ( 1.0/dt ) &
          + ( -0.5 * Lvb(ir) &
          - (1.0/r(ir)) * Grb(ir) ) *DeltaC

     A6c(ir) = ( -0.5*Lvc(ir) - (1.0/r(ir))*Grc(ir) ) *DeltaC

  enddo

!  if ((HeatingMode.eq.2).or.(HeatingMode.eq.3)) then

! Secular Cooling or thermochemical
! No ICB heat flux for perturbation

!     if (Aspect_ratio.ne.0.) then
!        A3a(NG) = 0.
!        do l = 0, Lmax
!           A3b(l, NG) = ( 1.0/dt ) &
!                + ((1.0 / (r(NG+1)-r(NG))**2 ) &
!                + (Real(l*(l+1)) /( 2 * (r(NG))**2))) *DeltaT
!        enddo
!        A3bp(NG) = ( 1.0/dt ) + (1.0 / (r(NG+1)-r(NG))**2) *DeltaT
!        A3c(NG) =  ( -1.0 / (r(NG+1)-r(NG))**2 ) *DeltaT
!     else
!        A3a(NG) = 0.
!        do l = 0, Lmax
!           A3b(l, NG) = ( 1.0/dt ) 
!        enddo
!        A3bp(NG) = ( 1.0/dt ) + (1.0 / (r(NG+1)-r(NG))**2) *DeltaT
!        A3c(NG) =  0.
!     endif
     
!  else

! Other driving modes: ICB temperature is fixed

     A6a(NG+1) = 0.0

!  endif

 ! if ((HeatingMode.eq.1).or.(HeatingMode.eq.2).or.(HeatingMode.eq.3) &
 !    .or.(HeatingMode.eq.4)) then
 !    
!! Chemical convection or secular cooling or thermochemical or ICT/OCflux
!! no CMB heat flux for the temperature perturbation!

!     A3a(NR) =  ( -1.0 / (r(NR)-r(NR-1))**2 ) *DeltaT
!     do l = 0, Lmax
!        A3b(l, NR) = ( 1.0/dt ) &
!             + ((1.0 / (r(NR)-r(NR-1))**2 ) &
!             + (Real(l*(l+1)) /( 2 * (r(NR))**2))) *DeltaT
!     enddo
!     A3bp(NR) = ( 1.0/dt ) + (1.0 / (r(NR)-r(NR-1))**2) *DeltaT
!     A3c(NR) = 0.0
!  else
! Delta T heating: CMB temperature is fixed
     A6c(NR-1)=0.0
!  endif

  do l = 0, Lmax
     if (A6b(l,NG+1) .eq. 0.) then
        if(rank==0) print *,'Pb A6b pour l =',l
        if(rank==0) print *, '2.tridiag (TriRes): cf. Num.Rec. p.43'
        call MPI_finalize(ier)
        stop
     endif
  enddo



  do i=ideb_all,ifin_all
     A6a_2d(:,i)=A6a(i)
     A6c_2d(:,i)=A6c(i)
  enddo

  do i=ideb_all,ifin_all
     do lm = 1, LMmax
        diag6(lm,i)=A6b(li(lm),i)              
     enddo
  enddo

  call factor_3_para(A6a_2d,diag6,A6c_2d,LMmax,ideb_C,ifin_C,1,LMmax)
  
  call solver_3_init(A6a_2d,diag6,A6c_2d,m6_l,m6_u,LMmax,ideb_C,ifin_C,1,LMmax)

end if

  new_dt=.false.

end subroutine compute_matrices


subroutine compute_matrices_hyp
use globall
use data_hyper
  integer :: l, lm, i, ir
  real :: tmp1, tmp2
  integer :: ier, NGG

  if (rank==0) write (6,'("Re-computing matrices at time ",d16.8)') time
  if (rank==0) then
     open (22, file='log.'//trim(runid), STATUS = 'old', position='append')
     write (22,'("Re-computing matrices at time ",d16.8)') time
     close(22)
  endif

  !**        Initialisation de la matrice A1



  ! ATTENTION, LE CODE N'A PAS ENCORE ETE VALIDE AVEC DeltaU 
  ! different de 1

  do ir = NG+1, NR-1

     do l = 1, Lmax

        A1a_hyp(l,ir) = (-0.5 * Lva(ir) - (1.0/r(ir)) * Gra(ir))*DeltaU*hyperdiffU(l)

        A1b(l, ir) = (1.0/dt) &
             +(- 0.5 * Lvb(ir) &
             - (1.0/r(ir)) * Grb(ir) &
             + (Real(l*(l+1)) &
             /( 2 * (r(ir))**2) ))*DeltaU * hyperdiffU(l)


        A1c_hyp(l, ir) = (-0.5*Lvc(ir) - (1.0/r(ir))*Grc(ir))*DeltaU*hyperdiffU(l)

     enddo

  enddo

  do l = 1, Lmax
     if (A1b(l,NG+1) .eq. 0.) then
        if(rank==0) print *,'Pb Resolution A1b :',l
        if(rank==0) print *,'tridiag (TriRes): cf. Num.Rec. p.43'
        call MPI_finalize(ier)
        stop
     endif
  enddo


!!!Inner boundary

   if (NOSLIPIC) then
      A1a_hyp(:,NG)=0.0
      A1a_hyp(:,NG+1) = 0.0
      A1b(:,NG) = 0.0
      A1c_hyp(:,NG)   = 0.0
   else
      tmp1=r(NG+1)-r(NG)
      tmp2=tmp1*tmp1

      do l=1, Lmax

         A1a_hyp(l,NG) = 0.0

         A1b(l,NG) = 1./dt +DeltaU*hyperdiffU(l)*(1./tmp2   &
              + real(l*(l+1))/(2*r(NG)**2))

         A1c_hyp(l,NG) = DeltaU*hyperdiffU(l)*(-1./tmp2*r(NG)/r(NG+1)-1/r(NG)/r(NG+1))
      enddo

   endif


!!!Outer boundary

   if (NOSLIPOC) then
      A1c_hyp(:,NR-1) = 0.0
      A1a_hyp(:,NR)   = 0.0
      A1b(:,NR) = 0.0
      A1c_hyp(:,NR)   = 0.0
   else
      tmp1=r(NR)-r(NR-1)
      tmp2=tmp1*tmp1
      do l=1, Lmax

         A1a_hyp(l,NR) = DeltaU*hyperdiffU(l)*(-1./tmp2*r(NR)/r(NR-1)-1/r(NR)/r(NR-1))

         A1b(l,NR) = 1./dt +DeltaU*hyperdiffU(l)*(1/tmp2    &
                   + real(l*(l+1))/(2*r(NR)**2))

         A1c_hyp(l,NR) = 0.0 
      enddo
   endif

  do i=ideb_all,ifin_all
     do lm = 2, LMmax
        A1a_2d(lm,i) = A1a_hyp(li(lm),i)
        A1c_2d(lm,i) = A1c_hyp(li(lm),i)
     end do
  enddo

  do i=ideb_all,ifin_all
     do lm = 2, LMmax
        diag1(lm,i)=A1b(li(lm),i)              
     enddo
  enddo

  call factor_3_para(A1a_2d,diag1,A1c_2d,LMmax,ideb_Vt,ifin_Vt,2,LMmax)
  
  call solver_3_init(A1a_2d,diag1,A1c_2d,m1_l,m1_u,LMmax,ideb_Vt,ifin_Vt,2,LMmax)



!!!        Initialisation des matrices A2 et B2

  !
  ! C.L. ICB (r = r_i)
  !
  ! on utilise :
  !      CLi =  -0.5 * Lva(NG+1) - (1.0/r(NG+1)) * Gra(NG+1)

  ir = NG+1



  do l = 1, Lmax

!!!        A2 = A1 * Lap

     A2LU(l, ir, 1) = 0.0
     A2LU(l, ir, 2) = 0.0
     if (Aspect_ratio.ne.0.) then
     if (NOSLIPIC) then
        A2LU(l, ir, 3) = CLi * ( 2. / ((r(ir)-r(ir-1))**2)) &
          + A1b(l,ir) * ( Lb(ir)-real(l*(l+1))*r_2(ir) ) &
          + A1c_hyp(l,ir)   * La(ir+1)
     else
        A2LU(l, ir, 3) = CLi * ( 2. / ( r(NG)*(r(NG+1)-r(NG)) )) &
          + A1b(l,ir) * ( Lb(ir)-real(l*(l+1))*r_2(ir) ) &
          + A1c_hyp(l,ir)   * La(ir+1)
     endif
     else
        A2LU(l, ir, 3) = 0. &
          + A1b(l,ir) * ( Lb(ir)-real(l*(l+1))*r_2(ir) ) &
          + A1c_hyp(l,ir)   * La(ir+1)
     endif
     A2LU(l, ir, 4) = A1b(l,ir) * Lc(ir) &
          + A1c_hyp(l,ir)   * ( Lb(ir+1)-real(l*(l+1))*r_2(ir+1) )
     A2LU(l, ir, 5) = A1c_hyp(l,ir)   * Lc(ir+1)

!!!        B2 = (2/dt) * Lap - A2

     B2(l, ir, 1) =                   - A2LU(l, ir, 1)
     B2(l, ir, 2) = (2./dt) * La(ir)   - A2LU(l, ir, 2)
     B2(l, ir, 3) = (2./dt) * ( Lb(ir) - real(l*(l+1))*r_2(ir) ) &
          - A2LU(l, ir, 3)
     B2(l, ir, 4) = (2./dt) * Lc(ir)   - A2LU(l, ir, 4)
     B2(l, ir, 5) =                   - A2LU(l, ir, 5)
  enddo



  do ir = NG+2, NR-2

     do l = 1, Lmax


!!!        A2 = A1 * Lap

        A2LU(l,ir,1)=A1a_hyp(l,ir)   * La(ir-1)
        A2LU(l,ir,2)=A1a_hyp(l,ir)*(Lb(ir-1)-real(l*(l+1))*r_2(ir-1)) &
             + A1b(l,ir) * La(ir)
        A2LU(l,ir,3)=A1a_hyp(l,ir)   * Lc(ir-1) &
             + A1b(l,ir)*( Lb(ir)-real(l*(l+1))*r_2(ir)) &
             + A1c_hyp(l,ir)   * La(ir+1)
        A2LU(l,ir,4)=A1b(l,ir) * Lc(ir) &
             + A1c_hyp(l,ir)*( Lb(ir+1)-real(l*(l+1))*r_2(ir+1))
        A2LU(l,ir,5)=A1c_hyp(l,ir)   * Lc(ir+1)

!!!        B2 = (2/dt) * Lap - A2

        B2(l, ir, 1) =                   - A2LU(l, ir, 1)
        B2(l, ir, 2) = (2./dt) * La(ir)   - A2LU(l, ir, 2)
        B2(l, ir, 3) = (2./dt) *(Lb(ir) - real(l*(l+1))*r_2(ir)) &
             - A2LU(l, ir, 3)
        B2(l, ir, 4) = (2./dt) * Lc(ir)   - A2LU(l, ir, 4)
        B2(l, ir, 5) =                   - A2LU(l, ir, 5)

     enddo
  enddo


  !
  ! C.L. CMB (r = r_e)
  !
  ! on utilise :
  !      CLe =  -0.5 * Lvc(NR-1) - (1.0/r(NR-1)) * Grc(NR-1)

  ir= NR-1

  do l = 1, Lmax

!!!        A2 = A1 * Lap

     A2LU(l,ir,1) = A1a_hyp(l,ir)* La(ir-1)
     A2LU(l,ir,2) = A1a_hyp(l,ir)*(Lb(ir-1)-real(l*(l+1))*r_2(ir-1) ) &
          + A1b(l,ir)* La(ir)
     if (NOSLIPOC) then
     A2LU(l,ir,3) = A1a_hyp(l,ir)* Lc(ir-1) &
          + A1b(l,ir)*( Lb(ir)-real(l*(l+1))*r_2(ir) ) &
          + CLe * (2./ ((r(ir+1) - r(ir))**2))
     else
     A2LU(l,ir,3) = A1a_hyp(l,ir)* Lc(ir-1) &
          + A1b(l,ir)*( Lb(ir)-real(l*(l+1))*r_2(ir) ) &
          -  Cle * (2./ ( r(NR)*(r(NR) - r(NR-1)))  )
     endif
     A2LU(l,ir,4) = 0.0
     A2LU(l,ir,5) = 0.0

!!!        B2 = (2/dt) * Lap - A2

     B2(l, ir, 1) =                   - A2LU(l, ir, 1)
     B2(l, ir, 2) = (2./dt) * La(ir)   - A2LU(l, ir, 2)
     B2(l, ir, 3) = (2./dt) * ( Lb(ir) - real(l*(l+1))*r_2(ir) ) &
          - A2LU(l, ir, 3)
     B2(l, ir, 4) = (2./dt) * Lc(ir)   - A2LU(l, ir, 4)
     B2(l, ir, 5) =                   - A2LU(l, ir, 5)
  enddo



  do l = 1, Lmax
     A2LU(l, NG, 1)    = 0.0
     A2LU(l, NG, 2)    = 0.0
     A2LU(l, NG, 3)    = 0.0
     A2LU(l, NG, 4)    = 0.0
     A2LU(l, NG, 5)    = 0.0
     A2LU(l, NG+1, 1)  = 0.0
     A2LU(l, NG+1, 2)  = 0.0
     A2LU(l, NG+2, 1)  = 0.0
     A2LU(l, NR-2, 5)  = 0.0
     A2LU(l, NR-1, 4)  = 0.0
     A2LU(l, NR-1, 5)  = 0.0
     A2LU(l, NR, 1)    = 0.0
     A2LU(l, NR, 2)    = 0.0
     A2LU(l, NR, 3)    = 0.0
     A2LU(l, NR, 4)    = 0.0
     A2LU(l, NR, 5)    = 0.0
     B2(l, NG, 1)     = 0.0
     B2(l, NG, 2)     = 0.0
     B2(l, NG, 3)     = 0.0
     B2(l, NG, 4)     = 0.0
     B2(l, NG, 5)     = 0.0
     B2(l, NG+1, 1)   = 0.0
     B2(l, NG+1, 2)   = 0.0
     B2(l, NG+2, 1)   = 0.0
     B2(l, NR-2, 5)   = 0.0
     B2(l, NR-1, 4)   = 0.0
     B2(l, NR-1, 5)   = 0.0
     B2(l, NR, 1)     = 0.0
     B2(l, NR, 2)     = 0.0
     B2(l, NR, 3)     = 0.0
     B2(l, NR, 4)     = 0.0
     B2(l, NR, 5)     = 0.0
  enddo



  !! Produit

  do ir = NG, NR
     do lm = 2, LMmax
        l = li(lm)
        B2a(lm, ir) = B2(l, ir, 1)
        B2b(lm, ir) = B2(l, ir, 2)
        B2c(lm, ir) = B2(l, ir, 3)
        B2d(lm, ir) = B2(l, ir, 4)
        B2e(lm, ir) = B2(l, ir, 5)
     enddo
  enddo

  do i=ideb_all,ifin_all
     do lm = 2, LMmax
        A2U_big(lm,i,:)=A2LU(li(lm),i,:)
     enddo
  enddo

!!!        Decomposition L.U de A2


  call factor_5_para(A2U_big,LMmax,ideb_Vp,ifin_Vp)
  
  call solver_5_init(A2U_big,m51_l,m52_l,m51_u,m52_u,LMmax,ideb_Vp,ifin_Vp)


!!!        Initialisation de A3


  do ir = NG+1, NR-1

     do l = 0, Lmax

        A3a_hyp(l,ir) =  ( -0.5*Lva(ir) - (1.0/r(ir))*Gra(ir) ) *DeltaT * hyperdiffT(l)

        A3b(l, ir) = ( 1.0/dt ) &
             + ( -0.5 * Lvb(ir) &
             - (1.0/r(ir)) * Grb(ir) &
             + (Real(l*(l+1)) &
             /( 2 * (r(ir))**2) ) ) *DeltaT  * hyperdiffT(l)
     
        A3bp_hyp(l,ir) = ( 1.0/dt ) &
           + ( -0.5 * Lvb(ir) &
           - (1.0/r(ir)) * Grb(ir) ) *DeltaT * hyperdiffT(l)

        A3c_hyp(l,ir) = ( -0.5*Lvc(ir) - (1.0/r(ir))*Grc(ir) ) *DeltaT * hyperdiffT(l) 

     enddo

  enddo

  if ((HeatingMode.eq.2).or.(HeatingMode.eq.3).or.(HeatingMode.eq.5)) then

! Secular Cooling or thermochemical
! No ICB heat flux for perturbation

     if (Aspect_ratio.ne.0.) then

        A3a_hyp(:,NG) = 0.

        do l = 0, Lmax
           A3b(l, NG) = ( 1.0/dt ) &
                + ((1.0 / (r(NG+1)-r(NG))**2 ) &
                + (Real(l*(l+1)) /( 2 * (r(NG))**2))) *DeltaT * hyperdiffT(l)

           A3bp_hyp(l,NG) = ( 1.0/dt ) + (1.0 / (r(NG+1)-r(NG))**2) *DeltaT*hyperdiffT(l)

           A3c_hyp(l,NG) =  ( -1.0 / (r(NG+1)-r(NG))**2 ) *DeltaT * hyperdiffT(l)
        enddo

     else

        A3a_hyp(:,NG) = 0.

        do l = 0, Lmax
           A3b(l, NG) = ( 1.0/dt ) 
           A3bp_hyp(l,NG) = ( 1.0/dt ) + (1.0 / (r(NG+1)-r(NG))**2) *DeltaT * hyperdiffT(l)
        enddo

        A3c_hyp(:,NG) =  0.
     endif
     
  else

! Other driving modes: ICB temperature is fixed

     A3a_hyp(:,NG+1) = 0.0

  endif

  if ((HeatingMode.eq.1).or.(HeatingMode.eq.2).or.(HeatingMode.eq.3) &
     .or.(HeatingMode.eq.4)) then
     
! Chemical convection or secular cooling or thermochemical or ICT/OCflux
! no CMB heat flux for the temperature perturbation

     do l = 0, Lmax
        A3a_hyp(l,NR) = ( -1.0 / (r(NR)-r(NR-1))**2 ) *DeltaT * hyperdiffT(l)

        A3b(l, NR) = ( 1.0/dt ) &
             + ((1.0 / (r(NR)-r(NR-1))**2 ) &
             + (Real(l*(l+1)) /( 2 * (r(NR))**2))) *DeltaT * hyperdiffT(l)
        A3bp_hyp(l,NR) = ( 1.0/dt ) + (1.0 / (r(NR)-r(NR-1))**2) *DeltaT * hyperdiffT(l)
     enddo

     A3c_hyp(:,NR) = 0.0

  else
! Delta T heating: CMB temperature is fixed
     A3c_hyp(:,NR-1) = 0.0
  endif

  do l = 0, Lmax
     if (A3b(l,NG+1) .eq. 0.) then
        if(rank==0) print *,'Pb A3b pour l =',l
        if(rank==0) print *, '2.tridiag (TriRes): cf. Num.Rec. p.43'
        call MPI_finalize(ier)
        stop
     endif
  enddo



  do i=ideb_all,ifin_all
!alex
!    A3a_2d(:,i)=A3a(i)
!    A3c_2d(:,i)=A3c(i)
     do lm = 1, LMmax
        A3a_2d(lm,i)=A3a_hyp(li(lm),i)
        A3c_2d(lm,i)=A3c_hyp(li(lm),i)
     enddo
  enddo

  do i=ideb_all,ifin_all
     do lm = 1, LMmax
        diag3(lm,i)=A3b(li(lm),i)              
     enddo
  enddo

  call factor_3_para(A3a_2d,diag3,A3c_2d,LMmax,ideb_Tt,ifin_Tt,1,LMmax)
  
  call solver_3_init(A3a_2d,diag3,A3c_2d,m3_l,m3_u,LMmax,ideb_Tt,ifin_Tt,1,LMmax)


  !**   Initialisation de la matrice A4 (poloidal de B)


  if (CondIC) then
     NGG=1
  else
     NGG=2
  endif

  if (Aspect_ratio.eq.0.) NGG=1

  do ir = NGG, NR-1

     do l = 1, Lmax

        A4a_hyp(l, ir) = DeltaB*hyperdiffB(l)*(-0.5 * Lva(ir) - (1.0/r(ir)) * Gra(ir))

        A4b(l, ir) = (1.0/dt)+DeltaB*hyperdiffB(l)*( &
             - 0.5 * Lvb(ir) &
             - (1.0/r(ir)) * Grb(ir) &
             + (Real(l*(l+1)) &
             /( 2 * (r(ir))**2) ))

        A4c_hyp(l, ir) = DeltaB*hyperdiffB(l)*(-0.5*Lvc(ir) - (1.0/r(ir))*Grc(ir))       

     enddo

  enddo

! Match potential field outside
  tmp1 = r(NR)- r(NR-1)
  tmp2 = tmp1 * tmp1

  do l = 1, Lmax
     A4a_hyp(l, NR) = -(DeltaB)*hyperdiffB(l)*(1./tmp2)

     A4b(l, NR) = (1.0/dt)+ (DeltaB)*hyperdiffB(l)*( (1./tmp2) &
          + (Real(l+1)/(r(NR)*tmp1))            &
          + (Real(l+1)/(r(NR)**2))              &
          + Real(l*(l+1))/(2*(r(NR)**2)) )

     A4c_hyp(l,NR) = 0.0
  enddo


  if (InsulIC) then
! Match potential field inside

     if (Aspect_ratio.ne.0.) then

      tmp1 = r(NG+1)- r(NG)
      tmp2 = tmp1 * tmp1

      do l=1,Lmax

         A4a_hyp(l, NG) = 0.

         A4b(l, NG) = (1.0/dt)+ (DeltaB)*hyperdiffB(l)*( (1./tmp2) &
          + (Real(l)/(r(NG)*tmp1))                  &
          - (Real(l)/(r(NG)**2))                    &
          + Real(l*(l+1))/(2*(r(NG)**2)) )

         A4c_hyp(l, NG) = -(DeltaB)*hyperdiffB(l)*(1./tmp2)

      enddo

      endif

   endif

  if (CondIC) then
     do l = 1, Lmax
        if (A4b(l,1) .eq. 0.) then
           if(rank==0) print *,'Pb Resolution A4b :',l
           if(rank==0) print *,'tridiag (TriRes): cf. Num.Rec. p.43'
           call MPI_finalize(ier)
           stop
        endif
     enddo
  else
     do l = 1, Lmax
        if (A4b(l,NG+1) .eq. 0.) then
           if(rank==0) print *,'Pb Resolution A4b :',l
           if(rank==0) print *,'tridiag (TriRes): cf. Num.Rec. p.43'
           call MPI_finalize(ier)
           stop
        endif
     enddo
     endif
     
        do i=ideb_allB,ifin_allB
 !       A4a_2d(:,i)=A4a(i)
 !       A4c_2d(:,i)=A4c(i)
        do lm = 2, LMmax
           A4a_2d(lm,i)=A4a_hyp(li(lm),i)
           A4c_2d(lm,i)=A4c_hyp(li(lm),i)
        enddo
        enddo

        do i=ideb_allB,ifin_allB
        do lm = 2, LMmax
           diag4(lm,i)=A4b(li(lm),i)              
        enddo
        enddo

         call factor_3_para(A4a_2d,diag4,A4c_2d,LMmax,ideb_Bp,ifin_Bp,2,LMmax)
         call solver_3_init(A4a_2d,diag4,A4c_2d,m4_l,m4_u,LMmax,ideb_Bp,ifin_Bp,2,LMmax)


  !     Initialisation de la matrice A5 pour le champ toroidal

  do ir = 1, NR-1

     do l = 1, Lmax
        A5a_hyp(l,ir) = DeltaB*hyperdiffB(l)*(-0.5 * Lva(ir) - (1.0/r(ir)) * Gra(ir))

        A5b(l, ir) = (1.0/dt)+DeltaB*hyperdiffB(l)*( &
             - 0.5 * Lvb(ir) &
             - (1.0/r(ir)) * Grb(ir) &
             + (Real(l*(l+1)) &
             /( 2 * (r(ir))**2) ) &
             )

        A5c_hyp(l,ir) = DeltaB*hyperdiffB(l)*(-0.5*Lvc(ir) - (1.0/r(ir))*Grc(ir))

     enddo

  enddo

  !     conditions aux limites sur le toroidal de B:
  !     Bt=0, soit les points 0 et NR sont nuls:

  if (Aspect_ratio.ne.0.) then
  A5a_hyp(:,1) = 0.0
  if (InsulIC) A5a(2) =0.0
  if (InsulIC) A5a_hyp(:,2) =0.0
  else
  A5a_hyp(:,1) = 0.0
  endif

  A5c_hyp(:,NR-1) = 0.0

  do l = 1, Lmax
     if (A5b(l,2) .eq. 0.) then
        if(rank==0) print *,'Pb A5b pour l =',l
        if(rank==0) print *, '2.tridiag (TriRes): cf. Num.Rec. p.43'
        call MPI_finalize(ier)
        stop
     endif
  enddo


  do i=ideb_allB,ifin_allB
!    A5a_2d(:,i)=A5a(i)
!    A5c_2d(:,i)=A5c(i)
     do lm = 2, LMmax
      A5a_2d(lm,i)=A5a_hyp(li(lm),i)
      A5c_2d(lm,i)=A5c_hyp(li(lm),i)
     end do
  enddo
  
  do i=ideb_allB,ifin_allB
     do lm = 2, LMmax
        diag5(lm,i)=A5b(li(lm),i)              
     enddo
  enddo
  
  call factor_3_para(A5a_2d,diag5,A5c_2d,LMmax,ideb_Bt,ifin_Bt,2,LMmax)
  call solver_3_init(A5a_2d,diag5,A5c_2d,m5_l,m5_u,LMmax,ideb_Bt,ifin_Bt,2,LMmax)
        


  new_dt=.false.

end subroutine compute_matrices_hyp
  end module matrices_parody
