!*******************************************************************************************!
!                                grid FUSION ROUTINES                                       !  
!                                                                                           !
!                                Added on April 2017                                        ! 
!                                                                                           !
!                                                                                           !
!  Merge grid cells where the resolution becomes uselessly high due to                      !
!  spherical geometry                                                                       !
!  to decrease the time spent in dealing with "empty cells"                                 !
!                                                                                           !
!  Main steps are :                                                                         !
!                                                                                           !
!    1) Creation of a coarse grid by merging cells                                          !
!    2) Interpolate from tracers to coarse grid nodes                                       !
!    3) Completion of the coarse grid if necessary                                          !
!    4) Linear completion of the fine grid from the coarse grid                             !         
!                                                                                           !
!                                                                                           !
!*******************************************************************************************!


subroutine set_grid_fusion_lat ()
  ! Definition of the coarse grid in latitude. For each concentric
  ! sphere, 
  ! merge cells along latitude when the local resolution is uselessly
  ! high

  use globall
  use tracerstuff

  implicit none

  integer :: ir,ilat,ndiv_used,n_merge,ir_min
  real    :: dlat_mean,dlat_mean0,r_c
  real    :: div_work(Nlat/2) 

  ! Start by computing the divisors of Nlat
  ndiv_lat = 0
  do ilat = 2,Nlat/2+1
     if (mod(Nlat,ilat) == 0) then
        ndiv_lat = ndiv_lat + 1
        div_work(ndiv_lat) = ilat
     end if
  end do
  allocate(div_lat(ndiv_lat))
  div_lat(:) = div_work(1:ndiv_lat)

  
  ! Then determine where cells should be merged in latitude
  ndiv_used = 0
  dlat_mean0 = pi/(Nlat+1)
  dlat_mean = dlat_mean0
  n_merge = 1
  r_c = r(NR)*0.5

  if (Aspect_ratio == 0.) then
     ir_min = 2
     grid_fusion_lat(1) = 1
  else
     ir_min = 1
  end if

  do ir = Nr,ir_min,-1
     ! set criterium to merge
     if (r(ir) .ge. r_c) then ! top part, no merging
        grid_fusion_lat(ir) = 1
     else
        if (dlat_mean .gt. 0.75*dlat_mean0*sqrt(r_c/r(ir))) then ! if the cell is large enough, don't merge 
                grid_fusion_lat(ir) = n_merge
        else
           if (ndiv_used .lt. ndiv_lat) then! if too small, merge cells by packs if still available divisors
              ndiv_used = ndiv_used + 1
              grid_fusion_lat(ir) = div_lat(ndiv_used)
              n_merge = grid_fusion_lat(ir)
              dlat_mean = dlat_mean0 * grid_fusion_lat(ir)
           else !no available divisors left, don't merge 
              grid_fusion_lat(ir) = n_merge ! tot_merge
           end if
        end if
     end if
  end do

!  print*,rank,'grid_fusion_lat',grid_fusion_lat 
!  print*

end subroutine set_grid_fusion_lat
!-----------------------------------------------------------------------------------


subroutine set_grid_fusion_long ()
  ! Definition of the coarse grid in longitude. For each concentric
  ! sphere, 
  ! at each latitude of the coarse grid defined by set_grid_fusion_lat,
  ! merge cells along longitude when the local resolution is uselessly
  ! high

  use globall
  use tracerstuff
  
  implicit none

  integer :: ir,ilong,ilat,ir_min,ndiv_used,ndiv_used_Nlat_2,tot_merge,tot_merge_Nlat_2
  real    :: dlat_mean0,dlong_mean,dlong_mean0,dlong_mean_Nlat_2,dlong_mean0_Nlat_2,r_c,crit
  real    :: div_work(Nlong/2)

  ! Start by computing the divisors of Nlong
  ndiv_long = 0
  do ilong = 2,Nlong/2+1
     if (mod(Nlong,ilong) == 0) then
       ndiv_long = ndiv_long + 1
       div_work(ndiv_long) = ilong
     end if
  end do
  allocate(div_long(ndiv_long))
  div_long(:) = div_work(1:ndiv_long)

  ! Then determine where cells should be merged in longitude
  r_c = r(NR)*0.5

  dlat_mean0 = pi/(Nlat+1)

  if (Aspect_ratio == 0.) then
     ir_min = 2
     grid_fusion_long(:,1) = 1
  else
     ir_min = 1
  end if

  do ir = Nr,ir_min,-1
     tot_merge_Nlat_2 = 1
     ndiv_used_Nlat_2 = 0
     dlong_mean0_Nlat_2 = 2.*pi/Nlong
     dlong_mean_Nlat_2 = dlong_mean0_Nlat_2
     if (r(ir) .gt. r_c) then
        crit = 0.75*r(ir)*dlat_mean0
     else
        crit = 0.75*r(ir)*dlat_mean0*sqrt(r_c/r(ir))
     end if
     ! Start with Nlat/2
     if (r(ir)*sin(colat(Nlat/2))*dlong_mean_Nlat_2 .gt. crit) then
        grid_fusion_long(Nlat/2,ir) = tot_merge_Nlat_2
     else
        do while (r(ir)*sin(colat(Nlat/2))*dlong_mean_Nlat_2 .lt. crit)
           ndiv_used_Nlat_2 = ndiv_used_Nlat_2 + 1
           grid_fusion_long(Nlat/2,ir) = div_long(ndiv_used_Nlat_2)
           tot_merge_Nlat_2 = div_long(ndiv_used_Nlat_2)
           dlong_mean_Nlat_2 = dlong_mean0_Nlat_2 * tot_merge_Nlat_2
           if (ndiv_used_Nlat_2 .ge. ndiv_long-2) exit 
        end do
     end if

     ! First half of latitudes
     dlong_mean0 = 2.*pi/Nlong
     dlong_mean = dlong_mean_Nlat_2
     ndiv_used = ndiv_used_Nlat_2
     tot_merge = tot_merge_Nlat_2
     
     do ilat = Nlat/2-1,1,-1
        if (r(ir)*sin(colat(ilat))*dlong_mean .gt. crit) then ! if the cell is large enough, don't merge
           grid_fusion_long(ilat,ir) = tot_merge
        else
           if (ndiv_used .lt. ndiv_long-2) then! if too small, merge cells by packs if still available divisors
              ndiv_used = ndiv_used + 1
              grid_fusion_long(ilat,ir) = div_long(ndiv_used)
              tot_merge = grid_fusion_long(ilat,ir)
              dlong_mean = dlong_mean0 * tot_merge
           else !no available divisors left
              grid_fusion_long(ilat,ir) = tot_merge
           end if
        end if
     end do

     ! Second half
     dlong_mean0 = 2.*pi/Nlong
     dlong_mean = dlong_mean_Nlat_2
     ndiv_used = ndiv_used_Nlat_2
     tot_merge = tot_merge_Nlat_2
     
     do ilat = Nlat/2+1,Nlat
        if (r(ir)*sin(colat(ilat))*dlong_mean .gt. crit) then ! if the cell is large enough, don't merge
           grid_fusion_long(ilat,ir) = tot_merge
        else
           if (ndiv_used .lt. ndiv_long-2) then! if too small, merge cells by packs of 2, 3 or 5 if still available divisors
              ndiv_used = ndiv_used + 1
              grid_fusion_long(ilat,ir) = div_long(ndiv_used)
              tot_merge = grid_fusion_long(ilat,ir)
              dlong_mean = dlong_mean0 * tot_merge
           else !no available divisors left
              grid_fusion_long(ilat,ir) = tot_merge
           end if
        end if
     end do
  end do

!  print*,rank,'grid_fusion_long',grid_fusion_long
!  print*  


end subroutine set_grid_fusion_long
!-----------------------------------------------------------------------------------

subroutine tracers_to_grid_special (field,ideb,ifin,FIELDPOS)

  use globall
  use tracerstuff
  use mpi
!$ use omp_lib
  
  implicit none

  integer,intent(in) :: ideb,ifin,FIELDPOS
  real,intent(inout),dimension(Nlong,Nlat,ideb-2:ifin+2) :: field ! temperature or composition
  
  integer :: irad0,irad1,ilat0,ilat1,ilong0,ilong1,Nlat_max,Nlong_max,i,ir,ilat,ilong,irad
  integer :: ilat_CG_r0_m,ilat_CG_r0_p,ilat_CG_r1_m,ilat_CG_r1_p
  integer :: ilong_CG_r0_tm_m,ilong_CG_r0_tm_p,ilong_CG_r0_tp_m,ilong_CG_r0_tp_p
  integer :: ilong_CG_r1_tm_m,ilong_CG_r1_tm_p,ilong_CG_r1_tp_m,ilong_CG_r1_tp_p
  integer :: nmerge_lat,nmerge_long
  real :: w1,w2,w3,w4
  real,dimension(Nlong,Nlat,ideb-2:ifin+2) :: n
  real :: rad,theta,phi,mtime_debut,mtime_fin
  integer :: n_zeros,n_CG,ier
  real,dimension(ideb-2:ifin+2) :: field_poleN,field_poleS,n_poleN,n_poleS
  logical :: pole,south_pole,north_pole,OpenMP_parallel
!$ integer :: nb_threads
!$ real,allocatable :: field_private(:,:,:),n_private(:,:,:)
!$ real,allocatable :: field_poleN_private(:),field_poleS_private(:)
!$ real,allocatable :: n_poleN_private(:),n_poleS_private(:)  


  print*,rank,'before tracers_to_C special'

  n(:,:,:) = 0.
  field(:,:,:) = 0.
  field_poleN(:) = 0.
  field_poleS(:) = 0.
  n_poleN(:) = 0.
  n_poleS(:) = 0.

!$OMP PARALLEL  &
!$OMP PRIVATE (irad0,irad1,ilat0,ilat1,ilong0,ilong1,ilat_CG_r0_m,ilat_CG_r0_p,ilat_CG_r1_m,ilat_CG_r1_p) &
!$OMP PRIVATE (ilong_CG_r0_tm_m,ilong_CG_r0_tm_p,ilong_CG_r0_tp_m,ilong_CG_r0_tp_p) &
!$OMP PRIVATE (ilong_CG_r1_tm_m,ilong_CG_r1_tm_p,ilong_CG_r1_tp_m,ilong_CG_r1_tp_p) &  
!$OMP PRIVATE (nmerge_lat,nmerge_long,Nlat_max,Nlong_max) &
!$OMP PRIVATE (rad,theta,phi,w1,w2,w3,w4,field_poleN_private,field_poleS_private,n_poleN_private,n_poleS_private) &
!$OMP PRIVATE (ir,irad,ilat,ilong,nb_threads,field_private,n_private,pole,north_pole,south_pole) 

!$ allocate(field_private(Nlong,Nlat,ideb-2:ifin+2),n_private(Nlong,Nlat,ideb-2:ifin+2))
!$ allocate(field_poleN_private(ideb-2:ifin+2),field_poleS_private(ideb-2:ifin+2))
!$ allocate(n_poleN_private(ideb-2:ifin+2),n_poleS_private(ideb-2:ifin+2))   
!$ field_private(:,:,:) = 0.
!$ n_private (:,:,:) = 0.
!$ field_poleN_private(:) = 0.
!$ field_poleS_private(:) = 0.
!$ n_poleN_private(:) = 0.
!$ n_poleS_private(:) = 0.


!$ if (.true.) then
!$OMP MASTER
!$     OpenMP_parallel=.true.
!$OMP END MASTER
!$  else
  OpenMP_parallel = .false.
!$  end if
!$OMP BARRIER

print*,'OpenMP_parallel',OpenMP_parallel

if (rank==0) mtime_debut = MPI_Wtime ()

!$ nb_threads = omp_get_num_threads ()

!      IF (rank == 0) THEN
!!            WRITE (6,'(A)')
!!            WRITE (6,'(A)')
!!            WRITE (6,'(A)')
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!!$          mtime_debut = omp_get_wtime()
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
!            mtime_debut = MPI_WTime ()
!!$       END IF
!!            WRITE (6,'(A)')
!!            WRITE (6,'(A)')
!!            WRITE (6,'(A)')
!      END IF

IF (.not. OpenMP_parallel) THEN

  do i = 1,ntr

     rad = tra(1,i)
     theta = tra(2,i)
     phi = tra(3,i)

     ! 1) Locate tracers in fine grid (already stored in
     ! memory)----------------------------
     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)
     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)
     ilong0 = closest_point_coord(1,3,i)
     ilong1 = closest_point_coord(2,3,i)
     
     ! 2) Find the nodes of the coarse grid the tracer will contribute
     ! to, compute weights
     !    and interpolation. Performed first for ir = irad0, then ir =
     !    irad1----------------
     
     !============= spherical shell ir = irad0 ==============
     pole = .false.
     south_pole = .false.
     north_pole = .false.

  if (Aspect_ratio .eq. 0. .and. irad0 .eq. 1) then ! center of the sphere 
     call distance_1 (irad0,1,1,rad,theta,phi,w1)
     field(:,:,irad0)  = field(:,:,irad0) + w1 * tra(FIELDPOS,i)
     n(:,:,irad0) = n(:,:,irad0) + w1 
  else
     ! LOCATE TRACERS IN COARSE GRID ALONG LATITUDE
     if (grid_fusion_lat(irad0) == 1) then ! no fusion
        ilat_CG_r0_m = ilat0
        ilat_CG_r0_p = ilat1
        if (theta .lt. colat(1)) then !pole
           pole = .true.
           north_pole = .true.
        end if
        if (ilat0 == Nlat) then
           pole = .true.
           south_pole = .true.
        end if
     else ! fusion
        ilat_CG_r0_m = (ilat0/grid_fusion_lat(irad0))*grid_fusion_lat(irad0)
        ilat_CG_r0_p = ilat_CG_r0_m + grid_fusion_lat(irad0)  
        if (ilat_CG_r0_m .le. 0) then
           pole = .true.
           north_pole = .true.
        else if (ilat_CG_r0_p .ge. Nlat) then ! careful, situation is not symmetric between south and north poles
           pole = .true.
           south_pole = .true.
        end if
     end if

     ! LOCATE TRACER IN COARSE GRID ALONG LONGITUDE
     if (.not. north_pole) then
        if (grid_fusion_long(ilat_CG_r0_m,irad0) == 1) then ! no fusion
           ilong_CG_r0_tm_m = ilong0
           ilong_CG_r0_tm_p = ilong1
        else ! fusion
           ilong_CG_r0_tm_m = 1 + ((ilong0-1)/grid_fusion_long(ilat_CG_r0_m,irad0))*grid_fusion_long(ilat_CG_r0_m,irad0)
           ilong_CG_r0_tm_p = max(mod(ilong_CG_r0_tm_m + grid_fusion_long(ilat_CG_r0_m,irad0),nlong+1),1) 
        end if
     end if
     if (.not. south_pole) then
        if (grid_fusion_long(ilat_CG_r0_p,irad0) == 1) then
           ilong_CG_r0_tp_m = ilong0
           ilong_CG_r0_tp_p = ilong1
        else
           ilong_CG_r0_tp_m = 1 + ((ilong0-1)/grid_fusion_long(ilat_CG_r0_p,irad0))*grid_fusion_long(ilat_CG_r0_p,irad0)
           ilong_CG_r0_tp_p = max(mod(ilong_CG_r0_tp_m + grid_fusion_long(ilat_CG_r0_p,irad0),nlong+1),1)
        end if
     end if

     ! WEIGHTS COMPUTATION AND INTERPOLATION
     if (.not. pole) then
        
        call distance_1 (irad0,ilat_CG_r0_m,ilong_CG_r0_tm_m,rad,theta,phi,w1)
        call distance_1 (irad0,ilat_CG_r0_m,ilong_CG_r0_tm_p,rad,theta,phi,w2)
        call distance_1 (irad0,ilat_CG_r0_p,ilong_CG_r0_tp_m,rad,theta,phi,w3)
        call distance_1 (irad0,ilat_CG_r0_p,ilong_CG_r0_tp_p,rad,theta,phi,w4)  

        field(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) = field(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) + w1 * tra(FIELDPOS,i)     
        field(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) = field(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) + w2 * tra(FIELDPOS,i)
        field(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) = field(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) + w3 * tra(FIELDPOS,i)
        field(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) = field(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) + w4 * tra(FIELDPOS,i)

        n(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) = n(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) + w1      
        n(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) = n(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) + w2 
        n(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) = n(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) + w3
        n(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) = n(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) + w4 

     else

        if (north_pole) then
           
           ! distance to nodes
           call distance_1 (irad0,ilat_CG_r0_p,ilong_CG_r0_tp_m,rad,theta,phi,w1)
           call distance_1 (irad0,ilat_CG_r0_p,ilong_CG_r0_tp_p,rad,theta,phi,w2)
           ! distance to north pole
           call distance_1 (irad0,0,1,rad,theta,phi,w3) 

           field(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) = field(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) + w1 * tra(FIELDPOS,i)
           field(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) = field(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) + w2 * tra(FIELDPOS,i)
           field_poleN(irad0) = field_poleN(irad0) + w3 * tra(FIELDPOS,i)

           n(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) = n(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) + w1
           n(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) = n(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) + w2
           n_poleN(irad0) = n_poleN(irad0) + w3       

        else if (south_pole) then

           ! distance to nodes
           call distance_1 (irad0,ilat_CG_r0_m,ilong_CG_r0_tm_m,rad,theta,phi,w1)
           call distance_1 (irad0,ilat_CG_r0_m,ilong_CG_r0_tm_p,rad,theta,phi,w2)
           ! distance to south pole
           call distance_1 (irad0,Nlat+1,1,rad,theta,phi,w3) 

           field(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) = field(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) + w1 * tra(FIELDPOS,i)     
           field(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) = field(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) + w2 * tra(FIELDPOS,i)
           field_poleS(irad0) = field_poleS(irad0) + w3 * tra(FIELDPOS,i)

           n(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) = n(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) + w1      
           n(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) = n(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) + w2
           n_poleS(irad0) = n_poleS(irad0) + w3
           
        end if

     end if

  end if ! if center

     !============= spherical shell ir = irad1 ==============
     pole = .false.
     south_pole = .false.
     north_pole = .false.

     ! LOCATE TRACERS IN COARSE GRID ALONG LATITUDE
     if (grid_fusion_lat(irad1) == 1) then ! no fusion
        ilat_CG_r1_m = ilat0
        ilat_CG_r1_p = ilat1
        if (theta .lt. colat(1)) then !pole
           pole = .true.
           north_pole = .true.
        end if
        if (ilat0 == Nlat) then
           pole = .true.
           south_pole = .true.
        end if
     else ! fusion
        ilat_CG_r1_m = (ilat0/grid_fusion_lat(irad1))*grid_fusion_lat(irad1)
        ilat_CG_r1_p = ilat_CG_r1_m + grid_fusion_lat(irad1)  
        if (ilat_CG_r1_m .le. 0) then
           pole = .true.
           north_pole = .true.
        else if (ilat_CG_r1_p .ge. Nlat) then ! careful, situation is not symmetric between south and north poles
           pole = .true.
           south_pole = .true.
        end if
     end if

     ! LOCATE TRACER IN COARSE GRID ALONG LONGITUDE
     if (.not. north_pole) then
        if (grid_fusion_long(ilat_CG_r1_m,irad0) == 1) then ! no fusion
           ilong_CG_r1_tm_m = ilong0
           ilong_CG_r1_tm_p = ilong1
        else ! fusion
           ilong_CG_r1_tm_m = 1 + ((ilong0-1)/grid_fusion_long(ilat_CG_r1_m,irad1))*grid_fusion_long(ilat_CG_r1_m,irad1)
           ilong_CG_r1_tm_p = max(mod(ilong_CG_r1_tm_m + grid_fusion_long(ilat_CG_r1_m,irad1),nlong+1),1) 
        end if
     end if
     if (.not. south_pole) then
        if (grid_fusion_long(ilat_CG_r1_p,irad1) == 1) then
           ilong_CG_r1_tp_m = ilong0
           ilong_CG_r1_tp_p = ilong1
        else
           ilong_CG_r1_tp_m = 1 + ((ilong0-1)/grid_fusion_long(ilat_CG_r1_p,irad1))*grid_fusion_long(ilat_CG_r1_p,irad1)
           ilong_CG_r1_tp_p = max(mod(ilong_CG_r1_tp_m + grid_fusion_long(ilat_CG_r1_p,irad1),nlong+1),1)
        end if
     end if
     
     ! WEIGHTS COMPUTATION AND INTERPOLATION
     if (.not. pole) then
        
        call distance_1 (irad1,ilat_CG_r1_m,ilong_CG_r1_tm_m,rad,theta,phi,w1)
        call distance_1 (irad1,ilat_CG_r1_m,ilong_CG_r1_tm_p,rad,theta,phi,w2)
        call distance_1 (irad1,ilat_CG_r1_p,ilong_CG_r1_tp_m,rad,theta,phi,w3)
        call distance_1 (irad1,ilat_CG_r1_p,ilong_CG_r1_tp_p,rad,theta,phi,w4)  

        field(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) = field(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) + w1 * tra(FIELDPOS,i)     
        field(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) = field(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) + w2 * tra(FIELDPOS,i)
        field(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) = field(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) + w3 * tra(FIELDPOS,i)
        field(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) = field(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) + w4 * tra(FIELDPOS,i)

        n(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) = n(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) + w1      
        n(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) = n(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) + w2 
        n(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) = n(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) + w3
        n(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) = n(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) + w4 

     else

        if (north_pole) then
           
           ! distance to nodes
           call distance_1 (irad1,ilat_CG_r1_p,ilong_CG_r1_tp_m,rad,theta,phi,w1)
           call distance_1 (irad1,ilat_CG_r1_p,ilong_CG_r1_tp_p,rad,theta,phi,w2)
           ! distance to north pole
           call distance_1 (irad1,0,1,rad,theta,phi,w3) 

           field(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) = field(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) + w1 * tra(FIELDPOS,i)
           field(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) = field(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) + w2 * tra(FIELDPOS,i)
           field_poleN(irad1) = field_poleN(irad1) + w3 * tra(FIELDPOS,i)

           n(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) = n(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) + w1
           n(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) = n(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) + w2
           n_poleN(irad1) = n_poleN(irad1) + w3       

        else if (south_pole) then

           ! distance to nodes
           call distance_1 (irad1,ilat_CG_r1_m,ilong_CG_r1_tm_m,rad,theta,phi,w1)
           call distance_1 (irad1,ilat_CG_r1_m,ilong_CG_r1_tm_p,rad,theta,phi,w2)
           ! distance to south pole
           call distance_1 (irad1,Nlat+1,1,rad,theta,phi,w3) 

           field(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) = field(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) + w1 * tra(FIELDPOS,i)     
           field(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) = field(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) + w2 * tra(FIELDPOS,i)
           field_poleS(irad1) = field_poleS(irad1) + w3 * tra(FIELDPOS,i)

           n(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) = n(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) + w1      
           n(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) = n(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) + w2
           n_poleS(irad1) = n_poleS(irad1) + w3
           
        end if

     end if

  end do

ELSE !if OpenMP

!$OMP DO SCHEDULE(STATIC,ntr/nb_threads)
   do i = 1,ntr
      
      rad = tra(1,i)
      theta = tra(2,i)
      phi = tra(3,i)

      ! 1) Locate tracers in fine grid (already stored in
      ! memory)----------------------------
      irad0 = closest_point_coord(1,1,i)
      irad1 = closest_point_coord(2,1,i)
      ilat0 = closest_point_coord(1,2,i)
      ilat1 = closest_point_coord(2,2,i)
      ilong0 = closest_point_coord(1,3,i)
      ilong1 = closest_point_coord(2,3,i)
  
!      print*,rank,irad0,irad1,ilat0,ilat1,ilong0,ilong1
      
      ! 2) Find the nodes of the coarse grid the tracer will contribute
      ! to, compute weights
      !    and interpolation. Performed first for ir = irad0, then ir =
      !    irad1----------------
      
      !============= spherical shell ir = irad0 ==============
      pole = .false.
      south_pole = .false.
      north_pole = .false.

  if (Aspect_ratio .eq. 0. .and. irad0 .eq. 1) then ! center of the sphere                               
     call distance_1 (irad0,1,1,rad,theta,phi,w1)
     field(:,:,irad0) = field(:,:,irad0) + w1 * tra(FIELDPOS,i)
     n(:,:,irad0) = n(:,:,irad0) + w1
  else
      
      ! LOCATE TRACERS IN COARSE GRID ALONG LATITUDE
      if (grid_fusion_lat(irad0) == 1) then ! no fusion
         ilat_CG_r0_m = ilat0
         ilat_CG_r0_p = ilat1
         if (theta .lt. colat(1)) then !pole
            pole = .true.
            north_pole = .true.
         end if
         if (ilat0 == Nlat) then
            pole = .true.
            south_pole = .true.
         end if
      else ! fusion
         ilat_CG_r0_m = (ilat0/grid_fusion_lat(irad0))*grid_fusion_lat(irad0)
         ilat_CG_r0_p = ilat_CG_r0_m + grid_fusion_lat(irad0)  
         if (ilat_CG_r0_m .le. 0) then
            pole = .true.
            north_pole = .true.
         else if (ilat_CG_r0_p .ge. Nlat) then ! careful, situation is not symmetric between south and north poles
            pole = .true.
            south_pole = .true.
         end if
      end if

!print*,rank,i,'after locate latitude'
      
      ! LOCATE TRACER IN COARSE GRID ALONG LONGITUDE
      if (.not. north_pole) then
         if (grid_fusion_long(ilat_CG_r0_m,irad0) == 1) then ! no fusion
            ilong_CG_r0_tm_m = ilong0
            ilong_CG_r0_tm_p = ilong1
         else ! fusion
            ilong_CG_r0_tm_m = 1 + ((ilong0-1)/grid_fusion_long(ilat_CG_r0_m,irad0))*grid_fusion_long(ilat_CG_r0_m,irad0)
            ilong_CG_r0_tm_p = max(mod(ilong_CG_r0_tm_m + grid_fusion_long(ilat_CG_r0_m,irad0),nlong+1),1) 
         end if
      end if
      if (.not. south_pole) then
         if (grid_fusion_long(ilat_CG_r0_p,irad0) == 1) then
            ilong_CG_r0_tp_m = ilong0
            ilong_CG_r0_tp_p = ilong1
         else
            ilong_CG_r0_tp_m = 1 + ((ilong0-1)/grid_fusion_long(ilat_CG_r0_p,irad0))*grid_fusion_long(ilat_CG_r0_p,irad0)
            ilong_CG_r0_tp_p = max(mod(ilong_CG_r0_tp_m + grid_fusion_long(ilat_CG_r0_p,irad0),nlong+1),1)
         end if
      end if

!print*,rank,i,'after locate longitude'

!print*,rank,'Nlat,Nlong',Nlat,Nlong 
!print*,rank,'rad,theta,phi',rad,theta,phi
if (ilat_CG_r0_m .le. 0) print*,rank,'ilat_CG_r0_m',ilat_CG_r0_m
if (ilat_CG_r0_p .ge. Nlat+1) print*,rank,'ilat_CG_r0_p',ilat_CG_r0_p
if (ilong_CG_r0_tm_m .le. 0) print*,rank,'ilong_CG_r0_tm_m',ilong_CG_r0_tm_m
if (ilong_CG_r0_tp_m .le. 0) print*,rank,'ilong_CG_r0_tp_m',ilong_CG_r0_tp_m
if (ilong_CG_r0_tm_p .ge. Nlong+1) print*,rank,'ilong_CG_r0_tm_p',ilong_CG_r0_tm_p
if (ilong_CG_r0_tp_p .ge. Nlong+1) print*,rank,'ilong_CG_r0_tp_p',ilong_CG_r0_tp_p 

      ! WEIGHTS COMPUTATION AND INTERPOLATION
      if (.not. pole) then
         
         call distance_1 (irad0,ilat_CG_r0_m,ilong_CG_r0_tm_m,rad,theta,phi,w1)
         call distance_1 (irad0,ilat_CG_r0_m,ilong_CG_r0_tm_p,rad,theta,phi,w2)
         call distance_1 (irad0,ilat_CG_r0_p,ilong_CG_r0_tp_m,rad,theta,phi,w3)
         call distance_1 (irad0,ilat_CG_r0_p,ilong_CG_r0_tp_p,rad,theta,phi,w4)  
         
!$  field_private(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) = field_private(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) + w1 * tra(FIELDPOS,i)     
!$  field_private(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) = field_private(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) + w2 * tra(FIELDPOS,i)
!$  field_private(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) = field_private(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) + w3 * tra(FIELDPOS,i)
!$  field_private(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) = field_private(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) + w4 * tra(FIELDPOS,i)
         
!$  n_private(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) = n_private(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) + w1      
!$  n_private(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) = n_private(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) + w2 
!$  n_private(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) = n_private(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) + w3
!$  n_private(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) = n_private(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) + w4 

      else

         if (north_pole) then
           
            ! distance to nodes
            call distance_1 (irad0,ilat_CG_r0_p,ilong_CG_r0_tp_m,rad,theta,phi,w1)
            call distance_1 (irad0,ilat_CG_r0_p,ilong_CG_r0_tp_p,rad,theta,phi,w2)
            ! distance to north pole
            call distance_1 (irad0,0,1,rad,theta,phi,w3) 
            
!$  field_private(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) = field_private(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) + w1 * tra(FIELDPOS,i)
!$  field_private(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) = field_private(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) + w2 * tra(FIELDPOS,i)
!$  field_poleN_private(irad0) = field_poleN_private(irad0) + w3 * tra(FIELDPOS,i)

!$  n_private(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) = n_private(ilong_CG_r0_tp_m,ilat_CG_r0_p,irad0) + w1
!$  n_private(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) = n_private(ilong_CG_r0_tp_p,ilat_CG_r0_p,irad0) + w2
!$  n_poleN_private(irad0) = n_poleN_private(irad0) + w3       

         else if (south_pole) then

           ! distance to nodes
            call distance_1 (irad0,ilat_CG_r0_m,ilong_CG_r0_tm_m,rad,theta,phi,w1)
            call distance_1 (irad0,ilat_CG_r0_m,ilong_CG_r0_tm_p,rad,theta,phi,w2)
            ! distance to south pole
            call distance_1 (irad0,Nlat+1,1,rad,theta,phi,w3) 

!$  field_private(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) = field_private(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) + w1 * tra(FIELDPOS,i)     
!$  field_private(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) = field_private(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) + w2 * tra(FIELDPOS,i)
!$  field_poleS_private(irad0) = field_poleS_private(irad0) + w3 * tra(FIELDPOS,i)

!$  n_private(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) = n_private(ilong_CG_r0_tm_m,ilat_CG_r0_m,irad0) + w1      
!$  n_private(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) = n_private(ilong_CG_r0_tm_p,ilat_CG_r0_m,irad0) + w2
!$  n_poleS_private(irad0) = n_poleS_private(irad0) + w3
           
         end if
        
      end if

  end if ! center of the sphere

!print*,rank,i,'after irad0'
     
     !============= spherical shell ir = irad1 ==============
      pole = .false.
      south_pole = .false.
      north_pole = .false.

     ! LOCATE TRACERS IN COARSE GRID ALONG LATITUDE
      if (grid_fusion_lat(irad1) == 1) then ! no fusion
         ilat_CG_r1_m = ilat0
         ilat_CG_r1_p = ilat1
         if (theta .lt. colat(1)) then !pole
            pole = .true.
            north_pole = .true.
         end if
         if (ilat0 == Nlat) then
            pole = .true.
            south_pole = .true.
         end if
      else ! fusion
         ilat_CG_r1_m = (ilat0/grid_fusion_lat(irad1))*grid_fusion_lat(irad1)
         ilat_CG_r1_p = ilat_CG_r1_m + grid_fusion_lat(irad1)  
         if (ilat_CG_r1_m .le. 0) then
            pole = .true.
            north_pole = .true.
         else if (ilat_CG_r1_p .ge. Nlat) then ! careful, situation is not symmetric between south and north poles
            pole = .true.
            south_pole = .true.
         end if
      end if
     
!print*,rank,i,'after location latitude irad1'

     ! LOCATE TRACER IN COARSE GRID ALONG LONGITUDE
      if (.not. north_pole) then
         if (grid_fusion_long(ilat_CG_r1_m,irad0) == 1) then ! no fusion
            ilong_CG_r1_tm_m = ilong0
            ilong_CG_r1_tm_p = ilong1
         else ! fusion
            ilong_CG_r1_tm_m = 1 + ((ilong0-1)/grid_fusion_long(ilat_CG_r1_m,irad1))*grid_fusion_long(ilat_CG_r1_m,irad1)
            ilong_CG_r1_tm_p = max(mod(ilong_CG_r1_tm_m + grid_fusion_long(ilat_CG_r1_m,irad1),nlong+1),1) 
         end if
      end if
      if (.not. south_pole) then
         if (grid_fusion_long(ilat_CG_r1_p,irad1) == 1) then
            ilong_CG_r1_tp_m = ilong0
            ilong_CG_r1_tp_p = ilong1
         else
            ilong_CG_r1_tp_m = 1 + ((ilong0-1)/grid_fusion_long(ilat_CG_r1_p,irad1))*grid_fusion_long(ilat_CG_r1_p,irad1)
            ilong_CG_r1_tp_p = max(mod(ilong_CG_r1_tp_m + grid_fusion_long(ilat_CG_r1_p,irad1),nlong+1),1)
         end if
      end if

!print*,rank,i,'after location longitude irad1'

if (ilat_CG_r1_m .le. 0) print*,rank,'ilat_CG_r1_m',ilat_CG_r1_m
if (ilat_CG_r1_p .ge. Nlat+1) print*,rank,'ilat_CG_r1_p',ilat_CG_r1_p
if (ilong_CG_r1_tm_m .le. 0) print*,rank,'ilong_CG_r1_tm_m',ilong_CG_r1_tm_m
if (ilong_CG_r1_tp_m .le. 0) print*,rank,'ilong_CG_r1_tp_m',ilong_CG_r1_tp_m
if (ilong_CG_r1_tm_p .ge. Nlong+1) print*,rank,'ilong_CG_r1_tm_p',ilong_CG_r1_tm_p
if (ilong_CG_r1_tp_p .ge. Nlong+1) print*,rank,'ilong_CG_r1_tp_p',ilong_CG_r1_tp_p


     ! WEIGHTS COMPUTATION AND INTERPOLATION
      if (.not. pole) then
        
         call distance_1 (irad1,ilat_CG_r1_m,ilong_CG_r1_tm_m,rad,theta,phi,w1)
         call distance_1 (irad1,ilat_CG_r1_m,ilong_CG_r1_tm_p,rad,theta,phi,w2)
         call distance_1 (irad1,ilat_CG_r1_p,ilong_CG_r1_tp_m,rad,theta,phi,w3)
         call distance_1 (irad1,ilat_CG_r1_p,ilong_CG_r1_tp_p,rad,theta,phi,w4)  

!$  field_private(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) = field_private(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) + w1 * tra(FIELDPOS,i)     
!$  field_private(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) = field_private(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) + w2 * tra(FIELDPOS,i)
!$  field_private(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) = field_private(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) + w3 * tra(FIELDPOS,i)
!$  field_private(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) = field_private(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) + w4 * tra(FIELDPOS,i)

!$  n_private(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) = n_private(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) + w1      
!$  n_private(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) = n_private(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) + w2 
!$  n_private(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) = n_private(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) + w3
!$  n_private(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) = n_private(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) + w4 

      else

         if (north_pole) then
           
            ! distance to nodes
            call distance_1 (irad1,ilat_CG_r1_p,ilong_CG_r1_tp_m,rad,theta,phi,w1)
            call distance_1 (irad1,ilat_CG_r1_p,ilong_CG_r1_tp_p,rad,theta,phi,w2)
            ! distance to north pole
            call distance_1 (irad1,0,1,rad,theta,phi,w3) 

!$  field_private(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) = field_private(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) + w1 * tra(FIELDPOS,i)
!$  field_private(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) = field_private(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) + w2 * tra(FIELDPOS,i)
!$  field_poleN_private(irad1) = field_poleN_private(irad1) + w3 * tra(FIELDPOS,i)

!$  n_private(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) = n_private(ilong_CG_r1_tp_m,ilat_CG_r1_p,irad1) + w1
!$  n_private(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) = n_private(ilong_CG_r1_tp_p,ilat_CG_r1_p,irad1) + w2
!$  n_poleN_private(irad1) = n_poleN_private(irad1) + w3       

         else if (south_pole) then

            ! distance to nodes
            call distance_1 (irad1,ilat_CG_r1_m,ilong_CG_r1_tm_m,rad,theta,phi,w1)
            call distance_1 (irad1,ilat_CG_r1_m,ilong_CG_r1_tm_p,rad,theta,phi,w2)
            ! distance to south pole
            call distance_1 (irad1,Nlat+1,1,rad,theta,phi,w3) 
            
!$  field_private(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) = field_private(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) + w1 * tra(FIELDPOS,i)     
!$  field_private(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) = field_private(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) + w2 * tra(FIELDPOS,i)
!$  field_poleS_private(irad1) = field_poleS_private(irad1) + w3 * tra(FIELDPOS,i)

!$  n_private(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) = n_private(ilong_CG_r1_tm_m,ilat_CG_r1_m,irad1) + w1      
!$  n_private(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) = n_private(ilong_CG_r1_tm_p,ilat_CG_r1_m,irad1) + w2
!$  n_poleS_private(irad1) = n_poleS_private(irad1) + w3
           
         end if

      end if

!print*,rank,i,'after irad1'

   end do
!$OMP END DO
!!$OMP END PARALLEL

!print*,rank,'before copying arrays'


!$ do irad = ideb-2,ifin+2
!$    do ilat = 1,Nlat
!$        do ilong = 1,Nlong
!$OMP ATOMIC
!$            field(ilong,ilat,irad) = field(ilong,ilat,irad) + field_private(ilong,ilat,irad)
!$OMP ATOMIC
!$            n(ilong,ilat,irad) = n(ilong,ilat,irad) + n_private(ilong,ilat,irad)
!$        end do
!$     end do
!$OMP ATOMIC
!$     field_poleN(irad) = field_poleN(irad) + field_poleN_private(irad)
!$OMP ATOMIC
!$     field_poleS(irad) = field_poleS(irad) + field_poleS_private(irad)
!$OMP ATOMIC
!$     n_poleN(irad) = n_poleN(irad) + n_poleN_private(irad)
!$OMP ATOMIC
!$     n_poleS(irad) = n_poleS(irad) + n_poleS_private(irad)
!$ end do

!!$ deallocate (field_private,n_private,field_poleN_private,field_poleS_private,n_poleN_private,n_poleS_private)


END IF ! OpenMP

!$OMP END PARALLEL

print*,rank,'end of first step'

! Exchange MPI buffer planes
!call exchange_ghost_planes (field,n,ideb,ifin)

print*,rank,'after exchange planes'

  ! Then, perform the average by dividing by the weights n(:,:,:)
  n_zeros = 0 ! number of nodes which received no information from tracers
  n_CG = 0    ! number of nodes that did

!$OMP PARALLEL PRIVATE(ilat,ilong,nmerge_lat,nmerge_long,Nlong_max,Nlat_max)
!$OMP DO SCHEDULE(DYNAMIC,1)
  do ir = ideb-2,ifin+2
     ! First, treatment of poles...
     if (n_poleN(ir) == 0.) then
        n_zeros = n_zeros + 1
     else
        field_poleN(ir) = field_poleN(ir)/n_poleN(ir)
        n_CG = n_CG + 1
     end if
     if (n_poleS(ir) == 0.) then
        n_zeros = n_zeros + 1
     else
        field_poleS(ir) = field_poleS(ir)/n_poleS(ir)
        n_CG = n_CG + 1
     end if
     ! ...then rest of the grid
     nmerge_lat = grid_fusion_lat(ir)
     if (nmerge_lat == 1) then
        Nlat_max = Nlat
     else
        Nlat_max = Nlat-nmerge_lat
     end if
     do ilat = max(1,nmerge_lat),Nlat_max,max(1,nmerge_lat) 
        nmerge_long = grid_fusion_long(ilat,ir)
        if (nmerge_long == 1) then
           Nlong_max = Nlong
        else
           Nlong_max = Nlong+1-nmerge_long
        end if
        do ilong = 1,Nlong_max,nmerge_long
           if (n(ilong,ilat,ir) == 0.) then
              n_zeros = n_zeros + 1
           else
              field(ilong,ilat,ir) = field(ilong,ilat,ir)/n(ilong,ilat,ir)
              n_CG = n_CG + 1
           end if
        end do
     end do
  end do
!$OMP END DO
!$OMP END PARALLEL

!IF (rank == 0) THEN
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          mtime_fin = omp_get_wtime()
!!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele tra_to_grid_special : ', mtime_fin-mtime_debut,'s'
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            mtime_fin = MPI_WTime ()
!            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele tra_to_coarse_grid : ', mtime_fin - mtime_debut
!!$       END IF
!      END IF

  ! 5) Fill empty nodes of coarse
  ! grid---------------------------------------------------
  call complete_coarse_grid (field_poleN,n_poleN,n_poleS,field_poleS,field,n,ideb,ifin)

  ! 6) Complete fine grid from the coarse grid
  ! ------------------------------------------
  call complete_fine_grid (field_poleN,field_poleS,field,ideb,ifin)!,symmetry_merge_lat)

  if (rank == 0) then
     mtime_fin = MPI_WTime ()
     print*,'TOTAL TIME SPENT IN TRA_TO_C_SPECIAL:',mtime_fin-mtime_debut
  end if

end subroutine tracers_to_grid_special
!----------------------------------------------------------------------------------------



subroutine complete_coarse_grid (field_poleN,n_poleN,n_poleS,field_poleS,field,n,ideb,ifin)
  ! Fills the points of the coarse grid that did not receive information
  ! from tracers
  ! Scans coarse grid: if point with no data, use the surrounding points
  ! of the coarse grid

  use globall
  use tracerstuff
  
  implicit none

  integer,intent(in) :: ideb,ifin
  real,intent(in),dimension(ideb-2:ifin+2) :: field_poleN,field_poleS,n_poleN,n_poleS
  real,intent(inout),dimension(Nlong,Nlat,ideb-2:ifin+2) :: n,field

  integer ir,ilat,ilong,i,j,k,nmerge_lat,isearch,nmerge_long,npts_used,irm,irp,itm,itp,ipm,ipp
  integer Nlat_max,Nlong_max
  real dist,n_work,weight
  real,dimension(Nlong,Nlat,ideb-2:ifin+2) :: field_work
  logical :: data
  

  ! First duplicate the array comp
  field_work(:,:,:) = field(:,:,:)
  
  do ir = ideb,ifin
        
     nmerge_lat = grid_fusion_lat(ir)
     if (nmerge_lat == 1) then
        Nlat_max = Nlat
     else
        Nlat_max = Nlat-nmerge_lat
     end if
!     if (.not. symmetry_merge_lat) then
        
        do ilat = max(1,nmerge_lat),Nlat_max,max(1,nmerge_lat) 
           nmerge_long = grid_fusion_long(ilat,ir)
           if (nmerge_long == 1) then
              Nlong_max = Nlong
           else
              Nlong_max = Nlong+1-nmerge_long
           end if
           do ilong = 1,Nlong_max,max(1,nmerge_long) 
              if (n(ilong,ilat,ir) == 0.) then ! Found a grid point with no data...
                 !...so give it some information using surrounding
                 !coarse grid points:
               
                 ! First, try with the six closest points (of coarse
                 ! grid), which may be sufficient in most cases
                 irm = max(1,ir-1)
                 irp = min(NR,ir+1)
                 itm = ilat-nmerge_lat
                 itp = ilat+nmerge_lat
                 ipm = ilong-nmerge_long 
                 if (ipm .le. 0) ipm = Nlong+1-nmerge_long
                 ipp = ilong+nmerge_long 
                 if (ipp .eq. Nlong+1) ipp = 1
   
                 n_work = 0.
                 npts_used = 0

                 if (n(ipm,ilat,ir) .ne. 0.) then
                    call distance_1 (ir,ilat,ilong,r(ir),colat(ilat),long(ipm),dist) 
                    field_work(ilong,ilat,ir) = field_work(ilong,ilat,ir) + field(ipm,ilat,ir)*dist
                    n_work = n_work + dist
                    npts_used = npts_used + 1
                 end if
                 if (n(ipp,ilat,ir) .ne. 0.) then
                    call distance_1 (ir,ilat,ilong,r(ir),colat(ilat),long(ipp),dist) 
                    field_work(ilong,ilat,ir) = field_work(ilong,ilat,ir) + field(ipp,ilat,ir)*dist
                    n_work = n_work + dist
                    npts_used = npts_used + 1
                 end if
                 if (itm .lt. 1)  then ! use pole
                    if (n_poleN(ir) .ne. 0.) then
                       call distance_1 (ir,ilat,ilong,r(ir),0.,long(ilong),dist) 
                       field_work(ilong,ilat,ir) = field_work(ilong,ilat,ir) + field_poleN(ir)*dist
                       n_work = n_work + dist
                       npts_used = npts_used + 1
                    end if
                 else if (n(ilong,itm,ir) .ne. 0.) then
                    call distance_1 (ir,ilat,ilong,r(ir),colat(itm),long(ilong),dist) 
                    field_work(ilong,ilat,ir) = field_work(ilong,ilat,ir) + field(ilong,itm,ir)*dist
                    n_work = n_work + dist
                    npts_used = npts_used + 1
                 end if
                 if (itp .gt. Nlat) then
                    if (n_poleS(ir) .ne. 0.) then ! use pole
                       call distance_1 (ir,ilat,ilong,r(ir),pi,long(ilong),dist) 
                       field_work(ilong,ilat,ir) = field_work(ilong,ilat,ir) + field_poleS(ir)*dist
                       n_work = n_work + dist
                       npts_used = npts_used + 1
                    end if
                 else if (n(ilong,itp,ir) .ne. 0.) then
                    call distance_1 (ir,ilat,ilong,r(ir),colat(itp),long(ilong),dist) 
                    field_work(ilong,ilat,ir) = field_work(ilong,ilat,ir) + field(ilong,itp,ir)*dist
                    n_work = n_work + dist
                    npts_used = npts_used + 1
                 end if
                 if (n(ilong,ilat,irm) .ne. 0.) then
                    call distance_1 (ir,ilat,ilong,r(irm),colat(ilat),long(ilong),dist) 
                    field_work(ilong,ilat,ir) = field_work(ilong,ilat,ir) + field(ilong,ilat,irm)*dist
                    n_work = n_work + dist
                    npts_used = npts_used + 1
                 end if
                 if (n(ilong,ilat,irp) .ne. 0) then
                    call distance_1 (ir,ilat,ilong,r(irp),colat(ilat),long(ilong),dist) 
                    field_work(ilong,ilat,ir) = field_work(ilong,ilat,ir) + field(ilong,ilat,irp)*dist
                    n_work = n_work + dist
                    npts_used = npts_used + 1
                 end if

                 if (npts_used .ne. 0) then
                    field(ilong,ilat,ir) = field_work(ilong,ilat,ir)/n_work
                 else
                    ! If this fails, then extend progressively the
                    ! number of coarse grid points that are used until
                    ! some information can be used 
                    data = .false.
                    isearch = 0
                    n_work = 0.
                    do while (.not. data)
                       isearch = isearch+1 !searches one level further in each direction
                       do i = max(ir-isearch,1),min(ir+isearch,Nr)
                          do j = ilat-isearch*nmerge_lat,ilat+isearch*nmerge_lat,nmerge_lat
                             if (j .le. 0) then ! use north pole
                                if (n_poleN(i) .ne. 0.) then
                                   call distance_1 (ir,ilat,ilong,r(i),0.,0.,dist)
                                   weight = dist/nmerge_lat/nmerge_long
                                   field_work(ilong,ilat,ir) = field_work(ilong,ilat,ir) + field_poleN(i)*weight 
                                   n_work = n_work + weight
                                   npts_used = npts_used + 1
                                end if
                             else if (j .ge. Nlat+1) then ! use south pole
                                if (n_poleS(i) .ne. 0.) then
                                   call distance_1 (ir,ilat,ilong,r(i),pi,0.,dist)
                                   weight = dist/nmerge_lat/nmerge_long
                                   field_work(ilong,ilat,ir) = field_work(ilong,ilat,ir) + field_poleS(i)*weight 
                                   n_work = n_work + weight
                                   npts_used = npts_used + 1
                                end if
                             else
                                do k = max(1,ilong-isearch*nmerge_long),min(Nlong,ilong+isearch*nmerge_long),nmerge_long
                                   if (n(k,j,i) .ne. 0.) then
                                      call distance_1 (ir,ilat,ilong,r(i),colat(j),long(k),dist)
                                      weight = dist/nmerge_lat/nmerge_long
                                      field_work(ilong,ilat,ir) = field_work(ilong,ilat,ir) + field(k,j,i)*weight 
                                      n_work = n_work + weight
                                      npts_used = npts_used + 1
                                   end if
                                end do
                             end if
                          end do
                       end do
                       if (npts_used .ne. 0) then
                          field(ilong,ilat,ir) = field_work(ilong,ilat,ir)/n_work
                          data = .true.
                       else
                          if (isearch .ge. 10) then
                             stop 'ERROR in complete_coarse_grid: too many empty cells'
                          end if
                       end if
                    end do
                 end if
                 !done
              end if
           end do
        end do
        
!     else ! if symmetry_merge_lat = .true.

!        do ilat =
!        max(1,nmerge_lat),min(Nlat,Nlat+1-nmerge_lat),max(1,nmerge_lat)
!        !!!!! ATTENTION AU CAS SANS FUSION A FAIRE DE MANIERE COHERENTE
!        PARTOUT
!           nmerge_long = grid_fusion_long(ilat,ir)
!           do ilong =
!           1,min(Nlong,Nlong+1-nmerge_long),max(1,nmerge_long) !!!!
!           IDEM
!              if (n(ilong,ilat,ir) == 0.) then ! Found a grid point
!              with no data...
!                 !...so give it some information using surrounding
!                 coarse grid points:

                 ! First try to use the six closest neighbours (of
                 ! coarse grid)
!                 irm = max(1,ir-1)
!                 irp = min(NR,ir+1)
!                 itm = max(1,ilat-nmerge_lat) ! TO BE CHECKED
!                 itp = min(Nlat,ilat+nmerge_lat) ! TO BE CHECKED
!                 ipm = ilong-nmerge_long ! NEED TO MIND WRAPPING UP
!                 if (ipm .le. 0) ipm = Nlong+1-nmerge_long
!                 ipp = ilong+nmerge_long ! IDEM
!                 if (ipp .eq. Nlong+1) ipp = 1
!                 
!                 n_work = 0.
!                 npts_used = 0

!                 if (n(ipm,ilat,ir) .ne. 0) then
!                    call distance_1
!                    (r,colat,long,Nr,Nlat,Nlong,ir,ilat,ilong,r(ir),colat(ilat),long(ipm),dist) 
!                    comp_work(ilong,ilat,ir) = comp_work(ilong,ilat,ir)
!                    + comp(ipm,ilat,ir)*dist
!                    n_work = n_work + dist
!                    npts_used = npts_used + 1
!                 end if
!                 if (n(ipp,ilat,ir) .ne. 0) then
!                    call distance_1
!                    (r,colat,long,Nr,Nlat,Nlong,ir,ilat,ilong,r(ir),colat(ilat),long(ipp),dist) 
!                    comp_work(ilong,ilat,ir) = comp_work(ilong,ilat,ir)
!                    + comp(ipp,ilat,ir)*dist
!                    n_work = n_work + dist
!                    npts_used = npts_used + 1
!                 end if
!                 if (n(ilong,itm,ir) .ne. 0) then
!                    call distance_1
!                    (r,colat,long,Nr,Nlat,Nlong,ir,ilat,ilong,r(ir),colat(itm),long(ilong),dist) 
!                    comp_work(ilong,ilat,ir) = comp_work(ilong,ilat,ir)
!                    + comp(ilong,itm,ir)*dist
!                    n_work = n_work + dist
!                    npts_used = npts_used + 1
!                 end if
!                 if (n(ilong,itp,ir) .ne. 0) then
!                    call distance_1
!                    (r,colat,long,Nr,Nlat,Nlong,ir,ilat,ilong,r(ir),colat(itp),long(ilong),dist) 
!                    comp_work(ilong,ilat,ir) = comp_work(ilong,ilat,ir)
!                    + comp(ilong,itp,ir)*dist
!                    n_work = n_work + dist
!                    npts_used = npts_used + 1
!                 end if
!                 if (n(ilong,ilat,irm) .ne. 0) then
!                    call distance_1
!                    (r,colat,long,Nr,Nlat,Nlong,ir,ilat,ilong,r(irm),colat(ilat),long(ilong),dist) 
!                    comp_work(ilong,ilat,ir) = comp_work(ilong,ilat,ir)
!                    + comp(ilong,ilat,irm)*dist
!                    n_work = n_work + dist
!                    npts_used = npts_used + 1
!                 end if
!                 if (n(ilong,ilat,irp) .ne. 0) then
!                    call distance_1
!                    (r,colat,long,Nr,Nlat,Nlong,ir,ilat,ilong,r(irp),colat(ilat),long(ilong),dist) 
!                    comp_work(ilong,ilat,ir) = comp_work(ilong,ilat,ir)
!                    + comp(ilong,ilat,irp)*dist
!                    n_work = n_work + dist
!                    npts_used = npts_used + 1
!                 end if

!                 if (npts_used .ne. 0) then
!                    comp(ilong,ilat,ir) =
!                    comp_work(ilong,ilat,ir)/n_work
!                 else ! if 6 points not enough, progressively enlarge
!                 the number of points until find some information
!                    data = .false.
!                    isearch = 0
!                    n_work = 0.
!                    do while (.not. data)
!                       isearch = isearch+1 !searches one level further
!                       in each direction
!                       do i = max(ir-isearch,1),min(ir+isearch,Nr)
!                          do j =
!                          max(1,ilat-isearch*nmerge_lat),min(Nlat,ilat+isearch*nmerge_lat),nmerge_lat
!                             do k =
!                             max(1,ilong-isearch*nmerge_long),min(Nlong,ilong+isearch*nmerge_long),nmerge_long
!                                if (n(k,j,i) .ne. 0.) then
!                                   call distance_1
!                                   (r,colat,long,Nr,Nlat,Nlong,ir,ilat,ilong,r(i),colat(j),long(k),dist)
!                                   weight = dist/nmerge_lat/nmerge_long
!                                   comp_work(ilong,ilat,ir) =
!                                   comp_work(ilong,ilat,ir) +
!                                   comp(k,j,i)*weight
!                                   n_work = n_work + weight
!                                end if
!                             end do
!                          end do
!                       end do
!                       if (n_work .ne. 0.) then
!                          comp(ilong,ilat,ir) =
!                          comp_work(ilong,ilat,ir)/n_work
!                          data = .true.
!                       else
!                          stop 'ERROR in complete_coarse_grid: too many
!                          empty points'
!                       end if
!                    end do !while no data
!                    !done
!                 end if
!              end if
!           end do
!        end do
!        
!     end if
     
  end do ! ir loop
     
end subroutine complete_coarse_grid
!----------------------------------------------------------------------------------------


subroutine complete_fine_grid (field_poleN,field_poleS,field,ideb,ifin)!,symmetry_merge_lat)
  ! Interpolate linearly the field on the fine grid from the coarse grid
  ! 1) Start by completing linearly all missing longitudes at each
  ! latitude of the coarse grid
  ! 2) Complete the longitudes at the missing latitudes linearly

  use globall
  use tracerstuff

  implicit none

  integer,intent(in) :: ideb,ifin
  real,intent(in),dimension(ideb-2:ifin+2) :: field_poleN,field_poleS 
  real,intent(inout),dimension(Nlong,Nlat,ideb-2:ifin+2) :: field

  integer ir,ilat,ilong,i,nmerge_lat,nmerge_long
  real dphi_merge_1

  do ir = ideb,ifin ! For each radius...
     ! 1) Starts by completing linearly missing longitudes at each
     ! latitude of the coarse grid :
     nmerge_lat = grid_fusion_lat(ir)
!     if (.not. symmetry_merge_lat) then
        do ilat = max(1,nmerge_lat),Nlat-nmerge_lat,max(1,nmerge_lat)
!...select only latitudes of the coarse grid...
           nmerge_long = grid_fusion_long(ilat,ir)
           dphi_merge_1 = 1./(nmerge_long*dphi)
           do ilong = 1,Nlong+1-2*nmerge_long,nmerge_long !...and complete the missing longitudes
              do i = 1,nmerge_long-1
                 field(ilong+i,ilat,ir) = field(ilong,ilat,ir) + &
                      (field(ilong+nmerge_long,ilat,ir)-field(ilong,ilat,ir))*dphi_merge_1*i*dphi
              end do
           end do
           ilong = Nlong+1-nmerge_long ! Mind rolling up in longitudes !
           do i = 1,nmerge_long-1
              field(ilong+i,ilat,ir) = field(ilong,ilat,ir) + (field(1,ilat,ir)-field(ilong,ilat,ir))*dphi_merge_1*i*dphi
           end do
        end do
!     else
!        do ilat =
!        max(1,nmerge_lat),min(Nlat,Nlat+1-nmerge_lat),max(1,nmerge_lat)
!           nmerge_long = grid_fusion_long(ilat,ir)
!           dphi_merge_1 = 1./(nmerge_long*dphi)
!           do ilong = 1,Nlong+1-2*nmerge_long,nmerge_long !...and
!           complete the missing longitudes
!              do i = 1,nmerge_long-1
!                 comp(ilong+i,ilat,ir) = comp(ilong,ilat,ir) + &
!                      (comp(ilong+nmerge_long,ilat,ir)-comp(ilong,ilat,ir))*dphi_merge_1*i*dphi
!              end do
!           end do
!           ilong = Nlong+1-nmerge_long ! Mind rolling up in longitudes
!           !
!           do i = 1,nmerge_long-1
!              comp(ilong+i,ilat,ir) = comp(ilong,ilat,ir) +
!              (comp(1,ilat,ir)-comp(ilong,ilat,ir))*dphi_merge_1*i*dphi
!           end do
!        end do
        !     end if
        
     ! 2) now fill the other latitudes if any 
     if (nmerge_lat .ne. 0) then 
        ! North pole
        do ilat = 1,nmerge_lat-1
           do ilong = 1,Nlong
              field(ilong,ilat,ir) = field_poleN(ir) + (field(ilong,nmerge_lat,ir)-field_poleN(ir))/colat(nmerge_lat)*colat(ilat)
           end do
        end do
        ! Middle
        do ilat = nmerge_lat,Nlat-2*nmerge_lat,nmerge_lat
           do i = 1,nmerge_lat-1
              do ilong = 1,Nlong
                 field(ilong,ilat+i,ir) = field(ilong,ilat,ir) + &
                      (field(ilong,ilat+nmerge_lat,ir)-field(ilong,ilat,ir))/ &
                      (colat(ilat+nmerge_lat)-colat(ilat))*(colat(ilat+i)-colat(ilat))
              end do
           end do
        end do
        ! South pole
!        if (.not. symmetry_merge_lat) then
           do ilat = Nlat-nmerge_lat+1,Nlat
              do ilong = 1,Nlong
                 field(ilong,ilat,ir) = field(ilong,Nlat-nmerge_lat,ir) + &
                      (field_poleS(ir)-field(ilong,Nlat-nmerge_lat,ir))/ &
                      (pi-colat(Nlat-nmerge_lat))*(colat(ilat)-colat(Nlat-nmerge_lat))
              end do
           end do
!        else
!           do ilat = Nlat-nmerge_lat+2,Nlat
!              do ilong = 1,Nlong
!                 comp(ilong,ilat,ir) = comp(ilong,Nlat-nmerge_lat,ir) +
!                 &
!                      (comp_poleS(ir)-comp(ilong,Nlat-nmerge_lat,ir))/
!                      &
!                      (3.14159-colat(Nlat-nmerge_lat))*(colat(ilat)-colat(Nlat-nmerge_lat))
!              end do
!           end do
!        end if
        end if
  end do
  
end subroutine complete_fine_grid
!----------------------------------------------------------------------------------------


subroutine distance_1 (irad0,ilat0,ilong0,rad,theta,phi,dist_1)
  ! Compute the inverse of the distance between points of coordinates
  ! (rad,theta,phi)
  ! and grid point of coordinates (irad0,ilat0,ilong0)
  ! Very costly routine due to the computation of trigonometric
  ! functions...

  use globall
  use tracerstuff
  
  implicit none

  integer,intent(in):: irad0,ilat0,ilong0
  real,intent(in):: rad,theta,phi
  real,intent(out):: dist_1

  real d21,d22,d23,cosp,sinp,rcost,rsint,cosp0,sinp0,rcost0,rsint0

  cosp = cos(phi)
  rcost = rad*cos(theta)
  sinp = sin(phi)
  rsint = rad*sin(theta)
  
  cosp0 = cosphi(ilong0)
  sinp0 = sinphi(ilong0)
  if (ilat0 .eq. 0) then
     rcost0 = r(irad0)
     rsint0 = 0.
  else if (ilat0 .eq. Nlat+1) then
     rcost0 = -r(irad0)
     rsint0 = 0.
  else
     rcost0 = r(irad0)*costheta(ilat0)
     rsint0 = r(irad0)*sintheta(ilat0)
  end if
! calculates distance (dist) between two points (irad0,ilat0,ilong0) and
! (irad,ilat,ilong) 
! in spherical coordinates

!  d21 = (rad*cos(phi)*sin(theta) -
!  r(irad0)*cos(long(ilong0))*sin(colat(ilat0)))**2
!  d22 = (rad*sin(phi)*sin(theta) -
!  r(irad0)*sin(long(ilong0))*sin(colat(ilat0)))**2
!  d23 = (rad*cos(theta) - r(irad0)*cos(colat(ilat0)))**2

  d21 = (cosp*rsint - cosp0*rsint0)**2
  d22 = (sinp*rsint - sinp0-rsint0)**2
  d23 = (rcost - rcost0)**2

  dist_1 = max(1e-10,sqrt(d21 + d22 + d23)) ! avoid zero value
  
  dist_1 = 1./dist_1

end subroutine distance_1


