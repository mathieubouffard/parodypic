!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                       !!
!!  Version JA-2.3 of Feb 2012                                           !!
!!                                                                       !!
!!  This contains the routines for radial MPI domain decomposition       !!
!!                                                                       !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine factor_3_para(l1,d,u1,nx,ideb,ifin,nxdeb,nxfin)

  use globall
  use mod_parallel
  implicit none

  integer,intent(in)::  nxdeb,nxfin,nx
  integer,intent(in)::  ideb,ifin
  real,intent(inout),dimension(nx,ideb-2:ifin+2)::l1,d,u1

  real,dimension(nx,2)::buf1,buf2

  integer iy,upy,lowy,csend,crecv,tagsend,tagrecv

  integer i,from,to,ier
  integer status(mpi_status_size)

  !===================================================================


  upy=mod(rank+1,size)
  lowy=mod(rank-1+size,size)

  csend=nx*2
  crecv=csend
  tagsend=rank
  tagrecv=mpi_any_tag

  if(rank==0) then
     do i=ideb+1,ifin
        d(nxdeb:nxfin,i)=d(nxdeb:nxfin,i)-l1(nxdeb:nxfin,i)*u1(nxdeb:nxfin,i-1)/d(nxdeb:nxfin,i-1)
     enddo
  endif

  call mpi_barrier(COMM_model,ier)

  do iy=1,size-1
     if(rank==iy) then
        from=lowy
        call MPI_recv(buf2,crecv,mpi_double_precision,from,tagrecv,COMM_model,status,ier)
        i=ideb
        d(nxdeb:nxfin,i)=d(nxdeb:nxfin,i)-l1(nxdeb:nxfin,i)*buf2(nxdeb:nxfin,1)/buf2(nxdeb:nxfin,2)
        do i=ideb+1,ifin
           d(nxdeb:nxfin,i)=d(nxdeb:nxfin,i)-l1(nxdeb:nxfin,i)*u1(nxdeb:nxfin,i-1)/d(nxdeb:nxfin,i-1)
        enddo
     endif
     if(rank==iy-1) then
        buf1(nxdeb:nxfin,1)=u1(nxdeb:nxfin,ifin)
        buf1(nxdeb:nxfin,2)=d(nxdeb:nxfin,ifin)
        to=upy
        call MPI_Send(buf1,csend,mpi_double_precision,to,tagsend,COMM_model,ier)
     endif

     call mpi_barrier(COMM_model,ier)
  enddo                                           ! iy

  ! invert d , scale u1,l1
     do i=ideb,ifin
        d(nxdeb:nxfin,i)=1./d(nxdeb:nxfin,i)
        u1(nxdeb:nxfin,i)=u1(nxdeb:nxfin,i)*d(nxdeb:nxfin,i)
        l1(nxdeb:nxfin,i)=l1(nxdeb:nxfin,i)*d(nxdeb:nxfin,i)
     enddo

  return
end subroutine factor_3_para

! factorisation axe 1 de 2 a nx

subroutine factor_5_para(mat,nx,ideb,ifin)

  use globall
  use mod_parallel
  implicit none

  integer,intent(in):: nx
  integer,intent(in):: ideb,ifin

  real,intent(inout),dimension(nx,ideb-2:ifin+2,5)::mat

  real,dimension(nx,6)::buf1,buf2

  integer iy,upy,lowy,csend,crecv,tagsend,tagrecv

  integer from,to,ier
  integer status(mpi_status_size)

  integer ::i,k

  upy=mod(rank+1,size)
  lowy=mod(rank-1+size,size)

  csend=(nx)*6
  crecv=csend
  tagsend=rank
  tagrecv=mpi_any_tag

  if(rank==0) then
     i=ideb+1
     do  k=2,nx
	mat(k,i,4)=mat(k,i,4)-mat(k,i,2)*mat(k,i-1,5)/mat(k,i-1,3)
	mat(k,i,3)=mat(k,i,3)-mat(k,i,2)*mat(k,i-1,4)/mat(k,i-1,3)
     enddo
     do i=ideb+2,ifin
	do k=2,nx
           mat(k,i,2)=mat(k,i,2)-mat(k,i,1)*mat(k,i-2,4)/mat(k,i-2,3)
           mat(k,i,4)=mat(k,i,4)-mat(k,i,2)*mat(k,i-1,5)/mat(k,i-1,3)
           mat(k,i,3)=mat(k,i,3)-mat(k,i,2)*mat(k,i-1,4)/mat(k,i-1,3)-mat(k,i,1)*mat(k,i-2,5)/mat(k,i-2,3)
	enddo
     enddo
  endif

  call mpi_barrier(COMM_model,ier)

  do iy=1,size-1
     if(rank==iy) then
	from=lowy
 	call MPI_recv(buf2,crecv,mpi_double_precision,from,tagrecv,COMM_model,status,ier)
	i=ideb
	do k=2,nx
           mat(k,i,2)=mat(k,i,2)-mat(k,i,1)*buf2(k,3)/buf2(k,5)
           mat(k,i,4)=mat(k,i,4)-mat(k,i,2)*buf2(k,2)/buf2(k,6)
           mat(k,i,3)=mat(k,i,3)-mat(k,i,2)*buf2(k,4)/buf2(k,6)-mat(k,i,1)*buf2(k,1)/buf2(k,5)
	enddo
	i=ideb+1
	do k=2,nx
           mat(k,i,2)=mat(k,i,2)-mat(k,i,1)*buf2(k,4)/buf2(k,6)
           mat(k,i,4)=mat(k,i,4)-mat(k,i,2)*mat(k,i-1,5)/mat(k,i-1,3)
           mat(k,i,3)=mat(k,i,3)-mat(k,i,2)*mat(k,i-1,4)/mat(k,i-1,3)-mat(k,i,1)*buf2(k,2)/buf2(k,6)
	enddo
        do i=ideb+2,ifin
           do k=2,nx
              mat(k,i,2)=mat(k,i,2)-mat(k,i,1)*mat(k,i-2,4)/mat(k,i-2,3)
              mat(k,i,4)=mat(k,i,4)-mat(k,i,2)*mat(k,i-1,5)/mat(k,i-1,3)
              mat(k,i,3)=mat(k,i,3)-mat(k,i,2)*mat(k,i-1,4)/mat(k,i-1,3)-mat(k,i,1)*mat(k,i-2,5)/mat(k,i-2,3)
           enddo
	enddo
     endif
     if(rank==iy-1) then
	do k=2,nx
           buf1(k,1:2)=mat(k,ifin-1:ifin,5)
           buf1(k,3:4)=mat(k,ifin-1:ifin,4)
           buf1(k,5:6)=mat(k,ifin-1:ifin,3)
	enddo
	to=upy
	call MPI_Send(buf1,csend,mpi_double_precision,to,tagsend,COMM_model,ier)
     endif

     call mpi_barrier(COMM_model,ier)
  enddo						! iy

  ! invert d , scale mat,mat,mat,mat
     do i=ideb,ifin
	do k=2,nx
           mat(k,i,3)=1./mat(k,i,3)
           mat(k,i,1)=mat(k,i,1)*mat(k,i,3)
           mat(k,i,2)=mat(k,i,2)*mat(k,i,3)
           mat(k,i,4)=mat(k,i,4)*mat(k,i,3)
           mat(k,i,5)=mat(k,i,5)*mat(k,i,3)
	enddo
     enddo

  return
end subroutine factor_5_para


subroutine solver_3_init(l1,d,u1,ml1,mu1,nx,ideb,ifin,nxdeb,nxfin)
  
  use globall
  
  implicit none 
  
  integer,intent(in):: nx,ideb,ifin,nxdeb,nxfin
  
  real,intent(in),dimension(nx,ideb-2:ifin+2)::l1,d,u1
  real, dimension(nx,ideb-2:ifin+2)::ml1,mu1
  integer :: j



!if (rank==0) ideb=ideb+1

  !================================
  ! do a local parallel recurrence  new rsh
  !================================
  
     j=ideb
     ml1(nxdeb:nxfin,j)=l1(nxdeb:nxfin,j)
     do j=ideb+1,ifin
        ml1(nxdeb:nxfin,j)=-l1(nxdeb:nxfin,j)*ml1(nxdeb:nxfin,j-1)
     enddo
  
  ! U solver
 
  !==============================================
  ! do a local parallel recurrence
  !===============================================
  
     
     j=ifin
     mu1(nxdeb:nxfin,j)=u1(nxdeb:nxfin,j)
     do j=ifin-1,ideb,-1
        mu1(nxdeb:nxfin,j)=-u1(nxdeb:nxfin,j)*mu1(nxdeb:nxfin,j+1)
     enddo

  return
end subroutine solver_3_init



subroutine  solver_5_init(mat,ml1,ml2,mu1,mu2,nx,ideb,ifin)

  use globall
  implicit none

  integer ,intent(in):: nx,ideb,ifin
  real,intent(in), dimension(nx,ideb-2:ifin+2,5):: mat

  real, dimension(nx,ideb-2:ifin+2)::ml1,ml2
  real, dimension(nx,ideb-2:ifin+2)::mu1,mu2

  integer i,j

     j=ideb
     ml1(2:nx,j)=mat(2:nx,j,1)
     ml2(2:nx,j)=mat(2:nx,j,2)
     j=ideb+1
     ml1(2:nx,j)=-mat(2:nx,j,2)*ml1(2:nx,j-1)
     ml2(2:nx,j)=mat(2:nx,j,1)-mat(2:nx,j,2)*ml2(2:nx,j-1)
     do j=ideb+2,ifin
        ml1(2:nx,j)=-mat(2:nx,j,2)*ml1(2:nx,j-1)-mat(2:nx,j,1)*ml1(2:nx,j-2)
        ml2(2:nx,j)=-mat(2:nx,j,2)*ml2(2:nx,j-1)-mat(2:nx,j,1)*ml2(2:nx,j-2)
     enddo



  ! U solver 
  !==============================================
  ! do a local parallel recurrence  
  !===============================================

     j=ifin
     mu1(2:nx,j)=mat(2:nx,j,4)
     mu2(2:nx,j)=mat(2:nx,j,5)
     j=ifin-1
     mu1(2:nx,j)=-mat(2:nx,j,4)*mu1(2:nx,j+1)+mat(2:nx,j,5)
     mu2(2:nx,j)=-mat(2:nx,j,4)*mu2(2:nx,j+1)
     do j=ifin-2,ideb,-1
        mu1(2:nx,j)=-mat(2:nx,j,4)*mu1(2:nx,j+1)-mat(2:nx,j,5)*mu1(2:nx,j+2)
        mu2(2:nx,j)=-mat(2:nx,j,4)*mu2(2:nx,j+1)-mat(2:nx,j,5)*mu2(2:nx,j+2)
     enddo

  return
end subroutine  solver_5_init






subroutine solver_3_para(l1,d,u1,rhs,x,ml1,mu1,nxdeb,nx,ideb,ifin)

  use globall
  use mod_parallel
  implicit none 


  integer,intent(in):: nx

   real,intent(in),dimension(nx,ideb-2:ifin+2)::l1,d,u1
   complex,dimension(0:nx+1,ideb-2:ifin+2)::x
   complex ,dimension(nx+1,ideb-2:ifin+2)::rhs
   real, dimension(nx,ideb-2:ifin+2)::ml1,mu1
  complex, dimension(nx):: buf1,buf2

  integer j,nxdeb
  integer upy,lowy,tagsend,tagrecv,csend,crecv
  integer to,from,iy,ier
  integer  status(mpi_status_size)
  integer type_mpi

  integer ::ideb,ifin

  !==================================================================================
  type_mpi=mpi_double_complex

!if (rank==0) ideb=ideb+1

  ! we  do the factorization  from NG+1 to NR-1
  ! find processor for NG and local  position

!   axis2_standard_length=(NR)/size
!   ng_proc=(NG-1)/axis2_standard_length
!   pos_ng=NG-ng_proc*axis2_standard_length

  upy=mod(rank+1,size)
  lowy=mod(rank-1+size,size)
  tagsend=rank
  tagrecv=mpi_any_tag
  csend=nx
  crecv=csend

!   if(rank==0) ideb=NG+1
!   if(rank==size-1) ifin=ifin-2

  !================================
  ! do a local parallel recurrence  new rsh
  !================================

  j=ideb
  x(nxdeb:nx,j)=rhs(nxdeb:nx,j)*d(nxdeb:nx,j)
  do j=ideb+1,ifin
     x(nxdeb:nx,j)=rhs(nxdeb:nx,j)*d(nxdeb:nx,j)-l1(nxdeb:nx,j)*x(nxdeb:nx,j-1)
  enddo


  !============================================================
  !  compute true  x(ifin(1:p-1)-1:ifin(1:p-1)) sequential
  !============================================================
  do iy=1,size-1
     if(rank==iy) then
        from=lowy
        call MPI_recv(buf2,crecv,type_mpi,from,tagrecv,COMM_model,status,ier)
        j=ifin
        x(nxdeb:nx,j)=x(nxdeb:nx,j)-ml1(nxdeb:nx,j)*buf2(nxdeb:nx)
     else if(rank==iy-1) then
        buf1=x(1:nx,ifin)
        to=upy
        call MPI_Send(buf1,csend,type_mpi,to,tagsend,COMM_model,ier)
     endif

     !	call mpi_barrier(COMM_model,ier)

  enddo

  !================================
  ! final correction
  !================================
     if (rank > 0) then
     do j=ideb,ifin-1
        x(nxdeb:nx,j)=x(nxdeb:nx,j)-ml1(nxdeb:nx,j)*buf2(nxdeb:nx)
     enddo
     endif

  
  !===============================================
  ! do a local parallel recurrence
  !===============================================
     j=ifin
     do j=ifin-1,ideb,-1
        x(nxdeb:nx,j)=x(nxdeb:nx,j)-u1(nxdeb:nx,j)*x(nxdeb:nx,j+1)
     enddo


  !============================================================
  !  compute true  x(ifin(1:p-1)) sequential
  !============================================================

  do iy=size-2,0,-1
     if(rank==iy) then
        from=upy
        call MPI_recv(buf2,crecv,type_mpi,from,tagrecv,COMM_model,status,ier)
        x(nxdeb:nx,ideb)=x(nxdeb:nx,ideb)-mu1(nxdeb:nx,ideb)*buf2(nxdeb:nx)
     else if(rank==iy+1) then
        buf1=x(1:nx,ideb)
        to=lowy
        call MPI_Send(buf1,csend,type_mpi,to,tagsend,COMM_model,ier)
     endif

     !	call mpi_barrier(COMM_model,ier)
  enddo

  !================================
  ! final parallel correction
  !================================
  if(rank<size-1) then
     do j=ideb+1,ifin
        x(nxdeb:nx,j)=x(nxdeb:nx,j)-mu1(nxdeb:nx,j)*buf2(nxdeb:nx)
     enddo
  endif


  return
end subroutine solver_3_para


subroutine  solver_5_para(mat,ml1,ml2,mu1,mu2,x,nx,ideb,ifin)

  use globall
  use mod_parallel
  implicit none

  integer ,intent(in):: nx,ideb,ifin
  real,intent(in), dimension(nx,ideb-2:ifin+2,5):: mat

  complex, dimension(0:LMmax+1,ideb-2:ifin+2)::x
  real, dimension(nx,ideb-2:ifin+2)::ml1,ml2
  real, dimension(nx,ideb-2:ifin+2)::mu1,mu2
  complex, dimension(nx,2):: buf1,buf2

  integer i,j
  integer upy,lowy,tagsend,tagrecv,csend,crecv
  integer from,to,iy,ier
  integer  status(mpi_status_size)
  integer :: type_message
  integer :: request

  type_message=mpi_double_complex

  upy=mod(rank+1,size)
  lowy=mod(rank-1+size,size)
  tagsend=rank
  tagrecv=mpi_any_tag
  csend=2*nx
  crecv=csend


     j=ideb
     x(2:nx,j)=x(2:nx,j)*mat(2:nx,j,3)
     j=ideb+1
     x(2:nx,j)=x(2:nx,j)*mat(2:nx,j,3)-mat(2:nx,j,2)*x(2:nx,j-1)
     do j=ideb+2,ifin
        x(2:nx,j)=x(2:nx,j)*mat(2:nx,j,3)-mat(2:nx,j,2)*x(2:nx,j-1)-mat(2:nx,j,1)*x(2:nx,j-2)
     enddo


  !============================================================
  !  compute true  x(ifin(1:p-1)-1:ifin(1:p-1)) sequential loop
  !============================================================
  do iy=1,size-1

     if(rank==iy) then
        from=lowy
        call MPI_irecv(buf2,crecv,type_message,from,tagrecv,COMM_model,request,ier)
     else 
        request=MPI_REQUEST_NULL
     endif

     if(rank==iy-1) then
	buf1=x(1:nx,ifin-1:ifin)
	to=upy
        call MPI_Send(buf1,csend,type_message,to,tagsend,COMM_model,ier)
     endif

     call MPI_Wait(request,status,ier)
     if(rank==iy) then
	j=ifin
	x(2:nx,j-1)=x(2:nx,j-1)-(ml1(2:nx,j-1)*buf2(2:nx,1)+ml2(2:nx,j-1)*buf2(2:nx,2))
	x(2:nx,j)=x(2:nx,j)-(ml1(2:nx,j)*buf2(2:nx,1)+ml2(2:nx,j)*buf2(2:nx,2))
     endif

     !	call mpi_barrier(COMM_model,ier)

  enddo

  !================================
  ! final correction 
  !================================
  if(rank>0) then
     do j=ideb,ifin-2
	x(2:nx,j)=x(2:nx,j)-ml1(2:nx,j)*buf2(2:nx,1)-ml2(2:nx,j)*buf2(2:nx,2)
     enddo
  endif


  ! U solver 


  !==============================================
  ! do a local parallel recurrence  
  !===============================================

     j=ifin
     x(2:nx,j)=x(2:nx,j)
     j=ifin-1
     x(2:nx,j)=x(2:nx,j)-mat(2:nx,j,4)*x(2:nx,j+1)
     do j=ifin-2,ideb,-1
        x(2:nx,j)=x(2:nx,j)-mat(2:nx,j,4)*x(2:nx,j+1)-mat(2:nx,j,5)*x(2:nx,j+2)
     enddo

  !============================================================
  !  compute true  x(ifin(1:p-1)-1:ifin(1:p-1)) sequential
  !============================================================

  do iy=size-2,0,-1
     if(rank==iy) then
        from=upy
        call MPI_irecv(buf2,crecv,type_message,from,tagrecv,COMM_model,request,ier)
     else
	request=MPI_REQUEST_NULL
     endif

     if(rank==iy+1) then
	buf1=x(1:nx,ideb:ideb+1)
	to=lowy
        call MPI_Send(buf1,csend,type_message,to,tagsend,COMM_model,ier)
     endif

     call MPI_Wait(request,status,ier)
     if(rank==iy) then
	x(2:nx,ideb)=x(2:nx,ideb)-(mu1(2:nx,ideb)*buf2(2:nx,1)+mu2(2:nx,ideb)*buf2(2:nx,2))
	x(2:nx,ideb+1)=x(2:nx,ideb+1)-(mu1(2:nx,ideb+1)*buf2(2:nx,1)+mu2(2:nx,ideb+1)*buf2(2:nx,2))
     endif
     !	call mpi_barrier(COMM_model,ier)

  enddo

  !================================
  ! final parallel correction 
  !================================
  if(rank<size-1) then
     do j=ideb+2,ifin
        x(2:nx,j)=x(2:nx,j)-(mu1(2:nx,j)*buf2(2:nx,1)+mu2(2:nx,j)*buf2(2:nx,2))
     enddo
  endif

  return
end subroutine  solver_5_para

subroutine exch1_2d(u,nx,ideb,ifin)

  use globall
  use mod_parallel
  implicit none
	

  integer :: nx,ideb,ifin

  complex,dimension(0:nx+1,ideb-2:ifin+2) ::u

  complex, dimension(0:nx+1)::buf1,buf2


  integer csend,tagsend,tagrecv
  integer to,from ,up,low
  integer  ier, status(mpi_status_size)
  integer type_send
  integer ix1
  
!=================================================================

  tagsend    = rank
  tagrecv   = MPI_ANY_TAG

  low=rank-1
  up=rank+1
  if(low.lt.0) low=size-1
  if(up.eq.size) up=0

  csend=nx+2
  
  type_send=mpi_double_complex

  ix1=mod(rank,2)



! send up receive from low
  from=low
  to=up

  if(ix1==0.and.rank>0) then
     call MPI_recv(buf2,csend,type_send,from,tagrecv,COMM_model,status,ier)
     u(0:nx+1,ideb-1)=buf2(0:nx+1)
  endif
  if(ix1>0.and.rank<size-1) then
     buf1(0:nx+1)=u(0:nx+1,ifin)
     call MPI_Send(buf1,csend,type_send,to,tagsend,COMM_model,ier)
  endif

  if(ix1>0.and.rank>0) then
     call MPI_recv(buf2,csend,type_send,from,tagrecv,COMM_model,status,ier)
     u(0:nx+1,ideb-1)=buf2(0:nx+1)
  endif
  if(ix1==0.and.rank<size-1) then
     buf1(0:nx+1)=u(0:nx+1,ifin)
     call MPI_Send(buf1,csend,type_send,to,tagsend,COMM_model,ier)
  endif


! send low receive from up

  from=up
  to=low

  if(ix1==0.and.rank<size-1) then
     call MPI_recv(buf2,csend,type_send,from,tagrecv,COMM_model,status,ier)
     u(0:nx+1,ifin+1)=buf2(0:nx+1)
  endif
  if(ix1>0.and.rank>0) then
     buf1(0:nx+1)=u(0:nx+1,ideb)
     call MPI_Send(buf1,csend,type_send,to,tagsend,COMM_model,ier)
  endif

  if(ix1>0.and.rank<size-1) then
     call MPI_recv(buf2,csend,type_send,from,tagrecv,COMM_model,status,ier)
     u(0:nx+1,ifin+1)=buf2(0:nx+1)
  endif
  if(ix1==0.and.rank>0) then
     buf1(0:nx+1)=u(0:nx+1,ideb)
     call MPI_Send(buf1,csend,type_send,to,tagsend,COMM_model,ier)
  endif


  return
end subroutine exch1_2d

subroutine exch2_2d(u,nx,ideb,ifin)

  use globall
  use mod_parallel
  implicit none


  integer :: nx,ideb,ifin
  complex,dimension(0:nx+1,ideb-2:ifin+2) ::u
  complex, dimension(0:nx+1,2)::buf1,buf2

  integer csend,tagsend,tagrecv
  integer to,from ,up,low
  integer  ier, status(mpi_status_size)
  integer type_message
  integer ix1
  !=================================================================

  type_message=mpi_double_complex


  tagsend    = rank
  tagrecv   = MPI_ANY_TAG

  low=rank-1
  up=rank+1
  if(low.lt.0) low=size-1
  if(up.eq.size) up=0

  csend=2*(nx+2)

  ix1=mod(rank,2)

  ! send up receive from low
  from=low
  to=up

  if(ix1==0.and.rank>0) then
     call MPI_recv(buf2,csend,type_message,from,tagrecv,COMM_model,status,ier)
     u(2:nx,ideb-2:ideb-1)=buf2(2:nx,:)
  endif
  if(ix1>0.and.rank<size-1) then
     buf1(2:nx,:)=u(2:nx,ifin-1:ifin)
     call MPI_Send(buf1,csend,type_message,to,tagsend,COMM_model,ier)
  endif

  if(ix1>0.and.rank>0) then
     call MPI_recv(buf2,csend,type_message,from,tagrecv,COMM_model,status,ier)
     u(2:nx,ideb-2:ideb-1)=buf2(2:nx,:)
  endif
  if(ix1==0.and.rank<size-1) then
     buf1(2:nx,:)=u(2:nx,ifin-1:ifin)
     call MPI_Send(buf1,csend,type_message,to,tagsend,COMM_model,ier)
  endif


  ! send low receive from up

  from=up
  to=low

  if(ix1==0.and.rank<size-1) then
     call MPI_recv(buf2,csend,type_message,from,tagrecv,COMM_model,status,ier)
     u(2:nx,ifin+1:ifin+2)=buf2(2:nx,:)
  endif
  if(ix1>0.and.rank>0) then
     buf1(2:nx,:)=u(2:nx,ideb:ideb+1)
     call MPI_Send(buf1,csend,type_message,to,tagsend,COMM_model,ier)
  endif

  if(ix1>0.and.rank<size-1) then
     call MPI_recv(buf2,csend,type_message,from,tagrecv,COMM_model,status,ier)
     u(2:nx,ifin+1:ifin+2)=buf2(2:nx,:)
  endif
  if(ix1==0.and.rank>0) then
     buf1(2:nx,:)=u(2:nx,ideb:ideb+1)
     call MPI_Send(buf1,csend,type_message,to,tagsend,COMM_model,ier)
  endif

  return
end subroutine exch2_2d





