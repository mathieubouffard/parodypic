!===================
  module data_hyper
!===================

  implicit none
  public
  SAVE
  logical :: hyperdiff
  integer :: lcutU,lcutB,lcutT
  integer :: expU,expB,expT
  real :: aU,aB,aT
!
  real, dimension(:), allocatable :: hyperdiffU
  real, dimension(:), allocatable :: hyperdiffB
  real, dimension(:), allocatable :: hyperdiffT
! 

  real, dimension(:), allocatable :: CLe_hyp, CLi_hyp
  real, dimension(:,:), allocatable :: A1a_hyp, A1c_hyp
  real, dimension(:,:), allocatable :: A3a_hyp, A3bp_hyp, A3c_hyp
  real, dimension(:,:), allocatable :: A4a_hyp, A4c_hyp
  real, dimension(:,:), allocatable :: A5a_hyp, A5c_hyp

!=======================
  end module data_hyper
!=======================
