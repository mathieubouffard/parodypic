!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                       !!
!!  Version JA-2.3 of Feb  2012                                          !!
!!                                                                       !!
!!  This is the big common block                                         !!
!!                                                                       !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Module globall
IMPLICIT NONE
SAVE

! Radial Grid
  
Integer ::     NR 
Integer ::     NG 
!NR=NF+NG-1
Integer ::     NF

!  Rayons et inverses geometriques
Real,allocatable :: r(:)
Real,allocatable :: r_1(:), r_2(:), r_3(:),r_4(:)
real :: Aspect_ratio 

! Ecarts pour pas de temps adaptatif
Real,allocatable :: dr2(:),dh2(:)

!  Gradient, Laplacien vertical, et Laplacien.
Real,allocatable  :: Gra(:), Grb(:), Grc(:)
Real,allocatable   :: Lva(:), Lvb(:), Lvc(:)
Real,allocatable   ::  La(:),  Lb(:), Lc(:)
Real,allocatable   :: dra(:), drb(:), drc(:)

Real                    :: Gre

Logical :: dd_convection

Logical :: non_lin
Integer :: HeatingMode
Integer :: ChemicalMode
Logical :: CondIC,InsulIC,NOSLIPOC,NOSLIPIC,new_dt
Logical :: Navier_Stokes,Heat_equation,Induction,Couette,MantleControl,BottomControl

Real :: CLi, CLe

! Lateral Grid

Integer ::     Mc
Integer ::     Lmin, Lmax
Integer ::     Mmin, Mmax
Integer ::     delta
Integer ::     Alpha_min
Integer ::     Alpha_max
Integer ::     LMmax
Integer ::     Ms

Integer ::     Nlat, Nlong
Integer ::     Nphi


! Ylm = H.S. normalisees
Real,allocatable ::    Ylm(:,:)
! dtYlm = (d Ylm / d theta)
Real,allocatable ::    dtYlm(:,:)
! dpYlm = (1./Im) * (d Ylm / d phi) / sin(theta)
Real,allocatable ::    dpYlm(:,:)

! poids * HS
Real,allocatable ::   xxy(:,:),xxt(:,:),xxp(:,:)

! tableau FFT

real,allocatable  :: trigsf(:)
integer, DIMENSION(13) :: ifaxf


! A/R optimises
Integer,allocatable :: NOIKIstart(:), NOIKIstop(:)
Logical,allocatable :: lmodd(:)
Integer,allocatable :: NOIKIstart2(:), NOIKIstop2(:)
Logical,allocatable :: lmodd2(:)
  

! ll = (l*(l+1))
Real,allocatable  ::    ll(:)
! ll_1 = 1/(l*(l+1))
Real,allocatable  ::    ll_1(:)

! Theta arrays
Real,allocatable  ::    racin(:)
real,allocatable  ::    colat(:)
real,allocatable  ::    sintheta(:)

! poids de Gauss
Real,allocatable  ::    Poids(:)

! pointeurs
Integer, allocatable :: li(:),mi(:)
Integer :: lm10,lm11
  
! Fields

Complex, allocatable ::  yVpr(:,:)
Complex, allocatable ::  yVtr(:,:)
Complex, allocatable ::  yTtr(:,:)
Complex, allocatable ::  yBtr(:,:)
Complex, allocatable ::  yBpr(:,:)
Complex, allocatable ::  yCr(:,:)

Complex, allocatable :: Adams1(:,:)
Complex, allocatable :: Adams2(:,:)
Complex, allocatable :: Adams3(:,:)
Complex, allocatable :: Adams4(:,:)
Complex, allocatable :: Adams5(:,:)
Complex, allocatable :: Adams6(:,:)

Real, TARGET, allocatable ::  Data_spec_spat(:,:,:)
Real, TARGET, allocatable ::  Data_spat_spec(:,:,:)

Real ::    C_old

Integer, allocatable :: Nb_rad(:)

Complex,allocatable :: yBpr_o(:),yBtr_o(:)
Complex,allocatable :: yVpr_o(:),yVtr_o(:)

! Mantle Control

Complex,allocatable :: yTtrMC(:,:)
Complex,allocatable :: yTtrMC_r(:,:)
Complex,allocatable :: ydTdrMC(:,:)

! Bottom Control

Complex,allocatable :: yTtrIC(:,:)
Complex,allocatable :: yTtrIC_r(:,:)
Complex,allocatable :: ydTdrIC(:,:)

!  pi number
Real    :: pi = 4.D0*datan(1.D0)
Real    :: deux_pi
!  Model
!  Real    :: Ek,Ra,Pr,Pm,Ro
Real    :: DeltaU, Coriolis, Buoyancy, Lorentz, ForcingU
Real    :: DeltaT, ForcingT
Real    :: DeltaB, ForcingB
Real    :: Pch


!  Complex constants
Complex :: Im
! Couples
Real    ::  Cpl_V, Cpl_M, omega, omega_old, couple, couple_old
Real    ::  gammatau

! Energies, spectra, angular momentum
Real  ::  energVP,energBP,energBiP
Real  ::  energVT,energBT,energBiT
Real  :: energVpa, energVta, energBpa, energBta, energBcmb,energBdipcmb
Real  :: energBcmb12,AD,NAD,SYM,ASYM,ZON,NZON
Real, allocatable :: spVm(:),spBm(:),spVl(:),spBl(:)
Real, allocatable :: spVm_ave(:),spBm_ave(:),spVl_ave(:),spBl_ave(:)
Real, allocatable :: spTl(:),spTm(:)
Real :: AngMom

! Time
Real    :: dt,dtfixed,time,dt_opt
Integer :: t,Iter_fin
Real    :: cf2,af2
Integer :: modulo,modulo2,modulo3,modulo4,modulo5
! 
Character  :: Entree *20
Real    ::  Ratio1, Ratio2
logical ::  grille,Init,Adaptative
Real    ::  Aliasing
integer ::  init_t, ControlType, ControlTypeBottom
real    ::  amp_b,amp_t,qstar,qstar_IC

! Parallelism

Integer size
Integer rank
integer :: axis2_standard_length,rem
integer :: ideb_Vt,ifin_Vt
integer :: ideb_Vp,ifin_Vp
integer :: ideb_Tt,ifin_Tt
integer :: ideb_C,ifin_C
integer :: ideb_Bt,ifin_Bt
integer :: ideb_Bp,ifin_Bp
integer :: ideb_all,ifin_all,ideb_allB,ifin_allB

! blocking
integer, allocatable :: mstart(:),mstop(:)
integer, allocatable :: latstart(:),latstop(:)
integer, allocatable :: start15(:),stop15(:)
integer, allocatable :: start7(:),stop7(:)
integer iblock,nblock,nblock7,nblock15,nmblock,maxmblock
Integer :: BLOCK1,BLOCK7,BLOCK15,BLOCKM,latrem
Integer :: IDEAL_BLOCK

! matrices

Real ,allocatable  :: A1a_2d(:,:), diag1(:,:), A1c_2d(:,:)
Real ,allocatable  :: A3a_2d(:,:), diag3(:,:), A3c_2d(:,:)
Real ,allocatable  :: A4a_2d(:,:), diag4(:,:), A4c_2d(:,:)
Real ,allocatable  :: A5a_2d(:,:), diag5(:,:), A5c_2d(:,:)
Real ,allocatable  :: A6a_2d(:,:), diag6(:,:), A6c_2d(:,:)
Real ,allocatable  :: A2U_big(:,:,:)
Real,allocatable :: B2a(:,:),  B2b(:,:), B2c(:,:), B2d(:,:),  B2e(:,:)
Real,allocatable :: A1a(:),A1b(:,:),A1c(:)
Real,allocatable :: A3a(:),A3b(:,:),A3c(:)
Real,allocatable :: A4a(:),A4b(:,:),A4c(:)
Real,allocatable :: A5a(:),A5b(:,:),A5c(:)
Real,allocatable :: A6a(:),A6b(:,:),A6c(:)
Real,allocatable :: A2LU(:,:,:), A2L(:,:,:)
Real,allocatable :: B2(:,:,:)
Real,allocatable :: A3bp(:)
Real,allocatable :: A6bp(:)

! Variables pour solveurs lineaires paralleles
Real,allocatable :: m1_l(:,:),   m1_u(:,:)
Real,allocatable :: m51_l(:,:), m51_u(:,:)
Real,allocatable :: m52_l(:,:), m52_u(:,:)
Real,allocatable :: m3_l(:,:),   m3_u(:,:)
Real,allocatable :: m4_l(:,:),   m4_u(:,:)
Real,allocatable :: m5_l(:,:),   m5_u(:,:)
Real,allocatable :: m6_l(:,:),   m6_u(:,:)

!  Control

integer :: stopcode
integer :: printgraph
logical :: l_printgraph
Character  :: RUNid *20

!  Power budget

Real :: powerbuoy,powervisc,powerohm,surfvisc

!  Average G file

Complex, allocatable ::  yVpr_ave(:,:)
Complex, allocatable ::  yVtr_ave(:,:)
Complex, allocatable ::  yTtr_ave(:,:)
Complex, allocatable ::  yBtr_ave(:,:)
Complex, allocatable ::  yBpr_ave(:,:)

!PENSER A RAJOUTER un yCr_ave ????

! Modulo number

Real mn

! Resubmission

logical Resub
Real Maxtime
character*200  ResubCommand

! Start time

Real startime

! Linear convection onset

Complex, allocatable :: CroissP(:),CroissT(:)
Real, allocatable :: CroissPA(:),CroissTA(:)

! Magnetic torque in the fluid shell

Real, allocatable :: cpl_M_shell(:), cpl_M_shell_ave(:)

Real Integral

! IO version
integer :: io_version

! Static temperature
real,allocatable:: Tstat(:),dTstat_dr(:)


End module globall

