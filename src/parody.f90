!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                       !!
!!  Version JA-2.3 of Feb 2012                                           !!
!!                                                                       !!
!!  This is the main code                                                !!
!!                                                                       !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program PARODY

  use globall
!alex
  use initialisation_parody
  use mod_parallel
  use matrices_parody
  use data_parody_timing
  use simulator_parody
  use tracerstuff
  use tracers

  Implicit none

  real stop_code
  external stop_code
  
  real print_graph
  external print_graph
!

!================================================================

call init_parallel_parody()
call init_parody()
call initial_condition_parody()

if (use_tracers) then !initialisation of the tracers
   call init_tracers ()
end if

call define_fw_matrices_parody()
  
  call cpu_time(time1)
 
! Init control flags

  stopcode=0
  printgraph=0
  l_printgraph=.false.

  call output_std_init

! Memorize and display starting time

  startime=time

  if(rank==0) then
     print*,'Starting run at time ',time
     print*,'With current time step ',dt
     open (22, file='log.'//trim(runid), STATUS = 'old', position='append')
     write(22,*)'Starting run at time ',time
     write(22,*)'With current time step ',dt
     close(22)
  endif ! rank==0


  !------------------------------------------------------------------------*
  !   MAIN LOOP
  !------------------------------------------------------------------------*

  if(rank==0) write(6,*)
  if(rank==0) write(6,*)' MAIN LOOP:'
  if(rank==0) write(6,*)' ----------'
  if(rank==0) then
     open (22, file='log.'//trim(runid), STATUS = 'old', position='append')
     if(rank==0) write(22,*)
     if(rank==0) write(22,*)' MAIN LOOP  :'
     if(rank==0) write(22,*)' -----------'
     close(22)
  endif

  call fw_parody(Iter_fin) 

  if(rank==0) print *,'TIMING MAIN LOOP:',time2-time1
  if(rank==0) then
     open (22, file='log.'//trim(runid), STATUS = 'old', position='append')
     write(22,*) 'TIMING MAIN LOOP:',time2-time1
     close(22)
  endif

  call output_std_end

  if(rank==0) print *
  if(rank==0) print *,'Thank you for using PARODY!'

  if(rank==0) then
     open (22, file='log.'//trim(runid), STATUS = 'old', position='append')
     write(22,*) 
     write(22,*) 'Thank you for using PARODY!'
     close(22)
  endif

  call exit_parallel_parody()

!-----------------------------------------------------------------------
!if (tracers) call free_tracers ()
!-----------------------------------------------------------------------

  stop

END program PARODY


subroutine stop_code
use globall
Implicit None

stopcode=1

return
end subroutine stop_code


subroutine print_graph
use globall
Implicit None

printgraph=1

return
end subroutine print_graph


subroutine check_new_dt
use globall

Real dt_fac,dt_2

dt_fac=2.
dt_2 =min(0.5*(1./dt_fac+1.0)*dt_opt,0.1/Coriolis)
dt_opt=min(dt_opt,0.1/Coriolis)

if (dt.gt.dt_opt) then
   if (rank==0) then
      open (3,file='timestep.'//trim(runid),form='formatted',position='append')
      write(3,*) time,dt
      write(3,*) time,dt_2
      close(3)
      write (6,'("iter ",i10, " reducing dt from ",d16.8," to ",d16.8)')t,dt,dt_2
      open(22,file='log.'//trim(runid),status='old',position='append')
      write(22,'("iter ",i10, " reducing dt from ",d16.8," to ",d16.8)')t,dt,dt_2
      close(22)
   endif
   dt=dt_2
   new_dt=.true.
endif

if (dt_fac*dt.lt.dt_opt) then
   if (rank==0) then
      open (3,file='timestep.'//trim(runid),form='formatted',position='append')
      write(3,*) time,dt
      write(3,*) time,dt_2
      close(3)
      write (6,'("iter ",i10, " increasing dt from ",d16.8," to ",d16.8)')t,dt,dt_2
      open(22,file='log.'//trim(runid),status='old',position='append')
      write(22,'("iter ",i10, " increasing dt from ",d16.8," to ",d16.8)')t,dt,dt_2
      close(22)
   endif
   dt=dt_2
   new_dt=.true.
endif

end subroutine check_new_dt
