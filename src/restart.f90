!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                       !!
!!  Version JA-2.3 of Feb 2012                                           !!
!!                                                                       !!
!!  This contains the restart file read/write subroutines                !!
!!                                                                       !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine lecture_v2
  use globall
  use mod_parallel


  Implicit none
  integer irank

  Complex, allocatable :: tmpC1(:),tmpC2(:),tmpC3(:),tmpC4(:),tmpC5(:)
  Complex, allocatable :: tmpC6(:),tmpC7(:),tmpC8(:),tmpC9(:),tmpC10(:)
  real, allocatable :: r_old(:)
  real :: rayon
  Integer :: ir,l,m,lmmax_ancien,i,lm,indx,NG_old,NR_old,j,ier

  real x1(4),x1i,tmp(4),tmp2,tmp3
  complex y1(4), val, dy



  read (22) time
  read (22) dt
  read (22) lmmax_ancien

  read (22) NG_old,NR_old

  allocate (r_old(NR_old))
  allocate (tmpC1(1:NR_old),tmpC2(1:NR_old))
  allocate (tmpC3(1:NR_old),tmpC4(1:NR_old))
  allocate (tmpC5(1:NR_old),tmpC6(1:NR_old))
  allocate (tmpC7(1:NR_old),tmpC8(1:NR_old))
  allocate (tmpC9(1:NR_old),tmpC10(1:NR_old))

  do ir=1,NR_old
     read (22) r_old(ir)
  enddo



  do i=1,lmmax_ancien
     

     read (22) l,m
     if ((l.eq.0).and.(m.eq.0)) then
        lm = 1
     else
        lm = Indx(l,m)
     endif


     if (condIC) then
        do ir = 1, NG_old-1
           read (22) tmpC1(ir)
           read (22) tmpC2(ir)
           read (22) tmpC3(ir)
           read (22) tmpC4(ir)
        enddo
     endif

     do ir = NG_old, NR_old
        read (22) tmpC1(ir)
        read (22) tmpC2(ir)
        read (22) tmpC3(ir)
        read (22) tmpC4(ir)
        read (22) tmpC5(ir)
        read (22) tmpC6(ir)
        read (22) tmpC7(ir)
        read (22) tmpC8(ir)
        read (22) tmpC9(ir)
        read (22) tmpC10(ir)
     enddo

     if (rank==0) then



        do ir = 1, NG-1
           rayon = r(ir)
           j=3
           do while (r_old(j).lt.rayon) 
              j=j+1
           enddo
           
           x1(1) = r_old(j-2)
           x1(2) = r_old(j-1)
           x1(3) = r_old(j)
           x1(4) = r_old(j+1)

           x1i=rayon

            y1(1) = tmpC1(j-2)
            y1(2) = tmpC1(j-1)
            y1(3) = tmpC1(j)
            y1(4) = tmpC1(j+1)

            call polint(x1,y1,x1i,val,dy)   
            yBtr(lm,ir) = val

            y1(1) = tmpC2(j-2)
            y1(2) = tmpC2(j-1)
            y1(3) = tmpC2(j)
            y1(4) = tmpC2(j+1)
            call polint(x1,y1,x1i,val,dy)   
            yBpr(lm,ir) = val

            y1(1) = tmpC3(j-2)
            y1(2) = tmpC3(j-1)
            y1(3) = tmpC3(j)
            y1(4) = tmpC3(j+1)
            call polint(x1,y1,x1i,val,dy)   
            adams4(lm,ir) = val

            y1(1) = tmpC4(j-2)
            y1(2) = tmpC4(j-1)
            y1(3) = tmpC4(j)
            y1(4) = tmpC4(j+1)
            call polint(x1,y1,x1i,val,dy)   
            adams5(lm,ir) = val
         enddo
      endif

! ATTENTION DEPASSSEMENT DE TABLEAU
      do ir = NG, NR
!      if (rank==0) print *,'BUGG LECTURE'
!      do ir = NG, NR-1
         if ((ideb_all.le.ir).and.(ir.le.ifin_all)) then

           rayon = r(ir)
           j=3
           do while (r_old(j).lt.rayon) 
              j=j+1
           enddo
           
           x1(1) = r_old(j-2)
           x1(2) = r_old(j-1)
           x1(3) = r_old(j)
           x1(4) = r_old(j+1)

           x1i=rayon

            y1(1) = tmpC1(j-2)
            y1(2) = tmpC1(j-1)
            y1(3) = tmpC1(j)
            y1(4) = tmpC1(j+1)
            call polint(x1,y1,x1i,val,dy)   
            yVtr(lm,ir) = val

            y1(1) = tmpC2(j-2)
            y1(2) = tmpC2(j-1)
            y1(3) = tmpC2(j)
            y1(4) = tmpC2(j+1)
            call polint(x1,y1,x1i,val,dy)   
            yVpr(lm,ir) = val

            y1(1) = tmpC3(j-2)
            y1(2) = tmpC3(j-1)
            y1(3) = tmpC3(j)
            y1(4) = tmpC3(j+1)
            call polint(x1,y1,x1i,val,dy)   
            yBtr(lm,ir) = val

            y1(1) = tmpC4(j-2)
            y1(2) = tmpC4(j-1)
            y1(3) = tmpC4(j)
            y1(4) = tmpC4(j+1)
            call polint(x1,y1,x1i,val,dy)   
            yBpr(lm,ir) = val

            y1(1) = tmpC5(j-2)
            y1(2) = tmpC5(j-1)
            y1(3) = tmpC5(j)
            y1(4) = tmpC5(j+1)
            call polint(x1,y1,x1i,val,dy)   
            yTtr(lm,ir) = val

            y1(1) = tmpC6(j-2)
            y1(2) = tmpC6(j-1)
            y1(3) = tmpC6(j)
            y1(4) = tmpC6(j+1)
            call polint(x1,y1,x1i,val,dy)   
            Adams1(lm,ir) = val

            y1(1) = tmpC7(j-2)
            y1(2) = tmpC7(j-1)
            y1(3) = tmpC7(j)
            y1(4) = tmpC7(j+1)
            call polint(x1,y1,x1i,val,dy)   
            Adams2(lm,ir) = val

            y1(1) = tmpC8(j-2)
            y1(2) = tmpC8(j-1)
            y1(3) = tmpC8(j)
            y1(4) = tmpC8(j+1)
            call polint(x1,y1,x1i,val,dy)   
            Adams3(lm,ir) = val

            y1(1) = tmpC9(j-2)
            y1(2) = tmpC9(j-1)
            y1(3) = tmpC9(j)
            y1(4) = tmpC9(j+1)
            call polint(x1,y1,x1i,val,dy)   
            Adams4(lm,ir) = val

            y1(1) = tmpC10(j-2)
            y1(2) = tmpC10(j-1)
            y1(3) = tmpC10(j)
            y1(4) = tmpC10(j+1)
            call polint(x1,y1,x1i,val,dy)   
            Adams5(lm,ir) = val

         endif
         enddo


     enddo ! lm loop

     read (22) omega
     read (22) omega_old
     read (22) couple
     read (22) couple_old

     close (22)


  deallocate (r_old)
  deallocate (tmpC1,tmpC2)
  deallocate (tmpC3,tmpC4)
  deallocate (tmpC5,tmpC6)
  deallocate (tmpC7,tmpC8)
  deallocate (tmpC9,tmpC10)

  call quick_fix_para
  call energie_para
        
end subroutine lecture_v2


subroutine lecture
  use globall
  use mod_parallel

  Implicit none
  integer irank

  Complex, allocatable :: tmpC1(:,:),tmpC2(:,:),tmpC3(:,:),tmpC4(:,:),tmpC5(:,:)
  Complex, allocatable :: tmpC6(:,:),tmpC7(:,:),tmpC8(:,:),tmpC9(:,:),tmpC10(:,:)
  Integer, allocatable :: l_old(:), m_old(:)

  real, allocatable :: r_old(:)
  real :: rayon
  Integer :: ir,l,m,lmmax_old,i,lm,indx,NG_old,NR_old,j,ier,lm_old

  real x1(4),x1i,tmp(4),tmp2,tmp3
  complex y1(4), val, dy


  read (22) time
  read (22) dt
  read (22) lmmax_old

  read (22) NG_old,NR_old

  allocate (r_old(0:NR_old+1))
  allocate (tmpC1(1:lmmax_old,0:NR_old+1),tmpC2(1:lmmax_old,0:NR_old+1))
  allocate (tmpC3(1:lmmax_old,0:NR_old+1),tmpC4(1:lmmax_old,0:NR_old+1))
  allocate (tmpC5(1:lmmax_old,0:NR_old+1),tmpC6(1:lmmax_old,0:NR_old+1))
  allocate (tmpC7(1:lmmax_old,0:NR_old+1),tmpC8(1:lmmax_old,0:NR_old+1))
  allocate (tmpC9(1:lmmax_old,0:NR_old+1),tmpC10(1:lmmax_old,0:NR_old+1))
  allocate (l_old(lmmax_old),m_old(lmmax_old))

  do ir=1,NR_old
     read (22) r_old(ir)
  enddo

  if (NG_old.eq.0) r_old(NG_old)=0.

  do lm_old=1,lmmax_old
     read(22) l_old(lm_old),m_old(lm_old)
  enddo

  read (22) omega
  read (22) omega_old
  read (22) couple
  read (22) couple_old
  
  if (condIC) then
     do ir = 1, NG_old-1
        read (22) tmpC1(1:lmmax_old,ir)
        read (22) tmpC2(1:lmmax_old,ir)
        read (22) tmpC3(1:lmmax_old,ir)
        read (22) tmpC4(1:lmmax_old,ir)
     enddo
  endif
     
  do ir = NG_old, NR_old
     read (22) tmpC1(1:lmmax_old,ir)
     read (22) tmpC2(1:lmmax_old,ir)
     read (22) tmpC3(1:lmmax_old,ir)
     read (22) tmpC4(1:lmmax_old,ir)
     read (22) tmpC5(1:lmmax_old,ir)
     read (22) tmpC6(1:lmmax_old,ir)
     read (22) tmpC7(1:lmmax_old,ir)
     read (22) tmpC8(1:lmmax_old,ir)
     read (22) tmpC9(1:lmmax_old,ir)
     read (22) tmpC10(1:lmmax_old,ir)
  enddo
    
  close(22)

  do lm_old=1,lmmax_old
     l=l_old(lm_old)
     m=m_old(lm_old)

     if ((l.le.lmax).and.(m.le.mmax).and.(m.ge.mmin)) then
        lm=indx(l,m)
      
        if ((rank==0).and.(CondIC)) then
           do ir = 1, NG-1
              rayon = r(ir)
              j=3
              do while (r_old(j).lt.rayon) 
                 j=j+1
              enddo
              
              x1(1) = r_old(j-2)
              x1(2) = r_old(j-1)
              x1(3) = r_old(j)
              x1(4) = r_old(j+1)
              
              x1i=rayon
              
              y1(1) = tmpC1(lm_old,j-2)
              y1(2) = tmpC1(lm_old,j-1)
              y1(3) = tmpC1(lm_old,j)
              y1(4) = tmpC1(lm_old,j+1)
              
              call polint(x1,y1,x1i,val,dy)
              yBtr(lm,ir) = val
              
              y1(1) = tmpC2(lm_old,j-2)
              y1(2) = tmpC2(lm_old,j-1)
              y1(3) = tmpC2(lm_old,j)
              y1(4) = tmpC2(lm_old,j+1)
              
              call polint(x1,y1,x1i,val,dy)   
              yBpr(lm,ir) = val
              
              y1(1) = tmpC3(lm_old,j-2)
              y1(2) = tmpC3(lm_old,j-1)
              y1(3) = tmpC3(lm_old,j)
              y1(4) = tmpC3(lm_old,j+1)
              
              call polint(x1,y1,x1i,val,dy)   
              adams4(lm,ir) = val
              
              y1(1) = tmpC4(lm_old,j-2)
              y1(2) = tmpC4(lm_old,j-1)
              y1(3) = tmpC4(lm_old,j)
              y1(4) = tmpC4(lm_old,j+1)
              
              call polint(x1,y1,x1i,val,dy)   
              adams5(lm,ir) = val
              
           enddo
        endif
        
        do ir = NG, NR
           if ((ideb_all.le.ir).and.(ir.le.ifin_all)) then
              
              rayon = r(ir)
              if ((rank==0).and.(NG==0)) then
              j=2
              else
              j=3
              endif
              do while (r_old(j).lt.rayon) 
                 j=j+1
              enddo
              
              x1(1) = r_old(j-2)
              x1(2) = r_old(j-1)
              x1(3) = r_old(j)
              x1(4) = r_old(j+1)
              
              x1i=rayon
              
              y1(1) = tmpC1(lm_old,j-2)
              y1(2) = tmpC1(lm_old,j-1)
              y1(3) = tmpC1(lm_old,j)
              y1(4) = tmpC1(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              yVtr(lm,ir) = val
              
              y1(1) = tmpC2(lm_old,j-2)
              y1(2) = tmpC2(lm_old,j-1)
              y1(3) = tmpC2(lm_old,j)
              y1(4) = tmpC2(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              yVpr(lm,ir) = val
              
              y1(1) = tmpC3(lm_old,j-2)
              y1(2) = tmpC3(lm_old,j-1)
              y1(3) = tmpC3(lm_old,j)
              y1(4) = tmpC3(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              yBtr(lm,ir) = val
              
              y1(1) = tmpC4(lm_old,j-2)
              y1(2) = tmpC4(lm_old,j-1)
              y1(3) = tmpC4(lm_old,j)
              y1(4) = tmpC4(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              yBpr(lm,ir) = val
              
              y1(1) = tmpC5(lm_old,j-2)
              y1(2) = tmpC5(lm_old,j-1)
              y1(3) = tmpC5(lm_old,j)
              y1(4) = tmpC5(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              yTtr(lm,ir) = val
              
              y1(1) = tmpC6(lm_old,j-2)
              y1(2) = tmpC6(lm_old,j-1)
              y1(3) = tmpC6(lm_old,j)
              y1(4) = tmpC6(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              Adams1(lm,ir) = val
              
              y1(1) = tmpC7(lm_old,j-2)
              y1(2) = tmpC7(lm_old,j-1)
              y1(3) = tmpC7(lm_old,j)
              y1(4) = tmpC7(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              Adams2(lm,ir) = val
              
              y1(1) = tmpC8(lm_old,j-2)
              y1(2) = tmpC8(lm_old,j-1)
              y1(3) = tmpC8(lm_old,j)
              y1(4) = tmpC8(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              Adams3(lm,ir) = val
              
              y1(1) = tmpC9(lm_old,j-2)
              y1(2) = tmpC9(lm_old,j-1)
              y1(3) = tmpC9(lm_old,j)
              y1(4) = tmpC9(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              Adams4(lm,ir) = val
              
              y1(1) = tmpC10(lm_old,j-2)
              y1(2) = tmpC10(lm_old,j-1)
              y1(3) = tmpC10(lm_old,j)
              y1(4) = tmpC10(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              Adams5(lm,ir) = val

           endif
        enddo

     endif
  enddo

  deallocate (r_old)
  deallocate (l_old,m_old)
  deallocate (tmpC1,tmpC2)
  deallocate (tmpC3,tmpC4)
  deallocate (tmpC5,tmpC6)
  deallocate (tmpC7,tmpC8)
  deallocate (tmpC9,tmpC10)

  call quick_fix_para
  call energie_para
       
end subroutine lecture


subroutine lecture_matlab
  use globall
  use mod_parallel

  Implicit none
  integer irank

  Complex, allocatable :: tmpC1(:,:),tmpC2(:,:),tmpC3(:,:),tmpC4(:,:),tmpC5(:,:)
  Integer, allocatable :: l_old(:), m_old(:)

  real, allocatable :: r_old(:)
  real :: rayon
  Integer :: ir,l,m,lmmax_old,i,lm,indx,NG_old,NR_old,j,ier,lm_old

  real x1(4),x1i,tmp(4),tmp2,tmp3
  complex y1(4), val, dy


  read (22) time
  read (22) dt
  read (22) lmmax_old

  read (22) NG_old,NR_old

  allocate (r_old(NR_old+1))
  allocate (tmpC1(1:lmmax_old,1:NR_old+1),tmpC2(1:lmmax_old,1:NR_old+1))
  allocate (tmpC3(1:lmmax_old,1:NR_old+1),tmpC4(1:lmmax_old,1:NR_old+1))
  allocate (tmpC5(1:lmmax_old,1:NR_old+1))
  allocate (l_old(lmmax_old),m_old(lmmax_old))

  do ir=1,NR_old
     read (22) r_old(ir)
  enddo

  do lm_old=1,lmmax_old
     read(22) l_old(lm_old),m_old(lm_old)
  enddo

  read (22) omega
  read (22) omega_old
  read (22) couple
  read (22) couple_old
  
  do ir = NG_old, NR_old
     read (22) tmpC1(1:lmmax_old,ir)
     read (22) tmpC2(1:lmmax_old,ir)
     read (22) tmpC3(1:lmmax_old,ir)
     read (22) tmpC4(1:lmmax_old,ir)
     read (22) tmpC5(1:lmmax_old,ir)
  enddo
    
  close(22)

  do lm_old=1,lmmax_old
     l=l_old(lm_old)
     m=m_old(lm_old)

     if ((l.le.lmax).and.(m.le.mmax)) then
        lm=indx(l,m)
      
        if ((rank==0).and.(CondIC)) then
           do ir = 1, NG-1
              rayon = r(ir)
              j=3
              do while (r_old(j).lt.rayon) 
                 j=j+1
              enddo
              
              x1(1) = r_old(j-2)
              x1(2) = r_old(j-1)
              x1(3) = r_old(j)
              x1(4) = r_old(j+1)
              
              x1i=rayon
              
              y1(1) = tmpC1(lm_old,j-2)
              y1(2) = tmpC1(lm_old,j-1)
              y1(3) = tmpC1(lm_old,j)
              y1(4) = tmpC1(lm_old,j+1)
              
              call polint(x1,y1,x1i,val,dy)
              yBtr(lm,ir) = val
              
              y1(1) = tmpC2(lm_old,j-2)
              y1(2) = tmpC2(lm_old,j-1)
              y1(3) = tmpC2(lm_old,j)
              y1(4) = tmpC2(lm_old,j+1)
              
              call polint(x1,y1,x1i,val,dy)   
              yBpr(lm,ir) = val
              
           enddo
        endif
        
        do ir = NG, NR
           if ((ideb_all.le.ir).and.(ir.le.ifin_all)) then
              
              rayon = r(ir)
              j=3
              do while (r_old(j).lt.rayon) 
                 j=j+1
              enddo
              
              x1(1) = r_old(j-2)
              x1(2) = r_old(j-1)
              x1(3) = r_old(j)
              x1(4) = r_old(j+1)
              
              x1i=rayon
              
              y1(1) = tmpC1(lm_old,j-2)
              y1(2) = tmpC1(lm_old,j-1)
              y1(3) = tmpC1(lm_old,j)
              y1(4) = tmpC1(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              yVtr(lm,ir) = val
              
              y1(1) = tmpC2(lm_old,j-2)
              y1(2) = tmpC2(lm_old,j-1)
              y1(3) = tmpC2(lm_old,j)
              y1(4) = tmpC2(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              yVpr(lm,ir) = val
              
              y1(1) = tmpC3(lm_old,j-2)
              y1(2) = tmpC3(lm_old,j-1)
              y1(3) = tmpC3(lm_old,j)
              y1(4) = tmpC3(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              yBtr(lm,ir) = val
              
              y1(1) = tmpC4(lm_old,j-2)
              y1(2) = tmpC4(lm_old,j-1)
              y1(3) = tmpC4(lm_old,j)
              y1(4) = tmpC4(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              yBpr(lm,ir) = val
              
              y1(1) = tmpC5(lm_old,j-2)
              y1(2) = tmpC5(lm_old,j-1)
              y1(3) = tmpC5(lm_old,j)
              y1(4) = tmpC5(lm_old,j+1)
              call polint(x1,y1,x1i,val,dy)   
              yTtr(lm,ir) = val
              
           endif
        enddo

     endif
  enddo

  deallocate (r_old)
  deallocate (l_old,m_old)
  deallocate (tmpC1,tmpC2)
  deallocate (tmpC3,tmpC4)
  deallocate (tmpC5)

  call quick_fix_para
  call energie_para
       
end subroutine lecture_matlab


subroutine ecriture
  use globall
  use mod_parallel

  Implicit none



  Complex, dimension(10) :: buffer
  Complex :: tmpC1,tmpC2,tmpC3,tmpC4,tmpC5,tmpC6,tmpC7,tmpC8,tmpC9,tmpC10
  Integer :: ir,l,m,lmmax_ancien,i,lm,indx,version,ier,irank
  Integer ::status(mpi_status_size)

  character*40 :: sortie, filecmd

  if     (time.le.9.99999) then
     write(sortie,'("Dt=",f7.5,".")') time
  elseif (time.le.99.9999) then
     write(sortie,'("Dt=",f7.4,".")') time
  elseif (time.le.999.999) then
     write(sortie,'("Dt=",f7.3,".")') time
  elseif (time.le.9999.99) then
     write(sortie,'("Dt=",f7.2,".")') time
  else
     write(sortie,'("Dt=",f7.1,".")') time
  endif

! Header by rank 0 only
  if (rank==0) then

     open(22, file=trim(sortie)//trim(runid), STATUS = 'unknown', FORM='unformatted')
     version = 3
     write (22) version
     write (22) time
     write (22) dt
     write (22) lmmax

     write (22) NG,NR
     do ir=1,NR
        write (22) r(ir)
     enddo
  
     do lm=1,lmmax
     l=li(lm)
     m=mi(lm)
     write (22) l,m
     enddo

     write (22) omega
     write (22) omega_old
     write (22) couple
     write (22) couple_old

     if (condIC) then
        do ir = 1, NG-1
           write (22) yBtr(1:lmmax,ir)
           write (22) yBpr(1:lmmax,ir)
           write (22) Adams4(1:lmmax,ir)
           write (22) Adams5(1:lmmax,ir)
        enddo
     endif

     do ir = ideb_all,ifin_all
        write (22) yVtr(1:lmmax,ir)
        write (22) yVpr(1:lmmax,ir)
        write (22) yBtr(1:lmmax,ir)
        write (22) yBpr(1:lmmax,ir)
        write (22) yTtr(1:lmmax,ir)
        write (22) Adams1(1:lmmax,ir)
        write (22) Adams2(1:lmmax,ir)
        write (22) Adams3(1:lmmax,ir)
        write (22) Adams4(1:lmmax,ir)
        write (22) Adams5(1:lmmax,ir)
     enddo
     close(22)
  endif

  call mpi_barrier(COMM_model,ier)
#ifndef XLF
  call sleep(1)
#endif

  do irank = 1,size-1
  
     if (rank==irank) then
        open(22, file=trim(sortie)//trim(runid), position = 'append', FORM='unformatted')
        do ir = ideb_all,ifin_all
           write (22) yVtr(1:lmmax,ir)
           write (22) yVpr(1:lmmax,ir)
           write (22) yBtr(1:lmmax,ir)
           write (22) yBpr(1:lmmax,ir)
           write (22) yTtr(1:lmmax,ir)
           write (22) Adams1(1:lmmax,ir)
           write (22) Adams2(1:lmmax,ir)
           write (22) Adams3(1:lmmax,ir)
           write (22) Adams4(1:lmmax,ir)
           write (22) Adams5(1:lmmax,ir)
        enddo
        close(22)
     endif
     call mpi_barrier(COMM_model,ier)
#ifndef XLF
     call sleep(1)
#endif
  enddo

! copy to "newest" data file

   write(filecmd,*) 'cp '//trim(sortie)//trim(runid)//' Dt=newest'
   if(rank==0) call system(trim(filecmd))

  if(rank==0) then
     print*,'Data file written at time ',time
     open (22, file='log.'//trim(runid), STATUS = 'old', position='append')
     write(22,*)'Data file written at time ',time
     close(22)

  endif ! rank==0
  
end subroutine ecriture
!-----------------------------------------------------------------------------------


subroutine lecture_tracers

  use tracerstuff
  use globall
  use mpi

  implicit none

  integer :: descripteur,nb_elements,size_old,ntracervar_old,i,j,ntr_global, &
              rank_max,rank_min,ntr_tot_old,old,ier,nb_octets_integer,nb_octets_real
  integer*8 nb_octets_real8,sum_rank,ntracervar8
  integer(kind=MPI_OFFSET_KIND) :: position_file,pos,position_tra
  integer, dimension(MPI_STATUS_SIZE) :: statut
  character*14 filename

  filename = trim(Entree_tra)
  
  if (rank == 0) then
     print*,'Restarting with tracers...'
  end if

! Open file using MPI: only problem is that it won't work in a scalar version...

  call MPI_TYPE_SIZE (MPI_DOUBLE_PRECISION,nb_octets_real,ier)
  call MPI_TYPE_SIZE (MPI_INTEGER,nb_octets_integer,ier)

  nb_octets_real8 = nb_octets_real
  ntracervar8 = ntracervar

  call MPI_FILE_OPEN (MPI_COMM_WORLD,filename,MPI_MODE_RDONLY,MPI_INFO_NULL, &
                      descripteur,ier)


  !Reads dimensions of tra(:,:)
  if (rank == 0) then
     position_file = 0
     call MPI_FILE_READ_AT (descripteur,position_file,size_old,1,MPI_INTEGER,statut,ier)
     position_file = nb_octets_integer
     call MPI_FILE_READ_AT (descripteur,position_file,ntracervar_old,1,MPI_INTEGER,statut,ier)
  end if 

!  if (rank == 0) print*,'size_old',size_old,ntracervar_old

  if (rank == 0 .and. (size_old .ne. size)) then
     stop 'ERROR: restarting with a different # of MPI-proc not implemented yet'
  end if

  if (rank == 0 .and. (ntracervar_old .ne. ntracervar)) then
     stop 'ERROR: cannot restart with a different number of traced variables (ntracervar)'
  end if


  position_file = 2*nb_octets_integer + rank*nb_octets_integer !position where each proc should read
  pos = 2*nb_octets_integer + size*nb_octets_integer !new position in the file after all have read
  call MPI_FILE_READ_AT (descripteur,position_file,ntr,1,MPI_INTEGER,statut,ier)
  call mpi_barrier (MPI_COMM_WORLD,ier)

!print*,rank,'ntr',ntr

  call MPI_ALLREDUCE (ntr,ntr_tot_old,1,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,ier)
  call MPI_ALLGATHER (ntr,1,MPI_INTEGER,ntr_all,1,MPI_INTEGER,MPI_COMM_WORLD,ier)
!  call mpi_barrier (MPI_COMM_WORLD,ier)
  
  if (rank == 0 .and. (ntr_tot_old .ne. ntr_tot)) then
     print*,'Restarting with',ntr_tot_old,'tracers:'
     do i=0,size-1
        print*,'proc',i,'has',ntr_all(i),'tracers'
     end do
  end if

  ntr_tot = ntr_tot_old
  
  position_file = pos + rank*nb_octets_real
  pos = pos + size*nb_octets_real
  call MPI_FILE_READ_AT (descripteur,position_file,rdeb_tra,1,MPI_DOUBLE_PRECISION,statut,ier)

  position_file = pos + rank*nb_octets_real
  pos = pos + size*nb_octets_real
  call MPI_FILE_READ_AT (descripteur,position_file,rfin_tra,1,MPI_DOUBLE_PRECISION,statut,ier)

 print*,rank,'rdeb,fin',rdeb_tra,rfin_tra

 
!  call mpi_barrier (MPI_COMM_WORLD,ier)

  if (rank == 0) then
     position_file = pos 
     call MPI_FILE_READ_AT (descripteur,position_file,nb_reg,1,MPI_INTEGER,statut,ier)

     position_file = position_file + nb_octets_integer
     call MPI_FILE_READ_AT (descripteur,position_file,nb_irreg,1,MPI_INTEGER,statut,ier)

     position_file = position_file + nb_octets_integer 
     call MPI_FILE_READ_AT (descripteur,position_file,tmp1_find,1,MPI_DOUBLE_PRECISION,statut,ier)
     
     position_file = position_file + nb_octets_real
     call MPI_FILE_READ_AT (descripteur,position_file,tmp1_find2,1,MPI_DOUBLE_PRECISION,statut,ier)

     position_file = position_file + nb_octets_real
     call MPI_FILE_READ_AT (descripteur,position_file,q1,1,MPI_DOUBLE_PRECISION,statut,ier)

     position_file = position_file + nb_octets_real
     call MPI_FILE_READ_AT (descripteur,position_file,q2,1,MPI_DOUBLE_PRECISION,statut,ier)

     position_file = position_file + nb_octets_real
     call MPI_FILE_READ_AT (descripteur,position_file,deltaR,1,MPI_DOUBLE_PRECISION,statut,ier)

     position_file = position_file + nb_octets_real
     call MPI_FILE_READ_AT (descripteur,position_file,coeffr,1,MPI_DOUBLE_PRECISION,statut,ier)

!     print*,'r parameters',tmp1_find,tmp1_find2,q1,q2,deltaR,coeffr
  end if
  pos = pos + 2*nb_octets_integer + 6*nb_octets_real

  call mpi_barrier (MPI_COMM_WORLD,ier)

  call mpi_bcast(nb_reg,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
  call mpi_bcast(nb_irreg,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
  call mpi_bcast(tmp1_find,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ier)
  call mpi_bcast(tmp1_find2,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ier)
  call mpi_bcast(q1,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ier)
  call mpi_bcast(q2,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ier)
  call mpi_bcast(deltaR,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ier)
  call mpi_bcast(coeffr,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ier)

!     print*,rank,'r parameters',nb_reg,nb_irreg,tmp1_find,tmp1_find2,q1,q2,deltaR,coeffr

  

!call MPI_ALLGATHER (rdeb,1,MPI_DOUBLE_PRECISION,rdeb_all,size,MPI_DOUBLE_PRECISION,MPI_COMM_WORLD,ier)
!  call MPI_ALLGATHER (rfin,1,MPI_DOUBLE_PRECISION,rfin_all,size,MPI_DOUBLE_PRECISION,MPI_COMM_WORLD,ier) 

  call mpi_barrier (MPI_COMM_WORLD,ier)     
  !Reads tra(:,:)
  nb_elements = ntracervar*ntr
  if (rank == 0) then
     position_file = pos
  else
     sum_rank = sum(ntr_all(0:rank-1))
     if (rank==7) print*,'ntr_all',pos,nb_octets_real,sum_rank,ntracervar,position_file
     position_file = pos + nb_octets_real8*sum_rank*ntracervar8
  end if
 if (rank==7)  print*,rank,'position_file,nb_elements',pos,position_file,nb_elements,ntr_all,ntracervar,nb_octets_real
!  call mpi_barrier (MPI_COMM_WORLD,ier)
  call MPI_FILE_READ_AT (descripteur,position_file,tra,nb_elements,MPI_DOUBLE_PRECISION, &
       statut,ier)
!  call mpi_barrier (MPI_COMM_WORLD,ier)
  print*,rank,'tra(:,1)',tra(:,1)

  call MPI_FILE_CLOSE (descripteur,ier) 


  last_tra_file = Entree_tra

 
  print*,rank,'Restarted successfully with',ntr,'tracers',ntr_tot
  

end subroutine lecture_tracers
!-----------------------------------------------------------------------


subroutine ecriture_tracers

  use tracerstuff
  use globall
  use mpi

  implicit none

  character*20 sortie

  integer descripteur,nb_octets_integer,nb_octets_real,nb_elements,j,ier
  integer*8 nb_octets_real8,sum_rank,ntracervar8 !to avoid integer overflow when computing position_file for storing tra(:,:)
  integer(kind=MPI_OFFSET_KIND) position_file,pos
  integer, dimension(MPI_STATUS_SIZE) :: statut

  real mtime_deb,mtime_fin

! writing of a tra file for restart
! each processor writes its own array tra(:,:) in the file
! the former file is deleted because too heavy

  if     (time.le.9.99999) then
     write(sortie,'("tra",f7.5,".dat")') time
  elseif (time.le.99.9999) then
     write(sortie,'("tra",f7.4,".dat")') time
  elseif (time.le.999.999) then
     write(sortie,'("tra",f7.3,".dat")') time
  elseif (time.le.9999.99) then
     write(sortie,'("tra",f7.2,".dat")') time
  else
     write(sortie,'("tra",f7.1,".dat")') time
  endif

!  do irank = 0,size-1
!     if (rank = irank) then
!        open(22, file=trim(sortie)//trim(runid), position = 'append', FORM='unformatted')
!        write (22) ntracervar
!        write (22) ntr_max
!        do j=1,ntracervar
!           write (22) tra(:,j)
!        end do
!     end if
!  end do


!  open(22, file=trim(last_tra_file)//trim(runid), status = 'replace', FORM='unformatted')
!  last_tra_file = sortie

 if (rank == 0) then
    print*
    print*,'writing tra_file for restart...'
    mtime_deb = MPI_Wtime()
 end if

! call mpi_barrier (MPI_COMM_WORLD,ier)

  call MPI_TYPE_SIZE (MPI_DOUBLE_PRECISION,nb_octets_real,ier)
  call MPI_TYPE_SIZE (MPI_INTEGER,nb_octets_integer,ier)

  nb_octets_real8 = nb_octets_real
  ntracervar8 = ntracervar


  call MPI_FILE_OPEN (MPI_COMM_WORLD,trim(sortie),MPI_MODE_WRONLY+MPI_MODE_CREATE, &
                      MPI_INFO_NULL,descripteur,ier)


  !writes size and dimensions of tra(:,:) and domain boundaries
  if (rank == 0) then
     position_file = 0
     call MPI_FILE_WRITE_AT (descripteur,position_file,size,1,MPI_INTEGER,statut,ier)
     position_file = nb_octets_integer
     call MPI_FILE_WRITE_AT (descripteur,position_file,ntracervar,1,MPI_INTEGER,statut,ier)
  end if

  position_file = 2*nb_octets_integer + rank*nb_octets_integer
  pos = 2*nb_octets_integer + size*nb_octets_integer
  call MPI_FILE_WRITE_AT (descripteur,position_file,ntr,1,MPI_INTEGER,statut,ier)

  position_file = pos + rank*nb_octets_real
  pos = pos + size*nb_octets_real 
  call MPI_FILE_WRITE_AT (descripteur,position_file,rdeb_tra,1,MPI_DOUBLE_PRECISION,statut,ier)

  position_file = pos + rank*nb_octets_real
  pos = pos + size*nb_octets_real
  call MPI_FILE_WRITE_AT (descripteur,position_file,rfin_tra,1,MPI_DOUBLE_PRECISION,statut,ier)

!  print*,rank,'mid file'

!!tmp1_find,tmp1_find2,q1,q2,deltaR,coeffr, won't be recalculated when restarting
  if (rank==0) then
     position_file = pos 
     call MPI_FILE_WRITE_AT (descripteur,position_file,nb_reg,1,MPI_INTEGER,statut,ier)

     position_file = position_file + nb_octets_integer
     call MPI_FILE_WRITE_AT (descripteur,position_file,nb_irreg,1,MPI_INTEGER,statut,ier)

     position_file = position_file + nb_octets_integer  
     call MPI_FILE_WRITE_AT (descripteur,position_file,tmp1_find,1,MPI_DOUBLE_PRECISION,statut,ier)

     position_file = position_file + nb_octets_real
     call MPI_FILE_WRITE_AT (descripteur,position_file,tmp1_find2,1,MPI_DOUBLE_PRECISION,statut,ier)

     position_file = position_file + nb_octets_real
     call MPI_FILE_WRITE_AT (descripteur,position_file,q1,1,MPI_DOUBLE_PRECISION,statut,ier)

     position_file = position_file + nb_octets_real
     call MPI_FILE_WRITE_AT (descripteur,position_file,q2,1,MPI_DOUBLE_PRECISION,statut,ier)

     position_file = position_file + nb_octets_real
     call MPI_FILE_WRITE_AT (descripteur,position_file,deltaR,1,MPI_DOUBLE_PRECISION,statut,ier)

     position_file = position_file + nb_octets_real
     call MPI_FILE_WRITE_AT (descripteur,position_file,coeffr,1,MPI_DOUBLE_PRECISION,statut,ier)
  end if
  pos = pos + 2*nb_octets_integer + 6*nb_octets_real

! Now write tra(:,:)
!ATTENTION, nb_elements N'EST PAS LE MEME SUR CHAQUE PROCESSEUR 
  call MPI_ALLGATHER (ntr,1,MPI_INTEGER,ntr_all,1,MPI_INTEGER,MPI_COMM_WORLD,ier)
  call MPI_BARRIER (MPI_COMM_WORLD,ier)

  nb_elements = ntracervar*ntr
  if (rank == 0) then
     position_file = pos
  else
     sum_rank = sum(ntr_all(0:rank-1))
     position_file = pos + nb_octets_real8*sum_rank*ntracervar8
  end if

  call MPI_FILE_WRITE_AT (descripteur,position_file,tra,nb_elements,MPI_DOUBLE_PRECISION,statut,ier)

!  print*,rank,'writing finished'
!  call mpi_barrier (MPI_COMM_WORLD,ier)
  call MPI_FILE_CLOSE (descripteur,ier)

     
  if(rank==0) then
     print*,'Tracers data file written at time ',time
     ! must delete the old tra files which are very heavy...
     print*,'Zeroing last-but-one tracer file'
     print*,'last_tra_file:',last_tra_file
     print*
  end if


  call MPI_FILE_OPEN (MPI_COMM_WORLD,trim(last_tra_file),MPI_MODE_RDONLY+MPI_MODE_DELETE_ON_CLOSE, &
                      MPI_INFO_NULL,descripteur,ier)
  call MPI_FILE_CLOSE (descripteur,ier)
!  rc = MPI_FILE_DELETE (last_tra_file,MPI_INFO_NULL,ier)

  last_tra_file = sortie

  if (rank==0) then
     mtime_fin = MPI_Wtime ()
     print*,'TIME SPENT IN ECRITURE_TRACERS:',mtime_fin-mtime_deb
  end if


end subroutine ecriture_tracers
!---------------------------------------------------------------------

