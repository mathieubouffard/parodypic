!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                       !!
!!  Version JA-2.3 of Feb  2012                                          !!
!!                                                                       !!
!!  This contains the Legendre transform routines                        !!
!!                                                                       !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Subroutine initHS

  !------------------------------------------------------------------------*
  !  Initialise : pi, par, znorm, racin, poids, iki,                       *
  !               Ylm, dtYlm, dpYlm, Table.                                *
  !------------------------------------------------------------------------*

  Use globall

  Implicit none


  !**        Variables locales

  Integer LMmax_big

  Integer lms, IndxAll
  Real,allocatable ::    Ylm_big(:,:)
  Real,allocatable ::  dtYlm_big(:,:)
  Real,allocatable ::  dpYlm_big(:,:)
  Complex f_l(1,1)
  Real    w_l(1,1), d_l(1,1)

  Integer ilat, lm, Indx, l, m,lmp1,lmm1
  Real sint, cost
  Real plgndr
  Real plgndr2
  Real bt

  Integer mm
  Integer NOIKIshift

LMmax_big=(Lmax+1)*(Lmax+2)/2+Lmax+2
allocate(Ylm_big(0:LMmax_big,Nlat))
allocate(dtYlm_big(0:LMmax_big,Nlat))
allocate(dpYlm_big(0:LMmax_big,Nlat))


  NOIKIstart(Mmin)=1
  NOIKIstop(Mmin)=lmax+1
  NOIKIshift=lmax
  lmodd(Mmin)=(mod(NOIKIstop(Mmin)-NOIKIstart(Mmin),2)==0)
  do m=Mmin+Mc,Mmax,Mc
     NOIKIshift=NOIKIshift-Mc
     NOIKIstart(m)=NOIKIstop(m-Mc)+1
     NOIKIstop(m)=NOIKIstart(m)+NOIKIshift
     lmodd(m)=(mod(NOIKIstop(m)-NOIKIstart(m),2)==0)
  enddo

  do m=Mmin,Mmax,Mc
     mm=2*(m/Mc)+1
     NOIKIstart2(mm)=NOIKIstart(m)

     NOIKIstop2(mm)=NOIKIstop(m)
     lmodd2(mm)=(mod(NOIKIstop2(mm)-NOIKIstart2(mm),2)==0)
  enddo

  pi=acos(-1.)
  call gauleg(-1.0,1.0,colat,poids,Nlat)

  do ilat=1,nlat
  racin(ilat)=cos(colat(ilat))  
  sintheta(ilat)=sin(colat(ilat))
  enddo

  poids(:)=poids(:)/4

  do ilat = 1, Nlat/2

     sint=sintheta(ilat)
     cost=racin(ilat)
     bt=colat(ilat)

     do l = 0, Lmax+1
        do m = 0, l
          lm = IndxAll(l,m)
          call pbar(l,m,bt,plgndr2)
          if (m .ne. 0) then
             ylm_big(lm,ilat)=plgndr2 * 2.
          else
             ylm_big(lm,ilat)=plgndr2 * sqrt(2.)
          endif

          dpYlm_big(lm,ilat) = m*Ylm_big(lm,ilat)/sint

        enddo
     enddo
  enddo

 
  do ilat = 1, Nlat/2

     cost = racin(ilat)
     sint=sintheta(ilat)
     bt=colat(ilat)

     do l = 2, Lmax
        do m = 0, l

           lm = IndxAll(l,m)
           lmp1 = IndxAll(l+1,m)
           lmm1 = IndxAll(l-1,m)
           dtYlm_big(lm,ilat) = 1./sint*( &
                l*sqrt(real((l+m+1)*(l-m+1))/real((2*l+3)*(2*l+1))) &
                *Ylm_big(lmp1,ilat) &
                -(l+1.)*sqrt(real((l-m)*(l+m))/real((2*l+1)*(2*l-1))) &
                *Ylm_big(lmm1,ilat) )
        enddo
     enddo

     ! dtYlm pour l=1
     l=1
     do m = 0, l
        lm = IndxAll(l,m)
        lmp1 = IndxAll(l+1,m)
        dtYlm_big(lm,ilat)=1./sint*( &
             l*sqrt(real((l+m+1)*(l-m+1))/real((2*l+3)*(2*l+1))) &
             *Ylm_big(lmp1,ilat) &
             -(l+1.)*sqrt(real((l-m)*(l+m))/real((2*l+1)*(2*l-1))) &
             *1. )
     enddo

     ! dtYlm pour l=0, m=0
     lm = IndxAll(0,0)
     dtYlm_big(lm,ilat) = 0.0


     do l = 0, Lmax
        do m = Mmin, min(l,Mmax), Mc

           lms = Indx(l,m)
           lm  = IndxAll(l,m)

           Ylm(lms,ilat) =   Ylm_big(lm,ilat)
           dtYlm(lms,ilat) = dtYlm_big(lm,ilat)
           dpYlm(lms,ilat) = dpYlm_big(lm,ilat)
        enddo
     enddo
  enddo


  do ilat=1,Nlat/2
     do lm=1,LMmax
        xxy(lm,ilat)=2.*poids(ilat)*Ylm(lm,ilat)
        xxt(lm,ilat)=2.*poids(ilat)*dtYlm(lm,ilat)
        xxp(lm,ilat)=2.*poids(ilat)*dpYlm(lm,ilat)
     enddo
   
  do l = 0, Lmax
     do m = Mmin, min(l,Mmax), Mc
        lm = Indx(l,m)
        
        if(m.gt.0) then 
           Ylm(lm,ilat) =Ylm(lm,ilat)/2.   
           dtYlm(lm,ilat) = dtYlm(lm,ilat)/2. 
           dpYlm(lm,ilat) = dpYlm(lm,ilat)/2.
        endif
     enddo
  enddo
enddo

  Return
END Subroutine initHS

!------------------------------------------------------------------------!
Subroutine pbar(l, m, x,plgndr2)

 !----------------------------------------------------------------------!
 !  Valeur du polynome de Legendre d'ordre m et de degre n au point x.
 !  Avec (0 <= m <= n) et (-1 <= x <= 1).
 !----------------------------------------------------------------------!

 !----------------------------------------------------------------------!
 !     Signification des parametres:                                    !
 !     En entree:                                                       !
 !        n         : degre du polynome,                                !
 !        m         : ordre du polynome,                                !
 !        x         : valeur du point de calcul.                        !
 !----------------------------------------------------------------------!


 Implicit NONE

 !**        Declaration des parametres

 Integer m, m1, i, j, l
 Real x, c, s
 Real plgndr2, p1, p2, pi_2

     s=sin(x)
     c=cos(x)
     plgndr2=1./sqrt(2.)
     if(m .ne. 0) then
        do i=1,m
           plgndr2=sqrt(float(2*i+1)/float(2*i))*s*plgndr2
        enddo
     endif

     if(l .ne. m) then
       p1=1.
       m1=m+1
        do j=m1,l
        p2=p1
        p1=plgndr2
        plgndr2=2.*sqrt((dble(j**2)-0.25)/dble(j**2-m**2))*c*p1-sqrt(dble((2*j+1)*(j-m-1)*(j+m-1))/dble((2*j-3)*(j-m)*(j+m)))*p2
        enddo
     endif
     return


END Subroutine pbar
!----------------------------------------------------------------------------------------------------------------!


!------------------------------------------------------------------------!
Integer function IndxAll(l, m)

  !----------------------------------------------------------------------!
  !  Calcul de l'index pour stoquer les Ylm dans un tableau 1D
  !  (Cumulative index of degrees and orders)
  !   Ancien index : tous les M...
  !----------------------------------------------------------------------!

  
  Implicit NONE


  !**        Declaration des parametres

  Integer l, m


  IndxAll = (l*(l+1))/2 + m+1


  Return
END function IndxAll


!------------------------------------------------------------------------!
Integer function Indx(l, m)

  !----------------------------------------------------------------------!
  !  Calcul de l'index pour stoquer les Ylm dans un tableau 1D           !
  !  (index en fonction du degree et de l'ordre)                         !
  !                                                                      !
  !      /!!!\ Ne modifier qu'avec precaution /!!!\                      !
  !----------------------------------------------------------------------!

  use globall

  Implicit NONE

  !**        Declaration des parametres

  Integer l, m

  !**        Variables locales

  Integer Alpha

  ! tous les m, dans le bon sens
  !
  !                 Indx = m * (Lmax-((m-1)/2)) + l
  !                                             (a verifier)
  !

  ! seulement les harmoniques de Mc de Mmin a Mmax dans le bon sens

  Alpha = m / Mc



! A ete ecrit pour que le (0,0) soit en 1 !

  Indx = ((Alpha-Alpha_min) * Lmax) &
       + Alpha - Alpha_min &
       - ((Alpha*(Alpha-1))-(Alpha_min*(Alpha_min-1)))/2 * Mc &
       + l - delta &
       - (Alpha-Alpha_min) * Mc + 1 &
       + 1

  Return
END function Indx


!------------------------------------------------------------------------!

Subroutine Spat_Spec_Pol_shell &
     (ir,Datarr, yYpr,m)

  !------------------------------------------------------------------------*
  ! Espace physique -> Espace spectral                                     *
  !------------------------------------------------------------------------*

  Use globall

  Implicit NONE


  !**        Declaration des parametres

  Complex, DIMENSION(nphi/2,nlat) :: Datarr
  Complex, DIMENSION(0:LMmax+1)    :: yYpr

  Integer :: ir

  !**        Variables locales

  Real    :: x1, x2
  Complex :: x12rn,x12rs
  Integer :: ilat, nilat, lm, lms, mreal,m,kblock

  mreal=(m-1)*Mc

  do kblock=1,nblock

     do ilat=latstart(kblock),latstop(kblock)
        
        nilat = Nlat+1-ilat
        
        x12rn=(Datarr(m,ilat) &
             +Datarr(m,nilat))
        x12rs=(Datarr(m,ilat) &
             -Datarr(m,nilat))
        
        lms=NOIKIstop(mreal)
        do lm=NOIKIstart(mreal),lms-1,2
           if (ilat.eq.1) then
              yYpr(lm)   = xxy(lm,ilat)*x12rn
              yYpr(lm+1) = xxy(lm+1,ilat)*x12rs
           else
              yYpr(lm) = yYpr(lm) &
                   +xxy(lm,ilat)*x12rn
              yYpr(lm+1) = yYpr(lm+1) &
                   +xxy(lm+1,ilat)*x12rs
           endif
        enddo
        if (lmodd(mreal)) then
           if (ilat.eq.1) then
              yYpr(lms) = xxy(lms,ilat)*x12rn
           else
              yYpr(lms) = yYpr(lms)+xxy(lms,ilat)*x12rn
           endif
        endif
        
     enddo
  enddo
  
  Return
End Subroutine Spat_Spec_Pol_shell

!------------------------------------------------------------------------*


Subroutine Spat_Spec_SpherTor_shell &
     (ir,Datatr,Datapr, yYsr,yYtr,m)

  !------------------------------------------------------------------------*
  ! Espace physique -> Espace spectral                                     *
  !------------------------------------------------------------------------*

  Use globall

  Implicit NONE


  !**        Declaration des parametres

  Complex, DIMENSION(nphi/2,nlat) :: Datarr,Datatr,Datapr
  Complex, DIMENSION(0:LMmax+1)    :: yYpr,yYtr,yYsr

  Integer :: ir

  !**        Variables locales

  Real    :: x1, x2
  Complex :: x12tn,x12ts,x12pn,x12ps
  Integer :: ilat, nilat, lm, lms, mreal,m,kblock

  mreal=(m-1)*Mc
  
  
  do kblock=1,nblock
     
     do ilat=latstart(kblock),latstop(kblock)
        
        nilat = Nlat+1-ilat
        
        x12tn=(Datatr(m,ilat) &
             +Datatr(m,nilat))
        x12ts=(Datatr(m,ilat) &
             -Datatr(m,nilat))
        x12pn=(Datapr(m,ilat) &
             +Datapr(m,nilat))
        x12ps=(Datapr(m,ilat) &
             -Datapr(m,nilat))
        
        
        lms=NOIKIstop(mreal)
        do lm=NOIKIstart(mreal),lms-1,2
           
           if (ilat.eq.1) then
              yYsr(lm) = -Im*xxp(lm,ilat)*x12pn &
                   +xxt(lm,ilat)*x12ts
              yYtr(lm) = -xxt(lm,ilat)*x12ps &
                   -Im*xxp(lm,ilat)*x12tn
              
              yYsr(lm+1) = -Im*xxp(lm+1,ilat)*x12ps &
                   +xxt(lm+1,ilat)*x12tn
              yYtr(lm+1) = -xxt(lm+1,ilat)*x12pn &
                   -Im*xxp(lm+1,ilat)*x12ts
           else
              yYsr(lm) = yYsr(lm) &
                   -Im*xxp(lm,ilat)*x12pn &
                   +xxt(lm,ilat)*x12ts
              yYtr(lm) = yYtr(lm) &
                   -xxt(lm,ilat)*x12ps &
                   -Im*xxp(lm,ilat)*x12tn
              
              yYsr(lm+1) = yYsr(lm+1) &
                   -Im*xxp(lm+1,ilat)*x12ps &
                   +xxt(lm+1,ilat)*x12tn
              yYtr(lm+1) = yYtr(lm+1) &
                   -xxt(lm+1,ilat)*x12pn &
                   -Im*xxp(lm+1,ilat)*x12ts
           endif
           
        enddo
        if (lmodd(mreal)) then
           if (ilat.eq.1) then
              yYsr(lms) = &
                   -Im*xxp(lms,ilat)*x12pn &
                   +xxt(lms,ilat)*x12ts
              yYtr(lms) =  &
                   -xxt(lms,ilat)*x12ps &
                   -Im*xxp(lms,ilat)*x12tn
           else
              yYsr(lms) = yYsr(lms) &
                   -Im*xxp(lms,ilat)*x12pn &
                   +xxt(lms,ilat)*x12ts
              yYtr(lms) = yYtr(lms) &
                   -xxt(lms,ilat)*x12ps &
                   -Im*xxp(lms,ilat)*x12tn
           endif
        endif
     enddo
  enddo

  Return
End Subroutine Spat_Spec_SpherTor_shell

!-----------------------------------------------------------


Subroutine Spat_Spec_Scal_shell(Data,yY,m)

  !------------------------------------------------------------------------*
  ! Espace physique -> Espace spectral                                     *
  !------------------------------------------------------------------------*

  Use globall

  Implicit NONE


  !**        Declaration des parametres

  Complex :: Data(nphi/2,nlat)
  Complex :: yY(0:LMmax+1)

  !**        Variables locales

  Real    :: xx, x1, x2
  Integer :: ilat, nilat, lm, lms, m
  Integer :: mreal
  Complex :: x12rn,x12rs
  Integer kblock

  mreal=(m-1)*Mc

  do kblock=1,nblock

  do ilat=latstart(kblock),latstop(kblock)
     nilat = nlat+1-ilat

        x12rn=(Data(m,ilat) &
             +Data(m,nilat))
        x12rs=(Data(m,ilat) &
             -Data(m,nilat))

        lms=NOIKIstop(mreal)

        do lm=NOIKIstart(mreal),lms-1,2
           if (ilat.eq.1) then
           yY(lm) = +xxy(lm,ilat)*x12rn
           yY(lm+1) = +xxy(lm+1,ilat)*x12rs
           else
             yY(lm) = yY(lm) &
                +xxy(lm,ilat)*x12rn
           yY(lm+1) = yY(lm+1) &
                +xxy(lm+1,ilat)*x12rs
           endif

        enddo
        if (lmodd(mreal)) then
           if (ilat.eq.1) then
           yY(lms) = xxy(lms,ilat)*x12rn
           else
              yY(lms) = yY(lms)+xxy(lms,ilat)*x12rn
              endif

        endif
     enddo
  enddo

  Return
End Subroutine Spat_Spec_Scal_shell

!------------------------------------------------------------------------*


Subroutine Surfint(Data,Integral2)
! Performs the integral of data on a spherical surface
  Use globall

  Implicit NONE

  Real :: Data(nphi,nlat)
  Real :: Funct(nlat)
  Real :: Integral2
  Integer :: ilat, nilat,ilong
  Complex :: x12rn

  funct=0.
  do ilong=1,nlong
     funct(:)=funct(:)+data(ilong,:)
  enddo
  funct(:)=funct(:)*2*pi/Nlong

  Integral2=0.

  do ilat = 1, nlat/2
     nilat = nlat+1-ilat
     x12rn=(Funct(ilat)+Funct(nilat))
     Integral2 = Integral2 + poids(ilat)*x12rn
  enddo
  integral2=integral2*4

End Subroutine Surfint

!------------------------------------------------------------------------
Subroutine Spec_Spat_Scal_shell (yY,Data,th1,th2)

!------------------------------------------------------------------------*
! Espace spectral -> Espace physique pour yY                             *
!------------------------------------------------------------------------*

  Use globall

  Implicit NONE

  !**        Variables locales

  Complex, DIMENSION(0:Lmmax+1) :: yY
  Complex, DIMENSION(Nphi/2,Nlat) :: Data

  Complex :: s12
  Complex :: z12
  Integer :: lm,ilat,nilat
  Integer :: m,lms,mreal,th1,th2

  do ilat=th1,th2

     nilat = nlat+1-ilat

     do m = 1,Nphi/2
        if ((m.ge.Alpha_min+1).and.(m.le.Alpha_max+1)) then
        mreal=(m-1)*Mc

        s12=0.
        z12=0.

        lms=NOIKIstop(mreal)

        do lm=NOIKIstart(mreal),lms-1,2
           s12=s12+yY(lm)*Ylm(lm,ilat)
           z12=z12+yY(lm+1)*Ylm(lm+1,ilat)
        enddo

        if (lmodd(mreal)) then
           s12=s12+yY(lms)*Ylm(lms,ilat)
        endif

        Data(m,ilat)    = s12+z12
        Data(m,nilat)   = s12-z12
        else
        Data(m,ilat)    = 0.
        Data(m,nilat)   = 0.
        endif
     enddo
  enddo

  Return
End Subroutine Spec_Spat_Scal_shell

!------------------------------------------------------------------------*
Subroutine Spec_Spat_Grad_shell &
     (ydTdr,yT_r, &
     Datarr,Datatr,Datapr,th1,th2)

  !------------------------------------------------------------------------*
  ! Espace spectral -> Espace physique pour Data                              *
  !------------------------------------------------------------------------*

  Use globall

  Implicit NONE

  !**        Variables locales

  Complex, DIMENSION(0:Lmmax+1) :: ydTdr,yT_r
  Complex, DIMENSION(Nphi/2,Nlat) :: Datarr,Datatr,Datapr

  Complex :: s12,s13,s15
  Complex :: z12,z13,z15
  Integer :: lm,ilat,nilat
  Integer :: m,lms,mreal,th1,th2

  do ilat=th1,th2

     nilat = nlat+1-ilat

     do m = 1,Nphi/2
        if ((m.ge.Alpha_min+1).and.(m.le.Alpha_max+1)) then
           
        mreal=(m-1)*Mc

        s12=0.
        z12=0.
        s13=0.
        z13=0.
        s15=0.
        z15=0.

        lms=NOIKIstop(mreal)

        do lm=NOIKIstart(mreal),lms-1,2

           s12=s12+ydTdr(lm)*Ylm(lm,ilat)
           z12=z12+ydTdr(lm+1)*Ylm(lm+1,ilat)

           s13=s13+yT_r(lm)*dtYlm(lm,ilat)
           z13=z13+yT_r(lm+1)*dtYlm(lm+1,ilat)

           s15=s15+yT_r(lm)*dpYlm(lm,ilat)
           z15=z15+yT_r(lm+1)*dpYlm(lm+1,ilat)

        enddo

        if (lmodd(mreal)) then
           s12=s12+ydTdr(lms)*Ylm(lms,ilat)
           s13=s13+yT_r(lms)*dtYlm(lms,ilat)
           s15=s15+yT_r(lms)*dpYlm(lms,ilat)
        endif

        Datarr(m,ilat)    = s12+z12
        Datarr(m,nilat)   = s12-z12

        Datatr(m,ilat)    = s13+z13
        Datatr(m,nilat)   = -(s13-z13)

        Datapr(m,ilat)    = Im*(s15+z15)
        Datapr(m,nilat)   = Im*(s15-z15)
        else
        Datarr(m,ilat)=0.
	Datarr(m,nilat)=0.
        Datatr(m,ilat)=0.
        Datatr(m,nilat)=0.
        Datapr(m,ilat)=0.
        Datapr(m,nilat)=0.
	endif

     enddo
  enddo

  Return
End Subroutine Spec_Spat_Grad_shell

!------------------------------------------------------------------------*

Subroutine Spec_Spat_PolTor_shell &
     (ir,yDatapr,yDatatr,yDatas, &
     Datarr,Datatr,Datapr,th1,th2)

  !------------------------------------------------------------------------*
  ! Espace spectral -> Espace physique pour Data                              *
  !------------------------------------------------------------------------*

  Use globall

  Implicit NONE


  !**        Variables locales

  Complex, DIMENSION(0:LMmax+1) :: yDatapr,yDatatr,yDatas
  Complex, DIMENSION(Nphi/2,Nlat) :: Datarr,Datatr,Datapr

  Complex :: s12,s13,s14,s15,s16
  Complex :: z12,z13,z14,z15,z16
  Integer :: ir, lm,ilat,nilat
  Integer :: m,lms,mreal,th1,th2

  do ilat=th1,th2

     nilat = nlat+1-ilat

     do m = 1,Nphi/2
        
        if ((m.ge.Alpha_min+1).and.(m.le.Alpha_max+1)) then
        mreal=(m-1)*Mc

        s12=0.
        z12=0.
        s13=0.
        z13=0.
        s14=0.
        z14=0.
        s15=0.
        z15=0.
        s16=0.
        z16=0.

        lms=NOIKIstop(mreal)

        do lm=NOIKIstart(mreal),lms-1,2

           s12=s12+ll(lm)*yDatapr(lm)*r_1(ir)*Ylm(lm,ilat)
           z12=z12+ll(lm+1)*yDatapr(lm+1)*r_1(ir)*Ylm(lm+1,ilat)

           s13=s13+yDatas(lm)*dtYlm(lm,ilat)
           z13=z13+yDatas(lm+1)*dtYlm(lm+1,ilat)

           s14=s14+yDatatr(lm)*dpYlm(lm,ilat)
           z14=z14+yDatatr(lm+1)*dpYlm(lm+1,ilat)

           s15=s15+yDatas(lm)*dpYlm(lm,ilat)
           z15=z15+yDatas(lm+1)*dpYlm(lm+1,ilat)

           s16=s16+yDatatr(lm)*dtYlm(lm,ilat)
           z16=z16+yDatatr(lm+1)*dtYlm(lm+1,ilat)

        enddo
        if (lmodd(mreal)) then
           s12=s12+ll(lms)*yDatapr(lms)*r_1(ir)*Ylm(lms,ilat)
           s13=s13+yDatas(lms)*dtYlm(lms,ilat)
           s14=s14+yDatatr(lms)*dpYlm(lms,ilat)
           s15=s15+yDatas(lms)*dpYlm(lms,ilat)
           s16=s16+yDatatr(lms)*dtYlm(lms,ilat)
        endif

        Datarr(m,ilat)    = s12+z12
        Datarr(m,nilat)   = s12-z12

        Datatr(m,ilat)    = s13+z13 +Im*(s14 + z14)
        Datatr(m,nilat)   = -(s13-z13 +Im*(-s14 + z14))

        Datapr(m,ilat)    = Im*(s15+z15)- s16 -z16
        Datapr(m,nilat)   = Im*(s15-z15) +s16 -z16
        else
        Datarr(m,ilat)=0.
	Datarr(m,nilat)=0.
	Datatr(m,ilat)=0.
	Datatr(m,nilat)=0.
	Datapr(m,ilat)=0.
	Datapr(m,nilat)=0.
	endif
   
     enddo
  enddo

  Return
End Subroutine Spec_Spat_PolTor_shell

!------------------------------------------------------------------------*

Subroutine Spec_Spat_PolTor_NoCore &
     (ir,yDatapr, &
     Datarr,Datatr,Datapr,th1,th2)

  !------------------------------------------------------------------------*
  ! Espace spectral -> Espace physique pour Data                              *
  !------------------------------------------------------------------------*

  Use globall

  Implicit NONE


  !**        Variables locales

  Complex, DIMENSION(0:LMmax+1) :: yDatapr
  Complex, DIMENSION(Nphi/2,Nlat) :: Datarr,Datatr,Datapr

  Complex :: s12,s13,s14,s15,s16
  Complex :: z12,z13,z14,z15,z16
  Integer :: ir, lm,ilat,nilat
  Integer :: m,lms,mreal,th1,th2

  do ilat=th1,th2

     nilat = nlat+1-ilat

     do m = 1,Nphi/2
        
        if ((m.ge.Alpha_min+1).and.(m.le.Alpha_max+1)) then
        mreal=(m-1)*Mc

        s12=0.
        z12=0.
        s13=0.
        z13=0.
        s14=0.
        z14=0.
        s15=0.
        z15=0.
        s16=0.
        z16=0.

        lms=NOIKIstop(mreal)

        do lm=NOIKIstart(mreal),lms-1,2

           s12=s12+ll(lm)*yDatapr(lm)*Ylm(lm,ilat)
           z12=z12+ll(lm+1)*yDatapr(lm+1)*Ylm(lm+1,ilat)

           s13=s13+2.*yDatapr(lm)*dtYlm(lm,ilat)
           z13=z13+2.*yDatapr(lm+1)*dtYlm(lm+1,ilat)

           s15=s15+2.*yDatapr(lm)*dpYlm(lm,ilat)
           z15=z15+2.*yDatapr(lm+1)*dpYlm(lm+1,ilat)

        enddo
        if (lmodd(mreal)) then
           s12=s12+ll(lms)*yDatapr(lms)*Ylm(lms,ilat)
           s13=s13+2.*yDatapr(lms)*dtYlm(lms,ilat)
           s15=s15+2.*yDatapr(lms)*dpYlm(lms,ilat)
        endif

        Datarr(m,ilat)    = s12+z12
        Datarr(m,nilat)   = s12-z12

        Datatr(m,ilat)    = s13+z13 +Im*(s14 + z14)
        Datatr(m,nilat)   = -(s13-z13 +Im*(-s14 + z14))

        Datapr(m,ilat)    = Im*(s15+z15)- s16 -z16
        Datapr(m,nilat)   = Im*(s15-z15) +s16 -z16
        else
        Datarr(m,ilat)=0.
	Datarr(m,nilat)=0.
	Datatr(m,ilat)=0.
	Datatr(m,nilat)=0.
	Datapr(m,ilat)=0.
	Datapr(m,nilat)=0.
	endif
   
     enddo
  enddo

  Return
End Subroutine Spec_Spat_PolTor_NoCore


SUBROUTINE GAULEG(X1,X2,X,W,N)

  IMPLICIT NONE
 
  !-- INPUT:
  REAL X1,X2 ! LOWER/UPPER INTERVALL BOUND IN RADIANTS 	
  INTEGER N    ! DESIRED MAXIMUM DEGREE

!-- OUTPUT:
  REAL X(*)  ! ZEROS COS(THETA)
  REAL W(*)  ! ASSOCIATED GAUSS LEGENDRE WEIGHTS

!-- LOCAL:
  INTEGER M,I,J
  REAL XM,XL,P1,P2,P3,PP,EPS,Z,Z1,PI
  PARAMETER (EPS=3.D-14)

!-- END OF DECLARATION
!-----------------------------------------------------------------------

  PI=4.*ATAN(1.)
  M=(N+1)/2  ! use symmetry
  
!-- Map on symmetric intervall: 
  XM=0.5*(X2+X1)
  XL=0.5*(X2-X1)

  DO I=1,M

!----- Initial guess for zeros:
     Z=COS( PI*( (real(I)-0.25)/(real(N)+0.5)) )
     
1  continue

!----- Use recurrence to calulate P(l=n,z=cos(theta))
  P1=1.
  P2=0.
  DO J=1,N   ! do loop over degree !
     P3=P2
     P2=P1
     P1=( real(2*J-1)*Z*P2-real(J-1)*P3 )/real(J)
  END DO

!----- Newton method to refine zero: PP is derivative !
  PP=real(N)*(Z*P1-P2)/(Z*Z-1.)
  Z1=Z
  Z=Z1-P1/PP
  IF( ABS( Z-Z1).GT.EPS ) GO TO 1 ! refine zero !

!----- Another zero found 
  X(I)=ACOS(XM+XL*Z)
  X(N+1-I)=ACOS(XM-XL*Z)
  W(I)=2.*XL/((1.-Z*Z)*PP*PP)
  W(N+1-I)=W(I)
  
END DO

RETURN
END
