!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                       !!
!!  Last update: 13/09/2017 - Mathieu Bouffard                           !!
!!                                                                       !!
!!  This module contains the treatment of compositional fields           !!
!!  using the particle-in-cell method                                    !!
!!                                                                       !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!==============================
module tracers
!==============================

  use tracerstuff
  use globall

  public :: init_tracers
  public :: tra_density
  public :: gauleg
  public :: impose_HS
  public :: tracers_to_Cf
  public :: advect_tracers
  public :: diffusion
  public :: enforce_bcs
  public :: calc_mean
  public :: Spec_spat_C
  public :: distance
  public :: adjust_tracers_C

  contains
!==============================
subroutine init_tracers ()

  use tracerstuff
  use globall
  use mpi

  implicit none

  real dr_tra, dtheta_tra, dphi_tra, rad, theta, phi,ratioV,theta_r,phi_r
  integer i,ier

  character*30 sortie

  Complex yTtr2(0:LMmax+1)
  

  !------------------------------------------------------------------------*
  !  Initialize tracers                                                    *
  !------------------------------------------------------------------------*


  if (rank == 0) then
     print*,'-----------------------------------------------------------------'
     print*, 'Initializing tracers'
     print*,'-----------------------------------------------------------------'
     print*
  end if


  call init_geometry_tracers ()

  call init_velocity_tracers ()

  if (.not. grid_fusion) then
     call init_empty_nodes_interpolation_arrays ()
  end if
  
  !allocate and init arrays for treating temperature and/or composition with tracers
  if (temperature_tracers) call init_temperature_arrays_tracers ()
  if (composition_tracers) call init_composition_arrays_tracers ()

  call init_bcs ()
  

  write(last_tra_file,'("tra0.dat")') !used in output, need to be initialised


  !**        figure out the number of variables per tracer and allocate storage
  ntracervar = 3     ! r,theta,phi coordinates

  tracervarname(1) = 'r '
  tracervarname(2) = 'theta '
  tracervarname(3) = 'phi '

  if (Reordering.or.track_tracers) then
     ntracervar = ntracervar + 1
     NCELLPOS = ntracervar
     tracervarname(ntracervar) = 'Cell number'
  end if
  
  if (composition_tracers) then  
     ntracervar = ntracervar + 1
     COMPPOS = ntracervar
     tracervarname(ntracervar) = 'Composition' 
  end if 

  !You should add here any supplementary compositional field to be treated COMPPOS2 etc.
  if (.not. reordering) then
     nCompFields = ntracervar - 3
  else
     nCompFields = ntracervar - 4
  end if

  if (temperature_tracers) then
     ntracervar = ntracervar + 1
     TEMPPOS = ntracervar
     tracervarname(ntracervar) = 'Temperature'
  end if

  !add here all the other things that need to be traced by incrementing ntracervar and giving a name


  if (rank == 0) then
     print*, 'Each tracer contains:'
     do i = 1,ntracervar
        print*,i,tracervarname(i)
     end do
     print*
  end if

  !**       set up region where tracers will be  

  dtheta_tra = pi
  dphi_tra = domain_long 

! Processor rank has its tracers between radii rdeb_tra and rfin_tra
  if (rank .ne. 0) then
     rdeb_tra = 0.5*(r(ideb_all-1)+r(ideb_all))
  else 
     rdeb_tra = r(NG)
  end if

  if (rank .ne. size-1) then
     rfin_tra = 0.5*(r(ifin_all)+r(ifin_all+1)) 
  else
     rfin_tra = r(NR)
  end if

  dr_tra = rfin_tra-rdeb_tra


  !**       insert the tracers

  ntr = 0. !number of tracers on each proc, incremented when tracers added

  ratioV = 4./3.*pi*(rfin_tra**3-rdeb_tra**3)/Mc/Vtot
  ntr_init = int(ntr_tot*ratioV)
!  print*,rank,'ratioV',ratioV,ntr_tot
!  print*,'Processor',rank,'will have',ntr_init,'tracers between radii',rdeb_tra,'and',rfin_tra

  ntr_max = ntr_init*4!*3/2 !50% safety margin => TOO MUCH ???

  maxcomtr = ntr_init !10 * ntr_max / (ifin_all-ideb_all)   ! estimate max #tr to communicate  
! MUST REVISE THIS ESTIMATION !!
                      
  allocate (tra(ntracervar,ntr_max))
  allocate (ntr_all(0:size-1))
  ntr_all(:) = 0

  allocate(weight_interp(8,ntr_max))
  allocate(closest_point_coord(2,3,ntr_max))

! Simple initialization to avoid bugs at the first step in calc_weights
  closest_point_coord(1,:,:) = 2
  closest_point_coord(2,:,:) = 1

  if (Init) then !NOT A RESTART
     if (initial_distribution == 0) then !tracers randomly dispersed

!     call random_seed 

        do i = 1,ntr_init

           call random_number(rad)
           call random_number(theta)
           call random_number(phi)
           
           rad = rad * dr_tra 
           theta = theta*dtheta_tra 
           phi = phi*dphi_tra
           
           call trpos_eqvol_transform(theta,dtheta_tra,rad,rdeb_tra,dr_tra) !gives same volume per tracer
           call add_tracer(rad,theta,phi)
           
        end do
        
     else if (initial_distribution == 1) then !tracers evenly dispersed
        
        call distribute_tracers_evenly ()
        
     else
        
        stop 'ERROR, initial_distribution not recognised'
        
     end if

  else !RESTART CASE
     
     call lecture_tracers

  end if

  ! initialise the weights for the trilinear interpolations
  call calc_weights_interpolation () 

  ! check the tracers are accordingly dispersed in the grid
  call tra_density ()

  ! initialize composition and temperature on grid and/or spectral space
  if (composition_tracers)  call init_composition_tracers ()
 
  if (temperature_tracers)  call init_temperature_tracers ()

  if (track_tracers) call init_tracked_tracers ()
  
  print*,rank,'number of tracers :',ntr
  call mpi_barrier (MPI_COMM_WORLD,ier)

  call MPI_ALLREDUCE (ntr,ntr_tot,1,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,ier)
  call MPI_ALLGATHER (ntr,1,MPI_INTEGER,ntr_all,1,MPI_INTEGER,MPI_COMM_WORLD,ier)

  !**********************************************
  if (rank == 0) then
     print*
     print*,'Total number of tracers inserted: ',ntr_tot

     call calc_adequate_Ntra ()
     print*

! compute the average number of tracers per cell in the first shell => OLD CRITERION TO UPDATE
     ratioV = 4./3.*pi*(r(2)**3-r(1)**3)/Vtot
     ntr_init = int(ratioV*ntr_tot)

     print*,'Average number of tracers per cell in the first shell:',ntr_init/real(Nlat+1)/real(Nlong)
     print*
     if ((ntr_init/real(Nlat+1)/real(Nlong)) .lt. 10) then
   print*,'WARNING!! less than 10 tracers per cell on average in the first shell: THERE MIGHT BE NOT ENOUGH TRACERS'
   print*
     end if
     print*
     print*,'TRACERS INITIALIZED'
  end if
  !*********************************************


end subroutine init_tracers
!-------------------------------------------------------------------------------



subroutine init_geometry_tracers ()

  use tracerstuff
  use globall

  implicit none

  integer i,ilat,ilong
  real dr_bot1,dr_bot2

  !**       allocate arrays used for tracers
  deux_pi = 2*pi

  ! useful in find_closest_lateral_grid_point
  theta_Nlat = colat(Nlat-1) + (colat(Nlat)-colat(Nlat-1))/2.

  ! useful arrays
  allocate(r_local(max(NG,ideb_all-2):min(NR,ifin_all+2)))
  allocate(long(Nlong*Mc))
  allocate(sinphi(Nlong*Mc))
  allocate(cosphi(Nlong*Mc))
  allocate(costheta(Nlat))
  allocate(sintheta_1(Nlat),sintheta_2(Nlat))

  r_local(:) = r(max(NG,ideb_all-2):min(NR,ifin_all+2))

  do i=1,Nlat
     costheta(i) = cos(colat(i))
     sintheta_1(i) = 1./sintheta(i)
     sintheta_2(i) = sintheta_1(i)/sintheta(i)
  end do

  domain_long = deux_pi/Mc
  dphi = domain_long/Nlong
  dphi_1 = 1./dphi
  dtheta_reg = pi/(Nlat+1)
  dtheta_reg_1 = 1./dtheta_reg


  do i = 1,Nlong*Mc
     long(i) = (i-1)/real(Nlong)*domain_long
     sinphi(i) = sin(long(i))
     cosphi(i) = cos(long(i))
  end do

  ! useful arrays (to calculates dV1 and dV2 but not only)
!ACTUALLY ONLY dlat1 and drad2 is useful in empty_nodes_interpolation for the moment...
  allocate(dlat1(nlat+1),dlat2(nlat),dlat1_1(nlat+1))

  dlat1(1) = colat(1) 
  do i=2,nlat
     dlat1(i)=colat(i)-colat(i-1)
  end do
  dlat1(nlat+1) = pi-colat(nlat)

  do i=1,nlat+1
     dlat1_1(i) = 1./dlat1(i)
  end do

  dlat_mean_1 = (Nlat+1)/pi

  dlat2(1) = dlat1(1) + 0.5*dlat1(2)
  do i=2,nlat-1
     dlat2(i) = 0.5*(dlat1(i)+dlat1(i+1))
  end do
  dlat2(nlat) = dlat1(nlat+1) + 0.5*dlat1(nlat)

  allocate(drad1(NG:NR-1),drad2(NG:NR),drad1_1(NG:NR-1))

  do i=NG,NR-1
     drad1(i) = r(i+1)-r(i)
  end do
  
  do i=NG,NR-1
     drad1_1(i) = 1./drad1(i)
  end do

  drad2(NG) = 0.5*(r(NG+1)-r(NG))
  do i=NG+1,NR-1
     drad2(i) = 0.5*(r(i+1)-r(i-1))
  end do
  drad2(NR) = 0.5*(r(NR)-r(NR-1))

  !important quantities to compute top and bottom fluxes
  dr_bot1 = r(NG+1) - r(NG)
  dr_bot2 = r(NG+2) - r(NG)

  cfb1 = -dr_bot2**2
  cfb2 = dr_bot1**2
  cfb3 = -cfb1 - cfb2
  cfb4 = 1./(dr_bot2*cfb2 + dr_bot1*cfb1)

  allocate(icb_dS(Nlong,Nlat))

  do ilat = 1,Nlat
     do ilong = 1,Nlong
        icb_dS(ilong,ilat) = r(NG)**2 * sintheta(ilat) * dlat2(ilat) * dphi
     end do
  end do
  surf_icb = 4.*pi*r(NG)**2

  ! volumes of each cell (two different definitions of a cell)
  allocate (dV1(nlong,0:nlat,ideb_all:min(ifin_all,NR-1)))
  allocate (dV2(nlong,nlat,ideb_all:ifin_all))

  ! calculates elementary volumes
  Vtot = 4./3.*pi*(r(NR)**3-r(NG)**3) / Mc 
  
  call elementary_volumes (dV1,dV2)
     

  ! number of tracers per cell and density (filled in tra_density)
  allocate (tra_per_cell(nlong,nlat,ideb_all:ifin_all))
  allocate (Dens_tra(nlong,nlat,ideb_all:ifin_all))


  if (grid_fusion) then ! a special treatment is performed for the tracer to grid interpolation

     allocate(grid_fusion_lat(Nr))
     allocate(grid_fusion_long(Nlat,Nr))
     
!     if (composition_tracers) then
!        allocate(comp_poleN(ideb_Ctra-2:ifin_Ctra+2),comp_poleS(ideb_Ctra-2:ifin_Ctra+2))
!     end if
!     if (temperature_tracers) then
!        allocate(temp_poleN(ideb_Ttra-2:ifin_Ttra+2),temp_poleS(ideb_Ttra-2:ifin_Ttra+2))
!     end if
 
     call set_grid_fusion_lat ()
     call set_grid_fusion_long ()

  end if


end subroutine init_geometry_tracers
!-------------------------------------------------------------------------------


subroutine init_velocity_tracers ()

  use tracerstuff
  use globall

  implicit none

  integer ilat,ilong
  real distN,distS


! initialising array for velocity in spherical coordinates
  ideb_veltra = ideb_all
  ifin_veltra = ifin_all

  if (rank == 0) ideb_veltra = ideb_all+2
  if (rank == size-1) ifin_veltra = ifin_all-2

  allocate(vel_spat(3,Nlong,Nlat,ideb_veltra-2:ifin_veltra+2)) 


  if (.not. tracers_interp2ord) then
     ! initialising arrays for velocity in cartesian coordinates
     
     ! two arrays : one for each pole (north and south)
     ! + 1 point of safety
     
     ! arrays required only if interpolation is trilinear
     
     ! Definition of the cone:
     call find_closest_lateral_grid_point (theta_poles,ilat_N,distN)
     call find_closest_lateral_grid_point (pi-theta_poles,ilat_S,distS)
     
     if (distN .gt. 0) then
        ilat_N = min(ilat_N+1,Nlat)
     else
        ilat_N = min(ilat_N+2,Nlat)
     end if
     
     if (distS .gt. 0) then
        ilat_S = min(ilat_S-2,1) 
     else
        ilat_S = min(ilat_S-1,1)
     end if
     
     ! allocation of the array for velocity: required only if interpolation is trilinear
     ! IMPORTANT: if Mc > 1, the arrays for cartesian velocity are duplicated
     ! to cover 2pi. Otherwise, problems arise near the poles... 
     Nlong_cart = Nlong*Mc
     allocate(vel_cart1(3,Nlong_cart,1:ilat_N,ideb_veltra-2:ifin_veltra+2))
     allocate(vel_cart2(3,Nlong_cart,ilat_S:Nlat,ideb_veltra-2:ifin_veltra+2))

     ! initialising the matrix for changing coordinates from spherical to cartesian
     allocate(Psc_N(9,Nlong_cart,1:ilat_N),Psc_S(9,Nlong_cart,ilat_S:Nlat))

     do ilat=1,ilat_N
        do ilong=1,Nlong_cart
           Psc_N(1,ilong,ilat) = sintheta(ilat)*cosphi(ilong)
           Psc_N(2,ilong,ilat) = costheta(ilat)*cosphi(ilong) 
           Psc_N(3,ilong,ilat) = -sinphi(ilong)
           Psc_N(4,ilong,ilat) = sintheta(ilat)*sinphi(ilong)
           Psc_N(5,ilong,ilat) = costheta(ilat)*sinphi(ilong)
           Psc_N(6,ilong,ilat) = cosphi(ilong)
           Psc_N(7,ilong,ilat) = costheta(ilat)
           Psc_N(8,ilong,ilat) = -sintheta(ilat) 
           Psc_N(9,ilong,ilat) = 0.
        end do
     end do

     do ilat=ilat_S,Nlat
        do ilong=1,Nlong_cart
           Psc_S(1,ilong,ilat) = sintheta(ilat)*cosphi(ilong)
           Psc_S(2,ilong,ilat) = costheta(ilat)*cosphi(ilong) 
           Psc_S(3,ilong,ilat) = -sinphi(ilong)
           Psc_S(4,ilong,ilat) = sintheta(ilat)*sinphi(ilong)
           Psc_S(5,ilong,ilat) = costheta(ilat)*sinphi(ilong)
           Psc_S(6,ilong,ilat) = cosphi(ilong)
           Psc_S(7,ilong,ilat) = costheta(ilat)
           Psc_S(8,ilong,ilat) = -sintheta(ilat) 
           Psc_S(9,ilong,ilat) = 0.
        end do
     end do

  else !velocity in cartesian coordinates still required at poles

     allocate (vel_cart_poles(3,Nlong_cart,ideb_veltra-2:ifin_veltra+2,2)) ! 3:velocity coordinates ; 2 : north and south poles
     allocate (Psc_N(9,Nlong_cart,1),Psc_S(9,Nlong_cart,Nlat:Nlat))

     ilat = 1
     do ilong=1,Nlong_cart
        Psc_N(1,ilong,ilat) = sintheta(ilat)*cosphi(ilong)
        Psc_N(2,ilong,ilat) = costheta(ilat)*cosphi(ilong) 
        Psc_N(3,ilong,ilat) = -sinphi(ilong)
        Psc_N(4,ilong,ilat) = sintheta(ilat)*sinphi(ilong)
        Psc_N(5,ilong,ilat) = costheta(ilat)*sinphi(ilong)
        Psc_N(6,ilong,ilat) = cosphi(ilong)
        Psc_N(7,ilong,ilat) = costheta(ilat)
        Psc_N(8,ilong,ilat) = -sintheta(ilat) 
        Psc_N(9,ilong,ilat) = 0.
     end do

     ilat = Nlat
     do ilong=1,Nlong_cart
        Psc_S(1,ilong,ilat) = sintheta(ilat)*cosphi(ilong)
        Psc_S(2,ilong,ilat) = costheta(ilat)*cosphi(ilong) 
        Psc_S(3,ilong,ilat) = -sinphi(ilong)
        Psc_S(4,ilong,ilat) = sintheta(ilat)*sinphi(ilong)
        Psc_S(5,ilong,ilat) = costheta(ilat)*sinphi(ilong)
        Psc_S(6,ilong,ilat) = cosphi(ilong)
        Psc_S(7,ilong,ilat) = costheta(ilat)
        Psc_S(8,ilong,ilat) = -sintheta(ilat) 
        Psc_S(9,ilong,ilat) = 0.
     end do
     
  end if

  !Initialise useful matrices for 2nd order interpolation of velocity
  if (tracers_interp2ord) then
     call init_coeff_second_deriv ()
     call init_matrices_2ndorderInterp ()
  end if

  if (triquadratic_interpolation) then
     call init_matrices_3Q ()
  end if


end subroutine init_velocity_tracers
!-------------------------------------------------------------------------------


subroutine init_temperature_arrays_tracers ()

! allocate the corresponding arrays for treating temperature with tracers

  use tracerstuff
  use globall

  implicit none

  integer :: ir,irad,ilat,ilong,irad0
  integer,allocatable:: nrad_search(:),nlat_search(:)
  real o_drsq,o_dthetasq,o_dphisq,dhsq,dist


  ideb_Ttra = ideb_all
  ifin_Ttra = ifin_all
  
  if (rank == 0) ideb_Ttra = ideb_all + 2
  if (rank == size-1) ifin_Ttra = ifin_all - 2

  ! calculate diffusion timescale for each grid cell
  ! this is the harmonic average of the timescales for each direction
  ! needed in smooth_tracer

  allocate(t_diff_T(Nlong,Nlat,ideb_Ttra-2:ifin_Ttra+2))
  
  t_diff_T = 0.
  
  do ir=ideb_all,ifin_all 
     o_drsq = 1./(drad2(ir))**2
     do ilat = 1,Nlat
        o_dthetasq = 1./(dlat2(ilat)*r(ir))**2  ! theta direction
        do ilong = 1,Nlong
           o_dphisq = 1./(dphi*r(ir)*sintheta(ilat))**2  ! phi direction
           dhsq = 0.25/(1/3.*o_drsq + 1/3.*o_dthetasq + 1/3.*o_dphisq)   ! (grid_spacing/2)**2
           
           t_diff_T(ilong,ilat,ir) = dhsq/DeltaT  
        end do
     end do
  end do
  
  call exchange_ghost_planes_dT (t_diff_T)


end subroutine init_temperature_arrays_tracers
!-------------------------------------------------------------------------------


subroutine init_composition_arrays_tracers ()

  use tracerstuff
  use tracers_bcs
  use data_parody_local
  use globall

  implicit none

  integer ir,irad,ilat,ilong,irad0
  real o_drsq,o_dthetasq,o_dphisq,dhsq,dist

  ! delimit the domains of each proc (including ghost points) 
  ideb_Ctra=ideb_all
  ifin_Ctra=ifin_all
  
  if(rank==0) ideb_Ctra=NG+2
  if (rank==size-1) ifin_Ctra=NR-2
  
  ideb_C=ideb_all
  ifin_C=ifin_all
  
  if(rank==0) ideb_C=NG+1
  if (rank==size-1) ifin_C=ifin_C-1
     
!  if (diffusion_composition) then

  ! calculate diffusion timescale for each grid cell
  ! this is the harmonic average of the timescales for each direction
  ! needed in smooth_tracer
     allocate(t_diff_C(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2))
     
     t_diff_C = 0.
     
     do ir=ideb_all,ifin_all 
        o_drsq = 1./(drad2(ir))**2
        do ilat = 1,Nlat
           o_dthetasq = 1./(dlat2(ilat)*r(ir))**2  ! theta direction
           do ilong = 1,Nlong
              o_dphisq = 1./(dphi*r(ir)*sintheta(ilat))**2  ! phi direction
              dhsq = 0.25/(1/3.*o_drsq + 1/3.*o_dthetasq + 1/3.*o_dphisq)   ! (grid_spacing/2)**2
              
              t_diff_C(ilong,ilat,ir) = dhsq/DeltaC  
           end do
        end do
     end do

     call exchange_ghost_planes_dC (t_diff_C)

     !allocate arrays for solving compositional diffusion 
     allocate (m6_l(LMmax,ideb_C-2:ifin_C+2),m6_u(LMmax,ideb_C-2:ifin_C+2))
     
     allocate (A6a(NG:NR), A6b(0:Lmax+1,NG:NR), A6c(NG:NR))
     allocate (A6bp(NG:NR))
     
     allocate (A6a_2d(LMmax,ideb_C-2:ifin_C+2)) 
     allocate (diag6(LMmax,ideb_C-2:ifin_C+2))
     allocate (A6c_2d(LMmax,ideb_C-2:ifin_C+2))
     
     allocate (Transit_C(LMmax+1, ideb_Vp-2:ifin_Vp+2))

!  else !A COMPLETER ENSUITE if (bcs_tra=='fixed flux') then !if Lewis is infinite, then just need composition for the boundary conditions

 !    if (rank == 0) then
 !       allocate(t_diff_C(Nlong,Nlat,NG:ir_botbcs))
  
 !       t_diff_C = 0.
        
 !       do ir=NG,ir_botbcs-1 
 !          o_drsq = 1./(drad2(ir))**2
 !          do ilat = 1,Nlat
 !             o_dthetasq = 1./(dlat2(ilat)*r(ir))**2  ! theta direction
 !             do ilong = 1,Nlong
 !                o_dphisq = 1./(dphi*r(ir)*sintheta(ilat))**2  ! phi direction
 !                dhsq = 0.25/(1/3.*o_drsq + 1/3.*o_dthetasq + 1/3.*o_dphisq)   ! (grid_spacing/2)**2
                 
!                 t_diff_C(ilong,ilat,ir) = dhsq/(DeltaT/(r(NG)-r(ir_botbcs))*(r(ir)-r(ir_botbcs))) 
!              end do
!           end do
!        end do
!     end if

!  end if


end subroutine init_composition_arrays_tracers
!-------------------------------------------------------------------------------


subroutine init_empty_nodes_interpolation_arrays ()

  use tracerstuff
  use globall
  
  implicit none

  integer ideb,ifin,irad,irad0,ilat
  integer,allocatable:: nrad_search(:),nlat_search(:)
  real dist

  ! ideb and ifin are identical to ideb_Ctra/ideb_Ttra and ifin_Ctra/ifin_Ttra
  ideb=ideb_all
  ifin=ifin_all
  
  if(rank==0) ideb=NG+2
  if (rank==size-1) ifin=NR-2
  

    !useful for empty_nodes_interpolation (): delimit broadly a smaller portion in
  !which to search for empty cells
  allocate (nlat_search(ideb-2:ifin+2),nrad_search(ideb-2:ifin+2))
  allocate (irad_min_emptyn0(ideb-2:ifin+2),irad_max_emptyn0(ideb-2:ifin+2))
  allocate (irad_min_emptyn(ideb-2:ifin+2),irad_max_emptyn(ideb-2:ifin+2))
  allocate (ilat_min_emptyn(Nlat,ideb-2:ifin+2),ilat_max_emptyn(Nlat,ideb-2:ifin+2))
  allocate (ilat_min_emptyn0(Nlat,ideb-2:ifin+2),ilat_max_emptyn0(Nlat,ideb-2:ifin+2))

! Short distance for empty_nodes_interpolation()
  dmax_empty0 = 3.*(3./4./pi*volmin)**(1./3.) !gross estimation
  dmax_empty0 = (Vtot/ntr_tot)**(1./3.)
  
  do irad=ideb-2,ifin+2 
     do ilat=1,Nlat
        ilat_min_emptyn0(ilat,irad) = max(ilat - 1,1)
        ilat_max_emptyn0(ilat,irad) = min(ilat + 1,Nlat)
     end do
  end do

  do irad0 = ideb-2,ifin+2
     nrad_search(irad0) = 0
     do irad=ideb-2,ifin+2 
        call distance (irad0,1,1,r(irad),colat(1),long(1),dist)
        if (dist .lt. dmax_empty0) then
           nrad_search(irad0)=nrad_search(irad0)+1
           irad_max_emptyn0(irad0) = irad
        end if
     end do
     irad_min_emptyn0(irad0) = irad_max_emptyn0(irad0) - nrad_search(irad0) + 1
  end do


! Large distance for empty_nodes_interpolation()
  dmax_empty = (3./4./pi*Vmax_empty)**(1./3.)
  
  do irad=ideb-2,ifin+2 
     nlat_search(irad) = 1 + floor(dmax_empty/(r(irad)*minval(dlat1)))
     do ilat=1,Nlat
        ilat_min_emptyn(ilat,irad) = max(ilat - nlat_search(irad),1)
        ilat_max_emptyn(ilat,irad) = min(ilat + nlat_search(irad),Nlat)
     end do
  end do
  
  
  do irad0 = ideb-2,ifin+2
     nrad_search(irad0) = 0
     do irad=ideb-2,ifin+2 
        call distance (irad0,1,1,r(irad),colat(1),long(1),dist)
        if (dist .lt. dmax_empty) then
           nrad_search(irad0)=nrad_search(irad0)+1
           irad_max_emptyn(irad0) = irad
        end if
     end do
     irad_min_emptyn(irad0) = irad_max_emptyn(irad0) - nrad_search(irad0) + 1
  end do
  
  deallocate (nlat_search,nrad_search)


end subroutine init_empty_nodes_interpolation_arrays
!------------------------------------------------------------------------------


subroutine init_bcs ()

  use tracerstuff
  use globall
  use tracers_bcs

  implicit none

  if (composition_tracers .and. .not. diffusion_composition) then
     call init_matrices_bcs () ! matrices to compute fluxes at the boundaries

!     if (TC_bcs == 'coupled') then        
!        allocate(C_flux(Nlong,Nlat))
!     end if

  end if


end subroutine init_bcs
!-------------------------------------------------------------------------------


subroutine add_tracer (rad,theta,phi)

!**        check tracers number and adds tracer 

  use tracerstuff

  implicit none
  real,intent(in)::rad,theta,phi

  real Y

  if (ntr>=ntr_max) stop 'ERROR in add_tracer: tracer storage full' !ntr_max NOT VERY WELL DEFINED YET
  ntr = min(ntr + 1,ntr_max)

  tra(1,ntr) = rad
  tra(2,ntr) = theta
  tra(3,ntr) = phi

  if (theta .eq. 0.) tra(2,ntr) = colat(1)/100. !avoid tracers exactly at pole 

  ! IDEE : if (theta == 0) then poles = .true.
  ! puis ajouter poles dans la condition suivante et n'ajouter le traceur que si poles == false

  if (temperature_tracers) then
     tra(TEMPPOS,ntr) = 0.
  end if


end subroutine add_tracer
!-------------------------------------------------------------------------------


subroutine trpos_eqvol_transform(theta,dtheta_tra,rad,rlo,dr_tra)

  !  maps tracer initial coordinates to give equal volume per tracer

  use tracerstuff
  implicit none
  real,intent(inout):: rad,theta
  real,intent(in):: dtheta_tra,dr_tra,rlo
  real rhi,thi,tlo,a,b

  rhi=dr_tra+rlo
  thi=dtheta_tra 
  tlo = 0.

  a = (rhi**3-rlo**3)/(3*(rhi-rlo))
  rad = (rlo**3+3*a*rad)**(1./3.) 

  b = (cos(tlo)-cos(thi))/(thi-tlo)
  theta = acos(cos(tlo)-b*theta)-tlo

end subroutine trpos_eqvol_transform
!-------------------------------------------------------------------------------


subroutine distribute_tracers_evenly ()

! First distribute points in a cube in which the outer core is inscribed
! For each point, test if it belongs to FOC and to which domain

  use globall
  use tracerstuff
  use mpi

  implicit none

  integer*8 :: Ncube,ix,iy,iz,Nx,Ny,Nz,ic,nc,ier,tag,ntrmax
  integer*8 :: ntra(0:size-1)
  integer, dimension(MPI_STATUS_SIZE) :: statut

  real Vcube,rad,theta,phi,x,y,z,S,Ccond

  logical sortie

     ! First compute how many points there should be in the cube to have Ntr_tot tracers in FOC
  Vcube = (2.*r(NR))**3
  Ncube = Ntr_tot * Vcube / Vtot

  Nx = floor(Ncube**(1./3.)+0.5) ! closest integer which is a cube
  if (mod(Nx,2) .eq. 1) then     ! keep only even values to avoid tracers at theta=0 
     Nx = Nx + 1
  end if
  Ny = Nx
  Nz = Nx

  ntr = 0
     
  do ix= 1,Nx
     do iy = 1,Ny
        do iz = 1,Nz

              ! cartesian coordinates of the points are -r(NR) * (ix/(Ncube+1),iy/(Ncube+1),iz/(Ncube+1))
              ! convert these coordinates into spherical coordinates
              
           x = -r(NR) + 2.*r(NR) * ix/(Nx+1)
           y = -r(NR) + 2.*r(NR) * iy/(Ny+1)
           z = -r(NR) + 2.*r(NR) * iz/(Nz+1)
              
           rad = sqrt(x**2 + y**2 + z**2) 
              
           if (rad .lt. rfin_tra .and. rad .gt. rdeb_tra) then ! keep only the points belonging to the MPI domain
              
              theta = acos (z/rad)
              if (y >= 0.) then
                 phi = acos(x/sqrt(x**2+y**2))
              else
                 phi = deux_pi - acos(x/sqrt(x**2+y**2)) 
              end if

              if (0. .le. phi .and. phi .lt. domain_long) then ! mind periodicity

                 ntr = ntr + 1
              
                 tra(1,ntr) = rad
                 tra(2,ntr) = theta
                 tra(3,ntr) = phi  
                 
                 if (temperature_tracers) then !AJOUTER LES DIFFERENTS MODES D'INITIALISATION
                    tra(TEMPPOS,ntr) =  0.!1.* exp(-8.*(theta-pi/2.)**2)*exp(-8.*(phi-pi)**2)*exp(-8.*(rad-((r(NR)+r(NG))/2.))**2)
                 end if
                 
                 if (composition_tracers) then
                    !FOR DOUBLE DIFFUSE BENCHMARK OF BREUER ET AL.
                    !                 Ccond = -Aspect_ratio/(Aspect_ratio-1)+Aspect_ratio/rad/(Aspect_ratio-1)**2
                    !                 x = 2.*rad-r(NG)-r(NR)
                    !                 call imposeHS(rad,theta,phi,5,4,S)
                    tra(COMPPOS,ntr) = sin(5.*phi)! Ccond + 0.1*(1.-3.*x**2+3.*x**4-x**6)*S
                 end if
                 
              end if
           end if
           
        end do
     end do
  end do


end subroutine distribute_tracers_evenly
!-------------------------------------------------------------------------------


subroutine tra_density ()

  use tracerstuff
  use globall
  use mpi

  implicit none
  
  integer irad,ilat,ilong,nzero,nzeros,ier

  character*15 sortie

  nzero = 0

!     Calculates the local density of tracers at each grid point

  call tracers_per_cell (tra_per_cell)

  do irad = ideb_all,ifin_all
     do ilat = 1,nlat
        do ilong = 1,nlong
           dens_tra(ilong,ilat,irad) = tra_per_cell(ilong,ilat,irad)/dV2(ilong,ilat,irad)
           if (tra_per_cell(ilong,ilat,irad) .eq. 0) then
              nzero = nzero + 1
           end if
        end do
     end do
  end do

  call MPI_REDUCE (nzero,nzeros,1,MPI_INTEGER,MPI_SUM,0,MPI_COMM_WORLD,ier)

  if (rank == 0) then
     print*
     print*,'----------in tracers_density------------'
     print*,'Total number of cells:', (NR-NG+1)*nlat*nlong
     print*,'Average number of tracers per cell:',real(ntr_tot)/((NR-NG+1)*nlat*nlong)
     print*,'Total number of empty cells:', nzeros
     print*,'Percentage of cells without tracers:',real(nzeros)/((NR-NG+1)*nlat*nlong)*100,'%'
     print*
  end if


end subroutine tra_density
!-------------------------------------------------------------------------------


subroutine tracers_per_cell (n)

  use tracerstuff
  use globall

  implicit none

  integer,intent(out):: n(nlong,nlat,ideb_all:ifin_all)

!  Counts number of tracers per cell,

  integer i,irad,ilat,ilong,j,jmin
  real dtheta,diff,diffmin,dist_rad,dist_lat

  n(:,:,:)=0

  do i = 1,ntr

     call find_closest_radial_grid_point(tra(1,i),irad,dist_rad)

     call find_closest_lateral_grid_point(tra(2,i),ilat,dist_lat)

     ilong = 1 + floor(tra(3,i)*dphi_1)

     if (tra(3,i) .gt. (long(ilong)+dphi/2.)) ilong = max(mod(ilong+1,Nlong+1),1)


     n(ilong,ilat,irad) = n(ilong,ilat,irad) + 1

  end do
  
end subroutine tracers_per_cell
!-----------------------------------------------------------------------------


subroutine elementary_volumes(dvol1,dvol2)

  use tracerstuff
  use globall
  use mpi

  implicit none

  real,intent(out):: dvol1(nlong,0:nlat,ideb_all:min(ifin_all,NR-1)),dvol2(nlong,nlat,ideb_all:ifin_all)

  integer irad,ilat,ilong,ier
  real dtheta,sint,sumvol1,sumvol2,minvol2,maxvol2,minvol,maxvol

  !      Calculates the elementary volume around each grid point
  ! dvol1: volume delimited by 8 grid points (which are its corners)
  ! dvol2: volume centered around a grid point

  do ilat = 0,nlat
     if (ilat .eq. 0) then
        sint = sin(colat(1)/2.)
        dtheta = dlat1(1)
     else if (ilat .eq. nlat) then
        sint = sin((pi+colat(nlat))/2.)
        dtheta = dlat1(nlat+1)
     else
        sint = sin((colat(ilat+1)+colat(ilat))/2.)
        dtheta = dlat1(ilat+1)
     end if

     do irad = ideb_all,min(ifin_all,NR-1)
        do ilong = 1,nlong
           dvol1(ilong,ilat,irad) = 1./3.*( r(irad+1)**3 - &
                                     r(irad)**3 )  &
                                     *sint*dtheta*dphi
        enddo
     enddo
  enddo

  do irad = ideb_all,ifin_all
     do ilat = 1,nlat   
        do ilong = 1,nlong
           dvol2(ilong,ilat,irad) = r(irad)**2 *sintheta(ilat)*drad2(irad)*dlat2(ilat)*dphi 
        end do
     end do
  end do

  sumvol1 = sum(dvol1)
  sumvol2 = sum(dvol2)

  minvol2 = minval(dvol2)
  maxvol2 = maxval(dvol2)

  call MPI_ALLREDUCE (sumvol1,Vtot1,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,ier)
  call MPI_ALLREDUCE (sumvol2,Vtot2,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,ier)
  call MPI_ALLREDUCE (minvol2,minvol,1,MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,ier)
  call MPI_ALLREDUCE (maxvol2,maxvol,1,MPI_DOUBLE_PRECISION,MPI_MAX,MPI_COMM_WORLD,ier)
  
  if (rank == 0) then
     print*,'Initialising volumes'
     print*,'Volume of the smallest cell:',minvol
     print*,'Volume of the biggest cell:',maxvol
     print*,'Vmax/Vmin=',maxvol/minvol
     print*,'Statistical number of tracers in smallest cell:',real(ntr_tot)/Vtot*minvol
     print*
  end if

  volmin = minvol
  volmax = maxvol

end subroutine elementary_volumes
!------------------------------------------------------------------------------


subroutine init_temperature_tracers ()

  use tracerstuff
  use globall

  implicit none

  integer i,ir,ier,ilat,ilong
  real,allocatable :: temp0(:,:,:),tempvar(:,:,:)

  character*30 :: sortie

  allocate(temp(nlong,nlat,ideb_Ttra-2:ifin_Ttra+2))
!  allocate(tempvar(nlong,nlat,ideb_Ttra-2:ifin_Ttra+2))
  allocate(temp_stat(nlong,nlat,ideb_Ttra-2:ifin_Ttra+2))
 
!  if (Init) then
!     call Spec_spat_T (temp) ! get variable part of temperature on grid
!     call exchange_ghost_planes_dT (temp)
!     !store variable part 
!     tempvar(:,:,:) = temp(:,:,:)
!  else
!     if (grid_fusion) then
!        call tracers_to_grid_special (temp,ideb_Ttra,ifin_Ttra,TEMPPOS)
!     else 
!        call tracers_to_T ()
!     end if
!     call exchange_ghost_planes_dT (temp)
!  end if
  
  
  ! Compute static temperature profile
!  do ir=ideb_all,ifin_all
!     if (HeatingMode.eq.0) then
!        temp_stat(:,:,ir)=r(NG)*r(NR)/r(ir)+1-r(NR)
!     endif
!     
!     if (HeatingMode.eq.1) then
!        temp_stat(:,:,ir)=(r(ir)**2-r(NG)**2)/6.+r(NR)**3/3.*(1/r(ir)-1/r(NG))
!     endif
!     
!     if (HeatingMode.eq.2) then
!        temp_stat(:,:,ir)=-r(ir)**2/6-r(NG)**3/(3.*r(ir))+r(NG)**2/2.
!     endif
!     
!     if (HeatingMode.eq.3) then
!        if (Aspect_ratio.eq.0) then
!           temp_stat(:,:,ir)=3*(2.*Pch-1)/(r(NR)**3-r(NG)**3)*r(ir)**2/6 
!        else
!           temp_stat(:,:,ir)=3*(2.*Pch-1)/(r(NR)**3-r(NG)**3)*r(ir)**2/6   &
!                - (r(NG)**3*(1.-Pch)-r(NR)**3*Pch)/(r(NR)**3-r(NG)**3)/r(ir)
!        endif
!     endif
!     
!     if (HeatingMode.eq.4) then
!        temp_stat(:,:,ir)=(1./r(ir)-1./r(NG))*r(NR)**2
!     endif
!  end do
!  
!  call exchange_ghost_planes_dT (temp_stat) ! exchange ghost planes between domains 
  
!  if (Init) then
!     temp(:,:,:) = temp_stat(:,:,:) + tempvar(:,:,:) !tracers carry the total temperature
!  else
!     tempvar(:,:,:) = temp(:,:,:) - temp_stat(:,:,:) !extract variable part
!  end if
  
  if (Init) then !NOT A RESTART
     
 !    allocate(temp0(nlong,nlat,ideb_Ttra-2:ifin_Ttra+2))
 !    temp0 = temp !exact value of initial temperature field

  temp_stat(:,:,:) = 0.
 
  do i = 1,ntr
     if (tra(1,i) .ge. 0.5*(r(NG)+r(NR))) then
        tra(TEMPPOS,i) = 1.
     end if
  enddo

 ! do ir = ideb_Ttra,ifin_Ttra
 !    do ilat = 1,Nlat
 !       do ilong = 1,Nlong
 !          temp(ilong,ilat,:) = r(ir)*sin(10.*long(ilong))*sin(colat(ilat))
 !       enddo
 !    enddo
 ! enddo
    
     ! now initialise the value for the tracers
!     call adjust_tracers_T (temp) 
     if (grid_fusion) then
        call tracers_to_grid_special (temp,ideb_Ttra,ifin_Ttra,TEMPPOS)
     else 
        call tracers_to_T ()
     end if
     call exchange_ghost_planes_dT (temp)  	
     call Spat_Spec_T (temp)
     call exch1_2d (yTtr,LMmax,ideb_Tt,ifin_Tt)
   
!     ! correction
!     tempvar = temp0-temp
!     call adjust_tracers_T (tempvar) !(-(temp-temp0))
!     if (grid_fusion) then
!        call tracers_to_grid_special (temp,ideb_Ttra,ifin_Ttra,TEMPPOS)
!     else     
!        call tracers_to_T ()
!     end if
!     call exchange_ghost_planes_dT (temp)
     
     !go back to spectral space for the variable part
!     tempvar(:,:,:) = temp(:,:,:)-temp_stat(:,:,:)
     
 !    deallocate(temp0)
     
  end if !NOT A RESTART
  
!  call Spat_spec_T (tempvar) !(temp-temp_stat)
!  call exch1_2d (yTtr,LMmax,ideb_Tt,ifin_Tt)

   if     (time.le.9.99999) then
            write(sortie,'("Tstat",f7.5,".dat")') time
         elseif (time.le.99.9999) then
            write(sortie,'("Tstat",f7.4,".dat")') time
         elseif (time.le.999.999) then
            write(sortie,'("Tstat",f7.3,".dat")') time
         elseif (time.le.9999.99) then
            write(sortie,'("Tstat",f7.2,".dat")') time
         else
            write(sortie,'("Tstat",f7.1,".dat")') time
         endif
         
         call write_files_real(temp_stat(:,:,ideb_all:ifin_all),sortie,ideb_all,ifin_all)

          if     (time.le.9.99999) then
            write(sortie,'("Tvar_init",f7.5,".dat")') time
         elseif (time.le.99.9999) then
            write(sortie,'("Tvar_init",f7.4,".dat")') time
         elseif (time.le.999.999) then
            write(sortie,'("Tvar_init",f7.3,".dat")') time
         elseif (time.le.9999.99) then
            write(sortie,'("Tvar_init",f7.2,".dat")') time
         else
            write(sortie,'("Tvar_init",f7.1,".dat")') time
         endif
         
         call write_files_real(temp(:,:,ideb_all:ifin_all),sortie,ideb_all,ifin_all)
  
!  deallocate(tempvar)


end subroutine init_temperature_tracers
!------------------------------------------------------------------------------


subroutine init_composition_tracers ()

  use tracerstuff
  use globall

  implicit none

  integer i,ir,l,m,lm,indx
  real x
  real,allocatable :: comp_var(:,:,:),comp0(:,:,:)

  character*30 sortie

  allocate (comp(nlong,nlat,ideb_Ctra-2:ifin_Ctra+2))
  allocate (yCr(0:LMmax+1, ideb_C-2:ifin_C+2))
  allocate (C_Cond(ideb_Ctra-2:ifin_Ctra+2))
  
  if (Init) then !NOT A RESTART
     allocate (comp0(nlong,nlat,ideb_Ctra-2:ifin_Ctra+2))
     allocate (comp_var(nlong,nlat,ideb_Ctra-2:ifin_Ctra+2))
     !Initialize spherical harmonic
      yCr(:,:) = 0.

      C_cond(:) = 0.
      tra(COMPPOS,:) = 0.

!      do i = 1,ntr
!         if (tra(1,i) .le. 0.5*(r(NG)+r(NR))) then
!            tra(COMPPOS,i) = 1.
!         end if
!      enddo

!     call tracers_to_Cf ()
!     call exchange_ghost_planes_dC (comp)
!     call Spat_spec_C (comp)
!     call exch1_2d (yCr,LMmax,ideb_C,ifin_C)
     
     !back to spatial space
!     call Spec_Spat_C (comp_var)
!     call exchange_ghost_planes_dC (comp_var)

   if     (time.le.9.99999) then
      write(sortie,'("Comp_var",f7.5,".dat")') time
   elseif (time.le.99.9999) then
      write(sortie,'("Comp_var",f7.4,".dat")') time
   elseif (time.le.999.999) then
      write(sortie,'("Comp_var",f7.3,".dat")') time
   elseif (time.le.9999.99) then
      write(sortie,'("Comp_var",f7.2,".dat")') time
   else
      write(sortie,'("Comp_var",f7.1,".dat")') time
   endif
   
   call write_files_real(comp_var(:,:,ideb_all:ifin_all),sortie,ideb_all,ifin_all)


  !compute total initial composition (BENCHMARK BREUER ET AL)
!     do ir = ideb_Ctra-2,ifin_Ctra+2
!        C_cond(ir) = -Aspect_ratio/(1-Aspect_ratio) + Aspect_ratio*r_1(ir)/(1-Aspect_ratio)**2
!        comp(:,:,ir) = comp_var(:,:,ir) + C_cond(ir)
!     end do
!     C_cond(:) = 0.
!     comp(:,:,:) = 0.    
!     comp0(:,:,:) = comp(:,:,:)
!     tra(COMPPOS,:) = 0.
     ! now initialise the value for the tracers
!     call adjust_tracers_C (comp0)  
!     call tracers_to_Cf ()
!     call exchange_ghost_planes_dC (comp)  	
     
!     comp0 = comp0-comp
!     call adjust_tracers_C (comp0) !(-(comp-comp0))
!     call tracers_to_Cf ()
!     call exchange_ghost_planes_dC (comp)
     
     deallocate (comp_var,comp0)

   if     (time.le.9.99999) then
      write(sortie,'("Comp_init",f7.5,".dat")') time
   elseif (time.le.99.9999) then
      write(sortie,'("Comp_init",f7.4,".dat")') time
   elseif (time.le.999.999) then
      write(sortie,'("Comp_init",f7.3,".dat")') time
   elseif (time.le.9999.99) then
      write(sortie,'("Comp_init",f7.2,".dat")') time
   else
      write(sortie,'("Comp_init",f7.1,".dat")') time
   endif
   
   call write_files_real(comp(:,:,ideb_all:ifin_all),sortie,ideb_all,ifin_all)


  else !RESTART

     if (grid_fusion) then
        call tracers_to_grid_special (comp,ideb_Ctra,ifin_Ctra,COMPPOS)
     else
        call tracers_to_Cf ()
     end if
     call exchange_ghost_planes_dC (comp)
     call Spat_spec_C (comp)

     do ir = ideb_Ctra-2,ifin_Ctra+2
        C_cond(ir) = 0.! -Aspect_ratio/(1-Aspect_ratio) + Aspect_ratio*r_1(ir)/(1-Aspect_ratio)**2
     end do

  end if
  

end subroutine init_composition_tracers
!------------------------------------------------------------------------------


subroutine tracers_to_Cf ()

  use tracerstuff 
  use globall
  use mpi
!$ use omp_lib

  implicit none

!        converts tracers to continuum fields 

  logical OpenMP_parallel,pole
  real,dimension(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2)::  n
  real,dimension(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2):: comp_copy
  integer i,irad0,irad1,ilat0,ilat1,ilong0,ilong1,iphi_closest,irad,ilat,iphi,ilong, &
           ier,nzero,nzeros
  real rint,lint,pint,dist_rad,dist_lat,dist_phi,n1,rad,theta,phi,cmean
  real mtime_debut,mtime_fin
!$ integer nb_threads
  !$ real,allocatable :: comp_private(:,:,:),n_private(:,:,:)
  !$ real ptime_debut,ptime_fin


  n(:,:,:) = 0.
  comp(:,:,:) = 0. 
!$OMP PARALLEL  &
!$OMP PRIVATE (irad0,irad1,ilat0,ilat1,ilong0,ilong1,iphi_closest) &
!$OMP PRIVATE (rint,lint,pint,dist_rad,dist_lat,dist_phi,n1,rad,theta,phi)    &
!$OMP PRIVATE (irad,ilat,ilong,pole,nb_threads,comp_private,n_private)

!$ allocate(comp_private(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2),n_private(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2))
!$ comp_private = 0.
!$ n_private = 0.


!$ if (.true.) then
!$OMP MASTER
!$     OpenMP_parallel=.true.
!$OMP END MASTER
!$  else
  OpenMP_parallel = .false.
!$  end if
!$OMP BARRIER

!$ nb_threads = omp_get_num_threads ()

      IF (rank == rank_time .and. measure_time) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!$       IF (.TRUE.) THEN
!$OMP MASTER
!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!$          ptime_debut = omp_get_wtime()
!$OMP END MASTER
!$OMP BARRIER
!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
            mtime_debut = MPI_WTime ()
!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
      END IF

IF (.not. OpenMP_parallel) THEN

  do i = 1,ntr
     
     pole = .false.

!        finds the coordinates of the corners of the volume element 
!        in which the tracer is located

     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)
     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)
     ilong0 = closest_point_coord(1,3,i)
     ilong1 = closest_point_coord(2,3,i)

!     if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true. 
     if (ilat1 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true.

!         tracer information gives contribution to each corner :

     if (.not. pole) then

        comp(ilong0,ilat0,irad0) = comp(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(COMPPOS,i)     
        comp(ilong1,ilat0,irad0) = comp(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(COMPPOS,i)
        comp(ilong0,ilat1,irad0) = comp(ilong0,ilat1,irad0) + weight_interp(3,i) * tra(COMPPOS,i)
        comp(ilong1,ilat1,irad0) = comp(ilong1,ilat1,irad0) + weight_interp(4,i) * tra(COMPPOS,i)
        comp(ilong0,ilat0,irad1) = comp(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(COMPPOS,i)
        comp(ilong1,ilat0,irad1) = comp(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(COMPPOS,i)
        comp(ilong0,ilat1,irad1) = comp(ilong0,ilat1,irad1) + weight_interp(7,i) * tra(COMPPOS,i)
        comp(ilong1,ilat1,irad1) = comp(ilong1,ilat1,irad1) + weight_interp(8,i) * tra(COMPPOS,i)
     
   
        n(ilong0,ilat0,irad0) = n(ilong0,ilat0,irad0) + weight_interp(1,i)
        n(ilong1,ilat0,irad0) = n(ilong1,ilat0,irad0) + weight_interp(2,i)   
        n(ilong0,ilat1,irad0) = n(ilong0,ilat1,irad0) + weight_interp(3,i)   
        n(ilong1,ilat1,irad0) = n(ilong1,ilat1,irad0) + weight_interp(4,i)  
        n(ilong0,ilat0,irad1) = n(ilong0,ilat0,irad1) + weight_interp(5,i)
        n(ilong1,ilat0,irad1) = n(ilong1,ilat0,irad1) + weight_interp(6,i)
        n(ilong0,ilat1,irad1) = n(ilong0,ilat1,irad1) + weight_interp(7,i) 
        n(ilong1,ilat1,irad1) = n(ilong1,ilat1,irad1) + weight_interp(8,i)
  
     else 
        
        comp(ilong0,ilat0,irad0) = comp(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(COMPPOS,i)          
        comp(ilong1,ilat0,irad0) = comp(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(COMPPOS,i)         
        comp(ilong0,ilat0,irad1) = comp(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(COMPPOS,i)
        comp(ilong1,ilat0,irad1) = comp(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(COMPPOS,i)
           
        n(ilong0,ilat0,irad0) = n(ilong0,ilat0,irad0) + weight_interp(1,i)   
        n(ilong1,ilat0,irad0) = n(ilong1,ilat0,irad0) + weight_interp(2,i)   
        n(ilong0,ilat0,irad1) = n(ilong0,ilat0,irad1) + weight_interp(5,i)    
        n(ilong1,ilat0,irad1) = n(ilong1,ilat0,irad1) + weight_interp(6,i)

     end if

  end do

ELSE !if OpenMP

!$OMP DO SCHEDULE(STATIC,ntr/nb_threads)
  do i=1,ntr

     pole = .false.

!        finds the coordinates of the corners of the volume element 
!        in which the tracer is located

     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)
     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)
     ilong0 = closest_point_coord(1,3,i)
     ilong1 = closest_point_coord(2,3,i)

     if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true. 

!         gives the weight each corner receives from the tracer 

     if (.not. pole) then

!$        comp_private(ilong0,ilat0,irad0) = comp_private(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat0,irad0) = comp_private(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(COMPPOS,i)
!$        comp_private(ilong0,ilat1,irad0) = comp_private(ilong0,ilat1,irad0) + weight_interp(3,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat1,irad0) = comp_private(ilong1,ilat1,irad0) + weight_interp(4,i) * tra(COMPPOS,i)
!$        comp_private(ilong0,ilat0,irad1) = comp_private(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat0,irad1) = comp_private(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(COMPPOS,i)
!$        comp_private(ilong0,ilat1,irad1) = comp_private(ilong0,ilat1,irad1) + weight_interp(7,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat1,irad1) = comp_private(ilong1,ilat1,irad1) + weight_interp(8,i) * tra(COMPPOS,i)

!$        n_private(ilong0,ilat0,irad0) = n_private(ilong0,ilat0,irad0) + weight_interp(1,i)
!$        n_private(ilong1,ilat0,irad0) = n_private(ilong1,ilat0,irad0) + weight_interp(2,i)
!$        n_private(ilong0,ilat1,irad0) = n_private(ilong0,ilat1,irad0) + weight_interp(3,i)
!$        n_private(ilong1,ilat1,irad0) = n_private(ilong1,ilat1,irad0) + weight_interp(4,i)
!$        n_private(ilong0,ilat0,irad1) = n_private(ilong0,ilat0,irad1) + weight_interp(5,i)
!$        n_private(ilong1,ilat0,irad1) = n_private(ilong1,ilat0,irad1) + weight_interp(6,i)
!$        n_private(ilong0,ilat1,irad1) = n_private(ilong0,ilat1,irad1) + weight_interp(7,i)
!$        n_private(ilong1,ilat1,irad1) = n_private(ilong1,ilat1,irad1) + weight_interp(8,i)
  
     else

!$        comp_private(ilong0,ilat0,irad0) = comp_private(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat0,irad0) = comp_private(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(COMPPOS,i)
!$        comp_private(ilong0,ilat0,irad1) = comp_private(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat0,irad1) = comp_private(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(COMPPOS,i)

!$        n_private(ilong0,ilat0,irad0) = n_private(ilong0,ilat0,irad0) + weight_interp(1,i)
!$        n_private(ilong1,ilat0,irad0) = n_private(ilong1,ilat0,irad0) + weight_interp(2,i)
!$        n_private(ilong0,ilat0,irad1) = n_private(ilong0,ilat0,irad1) + weight_interp(5,i)
!$        n_private(ilong1,ilat0,irad1) = n_private(ilong1,ilat0,irad1) + weight_interp(6,i)

     end if
  end do
!$OMP END DO

!IL FAUT RAJOUTER UN ATOMIC A CHAQUE LIGNE MAIS PEU EFFICACE...
!IDEE (GUY): FAIRE ACCEDER CHAQUE THREAD A DES PARTITIONS DU TABLEAU PUIS PERMUTER
!ON PEUT AVOIR AUSSI RECOURS A LA CLAUSE COLLPASE POUR:

!!$OMP CRITICAL
!!$ comp = comp + comp_private
!!$ n = n + n_private
!!$OMP END CRITICAL
!!$OMP BARRIER

!$  do irad = ideb_Ctra-2,ifin_Ctra+2
!$    do ilat = 1,Nlat
!$        do ilong = 1,Nlong
!$OMP ATOMIC
!$            comp(ilong,ilat,irad) = comp(ilong,ilat,irad) + comp_private(ilong,ilat,irad)
!$OMP ATOMIC
!$            n(ilong,ilat,irad) = n(ilong,ilat,irad) + n_private(ilong,ilat,irad)
!$        end do
!$     end do
!$  end do


!$ deallocate (comp_private,n_private)

END IF ! OpenMP


!  IF (rank == 0) THEN
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          ptime_fin = omp_get_wtime()
!!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele tra_to_T : ', ptime_fin - ptime_debut
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            mtime_fin = MPI_WTime ()
!            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele tra_to_T : ', mtime_fin - mtime_debut
!!$       END IF
!      END IF

!$OMP END PARALLEL


  call exchange_ghost_planes (comp,n,ideb_Ctra,ifin_Ctra)


! now convert to continuum field
!$OMP PARALLEL PRIVATE(ilat,ilong)
!$OMP DO SCHEDULE(DYNAMIC,1)
  do irad = ideb_Ctra-2,ifin_Ctra+2
     do ilat = 1,nlat
        do ilong = 1,nlong
           if (n(ilong,ilat,irad) .ne. 0.) then
              comp(ilong,ilat,irad) = comp(ilong,ilat,irad) / n(ilong,ilat,irad)
           end if
           comp_copy(ilong,ilat,irad) = comp(ilong,ilat,irad)
         end do
      end do
  end do
!$OMP END DO
!$OMP END PARALLEL


!LE RECOPIAGE DE CE TABLEAU POURRAIT SE FAIRE DANS LA BOUCLE PRECEDENTE
!  comp_copy(:,:,:) = comp(:,:,:) 

! special treatment for points around which no tracers were found
!$OMP PARALLEL PRIVATE(ilat,ilong,n1,cmean)
!$OMP DO SCHEDULE(DYNAMIC,1)
  do irad = ideb_all,ifin_all
     do ilat = 1,nlat
        do ilong = 1,nlong
           if (n(ilong,ilat,irad) .eq. 0.) then
              n1 = 0.
              cmean = 0.
              call empty_nodes_interpolation(irad,ilat,ilong,ideb_Ctra,ifin_Ctra,n1,n,comp_copy,cmean)
              comp(ilong,ilat,irad) = cmean / n1 
           end if
        end do
     end do
  end do
!$OMP END DO

IF (rank == rank_time .and. measure_time) then
!$  IF (.true.) THEN
!$OMP MASTER
!$      ptime_fin = omp_get_wtime ()
!$      Write (6,'(A,F10.2)') 'Temps ecoule, mixte region parallele tra_to_C :',ptime_fin-ptime_debut
!$OMP END MASTER
!$OMP BARRIER
!$  ELSE
         mtime_fin = MPI_WTime ()
         Write(6,'(A,F10.2)') 'Temps ecoule, MPI, region parallele tra_to_C :', mtime_fin - mtime_debut
!$  END IF
END IF

!$OMP END PARALLEL

  if (mod(t,modulo)==0) then

     nzero = 0

     do irad = ideb_all,ifin_all
        do ilat = 1,nlat
           do ilong = 1,nlong
              if (n(ilong,ilat,irad) .eq. 0)  nzero = nzero + 1
           end do
        end do
     end do
     
     call MPI_REDUCE (nzero,nzeros,1,MPI_INTEGER,MPI_SUM,0,MPI_COMM_WORLD,ier)
     
     if (rank == 0 .and. mod(t,modulo2)==0) then
        print*
        print*,'----------in Tracers_to_C-----------------'
        print*,'Total number of grid points:', (NR-NG+1)*Nlat*Nlong
        print*,'Number of grid points with 0 tracers:', nzeros
        print*,'Percentage of grid points with 0 tracers:', 100*nzeros/real((NR-NG+1)*nlat*nlong),'%'
        print*
     end if

  end if


end subroutine tracers_to_Cf
!-----------------------------------------------------------------------------


subroutine tracers_to_T ()

  use tracerstuff 
  use globall
  use mpi
!$ USE OMP_LIB

  implicit none

!        converts tracers to continuum fields 

  real,dimension(nlong,Nlat,ideb_Ttra-2:ifin_Ttra+2)::  n
  real,dimension(Nlong,Nlat,ideb_Ttra-2:ifin_Ttra+2):: temp_copy
  integer i,irad0,irad1,ilat0,ilat1,ilong0,ilong1,iphi_closest,irad,ilat,iphi,ilong, &
           ier,nzero,nzeros
  real rint,lint,pint,dist_rad,dist_lat,dist_phi,n1,rad,theta,phi,w1,w2,w3,w4,w5,w6,w7,w8, &
       dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8,tmean
  real mtime_debut,mtime_fin,ptime_debut,ptime_fin
  logical lat_edge,OpenMP_parallel
!$ integer nb_threads
!$ real,allocatable :: temp_private(:,:,:),n_private(:,:,:)


  n(:,:,:) = 0.
  temp(:,:,:) = 0. 


!$OMP PARALLEL  &
!$OMP PRIVATE (irad0,irad1,ilat0,ilat1,ilong0,ilong1,iphi_closest) &
!$OMP PRIVATE (rint,lint,pint,dist_rad,dist_lat,dist_phi,n1,rad,theta,phi,w1,w2,w3,w4,w5,w6,w7,w8)    &
!$OMP PRIVATE (dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8)  &
!$OMP PRIVATE (irad,ilat,ilong,lat_edge,nb_threads,temp_private,n_private)

!$ allocate(temp_private(Nlong,Nlat,ideb_Ttra-2:ifin_Ttra+2),n_private(Nlong,Nlat,ideb_Ttra-2:ifin_Ttra+2))
!$ temp_private = 0.
!$ n_private = 0.


!$ if (.true.) then
!$OMP MASTER
!$     OpenMP_parallel=.true.
!$OMP END MASTER
!$  else
  OpenMP_parallel = .false.
!$  end if
!$OMP BARRIER

!$ nb_threads = omp_get_num_threads ()

      IF (rank == rank_time .and. measure_time) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!$       IF (.TRUE.) THEN
!$OMP MASTER
!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!$          ptime_debut = omp_get_wtime()
!$OMP END MASTER
!$OMP BARRIER
!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
            mtime_debut = MPI_WTime ()
!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
      END IF

IF (.not. OpenMP_parallel) THEN

  do i = 1,ntr
     
     lat_edge = .false.

!        finds the coordinates of the corners of the volume element 
!        in which the tracer is located

     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)
     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)
     ilong0 = closest_point_coord(1,3,i)
     ilong1 = closest_point_coord(2,3,i)

!     if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) lat_edge = .true. 
     if (ilat1 .eq. 1 .or. ilat0 .eq. Nlat) lat_edge = .true.

!         tracer information gives contribution to each corner :

     if (.not. lat_edge) then

        temp(ilong0,ilat0,irad0) = temp(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(TEMPPOS,i)     
        temp(ilong1,ilat0,irad0) = temp(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(TEMPPOS,i)
        temp(ilong0,ilat1,irad0) = temp(ilong0,ilat1,irad0) + weight_interp(3,i) * tra(TEMPPOS,i)
        temp(ilong1,ilat1,irad0) = temp(ilong1,ilat1,irad0) + weight_interp(4,i) * tra(TEMPPOS,i)
        temp(ilong0,ilat0,irad1) = temp(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(TEMPPOS,i)
        temp(ilong1,ilat0,irad1) = temp(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(TEMPPOS,i)
        temp(ilong0,ilat1,irad1) = temp(ilong0,ilat1,irad1) + weight_interp(7,i) * tra(TEMPPOS,i)
        temp(ilong1,ilat1,irad1) = temp(ilong1,ilat1,irad1) + weight_interp(8,i) * tra(TEMPPOS,i)
     
   
        n(ilong0,ilat0,irad0) = n(ilong0,ilat0,irad0) + weight_interp(1,i)
        n(ilong1,ilat0,irad0) = n(ilong1,ilat0,irad0) + weight_interp(2,i)   
        n(ilong0,ilat1,irad0) = n(ilong0,ilat1,irad0) + weight_interp(3,i)   
        n(ilong1,ilat1,irad0) = n(ilong1,ilat1,irad0) + weight_interp(4,i)  
        n(ilong0,ilat0,irad1) = n(ilong0,ilat0,irad1) + weight_interp(5,i)
        n(ilong1,ilat0,irad1) = n(ilong1,ilat0,irad1) + weight_interp(6,i)
        n(ilong0,ilat1,irad1) = n(ilong0,ilat1,irad1) + weight_interp(7,i) 
        n(ilong1,ilat1,irad1) = n(ilong1,ilat1,irad1) + weight_interp(8,i)
  
     else 
        
        temp(ilong0,ilat0,irad0) = temp(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(TEMPPOS,i)          
        temp(ilong1,ilat0,irad0) = temp(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(TEMPPOS,i)         
        temp(ilong0,ilat0,irad1) = temp(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(TEMPPOS,i)
        temp(ilong1,ilat0,irad1) = temp(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(TEMPPOS,i)
           
        n(ilong0,ilat0,irad0) = n(ilong0,ilat0,irad0) + weight_interp(1,i)   
        n(ilong1,ilat0,irad0) = n(ilong1,ilat0,irad0) + weight_interp(2,i)   
        n(ilong0,ilat0,irad1) = n(ilong0,ilat0,irad1) + weight_interp(5,i)    
        n(ilong1,ilat0,irad1) = n(ilong1,ilat0,irad1) + weight_interp(6,i)

     end if

  end do

ELSE !if OpenMP

!$OMP DO SCHEDULE(STATIC,ntr/nb_threads)
  do i=1,ntr

     lat_edge = .false.

!        finds the coordinates of the corners of the volume element 
!        in which the tracer is located

     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)
     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)
     ilong0 = closest_point_coord(1,3,i)
     ilong1 = closest_point_coord(2,3,i)

     if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) lat_edge = .true. 

!         gives the weight each corner receives from the tracer 

     if (.not. lat_edge) then

!$        temp_private(ilong0,ilat0,irad0) = temp_private(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(TEMPPOS,i)
!$        temp_private(ilong1,ilat0,irad0) = temp_private(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(TEMPPOS,i)
!$        temp_private(ilong0,ilat1,irad0) = temp_private(ilong0,ilat1,irad0) + weight_interp(3,i) * tra(TEMPPOS,i)
!$        temp_private(ilong1,ilat1,irad0) = temp_private(ilong1,ilat1,irad0) + weight_interp(4,i) * tra(TEMPPOS,i)
!$        temp_private(ilong0,ilat0,irad1) = temp_private(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(TEMPPOS,i)
!$        temp_private(ilong1,ilat0,irad1) = temp_private(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(TEMPPOS,i)
!$        temp_private(ilong0,ilat1,irad1) = temp_private(ilong0,ilat1,irad1) + weight_interp(7,i) * tra(TEMPPOS,i)
!$        temp_private(ilong1,ilat1,irad1) = temp_private(ilong1,ilat1,irad1) + weight_interp(8,i) * tra(TEMPPOS,i)

!$        n_private(ilong0,ilat0,irad0) = n_private(ilong0,ilat0,irad0) + weight_interp(1,i)
!$        n_private(ilong1,ilat0,irad0) = n_private(ilong1,ilat0,irad0) + weight_interp(2,i)
!$        n_private(ilong0,ilat1,irad0) = n_private(ilong0,ilat1,irad0) + weight_interp(3,i)
!$        n_private(ilong1,ilat1,irad0) = n_private(ilong1,ilat1,irad0) + weight_interp(4,i)
!$        n_private(ilong0,ilat0,irad1) = n_private(ilong0,ilat0,irad1) + weight_interp(5,i)
!$        n_private(ilong1,ilat0,irad1) = n_private(ilong1,ilat0,irad1) + weight_interp(6,i)
!$        n_private(ilong0,ilat1,irad1) = n_private(ilong0,ilat1,irad1) + weight_interp(7,i)
!$        n_private(ilong1,ilat1,irad1) = n_private(ilong1,ilat1,irad1) + weight_interp(8,i)
  
     else

!$        temp_private(ilong0,ilat0,irad0) = temp_private(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(TEMPPOS,i)
!$        temp_private(ilong1,ilat0,irad0) = temp_private(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(TEMPPOS,i)
!$        temp_private(ilong0,ilat0,irad1) = temp_private(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(TEMPPOS,i)
!$        temp_private(ilong1,ilat0,irad1) = temp_private(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(TEMPPOS,i)

!$        n_private(ilong0,ilat0,irad0) = n_private(ilong0,ilat0,irad0) + weight_interp(1,i)
!$        n_private(ilong1,ilat0,irad0) = n_private(ilong1,ilat0,irad0) + weight_interp(2,i)
!$        n_private(ilong0,ilat0,irad1) = n_private(ilong0,ilat0,irad1) + weight_interp(5,i)
!$        n_private(ilong1,ilat0,irad1) = n_private(ilong1,ilat0,irad1) + weight_interp(6,i)

     end if
  end do
!$OMP END DO

!IL FAUT RAJOUTER UN ATOMIC A CHAQUE LIGNE MAIS PEU EFFICACE...
!IDEE (GUY): FAIRE ACCEDER CHAQUE THREAD A DES PARTITIONS DU TABLEAU PUIS PERMUTER
!ON PEUT AVOIR AUSSI RECOURS A LA CLAUSE COLLPASE POUR:

!!$OMP CRITICAL
!!$ temp = temp + temp_private
!!$ n = n + n_private
!!$OMP END CRITICAL
!!$OMP BARRIER

!$  do irad = ideb_Ttra-2,ifin_Ttra+2
!$    do ilat = 1,Nlat
!$        do ilong = 1,Nlong
!$OMP ATOMIC
!$            temp(ilong,ilat,irad) = temp(ilong,ilat,irad) + temp_private(ilong,ilat,irad)
!$OMP ATOMIC
!$            n(ilong,ilat,irad) = n(ilong,ilat,irad) + n_private(ilong,ilat,irad)
!$        end do
!$     end do
!$  end do


!$ deallocate (temp_private,n_private)

END IF ! OpenMP


!  IF (rank == 0) THEN
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          ptime_fin = omp_get_wtime()
!!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele tra_to_T : ', ptime_fin - ptime_debut
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            mtime_fin = MPI_WTime ()
!            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele tra_to_T : ', mtime_fin - mtime_debut
!!$       END IF
!      END IF

!$OMP END PARALLEL


  call exchange_ghost_planes_T (n)

! now convert to continuum field
!$OMP PARALLEL PRIVATE(ilat,ilong)
!$OMP DO SCHEDULE(DYNAMIC,1)
  do irad = ideb_Ttra-2,ifin_Ttra+2
     do ilat = 1,nlat
        do ilong = 1,nlong
           if (n(ilong,ilat,irad) .ne. 0) then
              temp(ilong,ilat,irad) = temp(ilong,ilat,irad) / n(ilong,ilat,irad)
           end if
           temp_copy(ilong,ilat,irad) = temp(ilong,ilat,irad)
         end do
      end do
  end do
!$OMP END DO
!$OMP END PARALLEL

! special treatment for points around which no tracers were found

!$OMP PARALLEL PRIVATE(ilat,ilong,n1,tmean)
!$OMP DO SCHEDULE(DYNAMIC,1)
  do irad = ideb_all,ifin_all
     do ilat = 1,nlat
        do ilong = 1,nlong
           if (n(ilong,ilat,irad) .eq. 0) then
              n1 = 0.
              tmean = 0.
              call empty_nodes_interpolation(irad,ilat,ilong,ideb_Ttra,ifin_Ttra,n1,n,temp_copy,tmean)
              temp(ilong,ilat,irad) = tmean/n1 !temp2(ilong,ilat,irad) / n1
           end if

         end do
      end do
  end do
!$OMP END DO

IF (rank == rank_time .and. measure_time) then
!$  IF (.true.) THEN
!$OMP MASTER
!$      ptime_fin = omp_get_wtime ()
!$      Write (6,'(A,F10.2)') 'Temps ecoule, mixte region parallele tra_to_T :',ptime_fin-ptime_debut
!$OMP END MASTER
!$OMP BARRIER
!$  ELSE
         mtime_fin = MPI_WTime ()
         Write(6,'(A,F10.2)') 'Temps ecoule, MPI, region parallele tra_to_T :', mtime_fin - mtime_debut
!$  END IF
END IF

!$OMP END PARALLEL

  if (mod(t,modulo)==0) then

     nzero = 0

     do irad = ideb_all,ifin_all
        do ilat = 1,nlat
           do ilong = 1,nlong
              if (n(ilong,ilat,irad) .eq. 0)  nzero = nzero + 1
           end do
        end do
     end do
     
     call MPI_REDUCE (nzero,nzeros,1,MPI_INTEGER,MPI_SUM,0,MPI_COMM_WORLD,ier)
     
     if (rank == 0 .and. mod(t,modulo2)==0) then
        print*
        print*,'----------in Tracers_to_T-----------------'
        print*,'Total number of grid points:', (NR-NG+1)*Nlat*Nlong
        print*,'Number of grid points with 0 tracers:', nzeros
        print*,'Percentage of grid points with 0 tracers:', 100*nzeros/real((NR-NG+1)*nlat*nlong),'%'
        print*
     end if

  end if


end subroutine tracers_to_T
!-----------------------------------------------------------------------------


subroutine advect_tracers(Nr_deb,Nr_fin)
 
 use sorting
 use tracerstuff
 use globall
 use mpi
!$ use OMP_LIB

 implicit none
 integer,intent(in):: Nr_deb,Nr_fin
 
 real vt(3),v1(3),v2(3),v3(3),v4(3),v5(3),v6(3),dto2, rad,theta,phi, phifac,thetafac, &
      x,y,z,xf,yf,zf
 real a21,a31,a32,a41,a42,a43,a51,a52,a53,a54,a61,a62,a63,a64,a65,b1,b3,b4,b5,b6
 real mtime_debut,mtime_fin,dist_rad,dist_lat,dist_long
 character*32:: sortie
 !$ real ptime_debut,ptime_fin
 integer i,j,b,ier,ilatm,ilatp,iradm,iradp,irad_closest,ilat_closest,ilong_closest,ilong0,ilong1,jtracked
 logical tracked
!$ integer nb_threads

 dto2 = dt/2.

 tracked = .false.

 if (tracers_advord == 5) then

    a21 = dt*1./4.
    a31 = dt*3./32.
    a32 = dt*9./32.
    a41 = dt*1932./2197.
    a42 = dt*(-7200.)/2197.
    a43 = dt*7296./2197.
    a51 = dt*439./216.
    a52 = dt*(-8.)
    a53 = dt*3680./513.
    a54 = dt*(-845.)/4104.
    a61 = dt*(-8.)/27.
    a62 = dt*2.
    a63 = dt*(-3544.)/2565.
    a64 = dt*1859./4104.
    a65 = dt*(-11.)/40.

    b1 = 16./135.
!    b2 = 0.
    b3 = 6656./12825.
    b4 = 28561./56430.
    b5 = -9./50.
    b6 = 2./55.

 end if

 if (triquadratic_interpolation) then
    call calc_coeffs_3Q ()
 end if

 if (rank == 0 .and. t==0) then
    if (tracers_noadvection) return
    print*
    if(tracers_advord==5) print'(a,$)','   Advecting tracers: 5th-order Runge-Kutta Fehlberg,'
    if(tracers_advord==4) print'(a,$)','   Advecting tracers: 4th-order Runge-Kutta,'
    if(tracers_advord==2) print'(a,$)','   Advecting tracers: 2nd-order Runge-Kutta,'
    if(tracers_advord==1) print'(a,$)','   Advecting tracers: 1st-order Euler,'
    if(tracers_interp2ord) then 
       print*,'linear interpolation near poles, 2nd-order interpolation elsewhere'
       print*,'(2nd-order interpolation NOT IMPLEMENTED in the cone around poles !)'
    end if
    if(.not.tracers_interp2ord) print*,'linear interpolation'
 end if


 if(tracers_interp2ord) then
    ! for each grid point, determine the direction of quadratic interpolation
    call calc_direction_2ndorderInterp ()
 end if

    !$OMP PARALLEL PRIVATE(rad,theta,phi,x,y,z,xf,yf,zf,thetafac,phifac,v1,v2,v3,v4,vt,j,nb_threads,ilatm,ilatp,iradm,iradp,tracked,jtracked)            

    !$ nb_threads = omp_get_num_threads ()

    IF (rank == rank_time .and. measure_time) THEN
 !      WRITE (6,'(A)')
 !      WRITE (6,'(A)')
 !      WRITE (6,'(A)')
!$       IF (.TRUE.) THEN
!$OMP MASTER
!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!$          ptime_debut = omp_get_wtime()
!$OMP END MASTER
!$OMP BARRIER
!$       ELSE
!       print*, '--------------------------------------------------------------------------------------'
!       WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!       WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
       mtime_debut = MPI_WTime ()
!$       END IF
!       WRITE (6,'(A)')
!       WRITE (6,'(A)')
!       WRITE (6,'(A)')
    END IF

!$OMP DO SCHEDULE(DYNAMIC,size_packs) !(STATIC,ntr/nb_threads)
    do i = 1,ntr   

       rad   = tra(1,i) 
       theta = tra(2,i) 
       phi   = tra(3,i)

       if (track_tracers) then
          if (tra(NCELLPOS,i).lt.0.) then
             call check_tracked_tracer (rad,theta,phi,tracked,jtracked)
          end if
       end if

       if ((theta .lt. theta_poles) .or. ((pi-theta) .lt. theta_poles)) then !if tracer at poles
          
          x = rad*sin(theta)*cos(phi)
          y = rad*sin(theta)*sin(phi)
          z = rad*cos(theta)
          
       else
          
          thetafac = 1./rad                 ! dtheta/dt = v_theta/r
          phifac   = 1./(rad*sin(theta)) ! dphi/dt = v_phi/(r*sin(theta))
          
       end if
      
       iradm = closest_point_coord(1,1,i)
       iradp = closest_point_coord(2,1,i)
       ilatm = closest_point_coord(1,2,i)
       ilatp = closest_point_coord(2,2,i)

       ! for each scheme: if tracer is at north pole: advect in cartesian coordinates with vel_cart1
       !                  if tracer is at south pole: advect in cartesian coordinates with vel_cart2
       !                  if tracer not at poles: advec in spherical coordinates with vel_spat
       

if (tracers_advord==5) then  ! 5th order Runge-Kutta-Fehlberg-------------------------------------

       if (theta .lt. theta_poles) then ! tracer at north pole 

 if (tracers_interp2ord) then
   call v_i2_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_spat,v1)
   call v_i2_cart (x+v1(1)*a21,y+v1(2)*a21,z+v1(3)*a21,iradm,iradp,ilatm,ilatp,vel_spat,v2)
   call v_i2_cart (x+v1(1)*a31+v2(1)*a32,y+v1(2)*a31+v2(2)*a32,z+v1(3)*a31+v2(3)*a32,iradm,iradp,ilatm,ilatp,vel_spat,v3)
   call v_i2_cart (x+v1(1)*a41+v2(1)*a42+v3(1)*a43,y+v1(2)*a41+v2(2)*a42+v3(2)*a43,z+v1(3)*a41+v2(3)*a42+v3(3)*a43,iradm,iradp,ilatm,ilatp,vel_spat,v4)
   call v_i2_cart (x+v1(1)*a51+v2(1)*a52+v3(1)*a53+v4(1)*a54, &
                   y+v1(2)*a51+v2(2)*a52+v3(2)*a53+v4(2)*a54, &
                   z+v1(3)*a51+v2(3)*a52+v3(3)*a53+v4(3)*a54,iradm,iradp,ilatm,ilatp,vel_spat,v5)
   call v_i2_cart (x+v1(1)*a61+v2(1)*a62+v3(1)*a63+v4(1)*a64+v5(1)*a65, &
                   y+v1(2)*a61+v2(2)*a62+v3(2)*a63+v4(2)*a64+v5(2)*a65, &
                   z+v1(3)*a61+v2(3)*a62+v3(3)*a63+v4(3)*a64+v5(3)*a65, &
                   iradm,iradp,ilatm,ilatp,vel_spat,v6)
!    call v_i2_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_spat,v1)
!    call v_i2_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,iradm,iradp,ilatm,ilatp,vel_spat,v2)
!    call v_i2_cart (x+v2(1)*dto2,y+v2(2)*dto2,z+v2(3)*dto2,iradm,iradp,ilatm,ilatp,vel_spat,v3)
!    call v_i2_cart (x+v3(1)*dt,y+v3(2)*dt,z+v3(3)*dt,iradm,iradp,ilatm,ilatp,vel_spat,v4)
 else
   call v_i1_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v1,.true.,i)
   call v_i1_cart (x+v1(1)*a21,y+v1(2)*a21,z+v1(3)*a21,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v2,.false.,i)
   call v_i1_cart (x+v1(1)*a31+v2(1)*a32,y+v1(2)*a31+v2(2)*a32,z+v1(3)*a31+v2(3)*a32,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v3,.false.,i)
   call v_i1_cart (x+v1(1)*a41+v2(1)*a42+v3(1)*a43, &
                   y+v1(2)*a41+v2(2)*a42+v3(2)*a43, &
                   z+v1(3)*a41+v2(3)*a42+v3(3)*a43,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v4,.false.,i)
   call v_i1_cart (x+v1(1)*a51+v2(1)*a52+v3(1)*a53+v4(1)*a54, &
                   y+v1(2)*a51+v2(2)*a52+v3(2)*a53+v4(2)*a54, &
                   z+v1(3)*a51+v2(3)*a52+v3(3)*a53+v4(3)*a54,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v5,.false.,i)
   call v_i1_cart (x+v1(1)*a61+v2(1)*a62+v3(1)*a63+v4(1)*a64+v5(1)*a65, &
                   y+v1(2)*a61+v2(2)*a62+v3(2)*a63+v4(2)*a64+v5(2)*a65, &
                   z+v1(3)*a61+v2(3)*a62+v3(3)*a63+v4(3)*a64+v5(3)*a65, &
                   iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v6,.false.,i)

!    call v_i1_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v1) !v1 = velocity in cart coord
!    call v_i1_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v2)
!    call v_i1_cart (x+v2(1)*dto2,y+v2(2)*dto2,z+v2(3)*dto2,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v3)
!    call v_i1_cart (x+v3(1)*dt,y+v3(2)*dt,z+v3(3)*dt,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v4)

 end if

       else if ((pi-theta) .lt. theta_poles) then ! tracer at south pole

if (tracers_interp2ord) then
   call v_i2_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_spat,v1)
   call v_i2_cart (x+v1(1)*a21,y+v1(2)*a21,z+v1(3)*a21,iradm,iradp,ilatm,ilatp,vel_spat,v2)
   call v_i2_cart (x+v1(1)*a31+v2(1)*a32,y+v1(2)*a31+v2(2)*a32,z+v1(3)*a31+v2(3)*a32,iradm,iradp,ilatm,ilatp,vel_spat,v3)
   call v_i2_cart (x+v1(1)*a41+v2(1)*a42+v3(1)*a43,y+v1(2)*a41+v2(2)*a42+v3(2)*a43,z+v1(3)*a41+v2(3)*a42+v3(3)*a43,iradm,iradp,ilatm,ilatp,vel_spat,v4)
   call v_i2_cart (x+v1(1)*a51+v2(1)*a52+v3(1)*a53+v4(1)*a54, &
                   y+v1(2)*a51+v2(2)*a52+v3(2)*a53+v4(2)*a54, &
                   z+v1(3)*a51+v2(3)*a52+v3(3)*a53+v4(3)*a54,iradm,iradp,ilatm,ilatp,vel_spat,v5)
   call v_i2_cart (x+v1(1)*a61+v2(1)*a62+v3(1)*a63+v4(1)*a64+v5(1)*a65, &
                   y+v1(2)*a61+v2(2)*a62+v3(2)*a63+v4(2)*a64+v5(2)*a65, &
                   z+v1(3)*a61+v2(3)*a62+v3(3)*a63+v4(3)*a64+v5(3)*a65, &
                   iradm,iradp,ilatm,ilatp,vel_spat,v6)
else
   call v_i1_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v1,.true.,i)
   call v_i1_cart (x+v1(1)*a21,y+v1(2)*a21,z+v1(3)*a21,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v2,.false.,i)
   call v_i1_cart (x+v1(1)*a31+v2(1)*a32,y+v1(2)*a31+v2(2)*a32,z+v1(3)*a31+v2(3)*a32,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v3,.false.,i)
   call v_i1_cart (x+v1(1)*a41+v2(1)*a42+v3(1)*a43, &
                   y+v1(2)*a41+v2(2)*a42+v3(2)*a43, &
                   z+v1(3)*a41+v2(3)*a42+v3(3)*a43,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v4,.false.,i)
   call v_i1_cart (x+v1(1)*a51+v2(1)*a52+v3(1)*a53+v4(1)*a54, &
                   y+v1(2)*a51+v2(2)*a52+v3(2)*a53+v4(2)*a54, &
                   z+v1(3)*a51+v2(3)*a52+v3(3)*a53+v4(3)*a54,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v5,.false.,i)
   call v_i1_cart (x+v1(1)*a61+v2(1)*a62+v3(1)*a63+v4(1)*a64+v5(1)*a65, &
                   y+v1(2)*a61+v2(2)*a62+v3(2)*a63+v4(2)*a64+v5(2)*a65, &
                   z+v1(3)*a61+v2(3)*a62+v3(3)*a63+v4(3)*a64+v5(3)*a65, &
                   iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v6,.false.,i)

!   call v_i1_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v1) !v1 = velocity in cart coord
!   call v_i1_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v2)
!   call v_i1_cart (x+v2(1)*dto2,y+v2(2)*dto2,z+v2(3)*dto2,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v3)
!   call v_i1_cart (x+v3(1)*dt,y+v3(2)*dt,z+v3(3)*dt,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v4)
end if

       else ! tracer not at poles 

if (tracers_interp2ord) then
   call v_i2 (rad,theta,phi,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v1)
   call v_i2 (rad+v1(1)*a21,theta+v1(2)*a21*thetafac,phi+v1(3)*a21*phifac,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v2)
   call v_i2 (rad+v1(1)*a31+v2(1)*a32,theta+(v1(2)*a31+v2(2)*a32)*thetafac,phi+(v1(3)*a31+v2(3)*a32)*phifac,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v3)
   call v_i2 (rad+v1(1)*a41+v2(1)*a42+v3(1)*a43, &
                   theta+(v1(2)*a41+v2(2)*a42+v3(2)*a43)*thetafac, & 
                   phi+(v1(3)*a41+v2(3)*a42+v3(3)*a43)*phifac,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v4)
   call v_i2 (rad+v1(1)*a51+v2(1)*a52+v3(1)*a53+v4(1)*a54, &
              theta+(v1(2)*a51+v2(2)*a52+v3(2)*a53+v4(2)*a54)*thetafac, &
              phi+(v1(3)*a51+v2(3)*a52+v3(3)*a53+v4(3)*a54)*phifac,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v5)
   call v_i2 (rad+v1(1)*a61+v2(1)*a62+v3(1)*a63+v4(1)*a64+v5(1)*a65, &
              theta+(v1(2)*a61+v2(2)*a62+v3(2)*a63+v4(2)*a64+v5(2)*a65)*thetafac, &
              phi+(v1(3)*a61+v2(3)*a62+v3(3)*a63+v4(3)*a64+v5(3)*a65)*phifac, &
              iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v6)
!   call v_i2 (rad,theta,phi,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v1)
!   call v_i2 (rad+v1(1)*dto2,theta+v1(2)*thetafac*dto2,phi+v1(3)*phifac*dto2,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v2)
!   call v_i2 (rad+v2(1)*dto2,theta+v2(2)*thetafac*dto2,phi+v2(3)*phifac*dto2,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v3)
!   call v_i2 (rad+v3(1)*dt,theta+v3(2)*thetafac*dt,phi+v3(3)*phifac*dt,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v4)
else
   call v_i1 (rad,theta,phi,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v1,.true.,i)
   call v_i1 (rad+v1(1)*a21,theta+v1(2)*a21*thetafac,phi+v1(3)*a21*phifac,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v2,.false.,i)
   call v_i1 (rad+v1(1)*a31+v2(1)*a32,theta+(v1(2)*a31+v2(2)*a32)*thetafac,phi+(v1(3)*a31+v2(3)*a32)*phifac,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v3,.false.,i)
   call v_i1 (rad+v1(1)*a41+v2(1)*a42+v3(1)*a43, &
              theta+(v1(2)*a41+v2(2)*a42+v3(2)*a43)*thetafac, & 
              phi+(v1(3)*a41+v2(3)*a42+v3(3)*a43)*phifac,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v4,.false.,i)
   call v_i1 (rad+v1(1)*a51+v2(1)*a52+v3(1)*a53+v4(1)*a54, &
              theta+(v1(2)*a51+v2(2)*a52+v3(2)*a53+v4(2)*a54)*thetafac, &
              phi+(v1(3)*a51+v2(3)*a52+v3(3)*a53+v4(3)*a54)*phifac,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v5,.false.,i)
   call v_i1 (rad+v1(1)*a61+v2(1)*a62+v3(1)*a63+v4(1)*a64+v5(1)*a65, &
              theta+(v1(2)*a61+v2(2)*a62+v3(2)*a63+v4(2)*a64+v5(2)*a65)*thetafac, &
              phi+(v1(3)*a61+v2(3)*a62+v3(3)*a63+v4(3)*a64+v5(3)*a65)*phifac, &
              iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v6,.false.,i)

!   call v_i1 (rad,theta,phi,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v1)
!   call v_i1 (rad+v1(1)*dto2,theta+v1(2)*thetafac*dto2,phi+v1(3)*phifac*dto2,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v2)
!   call v_i1 (rad+v2(1)*dto2,theta+v2(2)*thetafac*dto2,phi+v2(3)*phifac*dto2,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v3)
!   call v_i1 (rad+v3(1)*dt,theta+v3(2)*thetafac*dt,phi+v3(3)*phifac*dt,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v4)
end if

       end if

       do j = 1,3
          vt(j) = b1*v1(j) + b3*v3(j) + b4*v4(j) + b5*v5(j) + b6*v6(j) ! + b2*v2(j)
       end do

else if (tracers_advord==4) then  ! 4nd order Runge-Kutta -------------------------------------------
          
       if (theta .lt. theta_poles) then ! tracer at north pole 

 if (tracers_interp2ord) then
    call v_i2_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_spat,v1)
    call v_i2_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,iradm,iradp,ilatm,ilatp,vel_spat,v2)
    call v_i2_cart (x+v2(1)*dto2,y+v2(2)*dto2,z+v2(3)*dto2,iradm,iradp,ilatm,ilatp,vel_spat,v3)
    call v_i2_cart (x+v3(1)*dt,y+v3(2)*dt,z+v3(3)*dt,iradm,iradp,ilatm,ilatp,vel_spat,v4) 
 else 
    call v_i1_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v1,.true.,i) !v1 = velocity in cart coord
    call v_i1_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v2,.false.,i)
    call v_i1_cart (x+v2(1)*dto2,y+v2(2)*dto2,z+v2(3)*dto2,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v3,.false.,i)
    call v_i1_cart (x+v3(1)*dt,y+v3(2)*dt,z+v3(3)*dt,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v4,.false.,i)
      
 end if

       else if ((pi-theta) .lt. theta_poles) then ! tracer at south pole

if (tracers_interp2ord) then
   call v_i2_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_spat,v1)
   call v_i2_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,iradm,iradp,ilatm,ilatp,vel_spat,v2)
   call v_i2_cart (x+v2(1)*dto2,y+v2(2)*dto2,z+v2(3)*dto2,iradm,iradp,ilatm,ilatp,vel_spat,v3)
   call v_i2_cart (x+v3(1)*dt,y+v3(2)*dt,z+v3(3)*dt,iradm,iradp,ilatm,ilatp,vel_spat,v4)
else
   call v_i1_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v1,.true.,i) !v1 = velocity in cart coord
   call v_i1_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v2,.false.,i)
   call v_i1_cart (x+v2(1)*dto2,y+v2(2)*dto2,z+v2(3)*dto2,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v3,.false.,i)
   call v_i1_cart (x+v3(1)*dt,y+v3(2)*dt,z+v3(3)*dt,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v4,.false.,i)   
end if

       else ! tracer not at poles 
          
if (tracers_interp2ord) then
   call v_i2 (rad,theta,phi,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v1)
   call v_i2 (rad+v1(1)*dto2,theta+v1(2)*thetafac*dto2,phi+v1(3)*phifac*dto2,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v2)
   call v_i2 (rad+v2(1)*dto2,theta+v2(2)*thetafac*dto2,phi+v2(3)*phifac*dto2,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v3)
   call v_i2 (rad+v3(1)*dt,theta+v3(2)*thetafac*dt,phi+v3(3)*phifac*dt,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v4)
else
   call v_i1 (rad,theta,phi,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v1,.true.,i)
   call v_i1 (rad+v1(1)*dto2,theta+v1(2)*thetafac*dto2,phi+v1(3)*phifac*dto2,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v2,.false.,i)
   call v_i1 (rad+v2(1)*dto2,theta+v2(2)*thetafac*dto2,phi+v2(3)*phifac*dto2,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v3,.false.,i)
   call v_i1 (rad+v3(1)*dt,theta+v3(2)*thetafac*dt,phi+v3(3)*phifac*dt,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v4,.false.,i)
end if

       end if
          
       do j = 1,3
          vt(j) = (v1(j)+v4(j))/6. + (v2(j)+v3(j))/3.
       end do
   

 else if (tracers_advord==2) then  ! 2nd order Runge-Kutta----------------------------------

        if (theta .lt. theta_poles) then ! tracer at north pole

if (tracers_interp2ord) then
   call v_i2_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_spat,v1)
   call v_i2_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,iradm,iradp,ilatm,ilatp,vel_spat,vt)
 !             call v_i_cart (x,y,z,vel_cart1,vel_spat,Nr_deb,Nr_fin,1,ilat_N,v1)  
                 !            call v_i_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,vel_cart1,vel_spat,Nr_deb,Nr_fin,1,ilat_N,vt)
else
   call v_i1_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,v1,.true.,i) !v1 = velocity in cart coord
   call v_i1_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,iradm,iradp,ilatm,ilatp,vel_cart1,Nr_deb,Nr_fin,1,ilat_N,vt,.false.,i)
end if

        else if ((pi-theta) .lt. theta_poles) then ! tracer at south pole
   
 if (tracers_interp2ord) then
    call v_i2_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_spat,v1)
    call v_i2_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,iradm,iradp,ilatm,ilatp,vel_spat,vt)
!           call v_i_cart (x,y,z,vel_cart2,vel_spat,Nr_deb,Nr_fin,ilat_S,Nlat,v1)  
                    !           call v_i_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,vel_cart2,vel_spat,Nr_deb,Nr_fin,ilat_S,Nlat,vt)
 else
    call v_i1_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,v1,.true.,i) !v1 = velocity in cart coord
    call v_i1_cart (x+v1(1)*dto2,y+v1(2)*dto2,z+v1(3)*dto2,iradm,iradp,ilatm,ilatp,vel_cart2,Nr_deb,Nr_fin,ilat_S,Nlat,vt,.false.,i)
 end if

         else ! tracer not at poles

if (tracers_interp2ord) then
   call v_i2 (rad,theta,phi,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v1)
   call v_i2 (rad+v1(1)*dto2,theta+v1(2)*thetafac*dto2,phi+v1(3)*phifac*dto2,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,vt)
else
   call v_i1 (rad,theta,phi,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,v1,.true.,i)
   call v_i1 (rad+v1(1)*dto2,theta+v1(2)*thetafac*dto2,phi+v1(3)*phifac*dto2,iradm,iradp,ilatm,ilatp,vel_spat,Nr_deb,Nr_fin,vt,.false.,i)
end if
!           call v_i (rad,theta,phi,vel_spat,Nr_deb,Nr_fin,v1)   
!           call v_i (rad+v1(1)*dto2,theta+v1(2)*thetafac*dto2,phi+v1(3)*phifac*dto2,vel_spat,Nr_deb,Nr_fin,vt) 
        end if

!     else if (tracers_advord==1) then  ! 1st order (Euler's method)--------------------------------
     
!        if (theta .lt. theta_poles) then ! tracer at north pole      
!           call v_i_cart (x,y,z,vel_cart1,vel_spat,Nr_deb,Nr_fin,1,ilat_N,vt)

!        else if ((pi-theta) .lt. theta_poles) then ! tracer at south pole
!           call v_i_cart (x,y,z,vel_cart2,vel_spat,Nr_deb,Nr_fin,ilat_S,Nlat,vt) 
       
!        else ! tracer not at poles
!           call v_i (rad,theta,phi,vel_spat,Nr_deb,Nr_fin,vt)

!        end if


     else
        stop 'ERROR: tracers_advord must be 1,2 or 4'
     end if


     ! now advect the tracer and adjust its coordinates

       if ((theta .lt. theta_poles) .or. (theta .gt. (pi-theta_poles))) then

          xf = x + dt * vt(1)
          yf = y + dt * vt(2)
          zf = z + dt * vt(3)
          
          !then go back to spherical coordinates and adjust tracers position
          tra(1,i) = sqrt(xf**2+yf**2+zf**2)
          tra(2,i) = acos(zf/tra(1,i))
          if (yf >= 0.) then
             tra(3,i) = acos(xf/sqrt(xf**2+yf**2))
          else
             tra(3,i) = deux_pi - acos(xf/sqrt(xf**2+yf**2)) 
          end if
          
       else

          tra(1,i) = tra(1,i) + dt * vt(1) 
          tra(2,i) = tra(2,i) + dt * vt(2) * thetafac
          tra(3,i) = tra(3,i) + dt * vt(3) * phifac
          
       end if

       ! update the position of tracked tracers 
       if (track_tracers.and.tracked) then
          tracked_tracers(1:3,jtracked) = tra(1:3,i)
          tracked_tracers(4,jtracked) = tra(COMPPOS,i)
          tracked = .false.
       end if
 
    end do
!$OMP END DO

      IF (rank == rank_time .and. measure_time) THEN
!$       IF (.TRUE.) THEN
!$OMP MASTER
!$          ptime_fin = omp_get_wtime()
!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele advect_tracers : ', ptime_fin - ptime_debut
!$OMP END MASTER
!$OMP BARRIER
!$       ELSE
           mtime_fin = MPI_WTime ()
           Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele advect_tracers : ', mtime_fin - mtime_debut
!           print*, '--------------------------------------------------------------------------------------'
!$       END IF
      END IF


!$OMP END PARALLEL

!  call MPI_BARRIER (MPI_COMM_WORLD,ier)

  if (mod(t,modulo2) == 0)  print*,rank,'before exchange',ntr

  if (track_tracers) call exchange_tracked_tracers ()
  call exchange_tracers ()  ! for tracers that cross subdomain boundaries
  call check_tracers ()   ! check for tracer outside box

!  print*,rank,'after exchange and check'

!  if (mod(t,modulo) == 0) then
!     call mpi_barrier (MPI_COMM_WORLD,ier)
     call MPI_ALLREDUCE (ntr,ntr_tot,1,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,ier)

     if (mod(t,modulo2) == 0) then
        print*,rank,'after exchange',ntr

        if (rank == 0) then
           print*
           print*,'    after advection: total number of tracers =',ntr_tot
           print*
        end if
     end if

!  call mpi_barrier (MPI_COMM_WORLD,ier)

  if (reordering .and. mod(t,Modulo_reord) == 0) then
 !    do i = 1,ntr
 !       rad   = tra(1,i)
 !       theta = tra(2,i)
 !       phi   = tra(3,i)
 !       call find_closest_radial_grid_point(rad,irad_closest,dist_rad)
 !       call find_closest_lateral_grid_point(theta,ilat_closest,dist_lat)
 !       ilong0 = 1 + floor (phi*dphi_1)
 !       dist_long = (phi - long(ilong0))*dphi_1
 !       if (dist_long .le. 0.5) then
 !          ilong_closest = ilong0
 !       else
 !          ilong_closest = max(mod(ilong0+1,nlong+1),1)
 !       end if
 !       tra(NCELLPOS,i) = real(ilong_closest + Nlong*((ilat_closest-1) + Nlat*irad_closest))
 !    end do

     call calc_cell_number ()
     
     if (rank==0) print*, 'Sorting tracers...'
     mtime_debut = MPI_WTime ()
     call MTSort (tra(:,1:ntr),ntr,ntracervar,NCELLPOS,"Ascending")
     mtime_fin = MPI_WTime ()
     if (rank==0) print*, '...successfully sorted, time elapsed:',mtime_fin - mtime_debut

     ! Now mark again the tracked tracer for quicker tracking
!     if (track_tracers) then
!        do i = 1,ntr
!           rad = tra(1,i)
!           theta = tra(2,i)
!           phi = tra(3,i)
!           call check_tracked_tracer (rad,theta,phi,tracked,jtracked)
!           if (tracked) then
!              tra(NCELLPOS,i) = -tra(NCELLPOS,i)
 !             print*,'tra(NCELLPOS,i)',tra(NCELLPOS,i)
!           end if
!        end do
!     end if
     !print*,rank,tra(NCELLPOS,1),tra(NCELLPOS,1000:1010),tra(NCELLPOS,ntr)
  end if
!  end if

end subroutine advect_tracers
!-----------------------------------------------------------------------------


subroutine v_i1 (radi,thetai,phii,irad0,irad1,ilat0,ilat1,vel,Nr_deb,Nr_fin,vi,first_pass,i)
  
!     finds r,theta,phi velocities at tracers positions
!     first-order interpolation
  use globall,only : Nlat,Nlong
  use tracerstuff
  implicit none

  integer,intent(in):: Nr_deb,Nr_fin,i
  integer,intent(inout):: irad0,irad1,ilat0,ilat1
  real,intent(in):: radi,thetai,phii,vel(3,nlong,nlat,Nr_deb:Nr_fin)
  real,intent(out):: vi(3)
  logical,intent(in) :: first_pass

  logical pole

  integer ilong0,ilong1
  real dist_rad,dist_lat,dist_long,n,rad,theta,phi,w1,w2,w3,w4,w5,w6,w7,w8


  pole = .false.

  rad = radi
  theta = thetai
  phi = phii

  if (first_pass) then ! weights are already calculated, use them!

     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)
     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)
     ilong0 = closest_point_coord(1,3,i)
     ilong1 = closest_point_coord(2,3,i)

     if (ilat0 .eq. 1 .or. ilat0 .eq. nlat) then
        stop 'ERROR, cone to narrow to advect tracers in cartesian geometry'
     end if

     w1 = weight_interp(1,i)
     w2 = weight_interp(2,i)
     w3 = weight_interp(3,i)
     w4 = weight_interp(4,i)
     w5 = weight_interp(5,i)
     w6 = weight_interp(6,i)
     w7 = weight_interp(7,i)
     w8 = weight_interp(8,i)

  else

     call check_coordinates(rad,phi,irad0,irad1)

     if (rad .lt. r(irad0) .or. rad .gt. r(irad1)) then
        call find_closest_radial_grid_point(rad,irad0,dist_rad)
     
        if (dist_rad .gt. 0) then
           irad1 = irad0 + 1
        else
           irad1 = irad0
           irad0 = irad0 - 1
           dist_rad = rad - r(irad0)
        end if

        dist_rad = dist_rad*drad1_1(irad0) !/drad1(irad0) !(r(irad1)-r(irad0))

     else
        dist_rad = (rad-r(irad0))*drad1_1(irad0)!/drad1(irad0)
     end if


     if (theta .lt. colat(ilat0) .or. theta .gt. colat(ilat1)) then
        call find_closest_lateral_grid_point(theta,ilat0,dist_lat)
     
        if (dist_lat .gt. 0) then
           if (ilat0 .eq. nlat) then
              pole = .true.
           else 
              ilat1 = ilat0 + 1
              dist_lat = dist_lat*dlat1_1(ilat1)!/dlat1(ilat1) !(colat(ilat1)-colat(ilat0))
           end if
        else 
           if (ilat0 .eq. 1) then
              pole = .true.
           else
              ilat1 = ilat0
              ilat0 = ilat0 - 1
              dist_lat = (theta-colat(ilat0))*dlat1_1(ilat1)!/dlat1(ilat1) !(colat(ilat1)-colat(ilat0))
           end if
        end if

     else
        dist_lat = (theta-colat(ilat0))*dlat1_1(ilat1)!/dlat1(ilat1)
     end if


     ilong0 = 1 + floor (phi*dphi_1)
     ilong1 = max(mod(ilong0+1,nlong+1),1)
     dist_long = (phi - long(ilong0))*dphi_1
  

     if (pole) then
        stop 'ERROR: cone too narrow for advecting tracers in cartesian coordinates'
     end if

     ! compute the weights once for all
     w1 = (1-dist_long)*(1-dist_lat)*(1-dist_rad) 
     w2 =   dist_long  *(1-dist_lat)*(1-dist_rad) 
     w3 = (1-dist_long)*  dist_lat  *(1-dist_rad) 
     w4 =   dist_long  *  dist_lat  *(1-dist_rad) 
     w5 = (1-dist_long)*(1-dist_lat)*  dist_rad   
     w6 =   dist_long  *(1-dist_lat)*  dist_rad   
     w7 = (1-dist_long)*  dist_lat  *  dist_rad  
     w8 =   dist_long  *  dist_lat  *  dist_rad 
 
  end if ! first_pass

                                ! r interpolation

!  vi(1) =  (1-dist_long)*(1-dist_lat)*(1-dist_rad)* vel(ilong0,ilat0,irad0,1) +  &
!             dist_long  *(1-dist_lat)*(1-dist_rad)* vel(ilong1,ilat0,irad0,1) +  &
!           (1-dist_long)*  dist_lat  *(1-dist_rad)* vel(ilong0,ilat1,irad0,1) +  &
!             dist_long  *  dist_lat  *(1-dist_rad)* vel(ilong1,ilat1,irad0,1) +  &
!           (1-dist_long)*(1-dist_lat)*  dist_rad  * vel(ilong0,ilat0,irad1,1) +  &
!             dist_long  *(1-dist_lat)*  dist_rad  * vel(ilong1,ilat0,irad1,1) +  &
!           (1-dist_long)*  dist_lat  *  dist_rad  * vel(ilong0,ilat1,irad1,1) +  &
!             dist_long  *  dist_lat  *  dist_rad  * vel(ilong1,ilat1,irad1,1) 

  vi(1) = w1 * vel(1,ilong0,ilat0,irad0) +  &
          w2 * vel(1,ilong1,ilat0,irad0) +  &
          w3 * vel(1,ilong0,ilat1,irad0) +  &
          w4 * vel(1,ilong1,ilat1,irad0) +  &
          w5 * vel(1,ilong0,ilat0,irad1) +  &
          w6 * vel(1,ilong1,ilat0,irad1) +  &
          w7 * vel(1,ilong0,ilat1,irad1) +  &
          w8 * vel(1,ilong1,ilat1,irad1)
  
                                ! theta interpolation

!  vi(2) =  (1-dist_long)*(1-dist_lat)*(1-dist_rad)* vel(ilong0,ilat0,irad0,2) +  &
!             dist_long  *(1-dist_lat)*(1-dist_rad)* vel(ilong1,ilat0,irad0,2) +  &
!           (1-dist_long)*  dist_lat  *(1-dist_rad)* vel(ilong0,ilat1,irad0,2) +  &
!             dist_long  *  dist_lat  *(1-dist_rad)* vel(ilong1,ilat1,irad0,2) +  &
!           (1-dist_long)*(1-dist_lat)*  dist_rad  * vel(ilong0,ilat0,irad1,2) +  &
!             dist_long  *(1-dist_lat)*  dist_rad  * vel(ilong1,ilat0,irad1,2) +  &
!           (1-dist_long)*  dist_lat  *  dist_rad  * vel(ilong0,ilat1,irad1,2) +  &
!             dist_long  *  dist_lat  *  dist_rad  * vel(ilong1,ilat1,irad1,2) 

  vi(2) = w1 * vel(2,ilong0,ilat0,irad0) +  &
          w2 * vel(2,ilong1,ilat0,irad0) +  &
          w3 * vel(2,ilong0,ilat1,irad0) +  &
          w4 * vel(2,ilong1,ilat1,irad0) +  &
          w5 * vel(2,ilong0,ilat0,irad1) +  &
          w6 * vel(2,ilong1,ilat0,irad1) +  &
          w7 * vel(2,ilong0,ilat1,irad1) +  &
          w8 * vel(2,ilong1,ilat1,irad1)
      
                                ! phi interpolation

!  vi(3) =  (1-dist_long)*(1-dist_lat)*(1-dist_rad)* vel(ilong0,ilat0,irad0,3) +  &
!             dist_long  *(1-dist_lat)*(1-dist_rad)* vel(ilong1,ilat0,irad0,3) +  &
!           (1-dist_long)*  dist_lat  *(1-dist_rad)* vel(ilong0,ilat1,irad0,3) +  &
!             dist_long  *  dist_lat  *(1-dist_rad)* vel(ilong1,ilat1,irad0,3) +  &
!           (1-dist_long)*(1-dist_lat)*  dist_rad  * vel(ilong0,ilat0,irad1,3) +  &
!             dist_long  *(1-dist_lat)*  dist_rad  * vel(ilong1,ilat0,irad1,3) +  &
!           (1-dist_long)*  dist_lat  *  dist_rad  * vel(ilong0,ilat1,irad1,3) +  &
!             dist_long  *  dist_lat  *  dist_rad  * vel(ilong1,ilat1,irad1,3)  
         
  vi(3) = w1 * vel(3,ilong0,ilat0,irad0) +  &
          w2 * vel(3,ilong1,ilat0,irad0) +  &
          w3 * vel(3,ilong0,ilat1,irad0) +  &
          w4 * vel(3,ilong1,ilat1,irad0) +  &
          w5 * vel(3,ilong0,ilat0,irad1) +  &
          w6 * vel(3,ilong1,ilat0,irad1) +  &
          w7 * vel(3,ilong0,ilat1,irad1) +  &
          w8 * vel(3,ilong1,ilat1,irad1)


!  n     =  (1-dist_long)*(1-dist_lat)*(1-dist_rad) + &
!             dist_long  *(1-dist_lat)*(1-dist_rad) + &
!           (1-dist_long)*  dist_lat  *(1-dist_rad) + &
!             dist_long  *  dist_lat  *(1-dist_rad) + &
!           (1-dist_long)*(1-dist_lat)*  dist_rad   + &
!             dist_long  *(1-dist_lat)*  dist_rad   + &
!           (1-dist_long)*  dist_lat  *  dist_rad   + &
!             dist_long  *  dist_lat  *  dist_rad  
 
!  n = w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8 !THESE OPERATIONS ARE USELESS because n is always 1 by construction !

!  vi(1) = vi(1)/n
!  vi(2) = vi(2)/n
!  vi(3) = vi(3)/n

      
end subroutine v_i1
!------------------------------------------------------------------------------


subroutine v_i1_cart (x,y,z,irad0,irad1,ilat0,ilat1,vel,Nr_deb,Nr_fin,Nlat_deb,Nlat_fin,vi,first_pass,i)
  
!     finds x,y,z velocities at tracers positions 
!     first-order interpolation
 
  use tracerstuff
  use globall,only : Mc,Nlat,Nlong
  implicit none

  integer,intent(in):: Nr_deb,Nr_fin,Nlat_deb,Nlat_fin,i
  integer,intent(inout):: irad0,irad1,ilat0,ilat1
  real,intent(in):: x,y,z,vel(3,nlong_cart,Nlat_deb:Nlat_fin,Nr_deb:Nr_fin)
  real,intent(out):: vi(3)
  logical,intent(in) :: first_pass

  logical pole

  integer ilong0,ilong1
  real dist_rad,dist_lat,dist_long,n,dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8,rad,theta,phi,&
       w1,w2,w3,w4,w5,w6,w7,w8


  pole = .false.

  rad = sqrt(x**2+y**2+z**2)
  theta = acos(z/rad)
  if (y >= 0.) then
     phi = acos(x/max(sqrt(x**2+y**2),1e-6))
  else
     phi = deux_pi - acos(x/max(sqrt(x**2+y**2),1e-6)) 
  end if

  if (first_pass) then ! weights are already computed, use them directly!

     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)
     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)
     ilong0 = closest_point_coord(1,3,i)
     ilong1 = closest_point_coord(2,3,i)

!     if (ilat0 .eq. 1 .or. ilat0 .eq. nlat) pole = .true.
     if (ilat1 .eq. 1 .or. ilat0 .eq. nlat) pole = .true.    

  else 

     call check_coordinates_poles(rad,theta,phi,irad0,irad1) 


     if (rad .lt. r(irad0) .or. rad .gt. r(irad1)) then

        call find_closest_radial_grid_point(rad,irad0,dist_rad)
     
        if (dist_rad .gt. 0) then
           irad1 = irad0 + 1
        else
           irad1 = irad0
           irad0 = irad0 - 1
           dist_rad = rad - r(irad0)
        end if
        dist_rad = dist_rad*drad1_1(irad0) !/(r(irad1)-r(irad0))
 
     else
        dist_rad = (rad - r(irad0))*drad1_1(irad0) !/(r(irad1)-r(irad0))
     end if


     if (theta .lt. colat(ilat0) .or. theta .gt. colat(ilat1)) then

        call find_closest_lateral_grid_point(theta,ilat0,dist_lat)
     
        if (dist_lat .gt. 0) then
           if (ilat0 .eq. nlat) then
              pole = .true.
           else 
              ilat1 = ilat0 + 1
              dist_lat = dist_lat*dlat1_1(ilat1) !/(colat(ilat1)-colat(ilat0))
           end if
        else 
           if (ilat0 .eq. 1) then
              pole = .true.
           else
              ilat1 = ilat0
              ilat0 = ilat0 - 1
              dist_lat = theta - colat(ilat0)
              dist_lat = dist_lat*dlat1_1(ilat1) !/(colat(ilat1)-colat(ilat0))
           end if
        end if

     else
        dist_lat = (theta-colat(ilat0))*dlat1_1(ilat1) !/(colat(ilat1)-colat(ilat0))
     end if

     ilong0 = 1 + floor (phi*dphi_1)
     ilong1 = max(mod(ilong0+1,nlong_cart+1),1)
     if (ilong0 .le. 0) print*,'WARNING',rank,x,y,z
     dist_long = (phi-long(ilong0))*dphi_1

  end if ! first_pass

  if (irad0 .lt. ideb_veltra-2) print*,'WARNING v_i1_cart, tracer out of local domain',first_pass,rad,theta,phi,irad0
  if (irad1 .gt. ifin_veltra+2) print*,'WARNING v_i1_cart, tracer out of local domain',first_pass,rad,theta,phi,irad1 

  if (.not. pole) then

!   call distance(irad0,ilat0,ilong0,rad,theta,phi,dist1)
!   call distance(irad1,ilat0,ilong0,rad,theta,phi,dist2)
!   call distance(irad0,ilat1,ilong0,rad,theta,phi,dist3)
!   call distance(irad1,ilat1,ilong0,rad,theta,phi,dist4)
!   call distance(irad0,ilat0,ilong1,rad,theta,phi,dist5)
!   call distance(irad1,ilat0,ilong1,rad,theta,phi,dist6)
!   call distance(irad0,ilat1,ilong1,rad,theta,phi,dist7)
!   call distance(irad1,ilat1,ilong1,rad,theta,phi,dist8)

!   if (dist1 .eq. 0.) dist1 = 1e-10
!   if (dist2 .eq. 0.) dist2 = 1e-10
!   if (dist3 .eq. 0.) dist3 = 1e-10
!   if (dist4 .eq. 0.) dist4 = 1e-10
!   if (dist5 .eq. 0.) dist5 = 1e-10
!   if (dist6 .eq. 0.) dist6 = 1e-10
!   if (dist7 .eq. 0.) dist7 = 1e-10
!   if (dist8 .eq. 0.) dist8 = 1e-10

!   w1 = 1/dist1
!   w2 = 1/dist2
!   w3 = 1/dist3
!   w4 = 1/dist4
!   w5 = 1/dist5
!   w6 = 1/dist6
!   w7 = 1/dist7
!   w8 = 1/dist8

     if (first_pass) then
     
        w1 = weight_interp(1,i)
        w2 = weight_interp(2,i)
        w3 = weight_interp(3,i)
        w4 = weight_interp(4,i)
        w5 = weight_interp(5,i)
        w6 = weight_interp(6,i)
        w7 = weight_interp(7,i)
        w8 = weight_interp(8,i)

     else
  
        w1 = (1-dist_long)*(1-dist_lat)*(1-dist_rad)
        w2 =   dist_long  *(1-dist_lat)*(1-dist_rad)
        w3 = (1-dist_long)*  dist_lat  *(1-dist_rad)
        w4 =   dist_long  *  dist_lat  *(1-dist_rad)
        w5 = (1-dist_long)*(1-dist_lat)*  dist_rad 
        w6 =   dist_long  *(1-dist_lat)*  dist_rad 
        w7 = (1-dist_long)*  dist_lat  *  dist_rad 
        w8 =   dist_long  *  dist_lat  *  dist_rad

     end if
                                ! x interpolation

!  vi(1) =  (1-dist_rad)*(1-dist_lat)*(1-dist_long)* vel(irad0,ilat0,ilong0,1) +  &
!             dist_rad  *(1-dist_lat)*(1-dist_long)* vel(irad1,ilat0,ilong0,1) +  &
!           (1-dist_rad)*  dist_lat  *(1-dist_long)* vel(irad0,ilat1,ilong0,1) +  &
!             dist_rad  *  dist_lat  *(1-dist_long)* vel(irad1,ilat1,ilong0,1) +  &
!           (1-dist_rad)*(1-dist_lat)*  dist_long  * vel(irad0,ilat0,ilong1,1) +  &
!             dist_rad  *(1-dist_lat)*  dist_long  * vel(irad1,ilat0,ilong1,1) +  &
!           (1-dist_rad)*  dist_lat  *  dist_long  * vel(irad0,ilat1,ilong1,1) +  &
!             dist_rad  *  dist_lat  *  dist_long  * vel(irad1,ilat1,ilong1,1) 
 
  vi(1) =  w1 * vel(1,ilong0,ilat0,irad0) +  &
           w2 * vel(1,ilong1,ilat0,irad0) +  &
           w3 * vel(1,ilong0,ilat1,irad0) +  &
           w4 * vel(1,ilong1,ilat1,irad0) +  &
           w5 * vel(1,ilong0,ilat0,irad1) +  &
           w6 * vel(1,ilong1,ilat0,irad1) +  &
           w7 * vel(1,ilong0,ilat1,irad1) +  &
           w8 * vel(1,ilong1,ilat1,irad1)     
  
                                ! y interpolation


!  vi(2) =  (1-dist_rad)*(1-dist_lat)*(1-dist_long)* vel(irad0,ilat0,ilong0,2) +  &
!             dist_rad  *(1-dist_lat)*(1-dist_long)* vel(irad1,ilat0,ilong0,2) +  &
!           (1-dist_rad)*  dist_lat  *(1-dist_long)* vel(irad0,ilat1,ilong0,2) +  &
!             dist_rad  *  dist_lat  *(1-dist_long)* vel(irad1,ilat1,ilong0,2) +  &
!           (1-dist_rad)*(1-dist_lat)*  dist_long  * vel(irad0,ilat0,ilong1,2) +  &
!             dist_rad  *(1-dist_lat)*  dist_long  * vel(irad1,ilat0,ilong1,2) +  &
!           (1-dist_rad)*  dist_lat  *  dist_long  * vel(irad0,ilat1,ilong1,2) +  &
!             dist_rad  *  dist_lat  *  dist_long  * vel(irad1,ilat1,ilong1,2) 

  vi(2) =  w1 * vel(2,ilong0,ilat0,irad0) +  &
           w2 * vel(2,ilong1,ilat0,irad0) +  &
           w3 * vel(2,ilong0,ilat1,irad0) +  &
           w4 * vel(2,ilong1,ilat1,irad0) +  &
           w5 * vel(2,ilong0,ilat0,irad1) +  &
           w6 * vel(2,ilong1,ilat0,irad1) +  &
           w7 * vel(2,ilong0,ilat1,irad1) +  &
           w8 * vel(2,ilong1,ilat1,irad1) 

                                ! z interpolation


!  vi(3) =  (1-dist_rad)*(1-dist_lat)*(1-dist_long)* vel(irad0,ilat0,ilong0,3) +  &
!             dist_rad  *(1-dist_lat)*(1-dist_long)* vel(irad1,ilat0,ilong0,3) +  &
!           (1-dist_rad)*  dist_lat  *(1-dist_long)* vel(irad0,ilat1,ilong0,3) +  &
!             dist_rad  *  dist_lat  *(1-dist_long)* vel(irad1,ilat1,ilong0,3) +  &
!           (1-dist_rad)*(1-dist_lat)*  dist_long  * vel(irad0,ilat0,ilong1,3) +  &
!             dist_rad  *(1-dist_lat)*  dist_long  * vel(irad1,ilat0,ilong1,3) +  &
!           (1-dist_rad)*  dist_lat  *  dist_long  * vel(irad0,ilat1,ilong1,3) +  &
!             dist_rad  *  dist_lat  *  dist_long  * vel(irad1,ilat1,ilong1,3) 

  vi(3) =  w1 * vel(3,ilong0,ilat0,irad0) +  &
           w2 * vel(3,ilong1,ilat0,irad0) +  &
           w3 * vel(3,ilong0,ilat1,irad0) +  &
           w4 * vel(3,ilong1,ilat1,irad0) +  &
           w5 * vel(3,ilong0,ilat0,irad1) +  &
           w6 * vel(3,ilong1,ilat0,irad1) +  &
           w7 * vel(3,ilong0,ilat1,irad1) +  &
           w8 * vel(3,ilong1,ilat1,irad1)  
         

! n      =  (1-dist_rad)*(1-dist_lat)*(1-dist_long) +  &
!             dist_rad  *(1-dist_lat)*(1-dist_long) +  &
!           (1-dist_rad)*  dist_lat  *(1-dist_long) +  &
!             dist_rad  *  dist_lat  *(1-dist_long) +  &
!           (1-dist_rad)*(1-dist_lat)*  dist_long   +  &
!             dist_rad  *(1-dist_lat)*  dist_long   +  &
!           (1-dist_rad)*  dist_lat  *  dist_long   +  &
!             dist_rad  *  dist_lat  *  dist_long  

! n      =  w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8
      
  
   else !if poles then special treatment: average with points opposite to poles

      call distance(irad0,ilat0,ilong0,rad,theta,phi,dist1)
      call distance(irad1,ilat0,ilong0,rad,theta,phi,dist2)
      call distance(irad0,ilat0,ilong1,rad,theta,phi,dist3)
      call distance(irad1,ilat0,ilong1,rad,theta,phi,dist4)
      
      if (dist1 .eq. 0.) dist1 = 1e-10
      if (dist2 .eq. 0.) dist2 = 1e-10
      if (dist3 .eq. 0.) dist3 = 1e-10
      if (dist4 .eq. 0.) dist4 = 1e-10
      
      w1 = 1./dist1
      w2 = 1./dist2
      w3 = 1./dist3
      w4 = 1./dist4
      
      if (mod(nlong_cart,2) .eq. 0) then
         
         call distance(irad0,ilat0,mod(ilong0+nlong_cart/2-1,nlong_cart)+1,rad,theta,phi,dist5)
         call distance(irad0,ilat0,mod(ilong0+nlong_cart/2,nlong_cart)+1,rad,theta,phi,dist6)
         call distance(irad1,ilat0,mod(ilong0+nlong_cart/2-1,nlong_cart)+1,rad,theta,phi,dist7)
         call distance(irad1,ilat0,mod(ilong0+nlong_cart/2,nlong_cart)+1,rad,theta,phi,dist8)
         
         w5 = 1./dist5
         w6 = 1./dist6
         w7 = 1./dist7
         w8 = 1./dist8
         
      else 
         
         call distance(irad0,ilat0,mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1,rad,theta,phi,dist5)
         call distance(irad1,ilat0,mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1,rad,theta,phi,dist6)
         dist5 = 0.5*dist5
         dist6 = 0.5*dist6
         
         w5 = 1./dist5
         w6 = 1./dist6
         
      end if
      
      
      if (mod(nlong_cart,2) .eq. 0) then
         
         vi(1) =  w1 * vel(1,ilong0,ilat0,irad0) +  &
                  w2 * vel(1,ilong1,ilat0,irad0) +  &
                  w3 * vel(1,ilong0,ilat0,irad1) +  &
                  w4 * vel(1,ilong1,ilat0,irad1) +  &
                  w5 * vel(1,mod(ilong0+nlong_cart/2-1,nlong_cart)+1,ilat0,irad0) +  &
                  w6 * vel(1,mod(ilong0+nlong_cart/2,nlong_cart)+1,ilat0,irad0)   +  &
                  w7 * vel(1,mod(ilong0+nlong_cart/2-1,nlong_cart)+1,ilat0,irad1) +  &
                  w8 * vel(1,mod(ilong0+nlong_cart/2,nlong_cart)+1,ilat0,irad1)   
         
         vi(2) =  w1 * vel(2,ilong0,ilat0,irad0) +  &
                  w2 * vel(2,ilong1,ilat0,irad0) +  &
                  w3 * vel(2,ilong0,ilat0,irad1) +  &
                  w4 * vel(2,ilong1,ilat0,irad1) +  &
                  w5 * vel(2,mod(ilong0+nlong_cart/2-1,nlong_cart)+1,ilat0,irad0) +  &
                  w6 * vel(2,mod(ilong0+nlong_cart/2,nlong_cart)+1,ilat0,irad0)   +  &
                  w7 * vel(2,mod(ilong0+nlong_cart/2-1,nlong_cart)+1,ilat0,irad1) +  &
                  w8 * vel(2,mod(ilong0+nlong_cart/2,nlong_cart)+1,ilat0,irad1)   

         vi(3) =  w1 * vel(3,ilong0,ilat0,irad0) +  &
                  w2 * vel(3,ilong1,ilat0,irad0) +  &
                  w3 * vel(3,ilong0,ilat0,irad1) +  &
                  w4 * vel(3,ilong1,ilat0,irad1) +  &
                  w5 * vel(3,mod(ilong0+nlong_cart/2-1,nlong_cart)+1,ilat0,irad0) +  &
                  w6 * vel(3,mod(ilong0+nlong_cart/2,nlong_cart)+1,ilat0,irad0)   +  &
                  w7 * vel(3,mod(ilong0+nlong_cart/2-1,nlong_cart)+1,ilat0,irad1) +  &
                  w8 * vel(3,mod(ilong0+nlong_cart/2,nlong_cart)+1,ilat0,irad1)   


         n = 1./(w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8)
         
      else 
         
         vi(1) =  w1 * vel(1,ilong0,ilat0,irad0) +  &
                  w2 * vel(1,ilong1,ilat0,irad0) +  &
                  w3 * vel(1,ilong0,ilat0,irad1) +  &
                  w4 * vel(1,ilong1,ilat0,irad1) +  &
                  w5 * vel(1,mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1,ilat0,irad0) +  &
                  w6 * vel(1,mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1,ilat0,irad1) 

         vi(2) =  w1 * vel(2,ilong0,ilat0,irad0) +  &
                  w2 * vel(2,ilong1,ilat0,irad0) +  &
                  w3 * vel(2,ilong0,ilat0,irad1) +  &
                  w4 * vel(2,ilong1,ilat0,irad1) +  &
                  w5 * vel(2,mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1,ilat0,irad0) +  &
                  w6 * vel(2,mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1,ilat0,irad1)

         vi(3) =  w1 * vel(3,ilong0,ilat0,irad0) +  &
                  w2 * vel(3,ilong1,ilat0,irad0) +  &
                  w3 * vel(3,ilong0,ilat0,irad1) +  &
                  w4 * vel(3,ilong1,ilat0,irad1) +  &
                  w5 * vel(3,mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1,ilat0,irad0) +  &
                  w6 * vel(3,mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1,ilat0,irad1)     


         n = 1./(w1 + w2 + w3 + w4 + w5 + w6) 

      end if

   vi(1) = vi(1)*n
   vi(2) = vi(2)*n
   vi(3) = vi(3)*n

   end if


      
end subroutine v_i1_cart
!------------------------------------------------------------------------------


subroutine v_i2 (radi,thetai,phii,iradm,iradp,ilatm,ilatp,vel,Nr_deb,Nr_fin,vi)
  
!     finds r,theta,phi velocities at tracers positions
!     bi-linear interpolation in the direction perpendicular to the velocity
!     quadratic interpolation in direction parallel to velocity

  use globall,only : Nlat,Nlong
  use tracerstuff
  implicit none

  integer,intent(in):: Nr_deb,Nr_fin
  real,intent(in):: radi,thetai,phii,vel(3,nlong,nlat,Nr_deb:Nr_fin)
  real,intent(out):: vi(3)
  integer,intent(inout) :: iradm,iradp,ilatm,ilatp

  integer irad0,ilat0,ilong0,irad1,ilat1,ilong1,irad2,ilat2,ilong2,ilongm,ilongp,i
  integer dir(3)

  real dist_rad,dist_lat,dist_long,n,rad,theta,phi


  rad = radi
  theta = thetai
  phi = phii

  call check_coordinates(rad,phi,iradm,iradp) 

!------------------------------------------------------------
  ! find 3 closest grid points in r

  if (rad .lt. r(iradm) .or. rad .gt. r(iradp)) then !relocate tracer only if necessary

     call find_closest_radial_grid_point(rad,irad0,dist_rad)
     
     ! 3 closest points used for quadratic interpolation 
     if (irad0 .le. ideb_veltra-2) then ! Careful about mpi-domains lower boundaries !
        print*,'irad0 < ideb_veltra-2 on rank',rank,irad0,ideb_veltra-2
        irad0 = ideb_veltra-2
        irad1 = irad0 + 1
        irad2 = irad0 + 2
     else if (irad0 .ge. ifin_veltra+2) then ! Careful about mpi-domains upper boundaries !
        print*,'irad0 > ifin_veltra+2 on rank',rank,irad0,ifin_veltra-2
        irad2 = ifin_veltra+2
        irad1 = irad2 - 1
        irad0 = irad2 - 2
     else
        irad2 = irad0 + 1
        irad1 = irad0
        irad0 = irad1 - 1
     end if
     
     ! 2 closest points used for linear interpolation 
     if (dist_rad .gt. 0.) then
        iradm = irad1
        iradp = irad2
     else
        iradm = irad0
        iradp = irad1
     end if
     
  else

     if (rad-r(iradm) .lt. r(iradp)-rad) then
        irad0 = max(ideb_veltra-2,iradm-1)
        irad1 = irad0+1
        irad2 = irad0+2
     else
        irad2 = min(ifin_veltra+2,iradp+1)
        irad1 = irad2-1
        irad0 = irad2-2
     end if
     
  end if
 
 dist_rad = (rad - r(iradm))*drad1_1(iradm)!/(r(iradp)-r(iradm))


  ! find 3 closest grid points in theta
 if (theta .lt. colat(ilatm) .or. theta .gt. colat(ilatp)) then !relocate tracer only if necessary

    call find_closest_lateral_grid_point(theta,ilat0,dist_lat)
    
    !   3 closest points used for quadratic interpolation
    ilat2 = ilat0 + 1
    ilat1 = ilat0
    ilat0 = ilat1 - 1
    
    !   3 closest points for linear interpolation
    if (dist_lat .gt. 0.) then
       ilatm = ilat1
       ilatp = ilat2
    else
       ilatm = ilat0
       ilatp = ilat1
    end if

 else
    
    if (theta-colat(ilatm) .lt. colat(ilatp)-theta) then
       ilat0 = ilatm-1
       ilat1 = ilatm
       ilat2 = ilatp
    else
       ilat0 = ilatm
       ilat1 = ilatp
       ilat2 = ilatp+1
    end if
   
 end if
  
 dist_lat = (theta - colat(ilatm))*dlat1_1(ilatp)!/(colat(ilatp)-colat(ilatm))
     
   !  poles ! => not possible normally because the cone in which advection is performed
   !             in cartesian coord is supposed to be large enough 
   if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) then
      stop 'ERROR: cone too narrow for advecting tracers in cartesian coordinates'
   end if
     

   ! find 3 closest grid points in phi for quadratic interpolation (and 2 closest for linear interpolation)
   !IL DOIT Y AVOIR MOYEN DE CONTRACTER LA PARTIE CI-DESSOUS
   ilong0 = 1 + floor (phi*dphi_1)
   if (phi-long(ilong0) .gt. dphi*0.5) then
      ilong1 = max(mod(ilong0+1,nlong+1),1)
      ilong2 = max(mod(ilong1+1,nlong+1),1)
      ilongm = ilong0
      ilongp = ilong1
   else
      ilong1 = ilong0
      ilong2 = max(mod(ilong1+1,nlong+1),1)
      ilong0 = ilong1-1
      if (ilong0 == 0) ilong0 = Nlong
      ilongm = ilong1
      ilongp = ilong2
   end if

   dist_long = (phi - long(ilongm))*dphi_1

   if (phi <= dphi*0.5) phi = phi + deux_pi !CAREFUL ABOUT WRAPING FOR MATRIX INVERSE in quadratic_interpolation_p
     

   !Interpolation

   if (.not. triquadratic_interpolation) then
      
      !first, set the direction of quadratic interpolation for each component of the velocity
! CAREFUL if Mc>1; DIRECTION IS NOT DUPLICATED IN LONGITUDE
      dir(1) = direction_2ndord_Interp(1,max(mod(ilong1,Nlong),1),ilat1,irad1)
      dir(2) = direction_2ndord_Interp(2,max(mod(ilong1,Nlong),1),ilat1,irad1)
      dir(3) = direction_2ndord_Interp(3,max(mod(ilong1,Nlong),1),ilat1,irad1)
      
      do i = 1,3 !Vr,Vt,Vp
         
         if (dir(i) .eq. 1) then !interpolation along the r direction
            call quadratic_interpolation_r (irad0,irad1,irad2,ilatm,ilatp,ilongm,ilongp,dist_lat,dist_long,rad,vel, &
                 Nr_deb,Nr_fin,Nlat,Nlong,vi,i)
         else if (dir(i) .eq. 2) then !interpolation along the theta direction
            call quadratic_interpolation_t (ilat0,ilat1,ilat2,iradm,iradp,ilongm,ilongp,dist_rad,dist_long,theta,vel, &
                 Nr_deb,Nr_fin,Nlat,Nlong,vi,i)
         else if (dir(i) .eq. 3) then!interpolation along the phi direction
            phi = phi - long(ilong0) ! degeneracy in longtitude
            call quadratic_interpolation_p (ilong0,ilong1,ilong2,iradm,iradp,ilatm,ilatp,dist_rad,dist_lat,phi,vel, &
                 Nr_deb,Nr_fin,Nlat,Nlong,vi,i)
         else
            print*,'ERROR, no direction of interpolation !, rank',rank
         end if
         
      end do

   else ! triquadratic interpolation

      ! Degeneracy in longitude
      phi = phi - long(ilong0)
      call triQ_interpolation (rad,theta,phi,irad1,ilat1,vi)

   end if


end subroutine v_i2
!-----------------------------------------------------------------------------


subroutine v_i2_cart (x,y,z,iradm,iradp,ilatm,ilatp,vel,vi)
  
!     finds r,theta,phi velocities at tracers positions
!     bi-linear interpolation in the direction perpendicular to the velocity
!     quadratic interpolation parallel to velocity
!     CAREFUL: AT POLES QUADRATIC in r but LINEAR in theta and phi !!
!     interpolation performed in spherical coordinates, then velocity converted in cartesian coordinates 
!     only at the end

  use globall
  use tracerstuff
  implicit none

  real,intent(in):: x,y,z,vel(3,nlong_cart,Nlat,ideb_veltra-2:ifin_veltra+2) !velocity input in SPHERICAL coordinates
  real,intent(out):: vi(3) !velocity output in CARTESIAN coordinates
  integer,intent(inout) :: iradm,iradp,ilatm,ilatp

  logical pole,flag

  integer irad0,ilat0,ilong0,irad1,ilat1,ilong1,irad2,ilat2,ilong2,ilong3,&
           ilongm,ilongp,i,ipole
  integer dir(3)

  real dist_rad,dist_lat,dist_long,n,rad,theta,phi,w1,w2,w3,w4,w5,w6,w7,w8, &
       dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8,vs1,vs2,vs3
  real x2,y2,sinth,costh,sinph,cosph,phi_deg
  real A_1(3,3),Coeff(3),B(3)


  pole = .false.

  x2 = x**2
  y2 = y**2

  rad = sqrt(x2+y2+z**2)
  theta = acos(z/rad)
  if (theta == 0.) then !arbitrary moving the tracer to avoid tracer exactly at pole
     theta = colat(1)/100.
     phi = pi
  else
     if (y >= 0.) then
        phi = acos(x/sqrt(x2+y2))
     else
        phi = deux_pi - acos(x/sqrt(x2+y2)) 
     end if
  end if

  call check_coordinates_poles(rad,theta,phi,iradm,iradp) 


  ! find 3 closest points in r
  if (rad .lt. r(iradm) .or. rad .gt. r(iradp)) then !relocate tracer only if necessary

     call find_closest_radial_grid_point(rad,irad0,dist_rad)
     
     ! 3 closest points used for quadratic interpolation 
     if (irad0 .le. ideb_veltra-2) then ! Careful about mpi-domains lower boundaries !
        irad0 = ideb_veltra-2
        irad1 = irad0 + 1
        irad2 = irad0 + 2
     else if (irad0 .ge. ifin_veltra+2) then ! Careful about mpi-domains upper boundaries !
        irad2 = ifin_veltra+2
        irad1 = irad2 - 1
        irad0 = irad2 - 2
     else
        irad2 = irad0 + 1
        irad1 = irad0
        irad0 = irad1 - 1
     end if
     
     ! 2 closest points used for linear interpolation
     if (dist_rad .gt. 0.) then
        iradm = irad1
        iradp = irad2
     else
        iradm = irad0
        iradp = irad1
     end if

  else

     if (rad-r(iradm) .lt. r(iradp)-rad) then
        irad0 = max(ideb_veltra-2,iradm-1)
        irad1 = irad0+1
        irad2 = irad0+2
     else
        irad2 = min(ifin_veltra+2,iradp+1)
        irad1 = irad2-1
        irad0 = irad2-2
     end if
     
  end if

  dist_rad = (rad - r(iradm))*drad1_1(iradm)!/(r(iradp)-r(iradm))



  ! find 3 closest points in theta
  if (theta .lt. colat(ilatm) .or. theta .gt. colat(ilatp)) then !relocate only if necessary
     
     call find_closest_lateral_grid_point(theta,ilat0,dist_lat)
     
     if (ilat0 .eq. 1) then
        if (dist_lat .gt. 0.) then
           ! tracer between points ilatm=1 and ilatp=2, 3 closest: ilat0= 1, ilat1 = 2, ilat2 = 3
           ilat0 = 1
           ilat1 = 2
           ilat2 = 3
           ilatm = 1
           ilatp = 2
           dist_lat = (theta - colat(ilatm))*dlat1_1(ilatp) !/(colat(ilatp)-colat(ilatm))
        else
           ! tracer between pole and first point in latitude: linear treatment
           pole = .true.
        end if
     else if (ilat0 .eq. Nlat) then 
        if (dist_lat .lt. 0.) then
           ! tracer between points ilatm=Nlat-1 and ilatp=Nlat, 3 closest: ilat0,1,2 = Nlat-2, Nlat-1, Nlat
           ilat0 = Nlat - 2
           ilat1 = Nlat - 1
           ilat2 = Nlat 
           ilatm = Nlat - 1
           ilatp = Nlat
           dist_lat = (theta - colat(ilatm))*dlat1_1(ilatp) !/(colat(ilatp)-colat(ilatm))
        else
           ! tracer between last point in latitude and pole: linear treatment
           pole = .true.
        end if
     else
        ! tracer far from poles
        ilat2 = ilat0 + 1
        ilat1 = ilat0
        ilat0 = ilat1 - 1
        
        if (dist_lat .gt. 0) then
           ilatm = ilat1
           ilatp = ilat2
        else
           ilatm = ilat0
           ilatp = ilat1
        end if

        dist_lat = (theta - colat(ilatm))*dlat1_1(ilatp) !/(colat(ilatp)-colat(ilatm))
     end if
     
  else
    
    if (theta-colat(ilatm) .lt. colat(ilatp)-theta) then
       ilat0 = max(1,ilatm-1)
       ilat1 = ilat0+1
       ilat2 = ilat0+2
    else
       ilat2 = min(Nlat,ilatp+1)
       ilat1 = ilat2-1
       ilat0 = ilat2-2
    end if
   
    dist_lat = (theta - colat(ilatm))*dlat1_1(ilatp) !/(colat(ilatp)-colat(ilatm))

 end if

     
   ! find 3 closest grid points in phi for quadratic interpolation (and 2 closest for linear interpolation)
   ilong0 = 1 + floor (phi*dphi_1)
   if (phi-long(ilong0) .gt. dphi*0.5) then
      ilong1 = max(mod(ilong0+1,nlong_cart+1),1)
      ilong2 = max(mod(ilong1+1,nlong_cart+1),1)
      ilongm = ilong0
      ilongp = ilong1
   else
      ilong1 = ilong0
      ilong2 = max(mod(ilong1+1,nlong_cart+1),1)
      ilong0 = ilong1-1
      if (ilong0 == 0) ilong0 = Nlong_cart
      ilongm = ilong1
      ilongp = ilong2
   end if

   dist_long = (phi - long(ilongm))*dphi_1

   if (phi <= dphi*0.5) phi = phi + deux_pi !CAREFUL ABOUT WRAPING FOR MATRIX INVERSE in quadratic_interpolation_p

     

   if (.not. pole) then !quadratic in the direction // to velocity, bilinear in the perpendicular directions

      if (.not. triquadratic_interpolation) then
         
         dir(1) = direction_2ndord_Interp(1,ilong1,ilat1,irad1)
         dir(2) = direction_2ndord_Interp(2,ilong1,ilat1,irad1)
         dir(3) = direction_2ndord_Interp(3,ilong1,ilat1,irad1)
         
         
         do i = 1,3 !Vr,Vt,Vp
            
            if (dir(i) .eq. 1) then !interpolation along the r direction
               call quadratic_interpolation_r (irad0,irad1,irad2,ilatm,ilatp,ilongm,ilongp,dist_lat,dist_long,rad,vel, &
                    ideb_veltra-2,ifin_veltra+2,Nlat,Nlong,vi,i)
            else if (dir(i) .eq. 2) then !interpolation along the theta direction
               call quadratic_interpolation_t (ilat0,ilat1,ilat2,iradm,iradp,ilongm,ilongp,dist_rad,dist_long,theta,vel, &
                    ideb_veltra-2,ifin_veltra+2,Nlat,Nlong,vi,i)
            else !interpolation along the phi direction 
               phi_deg = phi - long(ilong0) ! degeneracy in longitude...   
               call quadratic_interpolation_p (ilong0,ilong1,ilong2,iradm,iradp,ilatm,ilatp,dist_rad,dist_lat,phi_deg,vel, &
                    ideb_veltra-2,ifin_veltra+2,Nlat,Nlong,vi,i)
            end if
            
         end do

      else ! triquadratic interpolation

         ! Degeneracy in longitude
         phi_deg = phi - long(ilong0)
         call triQ_interpolation (rad,theta,phi_deg,irad1,ilat1,vi)

      end if

      ! now convert the velocity back to cartesian coordinates
      vs1 = vi(1)
      vs2 = vi(2)
      vs3 = vi(3)
      
      !vi = Psc*vs = velocity in cartesian coordinates
      sinth = sin(theta)
      costh = cos(theta)
      sinph = sin(phi)
      cosph = cos(phi)
      
      vi(1) = sinth*cosph * vs1 + &
              costh*cosph * vs2 + &
                  -sinph  * vs3
      
      vi(2) = sinth*sinph * vs1 + &
              costh*sinph * vs2 + &
                   cosph  * vs3
      
      vi(3) = costh  * vs1 + &
              -sinth * vs2 
      

   else !LINEAR IN ALL DIRECTIONS!quadratic in r, linear for theta and phi

      !determines which pole the tracer is in
      if (ilat0 .eq. 1) then 
         ipole = 1 !North Pole
      else 
         ipole = 2 !South Pole
      end if

      if (mod(nlong_cart,2) .eq. 0) then !Nlong_cart is even

         !points on the other side of the pole
         ilong2 = mod(ilongm+nlong_cart/2-1,nlong_cart)+1
         ilong3 = mod(ilongm+nlong_cart/2,nlong_cart)+1    
        
         ! Computes weights for the trilinear interpolation
         call distance(iradm,ilat0,ilongm,rad,theta,phi,dist1)
         call distance(iradm,ilat0,ilongp,rad,theta,phi,dist2)
         call distance(iradp,ilat0,ilongm,rad,theta,phi,dist3)
         call distance(iradp,ilat0,ilongp,rad,theta,phi,dist4)
         
         if (dist1 .eq. 0.) dist1 = 1e-10
         if (dist2 .eq. 0.) dist2 = 1e-10
         if (dist3 .eq. 0.) dist3 = 1e-10
         if (dist4 .eq. 0.) dist4 = 1e-10
         
         call distance(iradm,ilat0,ilong2,rad,theta,phi,dist5)
         call distance(iradm,ilat0,ilong3,rad,theta,phi,dist6)
         call distance(iradp,ilat0,ilong2,rad,theta,phi,dist7)
         call distance(iradp,ilat0,ilong3,rad,theta,phi,dist8)
         
         w1 = 1./dist1
         w2 = 1./dist2
         w3 = 1./dist3
         w4 = 1./dist4
         w5 = 1./dist5
         w6 = 1./dist6
         w7 = 1./dist7
         w8 = 1./dist8
         
         n = 1./(w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8)

!        vi(1) =  w1 * vel(ilongm,ilat0,iradm,1) +  &
!                 w2 * vel(ilongp,ilat0,iradm,1) +  &
!                 w5 * vel(ilong2,ilat0,iradm,1) +  &
!                 w6 * vel(ilong3,ilat0,iradm,1) +  &
!                 w3 * vel(ilongm,ilat0,iradp,1) +  &
!                 w4 * vel(ilongp,ilat0,iradp,1) +  & 
!                 w7 * vel(ilong2,ilat0,iradp,1) +  &
!                 w8 * vel(ilong3,ilat0,iradp,1)

         vi(1) = w1 * vel_cart_poles (1,ilongm,iradm,ipole) + &
                 w2 * vel_cart_poles (1,ilongp,iradm,ipole) + &
                 w5 * vel_cart_poles (1,ilong2,iradm,ipole) + &
                 w6 * vel_cart_poles (1,ilong3,iradm,ipole) + &
                 w3 * vel_cart_poles (1,ilongm,iradp,ipole) + &
                 w4 * vel_cart_poles (1,ilongp,iradp,ipole) + &
                 w7 * vel_cart_poles (1,ilong2,iradp,ipole) + &
                 w8 * vel_cart_poles (1,ilong3,iradp,ipole)

         vi(1) = vi(1)*n
        
        
!        vi(2) =  w1 * vel(ilongm,ilat0,iradm,2) +  &
!                 w2 * vel(ilongp,ilat0,iradm,2) +  &
!                 w5 * vel(ilong2,ilat0,iradm,2) +  &
!                 w6 * vel(ilong3,ilat0,iradm,2) +  &
!                 w3 * vel(ilongm,ilat0,iradp,2) +  &
!                 w4 * vel(ilongp,ilat0,iradp,2) +  & 
!                 w7 * vel(ilong2,ilat0,iradp,2) +  &
!                 w8 * vel(ilong3,ilat0,iradp,2)

         vi(2) = w1 * vel_cart_poles (2,ilongm,iradm,ipole) + &
                 w2 * vel_cart_poles (2,ilongp,iradm,ipole) + &
                 w5 * vel_cart_poles (2,ilong2,iradm,ipole) + &
                 w6 * vel_cart_poles (2,ilong3,iradm,ipole) + &
                 w3 * vel_cart_poles (2,ilongm,iradp,ipole) + &
                 w4 * vel_cart_poles (2,ilongp,iradp,ipole) + &
                 w7 * vel_cart_poles (2,ilong2,iradp,ipole) + &
                 w8 * vel_cart_poles (2,ilong3,iradp,ipole)
         
         vi(2) = vi(2)*n


!        vi(3) =  w1 * vel(ilongm,ilat0,iradm,3) +  &
!                 w2 * vel(ilongp,ilat0,iradm,3) +  &
!                 w5 * vel(ilong2,ilat0,iradm,3) +  &
!                 w6 * vel(ilong3,ilat0,iradm,3) +  &
!                 w3 * vel(ilongm,ilat0,iradp,3) +  &
!                 w4 * vel(ilongp,ilat0,iradp,3) +  & 
!                 w7 * vel(ilong2,ilat0,iradp,3) +  &
!                 w8 * vel(ilong3,ilat0,iradp,3)
        
         vi(3) = w1 * vel_cart_poles (3,ilongm,iradm,ipole) + &
                 w2 * vel_cart_poles (3,ilongp,iradm,ipole) + &
                 w5 * vel_cart_poles (3,ilong2,iradm,ipole) + &
                 w6 * vel_cart_poles (3,ilong3,iradm,ipole) + &
                 w3 * vel_cart_poles (3,ilongm,iradp,ipole) + &
                 w4 * vel_cart_poles (3,ilongp,iradp,ipole) + &
                 w7 * vel_cart_poles (3,ilong2,iradp,ipole) + &
                 w8 * vel_cart_poles (3,ilong3,iradp,ipole)
         
         vi(3) = vi(3)*n     
         

     else !Nlong_cart is odd

        !1 point opposite to pole
        ilong2 = mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1

        !Computes weights for the trilinear interpolation
        call distance(iradm,ilat0,ilongm,rad,theta,phi,dist1)
        call distance(iradm,ilat0,ilongp,rad,theta,phi,dist2)
        call distance(iradp,ilat0,ilongm,rad,theta,phi,dist3)
        call distance(iradp,ilat0,ilongp,rad,theta,phi,dist4)
        
        if (dist1 .eq. 0.) dist1 = 1e-10
        if (dist2 .eq. 0.) dist2 = 1e-10
        if (dist3 .eq. 0.) dist3 = 1e-10
        if (dist4 .eq. 0.) dist4 = 1e-10
        
        call distance(iradm,ilat0,ilong2,rad,theta,phi,dist5)
        call distance(iradp,ilat0,ilong2,rad,theta,phi,dist6)

        w1 = 1./dist1
        w2 = 1./dist2
        w3 = 1./dist3
        w4 = 1./dist4
        w5 = 1./dist5
        w6 = 1./dist6

        n = 1./(w1 + w2 + w3 + w4 + w5 + w6)

        vi(1) =  w1 * vel_cart_poles (1,ilongm,iradm,ipole) +  &
                 w2 * vel_cart_poles (1,ilongp,iradm,ipole) +  &
                 w5 * vel_cart_poles (1,ilong2,iradm,ipole) +  &
                 w3 * vel_cart_poles (1,ilongm,iradp,ipole) +  &
                 w4 * vel_cart_poles (1,ilongp,iradp,ipole) +  &
                 w6 * vel_cart_poles (1,ilong2,iradp,ipole)

        vi(1) = vi(1)*n

!        vi(2) =  w1 * vel(ilongm,ilat0,iradm,2) +  &
!                 w2 * vel(ilongp,ilat0,iradm,2) +  &
!                 w5 * vel(ilong2,ilat0,iradm,2) +  &
!                 w3 * vel(ilongm,ilat0,iradp,2) +  &
!                 w4 * vel(ilongp,ilat0,iradp,2) +  &
!                 w6 * vel(ilong2,ilat0,iradp,2)

        vi(2) =  w1 * vel_cart_poles (2,ilongm,iradm,ipole) +  &
                 w2 * vel_cart_poles (2,ilongp,iradm,ipole) +  &
                 w5 * vel_cart_poles (2,ilong2,iradm,ipole) +  &
                 w3 * vel_cart_poles (2,ilongm,iradp,ipole) +  &
                 w4 * vel_cart_poles (2,ilongp,iradp,ipole) +  &
                 w6 * vel_cart_poles (2,ilong2,iradp,ipole) 

        vi(2) = vi(2)*n

!        vi(3) =  w1 * vel(ilongm,ilat0,iradm,3) +  &
!                 w2 * vel(ilongp,ilat0,iradm,3) +  &
!                 w5 * vel(ilong2,ilat0,iradm,3) +  &
!                 w3 * vel(ilongm,ilat0,iradp,3) +  &
!                 w4 * vel(ilongp,ilat0,iradp,3) +  &
!                 w6 * vel(ilong2,ilat0,iradp,3)

        vi(3) =  w1 * vel_cart_poles (3,ilongm,iradm,ipole) +  &
                 w2 * vel_cart_poles (3,ilongp,iradm,ipole) +  &
                 w5 * vel_cart_poles (3,ilong2,iradm,ipole) +  &
                 w3 * vel_cart_poles (3,ilongm,iradp,ipole) +  &
                 w4 * vel_cart_poles (3,ilongp,iradp,ipole) +  &
                 w6 * vel_cart_poles (3,ilong2,iradp,ipole) 

        vi(3) = vi(3)*n      


     end if

  end if


end subroutine v_i2_cart
!-----------------------------------------------------------------------------



subroutine change_spherical_cartesian (Nr_deb,Nr_fin,Nlat_deb,Nlat_fin,vs,vc,Psc)

  use globall
!$ USE OMP_LIB

  implicit none

  integer,intent(in):: Nr_deb,Nr_fin,Nlat_deb,Nlat_fin
  real,intent(in):: Psc(9,Nlong_cart,Nlat_deb:Nlat_fin)
  real,intent(in):: vs(3,Nlong,Nlat,Nr_deb:Nr_fin)
  real,intent(out):: vc(3,Nlong_cart,Nlat_deb:Nlat_fin,Nr_deb:Nr_fin)

  integer irad,ilat,ilong,i
  real vs1,vs2,vs3

!$OMP PARALLEL PRIVATE(ilat,ilong,vs1,vs2,vs3)

!!$ nb_threads = omp_get_num_threads ()

!      IF (rank == 0) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!!$          ptime_debut = omp_get_wtime()
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
!            mtime_debut = MPI_WTime ()
!!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!      END IF

!$OMP DO SCHEDULE(DYNAMIC,1)
  do irad = Nr_deb,Nr_fin
     do ilat = Nlat_deb,Nlat_fin
        do ilong = 1,Nlong_cart
 
           ! Careful, vs not duplicated when Mc>1
           vs1 = vs(1,max(mod(ilong,Nlong),1),ilat,irad)
           vs2 = vs(2,max(mod(ilong,Nlong),1),ilat,irad)
           vs3 = vs(3,max(mod(ilong,Nlong),1),ilat,irad)

  !vc = Psc*vs = velocity in cartesian coordinates
           vc(1,ilong,ilat,irad) = Psc(1,ilong,ilat)*vs1 + &
                                   Psc(2,ilong,ilat)*vs2 + &
                                   Psc(3,ilong,ilat)*vs3

           vc(2,ilong,ilat,irad) = Psc(4,ilong,ilat)*vs1 + &
                                   Psc(5,ilong,ilat)*vs2 + &
                                   Psc(6,ilong,ilat)*vs3

           vc(3,ilong,ilat,irad) = Psc(7,ilong,ilat)*vs1 + &
                                   Psc(8,ilong,ilat)*vs2 + &
                                   Psc(9,ilong,ilat)*vs3

        end do
        ! Now duplicate the arrays if necessary
     end do
  end do
!$OMP END DO

!IF (rank == 0) THEN
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          ptime_fin = omp_get_wtime()
!!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele change_spherical_cartesian : ', ptime_fin - ptime_debut
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            mtime_fin = MPI_WTime ()
!            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele change_spherical_cartesian : ', mtime_fin - mtime_debut
!!$       END IF
!      END IF

!$OMP END PARALLEL

end subroutine change_spherical_cartesian
!------------------------------------------------------------------------------


subroutine change_spherical_cartesian_poles ()

  use globall
  use tracerstuff
!$ USE OMP_LIB

  implicit none

  integer irad,ilong
  real vs1,vs2,vs3

!$OMP PARALLEL PRIVATE(ilong,vs1,vs2,vs3)

!!$ nb_threads = omp_get_num_threads ()

!      IF (rank == 0) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!!$          ptime_debut = omp_get_wtime()
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
!            mtime_debut = MPI_WTime ()
!!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!      END IF

!$OMP DO SCHEDULE(DYNAMIC,1)
  do irad = ideb_veltra-2,ifin_veltra+2

     !North Pole-------------------------------------------------
     do ilong = 1,Nlong_cart
        
        vs1 = vel_spat(1,ilong,1,irad)
        vs2 = vel_spat(2,ilong,1,irad)
        vs3 = vel_spat(3,ilong,1,irad)
        
        !vc = Psc*vs = velocity in cartesian coordinates
        vel_cart_poles (1,ilong,irad,1) = Psc_N(1,ilong,1)*vs1 + &
                                          Psc_N(2,ilong,1)*vs2 + &
                                          Psc_N(3,ilong,1)*vs3
        
        vel_cart_poles (2,ilong,irad,1) = Psc_N(4,ilong,1)*vs1 + &
                                          Psc_N(5,ilong,1)*vs2 + &
                                          Psc_N(6,ilong,1)*vs3
        
        vel_cart_poles (3,ilong,irad,1) = Psc_N(7,ilong,1)*vs1 + &
                                          Psc_N(8,ilong,1)*vs2 + &
                                          Psc_N(9,ilong,1)*vs3
        
     end do

     !South Pole--------------------------------------------------
     do ilong = 1,Nlong_cart
        
        vs1 = vel_spat(1,ilong,Nlat,irad)
        vs2 = vel_spat(2,ilong,Nlat,irad)
        vs3 = vel_spat(3,ilong,Nlat,irad)
        
        !vc = Psc*vs = velocity in cartesian coordinates
        vel_cart_poles (1,ilong,irad,2) = Psc_S(1,ilong,Nlat)*vs1 + &
                                          Psc_S(2,ilong,Nlat)*vs2 + &
                                          Psc_S(3,ilong,Nlat)*vs3
        
        vel_cart_poles (2,ilong,irad,2) = Psc_S(4,ilong,Nlat)*vs1 + &
                                          Psc_S(5,ilong,Nlat)*vs2 + &
                                          Psc_S(6,ilong,NLat)*vs3
        
        vel_cart_poles (3,ilong,irad,2) = Psc_S(7,ilong,Nlat)*vs1 + &
                                          Psc_S(8,ilong,Nlat)*vs2 + &
                                          Psc_S(9,ilong,Nlat)*vs3
        
     end do
     
  end do
!$OMP END DO

!IF (rank == 0) THEN
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          ptime_fin = omp_get_wtime()
!!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele change_spherical_cartesian : ', ptime_fin - ptime_debut
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            mtime_fin = MPI_WTime ()
!            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele change_spherical_cartesian : ', mtime_fin - mtime_debut
!!$       END IF
!      END IF

!$OMP END PARALLEL

end subroutine change_spherical_cartesian_poles
!------------------------------------------------------------------------------


subroutine adjust_tracers_C (dc)

! for tracer composition treatment, adjusts tracers according to 
! scalar field, interpolated to tracer positions.
! For diffusion calculated in spectral space

  use tracerstuff
  use globall
!$ use OMP_LIB

  implicit none

  real,intent(in):: dc(nlong,nlat,ideb_Ctra-2:ifin_Ctra+2)

  integer i,irad0,irad1,ilat0,ilat1,ilong0,ilong1
  real rad,theta,phi,dci,n,dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8, &
       w1,w2,w3,w4,w5,w6,w7,w8,dist_rad,dist_lat,dist_long
  logical pole

 real mtime_debut,mtime_fin,ptime_debut,ptime_fin
!$  integer nb_threads

!$OMP PARALLEL  &
!$OMP PRIVATE (irad0,irad1,ilat0,ilat1,ilong0,ilong1) &
!$OMP PRIVATE (dist_rad,dist_lat,dist_long,rad,theta,phi,dci,n,w1,w2,w3,w4,w5,w6,w7,w8)    &
!$OMP PRIVATE (dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8,nb_threads,pole) &
!$OMP FIRSTPRIVATE (r,colat,long,drad1,dlat1)  

!$ nb_threads = omp_get_num_threads ()

!      IF (rank == 0) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!!$          ptime_debut = omp_get_wtime()
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
!            mtime_debut = MPI_WTime ()
!!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!      END IF

!$OMP DO SCHEDULE(DYNAMIC,size_packs) !(STATIC,ntr/nb_threads) 
  do i = 1,ntr

     pole = .false.

     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)
     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)
     ilong0 = closest_point_coord(1,3,i)
     ilong1 = closest_point_coord(2,3,i)

!     if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true.
     if (ilat1 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true. 
  
     if (.not. pole) then

        dci  =   weight_interp(1,i) * dc(ilong0,ilat0,irad0) + &
                 weight_interp(2,i) * dc(ilong1,ilat0,irad0) + &
                 weight_interp(3,i) * dc(ilong0,ilat1,irad0) + &
                 weight_interp(4,i) * dc(ilong1,ilat1,irad0) + &
                 weight_interp(5,i) * dc(ilong0,ilat0,irad1) + &
                 weight_interp(6,i) * dc(ilong1,ilat0,irad1) + &
                 weight_interp(7,i) * dc(ilong0,ilat1,irad1) + &
                 weight_interp(8,i) * dc(ilong1,ilat1,irad1)


     else
! WARNING : IF Mc > 1 THIS WILL BUG SINCE THE ARRAY TEMP NEEDS TO BE DUPLICATED
! AROUND POLES
        rad = tra(1,i)
        theta = tra(2,i)
        phi = tra(3,i)

        call distance(irad0,ilat0,ilong0,rad,theta,phi,dist1)
        call distance(irad1,ilat0,ilong0,rad,theta,phi,dist2)
        call distance(irad0,ilat0,ilong1,rad,theta,phi,dist3)
        call distance(irad1,ilat0,ilong1,rad,theta,phi,dist4)
        
        if (dist1 .eq. 0.) dist1 = 1e-10
        if (dist2 .eq. 0.) dist2 = 1e-10
        if (dist3 .eq. 0.) dist3 = 1e-10
        if (dist4 .eq. 0.) dist4 = 1e-10

        w1 = 1/dist1
        w2 = 1/dist2
        w3 = 1/dist3
        w4 = 1/dist4
      
        if (mod(nlong_cart,2) .eq. 0) then
           call distance(irad0,ilat0,mod(ilong0+nlong_cart/2-1,nlong_cart)+1,rad,theta,phi,dist5)
           call distance(irad0,ilat0,mod(ilong0+nlong_cart/2,nlong_cart)+1,rad,theta,phi,dist6)
           call distance(irad1,ilat0,mod(ilong0+nlong_cart/2-1,nlong_cart)+1,rad,theta,phi,dist7)
           call distance(irad1,ilat0,mod(ilong0+nlong_cart/2,nlong_cart)+1,rad,theta,phi,dist8)

           if (dist5 .eq. 0.) dist5 = 1e-10
           if (dist6 .eq. 0.) dist6 = 1e-10
           if (dist7 .eq. 0.) dist7 = 1e-10
           if (dist8 .eq. 0.) dist8 = 1e-10

           w5 = 1/dist5
           w6 = 1/dist6
           w7 = 1/dist7
           w8 = 1/dist8

        else 
           call distance(irad0,ilat0,mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1,rad,theta,phi,dist5)
           call distance(irad1,ilat0,mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1,rad,theta,phi,dist6)
           dist5 = 0.5*dist5
           dist6 = 0.5*dist6

           if (dist5 .eq. 0.) dist5 = 1e-10
           if (dist6 .eq. 0.) dist6 = 1e-10
          
           w5 = 1/dist5
           w6 = 1/dist6

        end if

        if (mod(nlong_cart,2) .eq. 0) then

           dci =  w1 * dc(ilong0,ilat0,irad0) +  &
                  w2 * dc(ilong1,ilat0,irad0) +  &
                  w3 * dc(ilong0,ilat0,irad1) +  &
                  w4 * dc(ilong1,ilat0,irad1) +  &
                  w5 * dc(mod(ilong0+nlong_cart/2-1,nlong_cart)+1,ilat0,irad0) +  &
                  w6 * dc(mod(ilong0+nlong_cart/2,nlong_cart)+1,ilat0,irad0)   +  &
                  w7 * dc(mod(ilong0+nlong_cart/2-1,nlong_cart)+1,ilat0,irad1) +  &
                  w8 * dc(mod(ilong0+nlong_cart/2,nlong_cart)+1,ilat0,irad1)   

           n = w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8

         else  

            dci =  w1 * dc(ilong0,ilat0,irad0) +  &
                   w2 * dc(ilong1,ilat0,irad0) +  &
                   w3 * dc(ilong0,ilat0,irad1) +  &
                   w4 * dc(ilong1,ilat0,irad1) +  &
                   w5 * dc(mod(ilong0+(nlong_cart+1)/2-1,nlong_cart)+1,ilat0,irad0) +  &
                   w6 * dc(mod(ilong0+(nlong_cart+1)/2,nlong_cart)+1,ilat0,irad1) 
         
            n = w1 + w2 + w3 + w4 + w5 + w6 

         end if

         dci = dci/n

      end if


      tra(COMPPOS,i) = tra(COMPPOS,i) + dci

   end do
!$OMP END DO

!IF (rank == 0) THEN
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          ptime_fin = omp_get_wtime()
!!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele adjust_tracers_T : ', ptime_fin - ptime_debut
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            mtime_fin = MPI_WTime ()
!            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele adjust_tracers_T : ', mtime_fin - mtime_debut
!!$       END IF
!      END IF

!$OMP END PARALLEL

end subroutine adjust_tracers_C
!-------------------------------------------------------------------------


subroutine adjust_tracers_T (dtemp)

! for tracer composition treatment, adjusts tracers according to 
! scalar field, interpolated to tracer positions.
! For diffusion calculated in spectral space

  use tracerstuff
  use globall
  use mpi
!$ use OMP_LIB

  implicit none

  real,intent(in):: dtemp(nlong,nlat,ideb_Ttra-2:ifin_Ttra+2)

  integer i,irad0,irad1,ilat0,ilat1,ilong0,ilong1
  real rad,theta,phi,dti,n,dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8, &
       w1,w2,w3,w4,w5,w6,w7,w8,dist_rad,dist_lat,dist_long
  logical pole

 real mtime_debut,mtime_fin,ptime_debut,ptime_fin
!$  integer nb_threads

!$OMP PARALLEL  &
!$OMP PRIVATE (irad0,irad1,ilat0,ilat1,ilong0,ilong1) &
!$OMP PRIVATE (dist_rad,dist_lat,dist_long,rad,theta,phi,dti,n,w1,w2,w3,w4,w5,w6,w7,w8)    &
!$OMP PRIVATE (dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8,nb_threads,pole) &
!$OMP FIRSTPRIVATE (r,colat,long,drad1,dlat1)  

!$ nb_threads = omp_get_num_threads ()

      IF (rank == rank_time .and. measure_time) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!$       IF (.TRUE.) THEN
!$OMP MASTER
!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!$          ptime_debut = omp_get_wtime()
!$OMP END MASTER
!$OMP BARRIER
!$       ELSE
!           print*, '--------------------------------------------------------------------------------------'
!           WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!           WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
           mtime_debut = MPI_WTime ()
!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
      END IF

!$OMP DO SCHEDULE(DYNAMIC,size_packs) !(STATIC,ntr/nb_threads) 
  do i = 1,ntr
     
     pole = .false.

     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)
     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)
     ilong0 = closest_point_coord(1,3,i)
     ilong1 = closest_point_coord(2,3,i)

!     if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true.
     if (ilat1 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true.     
 
     if (.not. pole) then

        dti  =   weight_interp(1,i) * dtemp(ilong0,ilat0,irad0) + &
                 weight_interp(2,i) * dtemp(ilong1,ilat0,irad0) + &
                 weight_interp(3,i) * dtemp(ilong0,ilat1,irad0) + &
                 weight_interp(4,i) * dtemp(ilong1,ilat1,irad0) + &
                 weight_interp(5,i) * dtemp(ilong0,ilat0,irad1) + &
                 weight_interp(6,i) * dtemp(ilong1,ilat0,irad1) + &
                 weight_interp(7,i) * dtemp(ilong0,ilat1,irad1) + &
                 weight_interp(8,i) * dtemp(ilong1,ilat1,irad1)

     else  

        rad = tra(1,i)
        theta = tra(2,i)
        phi = tra(3,i)

        call distance(irad0,ilat0,ilong0,rad,theta,phi,dist1)
        call distance(irad1,ilat0,ilong0,rad,theta,phi,dist2)
        call distance(irad0,ilat0,ilong1,rad,theta,phi,dist3)
        call distance(irad1,ilat0,ilong1,rad,theta,phi,dist4)
        
        if (dist1 .eq. 0.) dist1 = 1e-10
        if (dist2 .eq. 0.) dist2 = 1e-10
        if (dist3 .eq. 0.) dist3 = 1e-10
        if (dist4 .eq. 0.) dist4 = 1e-10
        
        w1 = 1/dist1
        w2 = 1/dist2
        w3 = 1/dist3
        w4 = 1/dist4
        
        if (mod(nlong,2) .eq. 0) then
           call distance(irad0,ilat0,mod(ilong0+nlong*Mc/2-1,nlong)+1,rad,theta,phi,dist5)
           call distance(irad0,ilat0,mod(ilong0+nlong*Mc/2,nlong)+1,rad,theta,phi,dist6)
           call distance(irad1,ilat0,mod(ilong0+nlong*Mc/2-1,nlong)+1,rad,theta,phi,dist7)
           call distance(irad1,ilat0,mod(ilong0+nlong*Mc/2,nlong)+1,rad,theta,phi,dist8)
         
           if (dist5 .eq. 0.) dist5 = 1e-10
           if (dist6 .eq. 0.) dist6 = 1e-10
           if (dist7 .eq. 0.) dist7 = 1e-10
           if (dist8 .eq. 0.) dist8 = 1e-10

           w5 = 1/dist5
           w6 = 1/dist6
           w7 = 1/dist7
           w8 = 1/dist8
           
        else 
           call distance(irad0,ilat0,mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,rad,theta,phi,dist5)
           call distance(irad1,ilat0,mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,rad,theta,phi,dist6)
           dist5 = 0.5*dist5
           dist6 = 0.5*dist6
           
           if (dist5 .eq. 0.) dist5 = 1e-10
           if (dist6 .eq. 0.) dist6 = 1e-10
           
           w5 = 1/dist5
           w6 = 1/dist6
           
        end if
        
        if (mod(nlong,2) .eq. 0) then
           
           dti   =   w1 * dtemp(ilong0,ilat0,irad0) +  &
                     w2 * dtemp(ilong1,ilat0,irad0) +  &
                     w3 * dtemp(ilong0,ilat0,irad1) +  &
                     w4 * dtemp(ilong1,ilat0,irad1) +  &
                     w5 * dtemp(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * dtemp(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad0)   +  &
                     w7 * dtemp(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad1) +  &
                     w8 * dtemp(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad1)   

           n = w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8

        else

           dti   =   w1 * dtemp(ilong0,ilat0,irad0) +  &
                     w2 * dtemp(ilong1,ilat0,irad0) +  &
                     w3 * dtemp(ilong0,ilat0,irad1) +  &
                     w4 * dtemp(ilong1,ilat0,irad1) +  &
                     w5 * dtemp(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * dtemp(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad1) 
         
           n = w1 + w2 + w3 + w4 + w5 + w6 

        end if

        dti = dti/n

     end if

     tra(TEMPPOS,i) = tra(TEMPPOS,i) + dti

  end do
!$OMP END DO

IF (rank == rank_time .and. measure_time) THEN
!$       IF (.TRUE.) THEN
!$OMP MASTER
!$          ptime_fin = omp_get_wtime()
!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele adjust_tracers_T : ', ptime_fin - ptime_debut
!$OMP END MASTER
!$OMP BARRIER
!$       ELSE
            mtime_fin = MPI_WTime ()
            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele adjust_tracers_T : ', mtime_fin - mtime_debut
!$       END IF
      END IF

!$OMP END PARALLEL

end subroutine adjust_tracers_T
!-------------------------------------------------------------------------


subroutine smooth_tracer_T ()

  use tracerstuff
  use globall
  use mpi
!$ USE OMP_LIB

  implicit none

  integer i,irad0,irad1,ilat0,ilat1,ilong0,ilong1
  real rad,theta,phi,dist_rad,dist_lat,dist_long,w1,w2,w3,w4,w5,w6,w7,w8,w9,&
       dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8
  real Told,t_diffi,n,rfac,dTi,Ti

  real,dimension(Nlong,Nlat,ideb_Ttra-2:ifin_Ttra+2):: temp0,twork

  logical pole
  real mtime_debut,mtime_fin,ptime_debut,ptime_fin
!$ integer nb_threads

  ! smooth temperatures carried on tracers, similar to Gerya and Yuen (2003)
  ! (i) adjusts them towards an interpolated linear profile with a timescale that depends
  !      on local grid spacing and thermal diffusivity
  ! (ii) calculates grid-scale diffusion that results from this, and corrects for it

!$OMP PARALLEL &
!$OMP PRIVATE (irad0,irad1,ilat0,ilat1,ilong0,ilong1) &
!$OMP PRIVATE (dist_rad,dist_lat,dist_long,n,rad,theta,phi,w1,w2,w3,w4,w5,w6,w7,w8)    &
!$OMP PRIVATE (dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8)  &
!$OMP PRIVATE (pole,nb_threads,Told,t_diffi,rfac,dTi,Ti) &
!$OMP FIRSTPRIVATE (r,colat,long,drad1,dlat1)

!$ nb_threads = omp_get_num_threads ()

      IF (rank == rank_time .and. measure_time) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!$       IF (.TRUE.) THEN
!$OMP MASTER
!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!$          ptime_debut = omp_get_wtime()
!$OMP END MASTER
!$OMP BARRIER
!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
            mtime_debut = MPI_WTime ()
!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
      END IF

!$OMP DO SCHEDULE(DYNAMIC,size_packs) !(STATIC,ntr/nb_threads)
  do i=1,ntr

     pole = .false.

     rad = tra(1,i)
     theta = tra(2,i)
     phi = tra(3,i)

     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)
     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)
     ilong0 = closest_point_coord(1,3,i)
     ilong1 = closest_point_coord(2,3,i)

!     if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true.
     if (ilat1 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true. 
  
     if (.not. pole) then

        Ti   =   weight_interp(1,i) * temp(ilong0,ilat0,irad0) + &
                 weight_interp(2,i) * temp(ilong1,ilat0,irad0) + &
                 weight_interp(3,i) * temp(ilong0,ilat1,irad0) + &
                 weight_interp(4,i) * temp(ilong1,ilat1,irad0) + &
                 weight_interp(5,i) * temp(ilong0,ilat0,irad1) + &
                 weight_interp(6,i) * temp(ilong1,ilat0,irad1) + &
                 weight_interp(7,i) * temp(ilong0,ilat1,irad1) + &
                 weight_interp(8,i) * temp(ilong1,ilat1,irad1)

        t_diffi   =   weight_interp(1,i) * t_diff_T(ilong0,ilat0,irad0) + &
                      weight_interp(2,i) * t_diff_T(ilong1,ilat0,irad0) + &
                      weight_interp(3,i) * t_diff_T(ilong0,ilat1,irad0) + &
                      weight_interp(4,i) * t_diff_T(ilong1,ilat1,irad0) + &
                      weight_interp(5,i) * t_diff_T(ilong0,ilat0,irad1) + &
                      weight_interp(6,i) * t_diff_T(ilong1,ilat0,irad1) + &
                      weight_interp(7,i) * t_diff_T(ilong0,ilat1,irad1) + &
                      weight_interp(8,i) * t_diff_T(ilong1,ilat1,irad1)


     else !if pole

        call distance(irad0,ilat0,ilong0,rad,theta,phi,dist1)
        call distance(irad1,ilat0,ilong0,rad,theta,phi,dist2)
        call distance(irad0,ilat0,ilong1,rad,theta,phi,dist3)
        call distance(irad1,ilat0,ilong1,rad,theta,phi,dist4)
        
        if (dist1 .eq. 0.) dist1 = 1e-10
        if (dist2 .eq. 0.) dist2 = 1e-10
        if (dist3 .eq. 0.) dist3 = 1e-10
        if (dist4 .eq. 0.) dist4 = 1e-10
        
        w1 = 1/dist1
        w2 = 1/dist2
        w3 = 1/dist3
        w4 = 1/dist4
        
        if (mod(nlong,2) .eq. 0) then
           call distance(irad0,ilat0,mod(ilong0+nlong*Mc/2-1,nlong)+1,rad,theta,phi,dist5)
           call distance(irad0,ilat0,mod(ilong0+nlong*Mc/2,nlong)+1,rad,theta,phi,dist6)
           call distance(irad1,ilat0,mod(ilong0+nlong*Mc/2-1,nlong)+1,rad,theta,phi,dist7)
           call distance(irad1,ilat0,mod(ilong0+nlong*Mc/2,nlong)+1,rad,theta,phi,dist8)
         
           if (dist5 .eq. 0.) dist5 = 1e-10
           if (dist6 .eq. 0.) dist6 = 1e-10
           if (dist7 .eq. 0.) dist7 = 1e-10
           if (dist8 .eq. 0.) dist8 = 1e-10

           w5 = 1/dist5
           w6 = 1/dist6
           w7 = 1/dist7
           w8 = 1/dist8
           
        else 
           call distance(irad0,ilat0,mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,rad,theta,phi,dist5)
           call distance(irad1,ilat0,mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,rad,theta,phi,dist6)
           dist5 = 0.5*dist5
           dist6 = 0.5*dist6
           
           if (dist5 .eq. 0.) dist5 = 1e-10
           if (dist6 .eq. 0.) dist6 = 1e-10
           
           w5 = 1/dist5
           w6 = 1/dist6
           
        end if
        
        if (mod(nlong,2) .eq. 0) then
           
           Ti    =   w1 * temp(ilong0,ilat0,irad0) +  &
                     w2 * temp(ilong1,ilat0,irad0) +  &
                     w3 * temp(ilong0,ilat0,irad1) +  &
                     w4 * temp(ilong1,ilat0,irad1) +  &
                     w5 * temp(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * temp(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad0)   +  &
                     w7 * temp(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad1) +  &
                     w8 * temp(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad1)   

           t_diffi = w1 * t_diff_T(ilong0,ilat0,irad0) +  &
                     w2 * t_diff_T(ilong1,ilat0,irad0) +  &
                     w3 * t_diff_T(ilong0,ilat0,irad1) +  &
                     w4 * t_diff_T(ilong1,ilat0,irad1) +  &
                     w5 * t_diff_T(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * t_diff_T(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad0)   +  &
                     w7 * t_diff_T(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad1) +  &
                     w8 * t_diff_T(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad1)  

           n = w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8

        else

           Ti    =   w1 * temp(ilong0,ilat0,irad0) +  &
                     w2 * temp(ilong1,ilat0,irad0) +  &
                     w3 * temp(ilong0,ilat0,irad1) +  &
                     w4 * temp(ilong1,ilat0,irad1) +  &
                     w5 * temp(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * temp(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad1) 

           t_diffi = w1 * t_diff_T(ilong0,ilat0,irad0) +  &
                     w2 * t_diff_T(ilong1,ilat0,irad0) +  &
                     w3 * t_diff_T(ilong0,ilat0,irad1) +  &
                     w4 * t_diff_T(ilong1,ilat0,irad1) +  &
                     w5 * t_diff_T(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * t_diff_T(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad1)
         
           n = w1 + w2 + w3 + w4 + w5 + w6 

        end if

        Ti = Ti/n                
        t_diffi = t_diffi/n

  end if

     Told = tra(TEMPPOS,i)
     rfac = exp(-numerical_diffusion * dt/t_diffi)           ! fraction of old temperature to keep
     dTi = (Ti-Told)*rfac
     tra(TEMPPOS,i) = Ti - dTi 

  end do
!$OMP END DO

  IF (rank == rank_time .and. measure_time) THEN
!$       IF (.TRUE.) THEN
!$OMP MASTER
!$          ptime_fin = omp_get_wtime()
!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele smooth_tracer_T : ', ptime_fin - ptime_debut
!$OMP END MASTER
!$OMP BARRIER
!$       ELSE
            mtime_fin = MPI_WTime ()
            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele smooth_tracer_T : ', mtime_fin - mtime_debut
!$       END IF
      END IF

!$OMP END PARALLEL

  temp0 = temp

  if (grid_fusion) then
     call tracers_to_grid_special (temp,ideb_Ttra,ifin_Ttra,TEMPPOS)
  else
     call tracers_to_T ()
  end if

  call exchange_ghost_planes_dT(temp)


  Twork = -(temp-temp0)   ! temperature change due to erroneous diffusion

  !..subtract this.
  call adjust_tracers_T(Twork)

  ! NOTE: actually this will not perfectly subtract the
  !  diffusion. Trying testing this. Could even iterate to see if it
  !  eventally does subtract diffusion

 ! print*,rank,'after adjust_tracers_T'
  ! final update of temperature (which ideally would not have changed) USELESS IF DOING ADVECTION RIGHT AFTER
!  call tracers_to_T()
!  call exchange_ghost_planes_dT(temp)

  
end subroutine smooth_tracer_T
!---------------------------------------------------------------------------------------


subroutine smooth_tracer_C ()

  use tracerstuff
  use globall
  use mpi
!$ USE OMP_LIB

  implicit none

  integer i,irad0,irad1,ilat0,ilat1,ilong0,ilong1
  real rad,theta,phi,dist_rad,dist_lat,dist_long,w1,w2,w3,w4,w5,w6,w7,w8,w9,&
       dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8
  real Cold,t_diffi,n,rfac,dCi,Ci

  real,dimension(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2):: comp0,cwork

  logical pole
  real mtime_debut,mtime_fin,ptime_debut,ptime_fin
!$ integer nb_threads

  ! smooth temperatures carried on tracers, similar to Gerya and Yuen (2003)
  ! (i) adjusts them towards an interpolated linear profile with a timescale that depends
  !      on local grid spacing and thermal diffusivity
  ! (ii) calculates grid-scale diffusion that results from this, and corrects for it

!$OMP PARALLEL &
!$OMP PRIVATE (irad0,irad1,ilat0,ilat1,ilong0,ilong1) &
!$OMP PRIVATE (dist_rad,dist_lat,dist_long,n,rad,theta,phi,w1,w2,w3,w4,w5,w6,w7,w8)    &
!$OMP PRIVATE (dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8)  &
!$OMP PRIVATE (pole,nb_threads,Cold,t_diffi,rfac,dCi,Ci) &
!$OMP FIRSTPRIVATE (r,colat,long,drad1,dlat1)

!$ nb_threads = omp_get_num_threads ()

!      IF (rank == 0) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!!$          ptime_debut = omp_get_wtime()
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
!            mtime_debut = MPI_WTime ()
!!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!      END IF

!$OMP DO SCHEDULE(DYNAMIC,size_packs) !(STATIC,ntr/nb_threads)
  do i=1,ntr

     pole = .false.

     rad = tra(1,i)
     theta = tra(2,i)
     phi = tra(3,i)

     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)
     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)
     ilong0 = closest_point_coord(1,3,i)
     ilong1 = closest_point_coord(2,3,i)

!     if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true.
     if (ilat1 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true. 
  
     if (.not. pole) then

        Ci   =   weight_interp(1,i) * comp(ilong0,ilat0,irad0) + &
                 weight_interp(2,i) * comp(ilong1,ilat0,irad0) + &
                 weight_interp(3,i) * comp(ilong0,ilat1,irad0) + &
                 weight_interp(4,i) * comp(ilong1,ilat1,irad0) + &
                 weight_interp(5,i) * comp(ilong0,ilat0,irad1) + &
                 weight_interp(6,i) * comp(ilong1,ilat0,irad1) + &
                 weight_interp(7,i) * comp(ilong0,ilat1,irad1) + &
                 weight_interp(8,i) * comp(ilong1,ilat1,irad1)

        t_diffi   =   weight_interp(1,i) * t_diff_C(ilong0,ilat0,irad0) + &
                      weight_interp(2,i) * t_diff_C(ilong1,ilat0,irad0) + &
                      weight_interp(3,i) * t_diff_C(ilong0,ilat1,irad0) + &
                      weight_interp(4,i) * t_diff_C(ilong1,ilat1,irad0) + &
                      weight_interp(5,i) * t_diff_C(ilong0,ilat0,irad1) + &
                      weight_interp(6,i) * t_diff_C(ilong1,ilat0,irad1) + &
                      weight_interp(7,i) * t_diff_C(ilong0,ilat1,irad1) + &
                      weight_interp(8,i) * t_diff_C(ilong1,ilat1,irad1)


     else !if pole

        call distance(irad0,ilat0,ilong0,rad,theta,phi,dist1)
        call distance(irad1,ilat0,ilong0,rad,theta,phi,dist2)
        call distance(irad0,ilat0,ilong1,rad,theta,phi,dist3)
        call distance(irad1,ilat0,ilong1,rad,theta,phi,dist4)
        
        if (dist1 .eq. 0.) dist1 = 1.e-10
        if (dist2 .eq. 0.) dist2 = 1.e-10
        if (dist3 .eq. 0.) dist3 = 1.e-10
        if (dist4 .eq. 0.) dist4 = 1.e-10
        
        w1 = 1./dist1
        w2 = 1./dist2
        w3 = 1./dist3
        w4 = 1./dist4
        
        if (mod(nlong,2) .eq. 0) then
           call distance(irad0,ilat0,mod(ilong0+nlong*Mc/2-1,nlong)+1,rad,theta,phi,dist5)
           call distance(irad0,ilat0,mod(ilong0+nlong*Mc/2,nlong)+1,rad,theta,phi,dist6)
           call distance(irad1,ilat0,mod(ilong0+nlong*Mc/2-1,nlong)+1,rad,theta,phi,dist7)
           call distance(irad1,ilat0,mod(ilong0+nlong*Mc/2,nlong)+1,rad,theta,phi,dist8)
         
           if (dist5 .eq. 0.) dist5 = 1.e-10
           if (dist6 .eq. 0.) dist6 = 1.e-10
           if (dist7 .eq. 0.) dist7 = 1.e-10
           if (dist8 .eq. 0.) dist8 = 1.e-10

           w5 = 1./dist5
           w6 = 1./dist6
           w7 = 1./dist7
           w8 = 1./dist8
           
        else 
           call distance(irad0,ilat0,mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,rad,theta,phi,dist5)
           call distance(irad1,ilat0,mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,rad,theta,phi,dist6)
           dist5 = 0.5*dist5
           dist6 = 0.5*dist6
           
           if (dist5 .eq. 0.) dist5 = 1e-10
           if (dist6 .eq. 0.) dist6 = 1e-10
           
           w5 = 1/dist5
           w6 = 1/dist6
           
        end if
        
        if (mod(nlong,2) .eq. 0) then
           
           Ci    =   w1 * comp(ilong0,ilat0,irad0) +  &
                     w2 * comp(ilong1,ilat0,irad0) +  &
                     w3 * comp(ilong0,ilat0,irad1) +  &
                     w4 * comp(ilong1,ilat0,irad1) +  &
                     w5 * comp(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * comp(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad0)   +  &
                     w7 * comp(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad1) +  &
                     w8 * comp(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad1)   

           t_diffi = w1 * t_diff_C(ilong0,ilat0,irad0) +  &
                     w2 * t_diff_C(ilong1,ilat0,irad0) +  &
                     w3 * t_diff_C(ilong0,ilat0,irad1) +  &
                     w4 * t_diff_C(ilong1,ilat0,irad1) +  &
                     w5 * t_diff_C(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * t_diff_C(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad0)   +  &
                     w7 * t_diff_C(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad1) +  &
                     w8 * t_diff_C(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad1)  

           n = w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8

        else

           Ci    =   w1 * comp(ilong0,ilat0,irad0) +  &
                     w2 * comp(ilong1,ilat0,irad0) +  &
                     w3 * comp(ilong0,ilat0,irad1) +  &
                     w4 * comp(ilong1,ilat0,irad1) +  &
                     w5 * comp(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * comp(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad1) 

           t_diffi = w1 * t_diff_C(ilong0,ilat0,irad0) +  &
                     w2 * t_diff_C(ilong1,ilat0,irad0) +  &
                     w3 * t_diff_C(ilong0,ilat0,irad1) +  &
                     w4 * t_diff_C(ilong1,ilat0,irad1) +  &
                     w5 * t_diff_C(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * t_diff_C(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad1)
         
           n = w1 + w2 + w3 + w4 + w5 + w6 

        end if

        Ci = Ci/n                
        t_diffi = t_diffi/n

  end if

     Cold = tra(COMPPOS,i)
     rfac = exp(-numerical_diffusion * dt/t_diffi)           ! fraction of old composition to keep
     dCi = (Ci-Cold)*rfac
     tra(COMPPOS,i) = Ci - dCi 

  end do
!$OMP END DO

!  IF (rank == 0) THEN
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          ptime_fin = omp_get_wtime()
!!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele smooth_tracer_T : ', ptime_fin - ptime_debut
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            mtime_fin = MPI_WTime ()
!            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele smooth_tracer_T : ', mtime_fin - mtime_debut
!!$       END IF
!      END IF

!$OMP END PARALLEL

  comp0 = comp

  if (grid_fusion) then
     call tracers_to_grid_special (comp,ideb_Ctra,ifin_Ctra,COMPPOS)
  else 
     call tracers_to_Cf()
  end if
  call exchange_ghost_planes_dC(comp)


  Cwork = -(comp-comp0)   ! composition change due to erroneous diffusion
  if (rank==0) Cwork(:,:,NG) = 0. ! avoid erroneous diffusion at the boundaries
  if (rank==size-1) Cwork(:,:,NR) = 0.
  !..subtract this.
  call adjust_tracers_C(Cwork)

  ! NOTE: actually this will not perfectly subtract the
  !  diffusion. Trying testing this. Could even iterate to see if it
  !  eventally does subtract diffusion

 ! print*,rank,'after adjust_tracers_T'
  ! final update of temperature (which ideally would not have changed) USELESS IF DOING ADVECTION RIGHT AFTER
!!!  call tracers_to_Cf()
!!!  call exchange_ghost_planes_dC(comp)

  
end subroutine smooth_tracer_C
!---------------------------------------------------------------------------------------


subroutine calc_weights_interpolation ()

  use tracerstuff
  use globall
  use mpi
!$ use OMP_LIB

  implicit none

  logical pole
  integer i,irad0,irad1,ilat0,ilat1,ilong0,ilong1,irad_closest,ilat_closest,ilong_closest
  real rad,theta,phi,dist_rad,dist_lat,dist_long
  real mtime_debut,mtime_fin,ptime_debut,ptime_fin
!$  integer nb_threads

!$OMP PARALLEL  &
!$OMP PRIVATE (irad0,irad1,ilat0,ilat1,ilong0,ilong1) &
!$OMP PRIVATE (dist_rad,dist_lat,dist_long)    &
!$OMP PRIVATE (nb_threads,pole,rad,theta,phi) &
!$OMP FIRSTPRIVATE (r,colat,long,drad1,dlat1)  

!$ nb_threads = omp_get_num_threads ()

      IF (rank == rank_time .and. measure_time) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!$       IF (.TRUE.) THEN
!$OMP MASTER
!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!$          ptime_debut = omp_get_wtime()
!$OMP END MASTER
!$OMP BARRIER
!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
            mtime_debut = MPI_WTime ()
!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
      END IF

!$OMP DO SCHEDULE(DYNAMIC,size_packs) !(STATIC,ntr/nb_threads) 
  do i = 1,ntr
     
     pole = .false.

     rad = tra(1,i)
     theta = tra(2,i)
     phi = tra(3,i)

     irad0 = closest_point_coord(1,1,i)
     irad1 = closest_point_coord(2,1,i)

     ilat0 = closest_point_coord(1,2,i)
     ilat1 = closest_point_coord(2,2,i)

!        finds the coordinates of the corners of the volume element 
!        in which the tracer is located
     if (rad .lt. r(irad0) .or. rad .gt. r(irad1)) then
     call find_closest_radial_grid_point(rad,irad0,dist_rad)
     irad_closest = irad0    
 
     if (dist_rad .gt. 0) then
        irad1 = irad0 + 1
     else
        irad1 = irad0
        irad0 = irad0 - 1
        dist_rad = rad - r(irad0)
     end if
     dist_rad = dist_rad*drad1_1(irad0) !/drad1(irad0) !(r(irad1)-r(irad0))
     else
     dist_rad = (rad-r(irad0))*drad1_1(irad0)
     end if

     closest_point_coord(1,1,i) = irad0
     closest_point_coord(2,1,i) = irad1
     
     if (theta .lt. colat(ilat0) .or. theta .gt. colat(ilat1)) then
     call find_closest_lateral_grid_point(theta,ilat0,dist_lat)
     ilat_closest = ilat0

     if (dist_lat .gt. 0) then
        if (ilat0 .eq. nlat) then
           pole = .true.
           ilat1 = ilat0 
           dist_lat = dist_lat/(pi-colat(Nlat))
        else 
           ilat1 = ilat0 + 1
           dist_lat = dist_lat*dlat1_1(ilat1) !/dlat1(ilat1) !(colat(ilat1)-colat(ilat0))
        end if
     else 
        if (ilat0 .eq. 1) then
           pole = .true.
           ilat1 = ilat0 
           dist_lat = -dist_lat/colat(1)
        else
           ilat1 = ilat0
           ilat0 = ilat1 - 1
           dist_lat = (theta-colat(ilat0))*dlat1_1(ilat1) !/dlat1(ilat1) !(colat(ilat1)-colat(ilat0))
        end if        
     end if
     else 
     dist_lat = (theta-colat(ilat0))*dlat1_1(ilat1)
     end if

     closest_point_coord(1,2,i) = ilat0
     closest_point_coord(2,2,i) = ilat1

     ilong0 = 1 + floor (phi*dphi_1)
     ilong1 = max(mod(ilong0+1,nlong+1),1)
     dist_long = (phi - long(ilong0))*dphi_1
     
     closest_point_coord(1,3,i) = ilong0
     closest_point_coord(2,3,i) = ilong1

!     if (reordering) then 
 !       if (dist_long .le. 0.5) then
 !          ilong_closest = ilong0
 !       else
 !          ilong_closest = ilong1
 !       end if
 !       tra(NCELLPOS,i) = real(ilong_closest + Nlong*((ilat_closest-1) + Nlat*irad_closest))
 !   end if
     

!         tracer information gives contribution to each corner :

     if (.not. pole) then

        weight_interp(1,i) = (1-dist_long)*(1-dist_lat)*(1-dist_rad)
        weight_interp(2,i) =   dist_long  *(1-dist_lat)*(1-dist_rad)
        weight_interp(3,i) = (1-dist_long)*  dist_lat  *(1-dist_rad)
        weight_interp(4,i) =   dist_long  *  dist_lat  *(1-dist_rad)
        weight_interp(5,i) = (1-dist_long)*(1-dist_lat)*  dist_rad 
        weight_interp(6,i) =   dist_long  *(1-dist_lat)*  dist_rad 
        weight_interp(7,i) = (1-dist_long)*  dist_lat  *  dist_rad 
        weight_interp(8,i) =   dist_long  *  dist_lat  *  dist_rad 

     else
 
        weight_interp(1,i) = (1-dist_long)*(1-dist_lat)*(1-dist_rad)
        weight_interp(2,i) =   dist_long  *(1-dist_lat)*(1-dist_rad)
        weight_interp(5,i) = (1-dist_long)*(1-dist_lat)*  dist_rad 
        weight_interp(6,i) =   dist_long  *(1-dist_lat)*  dist_rad 

     end if

  end do
!$OMP END DO

IF (rank == rank_time .and. measure_time) THEN
!$       IF (.TRUE.) THEN
!$OMP MASTER
!$          ptime_fin = omp_get_wtime()
!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele calc_weight : ', ptime_fin - ptime_debut
!$OMP END MASTER
!$OMP BARRIER
!$       ELSE
            mtime_fin = MPI_WTime ()
            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele calc_weights : ', mtime_fin - mtime_debut
!$       END IF
      END IF

!$OMP END PARALLEL

end subroutine calc_weights_interpolation
!----------------------------------------------------------------------------------------


subroutine find_closest_radial_grid_point(rad,irad,dist) 

  use tracerstuff
  use globall

  implicit none

  real,intent(in):: rad
  integer,intent(out):: irad
  real,intent(out):: dist

 !! OLD VERSION
  !!        finds closest radial grid point and calculates the distance to this point: 
  !!        - first, finds if located in the regular or irregular part
  !!        - then finds closest point using an analytical formula (geometric progression)

!  if (rad .lt. r(NG+nb_irreg)) then!bottom irregular portion
!     irad = NG + int(log(1-(1-q1)/tmp1_find*(rad-r(NG))*coeffr)/log(q1))
!     if ((rad - r(irad)) .gt. (r(irad+1)-rad)) irad = irad + 1
!     if (irad .lt. NG .or. irad .gt. NR) print*,'in bottom irregular portion,irad=',irad

!  else if ((rad.ge.r(NG+nb_irreg)) .and. (rad .lt. r(NG+nb_irreg+nb_reg))) then !middle regular portion
!     irad = NG+nb_irreg + int((rad+deltaR/2-r(NG+nb_irreg))/deltaR)
!     if (irad .lt. NG .or. irad .gt. NR) print*,'in regular portion,irad=',irad

!  else !top irregular portion
!     irad = NG+nb_irreg+nb_reg-1 + int(log(1-(1-q2)/tmp1_find2*(rad-r(NG+nb_irreg+nb_reg-1))*coeffr)/log(q2))
!     if ((rad - r(irad)) .gt. (r(irad+1)-rad)) irad = irad + 1
!     if (irad .lt. NG .or. irad .gt. NR) print*,'in top irregular portion,irad=',irad

!  end if

!  dist = rad - r(irad) 

!! NEW VERSION: 
  dist = 1.

  irad = max(NG,ideb_all-3)
  do while (abs(dist) .ge. abs(rad-r(irad+1)))
     dist = rad-r(irad+1)
     irad = irad+1
     if (irad .eq. NR) EXIT
  end do

  if (irad .gt. ifin_all+2) then
     irad = ifin_all+2
     print*,'WARNING, tracers beyond radial MPI domain limits on proc',rank
  end if

end subroutine find_closest_radial_grid_point
!-----------------------------------------------------------------------------

subroutine find_closest_lateral_grid_point(theta,itheta,dist) 

  use tracerstuff
  use globall

  implicit none

  real,intent(in):: theta
  integer,intent(out):: itheta
  real,intent(out):: dist

  integer itheta0,ilat
  real distm,distp
!  dist = pi

  !VERY OLD METHOD
! if (theta .le. pi/2.) then

!     itheta = 0 !starts from colat(1)
!     do while (abs(dist) .gt. abs(theta-colat(itheta+1)))
!        dist = theta-colat(itheta+1)
!        itheta = itheta + 1
!     end do

!  else !if theta > pi/2

!     if (theta .ge. theta_Nlat) then !case theta > colat(Nlat)
!        itheta = Nlat
!        dist = theta - colat(Nlat)
!     else
!        itheta = Nlat/2 - 1 !starts from colat(Nlat/2)
!        do while (abs(dist) .gt. abs(theta-colat(itheta+1)))
!           dist = theta-colat(itheta+1)
!           itheta = itheta + 1
!        end do
!     end if

!  end if

!  itheta0 = itheta

  !OPTIMIZED METHOD: uses the fact the colocation points are NEARLY equidistant (dlat approximately constant).
  ! FIRST GUESS
  itheta = min(1+floor(theta*dlat_mean_1),Nlat)
  dist = theta - colat(itheta)

  ! NOW CHECK USING +/-1 POINTS
  do ilat = max(itheta-2,1),min(itheta+2,Nlat)
     if (abs(dist) .gt. abs(theta-colat(ilat))) then
        itheta = ilat
        dist = theta-colat(ilat)
     end if
  end do

!  if (itheta .ne. itheta0) print*,'ERROR,itheta,itheta0',itheta,itheta0

!ANOTHER OLD VERSION USING THE FACT THAT THE POINTS ARE NEARLY EQUIDISTANT IN LATITUDE
!  if (theta .le. pi/2.) then

!     itheta = int((theta+0.5*dtheta_reg)*dtheta_reg_1) !find closest regular grid point
!     if (itheta .eq. 0) then
!        itheta = 1
!        dist = theta - colat(1)
!     else
!        distm = theta - colat(itheta)        !compute the distance to the 2 closest grid points 
!        distp = theta - colat(itheta+1)
!        if (abs(distm) .lt. abs(distp)) then !compare and determine closest grid point
!           dist = distm
!        else
!           itheta = itheta+1
!           dist = distp
!        end if
!     end if

!  else !if theta > pi/2

!     itheta = int((theta+0.5*dtheta_reg)*dtheta_reg_1) !find closest regular grid point
!     distm = theta - colat(itheta-1)        !compute the distance to the 2 closest grid points 
!     distp = theta - colat(itheta)
!     if (abs(distm) .lt. abs(distp)) then
!        itheta = itheta-1
!        dist = distm
!     else
!        dist = distp
!     end if

!  end if

end subroutine find_closest_lateral_grid_point
!-----------------------------------------------------------------------------


subroutine find_closest_lateral_grid_point_fast(theta,itheta,dist,ilatm,ilatp) 
! similar to find_closest_lateral_grid_point but starts as close to the tracer as possible
!  use tracerstuff
  use globall

  implicit none

  real,intent(in):: theta
  integer,intent(in) :: ilatm,ilatp
  integer,intent(out):: itheta
  real,intent(out):: dist

  integer :: i0
  real,allocatable :: diff(:)

  if (theta .lt. colat(ilatm)) then
     if (ilatm .eq. 1) then
        itheta = 1
        dist = theta-colat(1)
     else
        i0 = max(1,ilatm-2)
        allocate(diff(i0:ilatm))
        diff(:) = theta-colat(i0:ilatm)
        itheta = minloc(abs(diff),DIM=1)+i0-1
        dist = diff(itheta)
        deallocate(diff)
     end if
  else if (theta .gt. colat(ilatp)) then
     if (ilatp .eq. Nlat) then
        itheta = Nlat
        dist = theta-colat(Nlat)
     else
        i0 = min(Nlat,ilatp+2)
        allocate(diff(ilatp:i0))
        diff(:) = theta-colat(ilatp:i0)
        itheta = minloc(abs(diff),DIM=1)+ilatp-1
        dist = diff(itheta)
        deallocate(diff)
     end if
  else
        stop 'ERROR: find_closest_lateral_grid_point_fast useless, tracer already between colat(ilatm) and colat(ilatp)'
  end if



!  dist = pi

!  itheta = ilatm-1 !starts from colat(1)
!  do while (abs(dist) .gt. abs(theta-colat(itheta+1)))
!     dist = theta-colat(itheta+1)
!     itheta = itheta + 1
!  end do

end subroutine find_closest_lateral_grid_point_fast
!-----------------------------------------------------------------------------


subroutine find_closest_radial_grid_point_fast (rad,irad,dist,iradm,iradp)

  use globall
  use tracerstuff

  implicit none

  integer,intent(in) :: iradm,iradp
  integer,intent(out) :: irad
  real,intent(in) :: rad
  real,intent(out) :: dist

  integer :: i0
  real,allocatable :: diff(:)

  if (rad .lt. r_local(iradm)) then
     i0 = max(NG,ideb_all-2,iradm-2)
     allocate(diff(i0:iradm))
     diff(:) = rad-r_local(i0:iradm)
     irad = minloc(abs(diff),DIM=1)+i0-1
     dist = diff(irad)
  else if (rad .gt. r_local(iradp)) then
     i0 = min(NR,ideb_all+2,iradp+2)
     allocate(diff(iradp:i0))
     diff(:) = rad-r_local(iradp:i0)
     irad = minloc(abs(diff),DIM=1)+iradp-1
     dist = diff(irad)
  else
        stop 'ERROR: find_closest_radial_grid_point_fast useless, tracer already between r(iradm) and r(iradp)'
  end if

  deallocate(diff)

end subroutine find_closest_radial_grid_point_fast
!--------------------------------------------------------------------------


subroutine check_tracers () 

!     checks for tracers erroneously outside domain boundaries

  use tracerstuff
  use mpi
!$ use OMP_LIB
  implicit none

  real mtime_debut,mtime_fin,eps,rad,theta,phi
!$ real ptime_debut,ptime_fin
  integer i,jtracked
  logical tracked
!$ integer nb_threads

  eps = 1e-10

!$OMP PARALLEL PRIVATE(i,jtracked,tracked,rad,theta,phi)

!$ nb_threads = omp_get_num_threads ()

!      IF (rank == 0) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!!$          ptime_debut = omp_get_wtime()
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
!            mtime_debut = MPI_WTime ()
!!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!      END IF

!$OMP DO SCHEDULE(DYNAMIC,size_packs) !(STATIC,ntr/nb_threads) 
  do i = 1,ntr

     rad = tra(1,i)
     theta = tra(2,i)
     phi = tra(3,i)
    
        if(rad<=r(NG)) then
           !print*,'WARNING: tracer r<r(NG)',i,(tra(j,i),j=1,3)
           tra(1,i) = r(NG)+(r(NG+1)-r(NG))*0.01 !
        end if
        if (rad>= r(NR)) then
           !print*,'WARNING: tracer r>r(NR)',i,(tra(j,i),j=1,3)
           tra(1,i) = r(NR)-(r(NG+1)-r(NG))*0.01 
        end if
        if (Aspect_ratio == 0. .and. rad .le. eps) then ! avoid tracer at the sphere's center
           tra(1,i) = r(2)*0.1
        end if
        if(theta<0.0) then
           !print*,'WARNING: tracer colat<0',i,(tra(j,i,b),j=1,3)
           tra(2,i) = -tra(2,i)
           tra(3,i) = mod(tra(3,i)+pi,domain_long)
        end if
        if (theta> pi) then
           !print*,'WARNING: tracer colat>pi',i,(tra(j,i,b),j=1,3)
	   tra(2,i) = pi-mod(tra(2,i),pi) 
           tra(3,i) = mod(tra(3,i)+pi,domain_long)
        end if
        if(phi<0.0) then
           !print*,'WARNING: tracer phi<0',i,(tra(j,i),j=1,3)
           tra(3,i) = domain_long + mod(tra(3,i),domain_long)
        end if
        if(phi>=domain_long) then
	 !print*,'WARNING: tracer phi>2*pi',i,(tra(j,i),j=1,3)
           tra(3,i) = mod(tra(3,i),domain_long)
        end if

        ! Check if tracked tracer and update
        if (tra(NCELLPOS,i) .lt. 0.) then
           call check_tracked_tracer (rad,theta,phi,tracked,jtracked)
           tracked_tracers(1:3,jtracked) = tra(1:3,i)
        end if

  end do
!$OMP END DO

!IF (rank == 0) THEN
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          ptime_fin = omp_get_wtime()
!!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele check_tracers : ', ptime_fin - ptime_debut
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            mtime_fin = MPI_WTime ()
!            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele check_tracers : ', mtime_fin - mtime_debut
!!$       END IF
!      END IF

!$OMP END PARALLEL
  
end subroutine check_tracers
!-----------------------------------------------------------------------------------


subroutine check_coordinates(rad,phi,iradm,iradp)
! make sure the tracer is not out of box limits
! case of a tracer far from poles (no check in theta)

  use tracerstuff

  implicit none

  real,intent(inout):: rad,phi
  integer,intent(inout) :: iradm,iradp 

        if(rad<=r(NG)) then
           !print*,'WARNING: tracer r<r(NG)',i,(tra(j,i),j=1,3)
           rad = r(NG)+1e-6
           iradm = NG
           iradp = NG+1
        end if
        if (Aspect_ratio == 0. .and. rad .le. 1e-10) then
           rad = r(2)*0.1
        end if
!        if(theta<0.0) then
!           !print*,'WARNING: tracer colat<0',i,(tra(j,i,b),j=1,3)
!           theta = -theta
!           phi = mod(phi+pi,domain_long)
!        end if
        if(phi<0.0) then
           !print*,'WARNING: tracer phi<0',i,(tra(j,i),j=1,3)
           phi = domain_long + mod(phi,domain_long)
        end if
        if (rad>= r(NR)) then
           !print*,'WARNING: tracer r>r(NR)',i,(tra(j,i),j=1,3)
           rad = r(NR)-1e-6
           iradm = NR-1
           iradp = NR
        end if
!        if (theta> pi) then
!           !print*,'WARNING: tracer colat>pi',i,(tra(j,i,b),j=1,3)
!           theta = pi- mod(theta,pi) 
!           phi = mod(phi+pi,domain_long)
!        end if
        if(phi>=domain_long) then
           !print*,'WARNING: tracer phi>2*pi',i,(tra(j,i),j=1,3)
	   phi = mod(phi,domain_long)  
        end if


end subroutine check_coordinates
!-----------------------------------------------------------------------------------


subroutine check_coordinates_poles(rad,theta,phi,iradm,iradp)
! make sure the tracer is not out of box limits
! case of a tracer close to poles (include check in theta)

  use tracerstuff

  implicit none

  real,intent(inout):: rad,theta,phi
  integer,intent(inout) :: iradm,iradp
 
  if(rad<=r(NG)) then
     !print*,'WARNING: tracer r<r(NG)',i,(tra(j,i),j=1,3)
     rad = r(NG)+1e-6
     iradm = NG
     iradp = NG+1
  end if
  if (rad>= r(NR)) then
     !print*,'WARNING: tracer r>r(NR)',i,(tra(j,i),j=1,3)
     rad = r(NR)-1e-6
     iradm = NR-1
     iradp = NR
  end if
  if(theta<0.0) then
     !print*,'WARNING: tracer colat<0',i,(tra(j,i,b),j=1,3)
     theta = -theta
     phi = mod(phi+pi,deux_pi)
  end if
  if (theta> pi) then
     !print*,'WARNING: tracer colat>pi',i,(tra(j,i,b),j=1,3)
     theta = pi- mod(theta,pi) 
     phi = mod(phi+pi,deux_pi)
  end if
  if(phi>=deux_pi) then
     !print*,'WARNING: tracer phi>2*pi',i,(tra(j,i),j=1,3)
     phi = mod(phi,deux_pi)  
  end if
  if(phi<0.0) then
     !print*,'WARNING: tracer phi<0',i,(tra(j,i),j=1,3)
     phi = deux_pi + mod(phi,deux_pi)
  end if
  

end subroutine check_coordinates_poles
!-----------------------------------------------------------------------------------


subroutine empty_nodes_interpolation(irad0,ilat0,ilong0,ideb,ifin,n1,n,cwork,cmean)

  use tracerstuff
  use globall

  implicit none

  integer,intent(in):: irad0,ilat0,ilong0,ideb,ifin
  real,intent(in):: cwork(nlong,nlat,ideb-2:ifin+2)
  real,intent(in):: n(nlong,nlat,ideb-2:ifin+2)
  real,intent(inout):: n1,cmean

  real dist,dist1,dist2,dist3,dist4,dist5,dist6,dl
  integer irad,ilat,ilong,npoints,nrad_search,irad_min,irad_max,nlat_search,ilat_min,ilat_max,i

!*****************************************************************************
  !take all points in a defined sphere around irad0,ilat0,ilong0 and use them to give a weighted average
  !To decrease computational time, 2 steps :
  ! 1) First exploration within a short distance dmax_empty0 may be sufficient for most points
  ! 2) If step 1) is unsuccessful, second exploration using a larger distance dmax_empty

! Initially written for composition, but also serves for temperature
!*****************************************************************************

  npoints = 0

!---------------FIRST EXPLORATION AT SHORT DISTANCE--------------------
  !  Delimit broadly a smaller portion to search points for the average

!  print*,'FIRST EXPLORATION'

  irad_min = irad_min_emptyn0(irad0)
  irad_max = irad_max_emptyn0(irad0)

  ilat_min = ilat_min_emptyn0(ilat0,irad0)
  ilat_max = ilat_max_emptyn0(ilat0,irad0)


  ! now take a contribution of all points in this portion

  do irad = irad_min,irad_max
     do ilat = ilat_min,ilat_max
        dl = 2./pi*r(irad)*sintheta(ilat)*dphi
        do ilong = 1,Nlong !2*nlong_search+1
           if (n(ilong,ilat,irad) .ne. 0. .and. (ilong*dl .lt. dmax_empty0)) then
!              print*,'ilong',ilong
              call distance_grid (irad0,ilat0,ilong0,irad,ilat,ilong,dist)
              if (dist .ne. 0.) then !(dist .lt. dmax_empty0) .and. (dist .ne. 0.)) then
                 cmean = cmean + cwork(ilong,ilat,irad)*(1/dist)
                 n1 = n1 + 1/dist
                 npoints = npoints + 1
              end if
           end if
        end do
     end do
  end do

!  print*,'NPOINTS USED:',npoints

  if (npoints == 0) then
     !------------------SECOND EXPLORATION--------------------------

!     print*,'SECOND EXPLORATION'

     !  Again, delimit broadly a smaller portion to search points for the average
     irad_min = irad_min_emptyn(irad0)
     irad_max = irad_max_emptyn(irad0)
     
     ilat_min = ilat_min_emptyn(ilat0,irad0)
     ilat_max = ilat_max_emptyn(ilat0,irad0)
     
     
     ! now take a contribution of all points in this portion
     
     do irad = irad_min,irad_max
        do ilat = ilat_min,ilat_max
           do ilong = 1,Nlong !2*nlong_search+1
              if (n(ilong,ilat,irad) .ne. 0.) then
                 call distance_grid (irad0,ilat0,ilong0,irad,ilat,ilong,dist)
                 if ((dist .lt. dmax_empty) .and. (dist .ne. 0.)) then
                    cmean = cmean + cwork(ilong,ilat,irad)*(1/dist)
                    n1 = n1 + 1/dist
                    npoints = npoints + 1
                 end if
              end if
           end do
        end do
     end do
     
  end if

 !   print*,'NPOINTS USED:',npoints

 ! print*,'Total number of points at less than dmax:',npts
 ! print*,'Number of points used for interpolation:',npoints
 ! print*


  if (npoints .eq. 0) then
    stop '- ERROR: GAP OF TRACERS SOMEWHERE, problem in tracers advection or increase Vmax_empty or ntr'
  end if

end subroutine empty_nodes_interpolation
!-----------------------------------------------------------------------------------

 
subroutine calc_Nu_T ()

  use globall
  use tracerstuff

  implicit none

  integer ilat,ilong,ir
  real Nu_T

  real,allocatable :: temp_bot(:,:,:)

  Real, POINTER, DIMENSION(:,:)    ::  Tt

  Complex yTtr2(0:LMmax+1)


  Tt => Data_spat_spec (:,:,7)

  allocate(temp_bot(Nlong,Nlat,3))


  Nu_T = 0.

  ! first, extract the 3 first radial layers of temperature
  do ir=NG,NG+2

     yTtr2=yTtr(:,ir)

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
     do iblock=1,nblock
        call Spec_Spat_Scal_Shell(yTtr2,Tt,latstart(iblock),latstop(iblock))
     enddo
!$OMP END PARALLEL DO
     
     call fourierr_spec_spat2(Data_spec_spat)     

     temp_bot(:,:,ir) = Tt(1:Nlong,:)!+C_cond(ir)

  end do
  
  !now compute the heat flux with a 3rd order scheme
  do ilat = 1,Nlat
     do ilong = 1,Nlong
        Nu_T = Nu_T + icb_dS(ilong,ilat) * cfb4 *(cfb3*temp_bot(ilong,ilat,NG)   + &
                                                  cfb2*temp_bot(ilong,ilat,NG+1) + &
                                                  cfb1*temp_bot(ilong,ilat,NG+2)   )
     end do
  end do

  Nu_T = -Aspect_ratio * Nu_T/surf_icb !(4*pi*r(NG)**2)

  print*,'-------------------------'
  print*,'Nu_T =',Nu_T
  print*,'-------------------------'

  deallocate(temp_bot)

end subroutine calc_Nu_T
!------------------------------------------------------------
 

subroutine calc_Nu_C ()

  use globall
  use tracerstuff
 
  implicit none

  integer ilat,ilong,ir
  real NuC
  real,allocatable :: comp_bot(:,:,:)
  real data(Nphi,Nlat)
  Complex yCr2(0:LMmax+1)


  allocate(comp_bot(Nlong,Nlat,NG:NG+2))

  NuC = 0.

  do ir=NG,NG+2

     yCr2(:) = yCr(:,ir)

     do iblock=1,nblock
        call Spec_Spat_Scal_shell(yCr2,data,latstart(iblock),latstop(iblock))
     enddo
     call fourtf(data,Nphi,1,Nlat)
!     comp_bot(:,:,ir) = comp(:,:,ir) - C_cond(ir)
 comp_bot(:,:,ir) = data(1:Nlong,:)    
  end do

  !now compute the heat flux with a 3rd order scheme
  do ilat = 1,Nlat
     do ilong = 1,Nlong
        NuC = NuC + icb_dS(ilong,ilat) *cfb4 *(cfb3*comp_bot(ilong,ilat,NG)   + &
                                               cfb1*comp_bot(ilong,ilat,NG+1) + &
                                               cfb2*comp_bot(ilong,ilat,NG+2)   )
     end do
  end do
  
  NuC = -Aspect_ratio * NuC/surf_icb !(4*pi*r(NG)**2)

  print*,'-------------------------'
  print*,'Nu_C =',NuC
  print*,'-------------------------'

  deallocate(comp_bot)

end subroutine calc_Nu_C
!------------------------------------------------------------
 

subroutine distance(irad0,ilat0,ilong0,rad,theta,phi,dist)

  use tracerstuff
  use globall

  implicit none

  integer,intent(in):: irad0,ilat0,ilong0
  real,intent(in):: rad,theta,phi
  real,intent(out):: dist

  real d2_1,d2_2,d2_3

! calculates distance (dist) between two points (irad0,ilat0,ilong0) and (irad,ilat,ilong) 
! in spherical coordinates

  d2_1 = (rad*cos(phi)*sin(theta) - r(irad0)*cosphi(ilong0)*sintheta(ilat0))**2
  d2_2 = (rad*sin(phi)*sin(theta) - r(irad0)*sinphi(ilong0)*sintheta(ilat0))**2
  d2_3 = (rad*cos(theta) - r(irad0)*costheta(ilat0))**2

  dist = sqrt(d2_1 + d2_2 + d2_3)


end subroutine distance
!-----------------------------------------------------------------------------------


subroutine distance_grid (irad0,ilat0,ilong0,irad1,ilat1,ilong1,dist)

  ! Same as distance but faster because does not recalculates the sin/cos
  ! which are already stored in memory

  use tracerstuff
  use globall

  implicit none

  integer,intent(in):: irad0,ilat0,ilong0,irad1,ilat1,ilong1
  real,intent(out):: dist

  real d2_1,d2_2,d2_3

! calculates distance (dist) between two points (irad0,ilat0,ilong0) and (irad,ilat,ilong) 
! in spherical coordinates

  d2_1 = (r(irad1)*cosphi(ilong1)*sintheta(ilat1) - r(irad0)*cosphi(ilong0)*sintheta(ilat0))**2
  d2_2 = (r(irad1)*sinphi(ilong1)*sintheta(ilat1) - r(irad0)*sinphi(ilong0)*sintheta(ilat0))**2
  d2_3 = (r(irad1)*costheta(ilat1) - r(irad0)*costheta(ilat0))**2

  dist = sqrt(d2_1 + d2_2 + d2_3)


end subroutine distance_grid
!-----------------------------------------------------------------------------------


subroutine imposeHS(rad,theta,phi,l,m,Y)

implicit none

real,intent(in):: rad,theta,phi
integer,intent(in):: l,m
real,intent(out):: Y

real plgndr2

call pbar(l,m,theta,plgndr2)

Y = cos(m*phi)*plgndr2


end subroutine imposeHS
!-----------------------------------------------------------------


subroutine calc_mean4 (X,d1,d2,d3,d4,Xmean) 

  implicit none

  integer,intent(in):: d1,d2,d3,d4 
  real,intent(in):: X(d1,d2,d3,d4)
  real,intent(out):: Xmean(d4)

  integer icomp
  
  do icomp = 1,d4
     Xmean(icomp) = sum(X(:,:,:,icomp) * dV2(:,:,:)) / sum(dV2(:,:,:))
  end do

end subroutine calc_mean4
!----------------------------------------------------------------


subroutine inverse(a,c,n)
!============================================================
! Inverse matrix
! Method: Based on Doolittle LU factorization for Ax=b
! Alex G. December 2009
!-----------------------------------------------------------
! input ...
! a(n,n) - array of coefficients for matrix A
! n      - dimension
! output ...
! c(n,n) - inverse matrix of A
! comments ...
! the original matrix a(n,n) will be destroyed 
! during the calculation
!===========================================================
implicit none 
integer n
real a(n,n), c(n,n)
real L(n,n), U(n,n), b(n), d(n), x(n)
real coeff
integer i, j, k

! step 0: initialization for matrices L and U and b
! Fortran 90/95 aloows such operations on matrices
L=0.0
U=0.0
b=0.0

! step 1: forward elimination
do k=1, n-1
   do i=k+1,n
      coeff=a(i,k)/a(k,k)
      L(i,k) = coeff
      do j=k+1,n
         a(i,j) = a(i,j)-coeff*a(k,j)
      end do
   end do
end do

! Step 2: prepare L and U matrices 
! L matrix is a matrix of the elimination coefficient
! + the diagonal elements are 1.0
do i=1,n
  L(i,i) = 1.0
end do
! U matrix is the upper triangular part of A
do j=1,n
  do i=1,j
    U(i,j) = a(i,j)
  end do
end do

! Step 3: compute columns of the inverse matrix C
do k=1,n
  b(k)=1.0
  d(1) = b(1)
! Step 3a: Solve Ld=b using the forward substitution
  do i=2,n
    d(i)=b(i)
    do j=1,i-1
      d(i) = d(i) - L(i,j)*d(j)
    end do
  end do
! Step 3b: Solve Ux=d using the back substitution
  x(n)=d(n)/U(n,n)
  do i = n-1,1,-1
    x(i) = d(i)
    do j=n,i+1,-1
      x(i)=x(i)-U(i,j)*x(j)
    end do
    x(i) = x(i)/u(i,i)
  end do
! Step 3c: fill the solutions x(n) into column k of C
  do i=1,n
    c(i,k) = x(i)
  end do
  b(k)=0.0
end do
end subroutine inverse
!--------------------------------------------------------


SUBROUTINE M33INV (A, AINV, OK_FLAG)

IMPLICIT NONE

    real, DIMENSION(3,3), INTENT(IN)  :: A
    real, DIMENSION(3,3), INTENT(OUT) :: AINV
    LOGICAL, INTENT(OUT) :: OK_FLAG

    real, PARAMETER :: EPS = 1.0D-10
    real :: DET
    real, DIMENSION(3,3) :: COFACTOR


    DET =   A(1,1)*A(2,2)*A(3,3)  &
            - A(1,1)*A(2,3)*A(3,2)  &
            - A(1,2)*A(2,1)*A(3,3)  &
            + A(1,2)*A(2,3)*A(3,1)  &
            + A(1,3)*A(2,1)*A(3,2)  &
            - A(1,3)*A(2,2)*A(3,1)

    IF (ABS(DET) .LE. EPS) THEN
        AINV = 0.0D0
        OK_FLAG = .FALSE.
        RETURN
    END IF

    COFACTOR(1,1) = +(A(2,2)*A(3,3)-A(2,3)*A(3,2))
    COFACTOR(1,2) = -(A(2,1)*A(3,3)-A(2,3)*A(3,1))
    COFACTOR(1,3) = +(A(2,1)*A(3,2)-A(2,2)*A(3,1))
    COFACTOR(2,1) = -(A(1,2)*A(3,3)-A(1,3)*A(3,2))
    COFACTOR(2,2) = +(A(1,1)*A(3,3)-A(1,3)*A(3,1))
    COFACTOR(2,3) = -(A(1,1)*A(3,2)-A(1,2)*A(3,1))
    COFACTOR(3,1) = +(A(1,2)*A(2,3)-A(1,3)*A(2,2))
    COFACTOR(3,2) = -(A(1,1)*A(2,3)-A(1,3)*A(2,1))
    COFACTOR(3,3) = +(A(1,1)*A(2,2)-A(1,2)*A(2,1))

    AINV = TRANSPOSE(COFACTOR) / DET

    OK_FLAG = .TRUE.

    RETURN

END SUBROUTINE M33INV
!---------------------------------------------------------------------


REAL FUNCTION FindDet(matrix, n)
    IMPLICIT NONE
    REAL, DIMENSION(n,n) :: matrix
    INTEGER, INTENT(IN) :: n
    REAL :: m, temp
    INTEGER :: i, j, k, l
    LOGICAL :: DetExists = .TRUE.
    l = 1
    !Convert to upper triangular form
    DO k = 1, n-1
        IF (matrix(k,k) == 0) THEN
            DetExists = .FALSE.
            DO i = k+1, n
                IF (matrix(i,k) /= 0) THEN
                    DO j = 1, n
                        temp = matrix(i,j)
                        matrix(i,j)= matrix(k,j)
                        matrix(k,j) = temp
                    END DO
                    DetExists = .TRUE.
                    l=-l
                    EXIT
                ENDIF
            END DO
            IF (DetExists .EQV. .FALSE.) THEN
                FindDet = 0
                return
            END IF
        ENDIF
        DO j = k+1, n
            m = matrix(j,k)/matrix(k,k)
            DO i = k+1, n
                matrix(j,i) = matrix(j,i) - m*matrix(k,i)
            END DO
        END DO
    END DO
    
    !Calculate determinant by finding product of diagonal elements
    FindDet = l
    DO i = 1, n
        FindDet = FindDet * matrix(i,i)
    END DO
    
END FUNCTION FindDet
!----------------------------------------------------------------------


real function init_composition (rad,theta,phi,comp_mode,lc,mc)

! use globall

  implicit none

  integer,intent(in):: comp_mode,lc,mc
  real,intent(in):: rad,theta,phi

  if (comp_mode == 1) then !initial composition = 0
     init_composition = 0.
  else if (comp_mode == 2) then !initialise with a spherical harmonic (lc,mc)
     call imposeHS(rad,theta,phi,lc,mc,init_composition)
  else 
     print*,'ERROR: comp_mode must be 1 or 2'
  end if

end function init_composition
!----------------------------------------------------------


subroutine write_files_real (data,filename,ir_deb,ir_fin)

  use tracerstuff
  use globall
!  use mod_parallel
  use mpi

  implicit none

  integer,intent(in):: ir_deb,ir_fin
  real,intent(in):: data(Nlong,Nlat,ir_deb:ir_fin)
  character*30,intent(in):: filename

  integer descripteur,nb_octets,nb_elements,ilong,ilat,irad,ideb_all0,ier
  integer(kind=MPI_OFFSET_KIND):: position_file
  integer, dimension(MPI_STATUS_SIZE) :: statut

  integer irank,ir

!  do irank=0,size-1
!     if (rank==irank) then
!        open (300,file=trim(filename),position='append',form='unformatted')
!        do ir = ideb_all,ifin_all
!           do ilat=1,Nlat
!              write(300) (real(data(ilong,ilat,ir),4),ilong=1,Nlong)
!           end do
!        end do
!        close(300)
!     end if
!     call mpi_barrier(MPI_COMM_WORLD,ier)
!     call sleep(0.5)
!  end do
  

  call MPI_TYPE_SIZE (MPI_DOUBLE_PRECISION,nb_octets,ier)

  if (rank==0) ideb_all0 = ideb_all
  call MPI_BCAST (ideb_all0,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)

  nb_elements = Nlong*Nlat*(ifin_all-ideb_all+1)
  position_file = Nlong*Nlat*(ideb_all-ideb_all0)*nb_octets

  call MPI_FILE_OPEN (MPI_COMM_WORLD,trim(filename),MPI_MODE_WRONLY+MPI_MODE_CREATE, &
                      MPI_INFO_NULL,descripteur,ier)

  call MPI_FILE_WRITE_AT_ALL (descripteur,position_file,data,nb_elements,MPI_DOUBLE_PRECISION,statut,ier)

  call MPI_FILE_CLOSE (descripteur,ier)


end subroutine write_files_real
!-------------------------------------------------------------


subroutine write_files_integer (data,filename,ir_deb,ir_fin)

  use tracerstuff
  use globall
  use mpi

  implicit none

  integer,intent(in):: ir_deb,ir_fin
  integer,intent(in):: data(Nlong,Nlat,ir_deb:ir_fin)
  character*30,intent(in):: filename

  integer descripteur,nb_octets,nb_elements,ilong,ilat,irad,ideb_all0,ier
  integer(kind=MPI_OFFSET_KIND):: position_file
  integer, dimension(MPI_STATUS_SIZE) :: statut

  integer irank,ir

!  do irank=0,size-1
!     if (rank==irank) then
!        open (300,file=trim(filename),position='append',form='unformatted')
!        do ir = ideb_all,ifin_all
!           do ilat=1,Nlat
!              write(300) (int(data(ilong,ilat,ir),4),ilong=1,Nlong)
!           end do
!        end do
!        close(300)
!     end if
!  end do

  call MPI_TYPE_SIZE (MPI_INTEGER,nb_octets,ier)

  if (rank==0) ideb_all0 = ideb_all
  call MPI_BCAST (ideb_all0,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)

  nb_elements = Nlong*Nlat*(ifin_all-ideb_all+1)
  position_file = Nlong*Nlat*(ideb_all-ideb_all0)*nb_octets

  call MPI_FILE_OPEN (MPI_COMM_WORLD,trim(filename),MPI_MODE_WRONLY+MPI_MODE_CREATE, &
                      MPI_INFO_NULL,descripteur,ier)

  call MPI_FILE_WRITE_AT_ALL (descripteur,position_file,data(1,1,ideb_all),nb_elements,MPI_INTEGER,statut,ier)

  call MPI_FILE_CLOSE (descripteur,ier)


end subroutine write_files_integer
!-------------------------------------------------------------


subroutine balance_domains_for_tracers ()

! Reequilibration of the MPI-domains: modifies ideb_all and ifin_all (the boundaries of domains)
! to try to get equal volume for each domain, so that each MPI-proc has an equal number of tracers 

  use globall,only:NG,NR,ideb_all,ifin_all,size,rank
  use mpi

  implicit none

  integer finished

  integer i,irad,istep,nstep_max,ier
  real vmax,vmin,imax,imin, ratio_old,ratio_new
  real,allocatable:: r(:),vol_rank(:)
!  integer,allocatable:: npts_rank(:),ir_start(:),ir_end(:)
  integer,dimension(0:size-1)::ir_start,ir_end
  integer,allocatable:: npts_rank(:)

  !RECALCULER MANUELLEMENT r,drad2

! IL FAUT ENCORE AMELIORER CETTE ROUTINE POUR AVOIR TOUJOURS VMAX/VMIN MINIMAL ET PAS UNE OSCILLATION !!!


!  if (rank == 0) then
!     allocate (ir_start(0:size-1),ir_end(0:size-1))
!  end if
     
  call MPI_BARRIER (MPI_COMM_WORLD,ier)
  call MPI_GATHER (ideb_all,1,MPI_INTEGER,ir_start,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
  call MPI_GATHER (ifin_all,1,MPI_INTEGER,ir_end,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)

  if (rank == 0) then

     allocate(r(0:NR),vol_rank(0:size-1),npts_rank(0:size-1))
     !compute r
     call init_r (r)


     !computes the volume of each domain
     do i=0,size-1
        vol_rank(i) = 4./3.*(4.*atan(1.))* (r(ir_end(i))**3-r(ir_start(i))**3)
        npts_rank(i) = ir_end(i)-ir_start(i)+1
     end do

     imin = 0
     imax = 0

     istep = 0
     nstep_max = 100 !the algorithm should converge in less than 100 iterations. 
                     !However, for large numbers of points in the r direction,
                     !this could be insufficient : nstep_max could be an input parameter?? 
                     !or calculate it according to the number of points and procs ???

     finished = 0

     ratio_old = maxval(vol_rank(:))/minval(vol_rank(:))

!     print*,'npts_rank',npts_rank
!     print*,'vol_rank',vol_rank
!     print*,'vmax/vmin',ratio_old
!     print*
  
     do while (finished .ne. 1)

        !find the biggest and smallest volumes
        vmax = maxval(vol_rank(:))
        vmin = minval(vol_rank(:))

        do i=0,size-1
           if (vol_rank(i) == vmin) imin = i
           if (vol_rank(i) == vmax) imax = i
        end do

        !substract one point from the biggest volume and add one to the smallest:
        if (npts_rank(imax) .gt. 4) then
           npts_rank(imin) = npts_rank(imin)+1
           npts_rank(imax) = npts_rank(imax)-1
        else
           finished = 1
        end if
        
        !compute the new domains boundaries
        do i=1,size-1
           ir_start(i) = ir_start(i-1) + npts_rank(i-1) 
        end do
        do i=0,size-2
           ir_end(i) = ir_start(i) + npts_rank(i) - 1
        end do
        
        !computes the new volume of each domain
        do i=0,size-1
           vol_rank(i) = 4./3.*(4.*atan(1.))* (r(ir_end(i))**3-r(ir_start(i))**3)
        end do
        
        istep = istep+1
        if (istep == nstep_max) finished = 1
        
!        print*,'istep',istep
!        print*,'ir_start',ir_start
!        print*,'ir_end',ir_end
!        print*,'npts_rank',npts_rank
!        print*,'vol_rank',vol_rank
        print*,'vmax/vmin',maxval(vol_rank(:))/minval(vol_rank(:))
        !        print*

        rank_time = maxloc(vol_rank(:),DIM=1)
        print*,'rank_time,vmax',rank_time,vol_rank(rank_time)

     end do

  end if

  call MPI_BARRIER (MPI_COMM_WORLD,ier)
  call MPI_BCAST   (rank_time,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
  call MPI_SCATTER (ir_start,1,MPI_INTEGER,ideb_all,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
  call MPI_SCATTER (ir_end,1,MPI_INTEGER,ifin_all,1,MPI_INTEGER,0,MPI_COMM_WORLD,ier)
  call MPI_BARRIER (MPI_COMM_WORLD,ier)

  if (rank==0) then
     deallocate (r,vol_rank,npts_rank)!,ir_start,ir_end)
  end if

  print*,'rank,ideb_all,ifin_all',rank,ideb_all,ifin_all

 call MPI_BARRIER (MPI_COMM_WORLD,ier)

end subroutine balance_domains_for_tracers
!-----------------------------------------------------


subroutine init_r (r)

use globall,only: NG,NR,Ratio1,Ratio2,Aspect_ratio,rank

implicit none

real,intent(inout) :: r(0:NR)

integer :: i,ir
integer :: l,m
integer :: ier
real :: tmp1, tmp2

  !       Initialisation de "r".

  !  Ratio1 = nb intervals reguliers / nb intervals decroissants.
  !  Ratio2 = dx_min / dx_max.
  !  tmp1 : dx local.
  !  tmp2 : coefficient eta de decroissance geometrique.
  !  l    : nb inter irreg
  !  m    : nb inter reguliers


  if (Ratio2.eq.1.0) then
     tmp1 = (1.-Aspect_ratio)/real(NR-NG)
     r(nr) = 1.0
     do i = NR-1, NG,-1
        r(i) = r(i+1) - tmp1
     enddo

  else

     r(NG) = Aspect_ratio

     l = int( real(NR-NG) / (2.*(1. + Ratio1)))

     Ratio1 = real(NR-NG)/ real(2*l) - 1.


     if (Aspect_ratio.ne.0.) then

     m = NR - NG - 2*l

     tmp2 = exp(alog(Ratio2)/real(l))

     tmp1 = 1.
     do i = 1, l
        tmp1 = tmp1 * tmp2
     enddo
     tmp1 = (1. - Aspect_ratio) / (real(m) + 2. * tmp2 * &
          ((1.-tmp1)/(1.-tmp2)))

     else

     m = NR - NG - l
     tmp2 = exp(alog(Ratio2)/real(l))

     tmp1 = 1.
     do i = 1, l
        tmp1 = tmp1 * tmp2
     enddo
     tmp1 = (1. - Aspect_ratio) / (real(m) + tmp2 * &
          ((1.-tmp1)/(1.-tmp2)))
     endif

     do i = 1, l
        tmp1 = tmp1 * real(tmp2)
     enddo

     if (Aspect_ratio.ne.0.) then

     do i = 1, l
        r(NG+i) = r(NG+i-1) + tmp1
        tmp1 = tmp1 / tmp2
     enddo
     do i = 1, m
        r(NG+l+i) = r(NG+l+i-1) + tmp1
     enddo

     do i = 1, l
        tmp1 = tmp1 * tmp2
        r(NG+l+m+i) = r(NG+l+m+i-1) + tmp1
     enddo

     else

     do i = 1, l
     tmp1 = tmp1 / tmp2
     enddo
     do i = 1, m
        r(NG+i) = r(NG+i-1) + tmp1
     enddo
     do i = 1, l
        tmp1 = tmp1 * tmp2
        r(NG+m+i) = r(NG+m+i-1) + tmp1
     enddo
     endif

     if (r(NR) .ne. 1.0) then
        if (abs(r(NR) - 1.0).gt. tmp1) then
           if(rank==0) print *,'PB RAYON externe!!!'
           call MPI_finalize(ier)
           stop
        else
           r(NR)=1.0
        endif
     endif


     ! maillage graine serie arithmetique...

     if (NG.gt.1) then
        tmp2 = 2.0/real(NG*(NG-1)) * (Aspect_ratio - real(NG)*tmp1)

        do i = NG-1, 0 , -1
           r(i) = r(i+1) - tmp1
           tmp1 = tmp1 + tmp2
        enddo

        if (r(0) .ne. 0.) then
           if (abs(r(0)).gt. tmp1) then
              if(rank==0) print *,'PB RAYON centre!!!'
              call MPI_finalize(ier)
              stop
           else
              if(rank==0) print *,'On le met a 0.0'
              r(0)=0.0
           endif
        endif

     endif

  endif

  ! Modif de la grille pour comparaison avec Zhang et Jones,
  ! ou Sun et al. (i.e. d = r_e-r_i = 1 <==> Tchebychev).

  if (Grille) then
     do ir = 0, NR
        r(ir) = r(ir) / (Real(1.0) - Aspect_ratio)
     enddo
  endif

end subroutine init_r
!--------------------------------------------------------

subroutine Spat_Spec_C (c)

  use tracerstuff
  use globall
!$ use OMP_LIB

  implicit none

  real,intent(in) :: c(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2)

  integer ir,m
  real data(Nphi,Nlat)


  do ir=ideb_all,ifin_all

     data(1:Nlong,:) = c(:,:,ir)
     data(Nlong+1:Nphi,:) = 0.

     call fourtf(data,Nphi,-1,Nlat)

     do iblock=1,maxmblock
        if (iblock.ne.nmblock+1) then
           do m=mstart(iblock),mstop(iblock)            
              call Spat_Spec_scal_shell(data,yCr(:,ir),m)
              call Spat_Spec_scal_shell(data,yCr(:,ir),mstop(2*nmblock)+1-m)
           enddo
        else
           do m=mstop(2*nmblock)+1,Alpha_max+1
              call Spat_Spec_scal_shell(data,yCr(:,ir),m)
           enddo
        endif
     enddo
     
  end do

end subroutine Spat_Spec_C
!----------------------------------------------


subroutine Spec_spat_C (c)

  use tracerstuff
  use globall
!$ use OMP_LIB

  implicit none

  real,intent(out):: c(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2)

  integer ir
  real data(Nphi,Nlat)
  Complex yCr2(0:LMmax+1)

  do ir=ideb_all,ifin_all

     yCr2(:) = yCr(:,ir)

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
     do iblock=1,nblock
        call Spec_Spat_Scal_shell(yCr2,data,latstart(iblock),latstop(iblock))
     enddo
!$OMP END PARALLEL DO
     call fourtf(data,Nphi,1,Nlat)
     c(:,:,ir) = data(1:Nlong,:)
     
  end do


end subroutine Spec_spat_C
!-----------------------------------------------------


subroutine Spat_Spec_T (Temp)

  use globall
  use tracerstuff,only: ideb_Ttra,ifin_Ttra

  implicit none

  real,intent(in) :: Temp(nlong,nlat,ideb_Ttra-2:ifin_Ttra+2)

  integer ir,m
!  real data(Nphi,Nlat)

  Real, POINTER, DIMENSION(:,:)    ::  Tt

  Tt => Data_spat_spec (:,:,7)

  do ir=ideb_all,ifin_all

     Tt(1:Nlong,:) = Temp(:,:,ir)

!     call fourtf(data,Nphi,-1,Nlat)
     call fourierr_spat_spec2(Data_spat_spec)

!$OMP PARALLEL DO SCHEDULE (DYNAMIC,1)
     do iblock=1,maxmblock
        if (iblock.ne.nmblock+1) then
           do m=mstart(iblock),mstop(iblock)
              call Spat_Spec_scal_shell(Tt,yTtr(:,ir),m)
              call Spat_Spec_scal_shell(Tt,yTtr(:,ir),mstop(2*nmblock)+1-m)
           enddo
        else
           do m=mstop(2*nmblock)+1,Alpha_max+1
              call Spat_Spec_scal_shell(Tt,yTtr(:,ir),m)
           enddo
        endif

     enddo
!$OMP END PARALLEL DO

     
  end do

end subroutine Spat_Spec_T
!-------------------------------------------------------


subroutine Spec_spat_T (Temp)

  use globall
  use tracerstuff,only: ideb_Ttra,ifin_Ttra
!$ use OMP_LIB

  implicit none

  real,intent(out) :: Temp(nlong,nlat,ideb_Ttra-2:ifin_Ttra+2)

  integer ir

  Complex yTtr2(0:LMmax+1)
!  real data(Nphi,Nlat) !Size of data ????

  Real, POINTER, DIMENSION(:,:)    ::  Tt

  Tt => Data_spec_spat (:,:,7)

  do ir=ideb_all,ifin_all

!      do iblock=1,nblock
!         call Spec_Spat_Scal_shell(yTtr(:,ir),data,latstart(iblock),latstop(iblock))
!      enddo
!      call fourtf(data,Nphi,1,Nlat)
!      temp(:,:,ir) = data(:,:)

     yTtr2=yTtr(:,ir)

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
     do iblock=1,nblock
        call Spec_Spat_Scal_Shell(yTtr2,Tt,latstart(iblock),latstop(iblock))
     enddo
!$OMP END PARALLEL DO
     
     call fourierr_spec_spat2(Data_spec_spat)     

     Temp(:,:,ir) = Real(Tt(:,:),8)

  end do

end subroutine Spec_spat_T
!------------------------------------------------------------------


subroutine impose_rotation_axis(theta_r,phi_r,vel)

! theta_r, phi_r : define rotation axis

  use tracerstuff

  implicit none

  real,intent(in):: theta_r,phi_r
  real,intent(out):: vel(3,Nlong,Nlat,ideb_veltra-2:ifin_veltra+2)

  real s,A,B,E,N,Nv
  real,dimension(3,3) :: P
  real,dimension(3)   :: v_cart,u,pos
  integer irad,ilat,ilong


  ! u = unit vector parallel to the rotation axis, in cartesian coordinates
  u(1) = sin(theta_r)*cos(phi_r)
  u(2) = sin(theta_r)*sin(phi_r)
  u(3) = cos(theta_r)

  do irad=ideb_all,ifin_all
     do ilat=1,nlat
        do ilong=1,nlong
 
           !pos = vector position of grid points in cartesian coordinates
           pos(1) = r(irad)*sin(colat(ilat))*cos(long(ilong))
           pos(2) = r(irad)*sin(colat(ilat))*sin(long(ilong))
           pos(3) = r(irad)*cos(colat(ilat))

           !terms that appear in the calculation
           s = pos(1)*u(1) + pos(2)*u(2) + pos(3)*u(3) != u.pos
           N = sqrt(pos(1)**2+pos(2)**2+pos(3)**2)

           !v_cart = velocity in cartesian coordinates (make the calculation)
           v_cart(1) = (u(2)*(pos(3)-s*u(3)) - u(3)*(pos(2)-s*u(2)))
           v_cart(2) = (u(3)*(pos(1)-s*u(1)) - u(1)*(pos(3)-s*u(3)))
           v_cart(3) = (u(1)*(pos(2)-s*u(2)) - u(2)*(pos(1)-s*u(1)))
           
           !P = matrix to change coordinates from cartesian to spherical
           P(1,1) = (sqrt(pos(1)**2+pos(2)**2)/N)*pos(1)/sqrt(pos(1)**2+pos(2)**2) !sin(theta)*cos(phi)
           P(1,2) = (sqrt(pos(1)**2+pos(2)**2)/N)*pos(2)/sqrt(pos(1)**2+pos(2)**2) !sin(theta)*sin(phi) 
           P(1,3) = pos(3)/N !cos(theta)
           P(2,1) = pos(3)/N*pos(1)/sqrt(pos(1)**2+pos(2)**2) !cos(theta)*cos(phi)
           P(2,2) = pos(3)/N*pos(2)/sqrt(pos(1)**2+pos(2)**2) !cos(theta)*sin(phi)
           P(2,3) = -sqrt(pos(1)**2+pos(2)**2)/N !-sin(theta)
           P(3,1) = -pos(2)/sqrt(pos(1)**2+pos(2)**2) !-sin(phi)
           P(3,2) = pos(1)/sqrt(pos(1)**2+pos(2)**2) !cos(phi) 
           P(3,3) = 0

           !v = velocity in spherical coordinates = P*v_cart
           vel(1,ilong,ilat,irad) = P(1,1)*v_cart(1) + P(1,2)*v_cart(2) + P(1,3)*v_cart(3)
           vel(2,ilong,ilat,irad) = P(2,1)*v_cart(1) + P(2,2)*v_cart(2) + P(2,3)*v_cart(3)
           vel(3,ilong,ilat,irad) = P(3,1)*v_cart(1) + P(3,2)*v_cart(2) + P(3,3)*v_cart(3) 

        end do
     end do
  end do

  vel(:,:,:,:) = vel(:,:,:,:)*2.*pi/(800*dt) !r�gl� pour que 1 tour = 400 pas de temps


end subroutine impose_rotation_axis
!--------------------------------------------------------------------------------------


subroutine init_matrices_2ndorderInterp ()

! Initialise the matrices useful for 2ndorder interpolation.

  use globall
  use tracerstuff

  logical flag

  integer :: irad,ilat,ilong,irad_start,irad_end
  real,dimension(3,3) :: A,A_1


  if (rank .ne. 0) then 
     irad_start = ideb_all-2
  else
     irad_start = NG
  end if

  if (rank .ne. size-1) then
     irad_end = ifin_all+2
  else
     irad_end = NR-2
  end if

  allocate(Mat_r(3,3,irad_start:irad_end),Mat_t(3,3,Nlat-2),Mat_p(3,3))

  ! for r direction--------------------------------------
  do irad = irad_start,irad_end

     A(1,:) = (/  r(irad)  **2,  r(irad)  ,  1. /)
     A(2,:) = (/  r(irad+1)**2,  r(irad+1),  1. /)
     A(3,:) = (/  r(irad+2)**2,  r(irad+2),  1. /)

     !inverse the matrix
     call M33inv(A,A_1,flag)

     !save it
     Mat_r(:,:,irad) = A_1(:,:)

  end do

  ! for theta direction-----------------------------------
  do ilat = 1,Nlat-2

     A(1,:) = (/  colat(ilat)  **2,  colat(ilat)  ,  1. /)
     A(2,:) = (/  colat(ilat+1)**2,  colat(ilat+1),  1. /)
     A(3,:) = (/  colat(ilat+2)**2,  colat(ilat+2),  1. /)

     !inverse the matrix
     call M33inv(A,A_1,flag)

     !save it
     Mat_t(:,:,ilat) = A_1(:,:)

  end do

  ! for phi direction-------------------------------------
  ! Careful when wrapping !
!  do ilong = 1,Nlong-2
  ilong = 1 ! DEGENERACY IN LONGITUDE
    
  A(1,:) = (/  long(ilong)  **2,  long(ilong)  ,  1. /)
  A(2,:) = (/  long(ilong+1)**2,  long(ilong+1),  1. /)
  A(3,:) = (/  long(ilong+2)**2,  long(ilong+2),  1. /)

  !inverse the matrix
  call M33inv(A,A_1,flag)

  !save it
!     Mat_p(:,:,ilong) = A_1(:,:)
  Mat_p(:,:) = A_1(:,:)

!  end do

!  ! case ilong=Nlong-1
!  A(1,:) = (/  long(Nlong-1)**2,  long(Nlong-1),  1. /)
!  A(2,:) = (/  long(Nlong)  **2,  long(Nlong)  ,  1. /)
!  A(3,:) = (/  domain_long  **2,  domain_long  ,  1. /)

!  !inverse the matrix
!  call M33inv(A,A_1,flag)

!  !save it
!  Mat_p(:,:,Nlong-1) = A_1(:,:)

!  ! case ilong=Nlong
!  A(1,:) = (/  long(Nlong)**2       ,  long(Nlong)    ,  1. /)
!  A(2,:) = (/  domain_long**2       ,  domain_long     ,  1. /)
!  A(3,:) = (/  (domain_long+dphi)**2,  domain_long+dphi,  1. /)

!  !inverse the matrix
!  call M33inv(A,A_1,flag)

!  !save it
!  Mat_p(:,:,Nlong) = A_1(:,:)

end subroutine init_matrices_2ndorderInterp
!--------------------------------------------------------------------


subroutine calc_direction_2ndorderInterp ()

! For each component of the velocity (Vr,Vt,Vp), calculates the second derivative
! in each direction (r,theta,phi). The direction along which the second derivative is 
! larger (in absolute value) is the direction for the quadratic interpolation

!QUESTION : FAUT-IL METTRE LE TABLEAU direction_2ndorder_Interp en integer*4 (�conomie m�moire) 
! mais n�cessite une conversion dans la routine suivante ?

  use globall
  use tracerstuff
!$ USE OMP_LIB

  integer :: irad,ilat,ilong,irad_start,irad_end
  real ::    d2V(3)

  irad_start = ideb_all
  irad_end = ifin_all
  if (rank == 0) irad_start = NG+1
  if (rank == size-1) irad_end = NR-1  !A CALCULER AU DEPART PENDANt L'INITIALISATION ??

!$OMP PARALLEL PRIVATE(ilat,ilong,d2V)
!$OMP DO SCHEDULE(DYNAMIC,1)
  do irad = irad_start,irad_end !Interpolation // to r in boundary layers
     do ilat = 2,Nlat-1 !interpolation // to r at poles
        do ilong = 1,Nlong
           
           ! calculates absolute values of second derivatives of Vr
           call calc_abs_sec_deriv (irad,ilat,ilong,vel_spat,1,d2V)
           ! look for direction with larger absolute value of second derivative
           direction_2ndord_Interp(1,ilong,ilat,irad) = maxloc(d2V,DIM=1)
           
           ! same for Vt
           call calc_abs_sec_deriv (irad,ilat,ilong,vel_spat,2,d2V)
           ! look for direction with larger absolute value of second derivative
           direction_2ndord_Interp(2,ilong,ilat,irad) =  maxloc(d2V,DIM=1)

           ! same for Vp
           call calc_abs_sec_deriv (irad,ilat,ilong,vel_spat,3,d2V)  
           ! look for direction with larger absolute value of second derivative
           direction_2ndord_Interp(3,ilong,ilat,irad) = maxloc(d2V,DIM=1)

           
        end do
     end do
  end do
!$OMP END DO
!$OMP END PARALLEL

  call exchange_ghost_planes_dir (direction_2ndord_Interp,1,Nlat)


end subroutine calc_direction_2ndorderInterp
!-----------------------------------------------------------------------------


subroutine calc_abs_sec_deriv (irad,ilat,ilong,vel,i,d2V)

  use globall
  use tracerstuff

  integer,intent(in) :: irad,ilat,ilong,i
  real,intent(in) :: vel(3,Nlong,Nlat,ideb_veltra-2:ifin_veltra+2)
  real,intent(out) :: d2V(3)

  integer ilongm,ilongp

  !second derivative along the r direction (non regular spacing)
  d2V(1) = coeff_second_deriv_r(1,irad) * vel(i,ilong,ilat,irad-1) + &
           coeff_second_deriv_r(2,irad) * vel(i,ilong,ilat,irad  ) + &
           coeff_second_deriv_r(3,irad) * vel(i,ilong,ilat,irad+1)

  !second derivative along the theta direction (with the approximation that the spacing is quasi regular)
  d2V(2) = coeff_second_deriv_t(irad) * (vel(i,ilong,ilat-1,irad) - 2.*vel(i,ilong,ilat,irad) + vel(i,ilong,ilat+1,irad))
  

  !second derivative along the phi direction (regular spacing)
  ilongm = ilong-1
  ilongp = ilong+1
  if (ilong .eq. 1) ilongm = Nlong
  if (ilong .eq. Nlong) ilongp = 1
  d2V(3) = coeff_second_deriv_p(ilat,irad) * (vel(i,ilongm,ilat,irad) - 2.*vel(i,ilong,ilat,irad) + vel(i,ilongp,ilat,irad))

  
  d2V = abs(d2V)

end subroutine calc_abs_sec_deriv
!----------------------------------------------------------------


subroutine init_coeff_second_deriv ()

  ! initialise the arrays containing the coefficients which will serve
  ! when calculating the second derivative of the velocity
  ! Because only an evaluation of the second derivative is required,
  ! we make the approximation that the spacing in latitude (theta) is regular
  ! to save time and memory

  use globall,only : rank,size,NG,NR,Nlong,Nlat,sintheta,r_2,colat
  use tracerstuff!,only : ideb_veltra,ifin_veltra,coeff_second_deriv_r,coeff_second_deriv_t,coeff_second_deriv_p, &
                 !        dphi,drad1,direction_2ndord_Interp

  integer irad,ilat,ilong,irad_start,irad_end
  real dtheta,dtheta2,dphi_2,denom,dr1,dr2

  irad_start = ideb_veltra-2
  irad_end = ifin_veltra+2
  if (rank == 0) irad_start = NG+1
  if (rank == size-1) irad_end = NR-1


  allocate(coeff_second_deriv_r(3,irad_start:irad_end))
  allocate(coeff_second_deriv_t(irad_start:irad_end))
  allocate(coeff_second_deriv_p(2:Nlat,irad_start:irad_end))

  dtheta = (colat(Nlat-1)-colat(2))/(Nlat-3)
  dtheta_2 = (1./dtheta)**2

  dphi_2 = (1./dphi)**2

  do irad = irad_start,irad_end
     dr1 = drad1(irad-1)
     dr2 = drad1(irad)
     denom = 1./(dr1*dr2*(dr1+dr2)/2.) 
     
     coeff_second_deriv_r(1,irad) = dr2 * denom 
     coeff_second_deriv_r(2,irad) = -(dr1+dr2) * denom
     coeff_second_deriv_r(3,irad) = dr1 * denom

     do ilat = 2,Nlat-1
        coeff_second_deriv_p(ilat,irad) = r_2(irad) * (1./sintheta(ilat))**2 * dphi_2
     end do

     coeff_second_deriv_t(irad) = r_2(irad) * dtheta_2 
  end do


!allocate and initialise array for the direction of quadratic interpolation
  allocate(direction_2ndord_Interp(3,Nlong,Nlat,ideb_veltra-2:ifin_veltra+2))
  
  direction_2ndord_Interp(:,:,:,:) = 1


end subroutine init_coeff_second_deriv
!----------------------------------------------------------------



subroutine quadratic_interpolation_r (irad0,irad1,irad2,ilatm,ilatp,ilongm,ilongp,dist_lat,dist_long,rad,vel, &
                                      Nr_deb,Nr_fin,Nlat,Nlong,vi,i)

  use tracerstuff,only : Mat_r

  integer,intent(in) :: irad0,irad1,irad2,ilatm,ilatp,ilongm,ilongp,Nr_deb,Nr_fin,Nlat,Nlong,i
  real,intent(in) :: dist_lat,dist_long,rad
  real,intent(in) :: vel(3,nlong,nlat,Nr_deb:Nr_fin)
  real,intent(out) :: vi(3)

  real w1,w2,w3,w4
  real B(3),X(3),A_1(3,3)

  w1 = (1-dist_long)*(1-dist_lat) 
  w2 =   dist_long  *(1-dist_lat) 
  w3 = (1-dist_long)*  dist_lat   
  w4 =   dist_long  *  dist_lat
 

  B(1) = w1 * vel(i,ilongm,ilatm,irad0) + &
         w2 * vel(i,ilongp,ilatm,irad0) + &
         w3 * vel(i,ilongm,ilatp,irad0) + &
         w4 * vel(i,ilongp,ilatp,irad0)
  
  B(2) = w1 * vel(i,ilongm,ilatm,irad1) + &
         w2 * vel(i,ilongp,ilatm,irad1) + &
         w3 * vel(i,ilongm,ilatp,irad1) + &
         w4 * vel(i,ilongp,ilatp,irad1) 
  
  B(3) = w1 * vel(i,ilongm,ilatm,irad2) + &
         w2 * vel(i,ilongp,ilatm,irad2) + &
         w3 * vel(i,ilongm,ilatp,irad2) + &
         w4 * vel(i,ilongp,ilatp,irad2)
  
  A_1(:,:) = Mat_r(:,:,irad0)

  X(:) = matmul(A_1,B)


  vi(i) = X(1)*rad**2 + X(2)*rad + X(3)
     
end subroutine quadratic_interpolation_r
!---------------------------------------------------------------------------


subroutine quadratic_interpolation_t (ilat0,ilat1,ilat2,iradm,iradp,ilongm,ilongp,dist_rad,dist_long,theta,vel, &
                                      Nr_deb,Nr_fin,Nlat,Nlong,vi,i)

  use tracerstuff,only : Mat_t
    
  integer,intent(in) :: ilat0,ilat1,ilat2,iradm,iradp,ilongm,ilongp,Nr_deb,Nr_fin,Nlat,Nlong,i
  real,intent(in) :: dist_rad,dist_long,theta
  real,intent(in) :: vel(3,nlong,nlat,Nr_deb:Nr_fin)
  real,intent(out) :: vi(3)

  real w1,w2,w3,w4
  real B(3),X(3),A_1(3,3)

  
  w1 = (1-dist_long)*(1-dist_rad) 
  w2 =   dist_long  *(1-dist_rad) 
  w3 = (1-dist_long)*  dist_rad   
  w4 =   dist_long  *  dist_rad
     
  B(1) = w1 * vel(i,ilongm,ilat0,iradm) + &
         w2 * vel(i,ilongp,ilat0,iradm) + &
         w3 * vel(i,ilongm,ilat0,iradp) + &
         w4 * vel(i,ilongp,ilat0,iradp) 

  B(2) = w1 * vel(i,ilongm,ilat1,iradm) + &
         w2 * vel(i,ilongp,ilat1,iradm) + &
         w3 * vel(i,ilongm,ilat1,iradp) + &
         w4 * vel(i,ilongp,ilat1,iradp) 

  B(3) = w1 * vel(i,ilongm,ilat2,iradm) + &
         w2 * vel(i,ilongp,ilat2,iradm) + &
         w3 * vel(i,ilongm,ilat2,iradp) + &
         w4 * vel(i,ilongp,ilat2,iradp) 

  A_1(:,:) = Mat_t(:,:,ilat0)
  
  X(:) = matmul(A_1,B)

     
  vi(i) = X(1)*theta**2 + X(2)*theta + X(3)

end subroutine quadratic_interpolation_t
!---------------------------------------------------------------------------------


subroutine quadratic_interpolation_p (ilong0,ilong1,ilong2,iradm,iradp,ilatm,ilatp,dist_rad,dist_lat,phi,vel, &
                                      Nr_deb,Nr_fin,Nlat,Nlong,vi,i)

  use tracerstuff,only : Mat_p
    
  integer,intent(in) :: ilong0,ilong1,ilong2,iradm,iradp,ilatm,ilatp,Nr_deb,Nr_fin,Nlat,Nlong,i
  real,intent(in) :: dist_rad,dist_lat,phi
  real,intent(in) :: vel(3,nlong,nlat,Nr_deb:Nr_fin)
  real,intent(out) :: vi(3)

  real w1,w2,w3,w4
  real B(3),X(3),A_1(3,3)


  w1 = (1-dist_lat)*(1-dist_rad) 
  w2 =   dist_lat  *(1-dist_rad) 
  w3 = (1-dist_lat)*  dist_rad   
  w4 =   dist_lat  *  dist_rad 

  B(1) = w1 * vel(i,ilong0,ilatm,iradm) + &
         w2 * vel(i,ilong0,ilatp,iradm) + &
         w3 * vel(i,ilong0,ilatm,iradp) + &
         w4 * vel(i,ilong0,ilatp,iradp) 
  
  B(2) = w1 * vel(i,ilong1,ilatm,iradm) + &
         w2 * vel(i,ilong1,ilatp,iradm) + &
         w3 * vel(i,ilong1,ilatm,iradp) + &
         w4 * vel(i,ilong1,ilatp,iradp) 

  B(3) = w1 * vel(i,ilong2,ilatm,iradm) + &
         w2 * vel(i,ilong2,ilatp,iradm) + &
         w3 * vel(i,ilong2,ilatm,iradp) + &
         w4 * vel(i,ilong2,ilatp,iradp) 


  A_1(:,:) = Mat_p(:,:) ! Degeneracy in longitude since regular spacing

  X(:) = matmul(A_1,B)


  vi(i) = X(1)*phi**2 + X(2)*phi + X(3)

end subroutine quadratic_interpolation_p
!----------------------------------------------------------------------


subroutine init_matrices_3Q ()

  ! Initialization of the matrices used for trilinear interpolation
  ! Degeneracy is possible in longitude since the grid is regular

  use globall
  use tracerstuff
  
  implicit none

  integer :: i,ir,ilat,ideb,ifin
  real :: rad(0:2),theta(0:2),phi(0:2),phi2(0:2)
  real,dimension(27,27) :: mat3Q

  ideb = max(NG+1,ideb_veltra-2)
  ifin = min(NR-1,ifin_veltra+2)

  allocate(mat3Q_1(27,27,2:Nlat-1,ideb:ifin))
  allocate(coeffs_3Q(27,3,2:Nlat-1,ideb:ifin))

  ! Initilization of the matrices
  phi(0) = 0
  phi(1) = dphi
  phi(2) = 2*dphi

  phi2(0) = phi(0)**2
  phi2(1) = phi(1)**2
  phi2(2) = phi(2)**2

  do ir = ideb,ifin
     do ilat = 2,Nlat-1
  
        theta(0:2) = colat(ilat-1:ilat+1)
        rad(0:2)   = r(ir-1:ir+1)

        mat3Q(:,:) = 0.
        
        !column 1
        mat3Q(:,1) = 1.
        
        !column 2
        do i = 0,8
           mat3Q(3*i+1,2) = phi(0)
           mat3Q(3*i+2,2) = phi(1)
           mat3Q(3*i+3,2) = phi(2)
        end do
        
        !column 3
        do i = 0,8
           mat3Q(3*i+1,3) = phi2(0)
           mat3Q(3*i+2,3) = phi2(1)
           mat3Q(3*i+3,3) = phi2(2)
        end do
        
        !columns 4-6
        mat3Q(1:3,4:6)   = mat3Q(1:3,1:3)*theta(0)
        mat3Q(10:12,4:6) = mat3Q(1:3,1:3)*theta(0)
        mat3Q(19:21,4:6) = mat3Q(1:3,1:3)*theta(0)
        
        mat3Q(4:6,4:6)   = mat3Q(1:3,1:3)*theta(1)
        mat3Q(13:15,4:6) = mat3Q(1:3,1:3)*theta(1)
        mat3Q(22:24,4:6) = mat3Q(1:3,1:3)*theta(1)
        
        mat3Q(7:9,4:6)   = mat3Q(1:3,1:3)*theta(2)
        mat3Q(16:18,4:6) = mat3Q(1:3,1:3)*theta(2)
        mat3Q(25:27,4:6) = mat3Q(1:3,1:3)*theta(2)
        
        !columns 7-9
        mat3Q(1:3,7:9)   = mat3Q(1:3,4:6)*theta(0)
        mat3Q(10:12,7:9) = mat3Q(1:3,4:6)*theta(0)
        mat3Q(19:21,7:9) = mat3Q(1:3,4:6)*theta(0)
        
        mat3Q(4:6,7:9)   = mat3Q(4:6,4:6)*theta(1)
        mat3Q(13:15,7:9) = mat3Q(4:6,4:6)*theta(1)
        mat3Q(22:24,7:9) = mat3Q(4:6,4:6)*theta(1)
        
        mat3Q(7:9,7:9)   = mat3Q(7:9,4:6)*theta(2)
        mat3Q(16:18,7:9) = mat3Q(7:9,4:6)*theta(2)
        mat3Q(25:27,7:9) = mat3Q(7:9,4:6)*theta(2)
        
        !columns 10-18
        mat3Q(1:9,10:18)   = mat3Q(1:9,1:9)  *rad(0)
        mat3Q(10:18,10:18) = mat3Q(10:18,1:9)*rad(1)
        mat3Q(19:27,10:18) = mat3Q(19:27,1:9)*rad(2)
        
        !columns 19-27
        mat3Q(1:9,19:27)   = mat3Q(1:9,10:18)  *rad(0)
        mat3Q(10:18,19:27) = mat3Q(10:18,10:18)*rad(1)
        mat3Q(19:27,19:27) = mat3Q(19:27,10:18)*rad(2)
        
        
        !Finally inverse the matrix and store it
        call inverse (mat3Q,mat3Q_1(:,:,ilat,ir),27)

     end do
  end do
        
end subroutine init_matrices_3Q
!----------------------------------------------------------------


subroutine calc_coeffs_3Q ()

!$  use OMP_LIB
  use tracerstuff
  use globall

  implicit none

  integer i,ideb,ifin,irad1,ilat1,ilong0,ilong1,ilong2,irad0,irad2,ilat0,ilat2
!$ integer nb_threads
  real vec(27)

  ideb = max(NG+1,ideb_veltra-2)
  ifin = min(NR-1,ifin_veltra+2)
  ilong0 = 1
  ilong1 = 2
  ilong2 = 3
  
!$OMP PARALLEL PRIVATE(vec,irad0,irad1,irad2,ilat0,ilat1,ilat2,ilong0,ilong1,ilong2,i)
!$ nb_threads = omp_get_num_threads()
!$OMP DO SCHEDULE(DYNAMIC,1)
  do irad1 = ideb,ifin
     do ilat1 = 2,Nlat-1
        do i = 1,3

           irad0 = irad1-1
           irad2 = irad1+1
           ilat0 = ilat1-1
           ilat2 = ilat1+1
           
           vec(1)  = vel_spat(ilong0,ilat0,irad0,i)
           vec(2)  = vel_spat(ilong1,ilat0,irad0,i)
           vec(3)  = vel_spat(ilong2,ilat0,irad0,i)
           vec(4)  = vel_spat(ilong0,ilat1,irad0,i)
           vec(5)  = vel_spat(ilong1,ilat1,irad0,i)
           vec(6)  = vel_spat(ilong2,ilat1,irad0,i)
           vec(7)  = vel_spat(ilong0,ilat2,irad0,i)
           vec(8)  = vel_spat(ilong1,ilat2,irad0,i)
           vec(9)  = vel_spat(ilong2,ilat2,irad0,i)
           vec(10) = vel_spat(ilong0,ilat0,irad1,i)
           vec(11) = vel_spat(ilong1,ilat0,irad1,i)
           vec(12) = vel_spat(ilong2,ilat0,irad1,i)
           vec(13) = vel_spat(ilong0,ilat1,irad1,i)
           vec(14) = vel_spat(ilong1,ilat1,irad1,i)
           vec(15) = vel_spat(ilong2,ilat1,irad1,i)
           vec(16) = vel_spat(ilong0,ilat2,irad1,i)
           vec(17) = vel_spat(ilong1,ilat2,irad1,i)
           vec(18) = vel_spat(ilong2,ilat2,irad1,i)
           vec(19) = vel_spat(ilong0,ilat0,irad2,i)
           vec(20) = vel_spat(ilong1,ilat0,irad2,i)
           vec(21) = vel_spat(ilong2,ilat0,irad2,i)
           vec(22) = vel_spat(ilong0,ilat1,irad2,i)
           vec(23) = vel_spat(ilong1,ilat1,irad2,i)
           vec(24) = vel_spat(ilong2,ilat1,irad2,i)
           vec(25) = vel_spat(ilong0,ilat2,irad2,i)
           vec(26) = vel_spat(ilong1,ilat2,irad2,i)
           vec(27) = vel_spat(ilong2,ilat2,irad2,i)
           
           coeffs_3Q(:,i,ilat1,irad1) = matmul(mat3Q_1(:,:,ilat1,irad1),vec(:))
           
        end do
     end do
  end do
!$OMP END DO
!$OMP END PARALLEL
 

end subroutine calc_coeffs_3Q
!----------------------------------------------------------------


subroutine triQ_interpolation (rad,theta,phi,ir,ilat,vi)

  use tracerstuff

  implicit none

  integer,intent(in) :: ir,ilat
  real,intent(in) :: rad,theta,phi
  real,intent(out) :: vi(3)

  integer i
  real vec(27)

  vec(1:3) = (/ 1.,phi,phi**2 /)
  vec(4:6) = theta * vec(1:3)
  vec(7:9) = theta * vec(4:6)
  vec(10:18) = rad * vec(1:9)
  vec(19:27) = rad * vec(10:18)

  do i = 1,3
     vi(i) = dot_product(vec(:),coeffs_3Q(:,i,ilat,ir))
  end do

 
end subroutine triQ_interpolation
!----------------------------------------------------------


subroutine calc_adequate_Ntra ()

  use globall
  use tracerstuff

  implicit none

  integer Ntra
  real lmax_ri,rc,delta_r,delta_h,dmean_tra

  ! Criterium: there should be at least 5 tracers to describe any physical scale.
  ! First step: compute the smallest physical scale in radial and horizontal directions (radial=boundary layer, lateral=2*pi*r_i/lmax(r_i))
  ! then take the minimum
  ! Deduce the average distance between tracers and hence the total number of tracers

  rc = r(NR)/2.
  
  ! Ekman boundary layer's thickness
  delta_r = (r(NR)-r(NG)) * (1./Coriolis)**0.5

  ! Lateral resolution at r(NG)
  lmax_ri = lmax * sqrt(r(NG)/rc)
  delta_h = 2.*pi*r(NG)/lmax_ri

  ! Compute average distance between tracers
  dmean_tra = min(delta_r,delta_h)/5.

  ! Deduce the corresponding number of tracers
  Ntra = Vtot/dmean_tra**3
  
!  if (rank==0) print*,'YOU SHOULD USE AT LEAST',Ntra,'TRACERS'
  
  
end subroutine calc_adequate_Ntra
!-----------------------------------------------------------------------------------------------


subroutine write_equatorial_tracers ()

  use globall
  use tracerstuff
  use mpi

  implicit none

  integer j,i,n_eq,nb_octets,ier,n_eq_tot,nb_elements,descripteur
  real,allocatable :: tra_work(:,:)
  integer :: n_eq_all(0:size-1)
  character*32 :: sortie
  integer, dimension(MPI_STATUS_SIZE) :: statut
  integer(kind=MPI_OFFSET_KIND):: position_file

  allocate(tra_work(ntracervar,10*ntr/Nlat))

  n_eq = 0

!  print*,'pi/Nlat',pi/Nlat

  do i = 1,ntr
     if (abs(tra(1,i)*cos(tra(2,i))) .le. 0.5*r(NR)*cos(pi/2-pi/2/Nlat)) then
        n_eq = n_eq + 1
        tra_work(:,n_eq) = tra(:,i)
     end if
  enddo

 ! print*,rank,'n_eq',n_eq,4*ntr/Nlat
  call MPI_BARRIER (MPI_COMM_WORLD,ier)
 ! ! Then communicate
!  call MPI_REDUCE (n_eq,n_eq_tot,1,MPI_INTEGER,MPI_SUM,0,MPI_COMM_WORLD,ier)
  call MPI_ALLGATHER (n_eq,1,MPI_INTEGER,n_eq_all,1,MPI_INTEGER,MPI_COMM_WORLD,ier)
  call MPI_BARRIER (MPI_COMM_WORLD,ier)
 ! if (rank == 0) print*,'n_eq_all',n_eq_all
!  do rank=0,size-1
!     call MPI_GATHER
!     (tra_work(1,1),n_eq*ntracervar,MPI_REAL,tra_eq,n_eq_all(rank)*ntracervar,MPI_REAL,0,MPI_COMM_WORLD,ier)
!     call MPI_BARRIER (MPI_COMM_WORLD,ier)
!  enddo 

  if     (time.le.9.99999) then
     write(sortie,'("tra_equatorial",f7.5,".dat")') time
  elseif (time.le.99.9999) then
     write(sortie,'("tra_equatorial",f7.4,".dat")') time
  elseif (time.le.999.999) then
     write(sortie,'("tra_equatorial",f7.3,".dat")') time
  elseif (time.le.9999.99) then
     write(sortie,'("tra_equatorial",f7.2,".dat")') time
  else
     write(sortie,'("tra_equatorial",f7.1,".dat")') time
  endif

 !   print*,'n_eq_tot',n_eq_tot
 !   print*,tra_eq(:,1)
 !   print*,tra_eq(:,100010)
!n_eq_tot = sum(n_eq_all)
! if (rank == 0) then
!     open(300,file=trim(sortie),form='unformatted',position='append')
!        do i= 1,n_eq_tot
!           write(300) (real(tra_eq(j,i),4),j=1,ntracervar) 
!        enddo
!     close(300)

!  end if

  nb_elements = n_eq*ntracervar ! Nlong*Nlat*(ifin_all-ideb_all+1)

  call MPI_TYPE_SIZE (MPI_DOUBLE_PRECISION,nb_octets,ier)

  call MPI_FILE_OPEN (MPI_COMM_WORLD,trim(sortie),MPI_MODE_WRONLY+MPI_MODE_CREATE, &
                      MPI_INFO_NULL,descripteur,ier)

  if (rank == 0) then
     position_file = 0
     call MPI_FILE_WRITE_AT_ALL (descripteur,position_file,tra_work,nb_elements,MPI_DOUBLE_PRECISION,statut,ier)
  else
     position_file = nb_octets*ntracervar*sum(n_eq_all(0:rank-1))
     call MPI_FILE_WRITE_AT_ALL (descripteur,position_file,tra_work,nb_elements,MPI_DOUBLE_PRECISION,statut,ier)
  endif

  call MPI_FILE_CLOSE (descripteur,ier)

  deallocate(tra_work)

end subroutine write_equatorial_tracers
!---------------------------------------------------------------------------------------


subroutine calc_thickness_layer ()
  ! Compute the thickness of a top stratified layer

  use globall
  use tracerstuff
  use mpi

  integer :: ir,ilat,ilong,ir0,irmin,irmax,ier
  real :: integrale,int_var,Cmin
  logical :: stratification
  real,dimension(NG:NR) :: Tmean,Cmean,Tmean_loc,Cmean_loc,dTdr,dCdr,N2,C_mean_work
  real,dimension(Nlong,Nlat) :: twork,cwork
  real data(Nphi,Nlat)
  Real,POINTER,DIMENSION(:,:) :: Tt
  Complex yCr2(0:LMmax+1),yTtr2(0:LMmax+1)

  Tt => Data_spec_spat (:,:,7)

!  if (rank == 0) print*,'Computing stratified layer s thickness'

  !================================================
  ! FIRST METHOD USING THE BRUNT VAISALA FREQUENCY
  !================================================

  ! 1) Compute the mean composition and temperature radial profiles in the
  ! domain
  Tmean(:) = 0.
  Cmean(:) = 0.

  Tmean_loc(:) = 0.
  Cmean_loc(:) = 0.

  do ir = ideb_all,ifin_all
     yTtr2(:) = yTtr(:,ir)
     yCr2(:)  = yCr(:,ir)

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
     do iblock=1,nblock
        call Spec_Spat_Scal_Shell(yTtr2,Tt,latstart(iblock),latstop(iblock))
        call Spec_Spat_Scal_Shell(yCr2,data,latstart(iblock),latstop(iblock))
     enddo
!$OMP END PARALLEL DO

     call fourierr_spec_spat2(Data_spec_spat)
     call fourtf(data,Nphi,1,Nlat)

     twork(:,:) = Real(Tt(:,:),8) +3*(2.*Pch-1)/(r(NR)**3-r(NG)**3)*r(ir)**2/6 &
                  - (r(NG)**3*(1.-Pch)-r(NR)**3*Pch)/(r(NR)**3-r(NG)**3)/r(ir)

     cwork(:,:) = Real(data(1:Nlong,:),8)

     Tmean_loc(ir) = horizontal_mean(twork(:,:))
     Cmean_loc(ir) = horizontal_mean(cwork(:,:))
  end do

  ! 2) Communicate to proc 1
  call mpi_reduce (Tmean_loc,Tmean,NR+1-NG,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,ier)
  call mpi_reduce (Cmean_loc,Cmean,NR+1-NG,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,ier)

  if (rank == 0) then

     ! 3a) Compute mean temperature and compositional gradients
     do ir = NG+1,NR-1
        dTdr(ir) = 0.5*(Tmean(ir+1)-Tmean(ir-1))
        dCdr(ir) = 0.5*(Cmean(ir+1)-Cmean(ir-1))
     end do
     dTdr(NG) = dTdr(NG+1)
     dCdr(NG) = dCdr(NG+1)
     dTdr(NR) = dTdr(NR-1)
     dCdr(NR) = dCdr(NR-1)


     ! 3b) Criterium for stratification is N2 > 0. In non-dimensional values,
     ! this is equivalent to
     !    dTmean/dr + Buoyancy_ratio*dCmean/dr > 0:

     ! First, make sure there actually is a stratified layer...
     stratification = .false.

     do ir = Nr,Nr-7,-1
        ! Check among the 10 top points if at least 3 consecutive points are
        ! above 0 which defines stratification
        if ( (dTdr(ir)   * Buoyancy_ratio + dCdr(ir)  ) .ge. 0. .and. &
             (dTdr(ir-1) * Buoyancy_ratio + dCdr(ir-1)) .ge. 0. .and. &
             (dTdr(ir-2) * Buoyancy_ratio + dCdr(ir-2)) .ge. 0. ) then
           stratification = .true.
           ir0 = ir
        end if
     end do

     ! Detect first time N2 falls below 0.
     if (stratification) then
        ir = ir0
        do while (stratification)
           if (dTdr(ir) * Buoyancy_ratio + dCdr(ir) .le. 0.) then
              stratification = .false.
           else
              ir = ir - 1
           end if
           if (ir .eq. NG+1) exit
        end do

        thickness = r(NR) - r(ir)

     else

        thickness = 0.

     end if

     ! 4) Print this in a file
     open (300,file='thickness_strat_BV.dat',form='formatted',position='append')
     write(300,'(1p,d20.12,d16.8)') time,thickness
     close(300)

  end if

  !================================================
  ! SECOND METHOD BASED ON AN INTEGRAL ESTIMATION
  !================================================


if (rank == 0) then

     C_mean_work(:) = Cmean(:)
     ir0 = NG+(NR-NG)/4
     C_mean_work(NG:ir0) = 0. ! get rid of the bottom quarter where light elements are injected

     ! Find max value in top three quarter, then minimum value below the
     ! position of the max
     irmax = ir0-1 + maxloc(C_mean_work(ir0:NR),DIM=1)
     irmin = NG-1 + minloc(C_mean_work(NG:irmax),DIM=1)
     Cmin = C_mean_work(irmin)

     if (irmax .lt. NR-10) then ! arbitrary...
        stratification = .false.
     else
        stratification = .true.
     end if

     if (stratification) then

        ! Subtract minimum value 
        do ir = irmin,irmax
           C_mean_work(ir) = C_mean_work(ir) - Cmin
        end do

        int_var = 0.
        integrale = 0.

        ! Integral evaluation
        do ir = irmin,irmax
           int_var = int_var + C_mean_work(ir)*(r(irmax)-r(ir))*r(ir)**2 *drad2(min(ir,NR-1))
           integrale = integrale + C_mean_work(ir)*r(ir)**2 * drad2(min(ir,NR-1))
        end do

        ! Normalise by int to get a probability density function
        thickness = (r(NR)-r(irmax)) + int_var/integrale
       
     else

        thickness = 0.

     end if

     open (300,file='thickness_strat_INT.dat',form='formatted',position='append')
     write(300,'(1p,d20.12,d16.8)') time,thickness
     close(300)
     
     ! To finish, plot the maximum value of the Brunt Vaisala frequency
     do ir = NG,NR
        N2(ir) = dTdr(ir) + Buoyancy_ratio * dCdr(ir)
     enddo 

     open (300,file='max_N2.dat',form='formatted',position='append')
     write(300,'(1p,d20.12,d16.8)') time,maxval(N2)
     close(300)


  end if

  !================================================
  ! THIRD METHOD BASED ON A THRESHOLD (C=0.)
  !================================================

  if (rank == 0) then

     ! First check there is actually a layer
     Cmax = maxval(Cmean(NR-10:NR))

     if (Cmax .gt. 0.) then
        ir = maxloc(Cmean(NR-10:NR),DIM=1)
        do while (Cmean(ir) .gt. 0. .and. (ir .gt. NG))
           ir = ir - 1
        end do
        thickness = (r(NR)-r(ir))
     else
        thickness = 0.
     end if

     ! Print in a file
     open (300,file='thickness_strat_C0.dat',form='formatted',position='append')
     write(300,'(1p,d20.12,d16.8)') time,thickness
     close(300)

  end if

end subroutine calc_thickness_layer
!---------------------------------------------------------------------------------------



function horizontal_mean (field)

  use globall
  use tracerstuff

  real,intent(in) :: field(Nlong,Nlat)

  integer :: ilat,ilong
  real :: S,horizontal_mean

  S = 0.

  do ilat = 1,Nlat
     do ilong = 1,Nlong
        S = S + field(ilong,ilat) * sintheta(ilat) * dlat2(ilat) * dphi
     end do
  end do

  horizontal_mean = S/((4.*pi)/Mc)

end function horizontal_mean
!---------------------------------------------------------------------------------------


subroutine init_tracked_tracers ()

  use globall
  use tracerstuff

  implicit none

  integer :: j
  integer :: irand
  real :: rn

  ! First init cell numbers
  call calc_cell_number ()

  allocate(tracked_tracers(4,ntracked))
!  print*,'tracked_tracers is allocated'

  tracked_tracers(:,:) = BLANK

 ! print*,rank,'rdeb_tra,rfin_tra',rdeb_tra,rfin_tra,r(NG)+0.5*dist_flux_icb
  ! Tracked tracers are initialized only on MPI-proc where most of the light elements
  ! are injected 
  if ((rdeb_tra .le. r(NG)+0.5*dist_flux_icb) .and. (rfin_tra .ge. r(NG)+0.5*dist_flux_icb)) then
 
     print*
     print*, 'TRACKING',ntracked,'TRACERS DURING SIMULATION'
     print*, 'Initialization on proc',rank
     j = 0
     ! Picked randomly ntracked tracers located in the region where light
     ! elements are injected
     do while (j .lt. ntracked)
        call random_number(rn)
        irand = 1 + int(rn*ntr)
        if (tra(1,irand) .gt. r(NG) .and. tra(1,irand) .le. r(NG)+dist_flux_icb) then
           j = j + 1
           tracked_tracers(1:3,j) = tra(1:3,irand)
           tracked_tracers(4,j) = tra(COMPPOS,irand)
           ! Mark the tracer in tra(:,:) for easy tracking: assign negative cell number
           tra(NCELLPOS,irand) = -tra(NCELLPOS,irand)
        end if
        if (irand .eq. ntr) then
           print*,'tracking',j,'tracers instead of',ntracked
           j = ntracked
        end if
     end do
     print*
     print*,'tracked_tracers initial',tracked_tracers

  end if

end subroutine init_tracked_tracers
!---------------------------------------------------------------------------------------


subroutine write_tracked_tracers ()

  use globall
  use tracerstuff
  use mpi
  
  implicit none

  integer i,ier
  character*30 :: sortie
!  real :: tracked_tracers_w(4,ntracked),all_tracked(4*ntracked)

!  all_tracked(:) = 0.

!  ! 1) Copy the tracked_tracers array in a temporary array replacing BLANKs by 0s
!  tracked_tracers_w(:,:) = 0.
!  do i = 1,ntracked
!     if (tracked_tracers(1,i) .ne. BLANK) then
!        tracked_tracers_w(:,i) = tracked_tracers(:,i)
!     end if
!  end do

!  ! 2) MPI sum reduction of all the arrays to proc 0
!  call MPI_REDUCE (tracked_tracers_w,all_tracked,4*ntracked,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,ier)

!  ! 3) Proc 0 writes the array all at once in one single file
!  write(sortie,'("tracked_tracers.dat")') 
!  open(300,file=trim(sortie),form='formatted',position='append')
!  write(300,*) time,(all_tracked(i),i=1,4*ntracked)
!  close(300)

  do i = 1,ntracked
     if (tracked_tracers(1,i) .ne. BLANK) then

!        ! write radius, latitude and longtitude
        write(sortie,'("tracked_tracer_",i4.4,".dat")') i
        open(300,file=trim(sortie),form='formatted',position='append')
        write(300,'(5d16.8)') time,tracked_tracers(1,i),tracked_tracers(2,i),tracked_tracers(3,i),tracked_tracers(4,i) 
        close(300)

 !       ! write latitude
 !       write(sortie,'("t_",i4.4,".dat")') i
 !       open(300,file=trim(sortie),form='formatted',position='append')
 !       write(300,'(2d16.8)') time,tracked_tracers(2,i)
 !       close(300)

 !       ! write longitude
 !       write(sortie,'("p_",i4.4,".dat")') i
 !       open(300,file=trim(sortie),form='formatted',position='append')
 !       write(300,'(2d16.8)') time,tracked_tracers(3,i)
 !       close(300)

     end if
  end do

end subroutine write_tracked_tracers 
!--------------------------------------------------------------------------------------


subroutine check_tracked_tracer (rad,theta,phi,tracked,jtracked)

  use tracerstuff

  implicit none

  real,intent(in) :: rad,theta,phi
  logical,intent(out) :: tracked
  integer,intent(out) :: jtracked

  integer :: j
  real :: eps

  eps = 1e-10

  tracked = .false.
  jtracked = -1

  do j = 1,ntracked
     if (abs(rad-tracked_tracers(1,j)) .le. eps) then
        if (abs(theta-tracked_tracers(2,j)) .le. eps) then
           if (abs(phi-tracked_tracers(3,j)) .le. eps) then 
              tracked = .true.
              jtracked = j
           end if
        end if
     end if
  end do

end subroutine check_tracked_tracer
!-----------------------------------------------------------------------


subroutine calc_cell_number ()

  use tracerstuff
  use globall

  implicit none

  integer i,irad_closest,ilong0,ilong_closest,ilat_closest,jtracked
  real rad,theta,phi,dist_rad,dist_lat,dist_long
  logical tracked

  do i = 1,ntr
     rad   = tra(1,i)
     theta = tra(2,i)
     phi   = tra(3,i)
     call find_closest_radial_grid_point(rad,irad_closest,dist_rad)
     call find_closest_lateral_grid_point(theta,ilat_closest,dist_lat)
     ilong0 = 1 + floor (phi*dphi_1)
     dist_long = (phi - long(ilong0))*dphi_1
     if (dist_long .le. 0.5) then
        ilong_closest = ilong0
     else
        ilong_closest = max(mod(ilong0+1,nlong+1),1)
     end if
     if (tra(NCELLPOS,i) .ge. 0.) then ! leave marked tracers (NCELL < 0) untouched for tracking particles
        tra(NCELLPOS,i) = real(ilong_closest + Nlong*((ilat_closest-1) + Nlat*irad_closest))
     endif 
       
  end do
  
end subroutine calc_cell_number



!================================
end module tracers
!================================
