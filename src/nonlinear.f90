!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                       !!
!!  Version JA-2.3 of Feb 2012                                           !!
!!                                                                       !!
!!  This is the subroutine that computes nonlinear terms                 !!
!!                                                                       !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Subroutine NL2_para

  use globall
  use mod_parallel
  use tracerstuff
  Implicit None


  !**        Variables locales

  Complex CBp(0:LMmax+1),CVp(0:LMmax+1)
  Complex CBt(0:LMmax+1),CVt(0:LMmax+1)
  Complex yVs(0:LMmax+1,ideb_Vp-2:ifin_Vp+2)
  Complex yBs(0:LMmax+1,ideb_Bp-2:ifin_Bp+2)
  Complex yROTVtr(0:LMmax+1,ideb_Vp-2:ifin_Vp+2)
  Complex yROTVs(0:LMmax+1,ideb_Vt-2:ifin_Vt+2)
  Complex yROTBtr(0:LMmax+1,ideb_Bp-2:ifin_Bp+2)
  Complex yROTBs(0:LMmax+1,ideb_Bt-2:ifin_Bt+2)
  Complex  ydTdr(0:LMmax+1,ideb_Tt-2:ifin_Tt+2)
  Complex   yT_r(0:LMmax+1,ideb_Tt-2:ifin_Tt+2)
  Complex yNLttr(0:LMmax+1,ideb_Tt-2:ifin_Tt+2)
  Complex yNLCr(0:LMmax+1,ideb_C-2:ifin_C+2)
  Complex Ynltr(0:LMmax+1, ideb_Vp-2:ifin_Vp+2)
  Complex Ynlpr(0:LMmax+1, ideb_Vp-2:ifin_Vp+2)
  Complex Ynlsr(0:LMmax+1, ideb_Vp-2:ifin_Vp+2)
  Complex Ynltr2(0:LMmax+1, ideb_Vp-2:ifin_Vp+2)
  Complex Ynlsr2(0:LMmax+1, ideb_Vp-2:ifin_Vp+2)

  Complex atmp(0:LMmax+1),btmp(0:LMmax+1), ctmp(0:LMmax+1)
  Complex bound_yNLsr(0:LMmax+1)

  Complex,allocatable ::  ydCdr(:,:),yC_r(:,:)

  Real, POINTER, DIMENSION(:,:)    ::  Vr,Vt,Vp
  Real, POINTER, DIMENSION(:,:)    ::  Br,Bt,Bp
  Real, POINTER, DIMENSION(:,:)    ::  Gr,Gt,Gp
  Real, POINTER, DIMENSION(:,:)    ::  ROTVr,ROTVt,ROTVp
  Real, POINTER, DIMENSION(:,:)    ::  ROTBr,ROTBt,ROTBp

  Real, POINTER, DIMENSION(:,:)    ::  NL_Vr,NL_Vt,NL_Vp
  Real, POINTER, DIMENSION(:,:)    ::  NL_Tt
  Real, POINTER, DIMENSION(:,:)    ::  NL_Br,NL_Bt,NL_Bp

  Real, allocatable :: Gr_c(:,:),Gt_c(:,:),Gp_c(:,:)
  Real, allocatable :: NL_C(:,:)

  Real data(Nphi,Nlat)
  Real funct(nlat)

  real cpl,tmp1,tmp2,tmp3
  Integer Nilat,indx,ideb,ifin,ier

  Integer ir, i, l, m, lm, ilat, ilong,isigne
  Real C_v,C_m

  Real valri2,valhi2,vr2max,vh2max
  Real vflr2,valr,valr2,vflh2,valh2,valh2m
  Real dtr_opt,dth_opt

  Complex powerint(0:2,ideb_all-2:ifin_all+2)

  integer irank
  integer itype

  real vol

! Mantle control
  Complex yTtr2(0:LMmax+1)


  real timea,timeb
  
! Pointers

  Vr => Data_spec_spat(:,:,1)
  Vt => Data_spec_spat(:,:,2)
  Vp => Data_spec_spat(:,:,3)
  Br => Data_spec_spat(:,:,4)
  Bt => Data_spec_spat(:,:,5)
  Bp => Data_spec_spat(:,:,6)
  Gr => Data_spec_spat(:,:,7)
  Gt => Data_spec_spat(:,:,8)
  Gp => Data_spec_spat(:,:,9)
  ROTVr => Data_spec_spat(:,:,10)
  ROTVt => Data_spec_spat(:,:,11)
  ROTVp => Data_spec_spat(:,:,12)
  ROTBr => Data_spec_spat(:,:,13)
  ROTBt => Data_spec_spat(:,:,14)
  ROTBp => Data_spec_spat(:,:,15)


  NL_Vr => Data_spat_spec(:,:,1)
  NL_Vt => Data_spat_spec(:,:,2)
  NL_Vp => Data_spat_spec(:,:,3)
  NL_Tt => Data_spat_spec(:,:,4)
  NL_Br => Data_spat_spec(:,:,5)
  NL_Bt => Data_spat_spec(:,:,6)
  NL_Bp => Data_spat_spec(:,:,7)

  if (dd_convection .and. .not. use_tracers) then !non-linear advection term calculated on the grid
   allocate(ydCdr(0:LMmax+1,ideb_C-2:ifin_C+2))
   allocate(yC_r(0:LMmax+1,ideb_C-2:ifin_C+2))
   allocate(Gr_c(Nphi,Nlat))
   allocate(Gt_c(Nphi,Nlat))
   allocate(Gp_c(Nphi,Nlat))
   allocate(NL_c(Nphi,Nlat))
end if


if (rank==size-1)   Ynlsr2(:,NR) = 0.0


! Spheroidals and Curls


if (rank==0) then
  !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC Graine
  ir = NG

if (Aspect_ratio.eq.0.) then
   do lm=1,lmmax
      l=li(lm)
      if (l.eq.1) then
         CBp(lm)=yBpr(lm,ir+1)/r(ir+1)
         CVp(lm)=yVpr(lm,ir+1)/r(ir+1)
         CBt(lm)=yBtr(lm,ir+1)/r(ir+1)
         CVt(lm)=yVtr(lm,ir+1)/r(ir+1)
      else
         CBp(lm)=0.
         CVp(lm)=0.
         CBt(lm)=0.
         CVt(lm)=0.
      endif
   enddo

else

if (CondIC) then
  yBs(:,ir) = dra(ir)*yBpr(:,ir-1) &
       + drb(ir)*yBpr(:,ir) &
       + drc(ir)*yBpr(:,ir+1)


  yROTBs(:,ir) = dra(ir)*yBtr(:,ir-1) &
       + drb(ir)*yBtr(:,ir) &
       + drc(ir)*yBtr(:,ir+1)

  yROTBtr(:,ir) = -( la(ir)*yBpr(:,ir-1) &
       +(lb(ir)-ll(:)*r_2(ir))*yBpr(:,ir) &
       + lc(ir)*yBpr(:,ir+1))
else
  do lm=1,LMmax
     l=li(lm)

     yBs(lm,ir)=real(l+1)*yBpr(lm,ir)/r(ir)

     yROTBtr(lm,ir)=                                         &
             -2.*yBpr(lm,ir+1)/(r(ir+1)-r(ir))**2+yBpr(lm,ir)& 
                *(2./(r(ir+1)-r(ir))**2                      &
                +2.*real(l)/((r(ir+1)-r(ir))*r(ir))          &
                +(real(l)*real(l-1)/r(ir)**2))
     yROTBs(lm,ir) = (3./2.) * (r(ir+1)*yBtr(lm,ir+1)        &
                 -r(ir)*yBtr(lm,ir))                         &
                / ((r(ir+1)-r(ir))*r(ir))                    &
             - (1./2.) * (r(ir+2)*yBtr(lm,ir+2)              &
             -r(ir+1)*yBtr(lm,ir+1))                         &
                / ((r(ir+2)-r(ir+1))*r(ir))
  enddo
endif

     tmp1=r(ir+1)-r(ir)
     tmp2=r(ir+2)-r(ir)  
     
     yROTVtr(:,ir) = -2.*yVpr(:,ir+1)/tmp1**2

   if (NOSLIPIC) then
    yVtr(:,ir-1)=yVtr(:,ir+1)-2*(tmp2**2*yVtr(:,ir+1)-tmp1**2*yVtr(:,ir+2))&
                             &/(tmp2**2-tmp1*tmp2)
      yVs(:,ir)=0
     yROTVs(:,ir) =(yVtr(:,ir+1)-yVtr(:,ir-1))/(2*tmp1)
    else
     yVtr(:,ir-1)=yVtr(:,ir+1)*(r(ir)-tmp1)/(r(ir)+tmp1)
     yROTVs(:,ir)=(yVtr(:,ir+1)-yVtr(:,ir-1))/(2*tmp1)+1./r(ir)*yVtr(:,ir)

     yVpr(:,ir-1)=-yVpr(:,ir+1)
     yVs(:,ir)=(yVpr(:,ir+1)-yVpr(:,ir-1))/(2*tmp1)
    endif

endif

endif


  !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC NG+1 -> NR-1
  do ir=ideb_Vp,ifin_Vp
     yVs(:,ir) = dra(ir)*yVpr(:,ir-1) &
          + drb(ir)*yVpr(:,ir) &
          + drc(ir)*yVpr(:,ir+1)

     !     dr . = 1/r d/dr r.
     yBs(:,ir) = dra(ir)*yBpr(:,ir-1) &
          + drb(ir)*yBpr(:,ir) &
          + drc(ir)*yBpr(:,ir+1)


     !cc  Calcul de rot(V)
     !c                        rot(V) = rot (rot(rot(r.Vp))+rot(r.Vt))
     !c                               = rot(-r.Lap(Vp)) + rot(rot(r.Vt))

     yROTVs(:,ir) = dra(ir)*yVtr(:,ir-1) &
          + drb(ir)*yVtr(:,ir) &
          + drc(ir)*yVtr(:,ir+1)

     yROTVtr(:,ir) = -( la(ir)*yVpr(:,ir-1) &
          +(lb(ir)-ll(:)*r_2(ir))*yVpr(:,ir) &
          + lc(ir)*yVpr(:,ir+1))

     !cc  Calcul de rot(B)
     !c                        rot(B) = rot (rot(rot(r.Bp))+rot(r.Bt))
     !c                               = rot(-r.Lap(Bp)) + rot(rot(r.Bt))

     yROTBs(:,ir) = dra(ir)*yBtr(:,ir-1) &
          +drb(ir)*yBtr(:,ir) &
          +drc(ir)*yBtr(:,ir+1)

     yROTBtr(:,ir) = -( la(ir)*yBpr(:,ir-1) &
          +(lb(ir)-ll(:)*r_2(ir))*yBpr(:,ir) &
          + lc(ir)*yBpr(:,ir+1))


  enddo

  if (rank==size-1) then
  !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC NR
  ir=NR

  tmp1=r(ir)-r(ir-1)
  tmp2=r(ir)-r(ir-2) 
  yROTVtr(:,ir) = -2.*yVpr(:,ir-1)/tmp1**2

   if (NOSLIPOC)then
     yVtr(:,ir+1)=yVtr(:,ir-1)-2*(tmp2**2*yVtr(:,ir-1) &
          & -tmp1**2*yVtr(:,ir-2))  &
          & /(tmp2**2-tmp1*tmp2)
     yROTVs(:,ir)=(yVtr(:,ir-1)-yVtr(:,ir+1))/(2*tmp1)
     
     yVs(:,ir)=0.
     
     
  else
     yVtr(:,ir+1)=2*tmp1/r(ir)*yVtr(:,ir)+yVtr(:,ir-1)
     
     yROTVs(:,ir)=(yVtr(:,ir+1)-yVtr(:,ir-1))/(2*tmp1)+1./r(ir)*yVtr(:,ir)
     
     yVpr(:,ir+1)=-yVpr(:,ir-1)
     yVs(:,ir)=(yVpr(:,ir+1)-yVpr(:,ir-1))/(2*tmp1)
     
  endif
  
  do lm=1,LMmax
     l=li(lm)

     yBs(lm,ir)=-(real(l)*yBpr(lm,ir))/r(ir)

     yROTBtr(lm,ir)= &
          -2.*yBpr(lm,ir-1)/(r(ir)-r(ir-1))**2+yBpr(lm,ir) &
          *(2./(r(ir)-r(ir-1))**2 &
          +2.*real(l+1)/((r(ir)-r(ir-1))*r(ir)) &
          +(real(l+1)*real(l+2)/r(ir)**2))
     yROTBs(lm,ir) = (3./2.) * (r(ir)*yBtr(lm,ir) &
          -r(ir-1)*yBtr(lm,ir-1)) &
          / ((r(ir)-r(ir-1))*r(ir)) &
          - (1./2.) * (r(ir-1)*yBtr(lm,ir-1) &
          -r(ir-2)*yBtr(lm,ir-2)) &
          / ((r(ir-1)-r(ir-2))*r(ir))
  enddo
endif

  do ir = ideb_Vp, ifin_Vp

     ! gr . = d/dr .
     ydTdr(:,ir) = gra(ir) * yTtr(:,ir-1) &
          + grb(ir) * yTtr(:,ir) &
          + grc(ir) * yTtr(:,ir+1)

     yT_r(:,ir) =  yTtr(:,ir)*r_1(ir)

! Mantle Control
     if (MantleControl) then
     ydTdr(:,ir) = ydTdr(:,ir) +  ydTdrMC(:,ir)
     yT_r(:,ir) =   yT_r(:,ir) + yTtrMC_r(:,ir)
  endif

! Bottom Control
  if (BottomControl) then
     ydTdr(:,ir) = ydTdr(:,ir) +  ydTdrIC(:,ir)
     yT_r(:,ir) =   yT_r(:,ir) + yTtrIC_r(:,ir)
  endif
  

       if (dd_convection .and. .not. use_tracers) then
        ! gr . = d/dr .
        ydCdr(:,ir) = gra(ir) * yCr(:,ir-1) &
             + grb(ir) * yCr(:,ir) &
             + grc(ir) * yCr(:,ir+1)
        
        yC_r(:,ir) =  yCr(:,ir)*r_1(ir)
     end if

  enddo

  dtr_opt=1.0
  dth_opt=1.0

  powerint=0.
  surfvisc=0.

  do ir=ideb_all,ifin_all
     if ((ir.eq.NG).and.(Aspect_ratio.eq.0.)) then
!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
        do iblock=1,nblock
           call Spec_Spat_PolTor_Nocore &
                (ir,CVp, &
                Vr,Vt,Vp,latstart(iblock),latstop(iblock))
           call Spec_Spat_PolTor_Nocore &
                (ir,CBp, &
                Br,Bt,Bp,latstart(iblock),latstop(iblock))
           call Spec_Spat_PolTor_Nocore &
                (ir,CVt, &
                ROTVr,ROTVt,ROTVp,latstart(iblock),latstop(iblock))
           call Spec_Spat_PolTor_Nocore &
                (ir,CBt, &
                ROTBr,ROTBt,ROTBp,latstart(iblock),latstop(iblock))
           call Spec_Spat_Grad_shell &
                (ydTdr(:,ir),yT_r(:,ir), &
                Gr,Gt,Gp,latstart(iblock),latstop(iblock))
           if (dd_convection .and. .not. use_tracers) then
              call Spec_Spat_Grad_shell &
                   (ydCdr(:,ir),yC_r(:,ir), &
                   Gr_c,Gt_c,Gp_c,latstart(iblock),latstop(iblock))
           end if
        enddo
!!$OMP END PARALLEL DO
        
     else

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
        do iblock=1,nblock
           call Spec_Spat_PolTor_shell &
                (ir,yVpr(:,ir),yVtr(:,ir),yVs(:,ir), &
                Vr,Vt,Vp,latstart(iblock),latstop(iblock))
           call Spec_Spat_PolTor_shell &
                (ir,yBpr(:,ir),yBtr(:,ir),yBs(:,ir), &
                Br,Bt,Bp,latstart(iblock),latstop(iblock))
           call Spec_Spat_PolTor_shell &
                (ir,yVtr(:,ir),yROTVtr(:,ir),yROTVs(:,ir), &
                ROTVr,ROTVt,ROTVp,latstart(iblock),latstop(iblock))
           call Spec_Spat_PolTor_shell &
                (ir,yBtr(:,ir),yROTBtr(:,ir),yROTBs(:,ir), &
                ROTBr,ROTBt,ROTBp,latstart(iblock),latstop(iblock))
           call Spec_Spat_Grad_shell &
                (ydTdr(:,ir),yT_r(:,ir), &
                Gr,Gt,Gp,latstart(iblock),latstop(iblock))
           if (dd_convection .and. .not. use_tracers) then
              call Spec_Spat_Grad_shell &
                   (ydCdr(:,ir),yC_r(:,ir), &
                   Gr_c,Gt_c,Gp_c,latstart(iblock),latstop(iblock))
           end if
        enddo
!$OMP END PARALLEL DO

     endif

     call fourierr_spec_spat2(Data_spec_spat)

     if (dd_convection .and. .not. use_tracers) then
        call fourtf(Gr_c,Nphi,1,Nlat)
        call fourtf(Gt_c,Nphi,1,Nlat)
        call fourtf(Gp_c,Nphi,1,Nlat)
     end if

     if (mod(t,modulo)==0) then
! Get power budget for diagnostics

! Volume contribution
        yTtr2=yTtr(:,ir)
! Mantle Control
        if (MantleControl) yTtr2=yTtr2+yTtrMC(:,ir)
! Bottom Control
        if (BottomControl) yTtr2=yTtr2+yTtrIC(:,ir)

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
do iblock=1,nblock
        call Spec_Spat_Scal_shell(yTtr2,data,latstart(iblock),latstop(iblock))
enddo
!$OMP END PARALLEL DO

!        Plus necessaire (nouvelle normalisation des xxt,y,p)
!        data(3:Nphi,:)=data(3:Nphi,:)*0.5
        call fourtf(data,Nphi,1,Nlat)
        data=r(ir)*Vr*data*Buoyancy/r(NR)
        call Surfint(Data,powerint(0,ir))
        data=(ROTVr**2+ROTVt**2+ROTVp**2)*DeltaU
        call Surfint(Data,powerint(1,ir))
        data=(ROTBr**2+ROTBt**2+ROTBp**2)*DeltaB*Lorentz
        call Surfint(Data,powerint(2,ir))

!Surface contribution
        if (ir==NG) then
           data=2.*(Vp**2+Vt**2)/r(ir)
           call Surfint(Data,tmp1)
           surfvisc=surfvisc+tmp1
        endif
        if (ir==NR) then
           data=-2.*(Vp**2+Vt**2)/r(ir)
           call Surfint(Data,tmp1)
           surfvisc=surfvisc+tmp1
        endif

! Get gradients for scales

     endif
     
! Computing Courant criterion for time-stepping

     if ((Aspect_ratio.ne.0).or.(ir.ne.NG)) then

        valri2=(0.5*(1.+DeltaB))**2/dr2(ir)
        valhi2=(0.5*(1.+DeltaB))**2/dh2(ir)
 
        vr2max=0.
        vh2max=0.

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)  &
!$OMP PRIVATE (ilong,ilat) &
!$OMP PRIVATE (vflr2,valr,valr2)    &
!$OMP PRIVATE (vflh2,valh2,valh2m)  &
!$OMP REDUCTION (MAX:vr2max,vh2max)
	do iblock=1,2*nblock
              do ilat=latstart(iblock),latstop(iblock)
                 do ilong=1,nlong

                 vflr2=Vr(ilong,ilat)**2
                 valr =Br(ilong,ilat)**2*Lorentz
                 valr2=valr*valr/(valr+valri2)
                 vr2max=max(vr2max,(cf2*vflr2+af2*valr2))
                 
                 vflh2=  (Vt(ilong,ilat)**2 + Vp(ilong,ilat)**2) 
                 valh2=  (Bt(ilong,ilat)**2 + Bp(ilong,ilat)**2)*Lorentz
                 valh2m=valh2*valh2/(valh2+valhi2)
                 vh2max=max(vh2max,(cf2*vflh2+af2*valh2m))
                 
              enddo
           enddo
	enddo
!$OMP END PARALLEL DO

       dtr_opt=min(dtr_opt,sqrt(dr2(ir)/vr2max))
       dth_opt=min(dth_opt,sqrt(dh2(ir)/vh2max))

     endif

! Calcul du couple magnetique si condIC

     if ((condIC) .and. (ir.eq.NG)) then
        
        data = Br * Bp
        
        do ilat = 1,Nlat/2
           nilat = nlat+1-ilat
           data(:,ilat) = data(:,ilat) + data(:,nilat)
        enddo

        
        ! calcul de sin theta * Br Bphi
        ! calcul de l'integrale
        cpl = 0.0
        do ilat=1,nlat/2
           tmp1=0.
           do ilong=1,nlong
              tmp1  = tmp1 + data(ilong,ilat)
           enddo
           cpl = cpl + xxt(lm10,ilat)*tmp1
        enddo
        
        ! On multiplie par dphi = 2 pi/nlong dans la formule,
        !   et par 4 pi a cause des poids !
        !        cpl = -8 * pi * pi * (r(NG) **3) * cpl /(nlong*sqrt(3.0))
        ! Finalement on redivise par 2
        ! du fait de la nouvelle normalisation des xxt
        cpl = -4 * pi * (r(NG) **3) * cpl /(nlong*sqrt(3.0))
        
        ! dimensionement de la benchmark
        cpl_M = cpl * Lorentz
        
     endif


! Calcul du couple magnétique dans toute la coquille

     if ((condIC).and.(mod(t,modulo).eq.0)) then

                data = Br * Bp
        
        do ilat = 1,Nlat/2
           nilat = nlat+1-ilat
           data(:,ilat) = data(:,ilat) + data(:,nilat)
        enddo

        
        ! calcul de sin theta * Br Bphi
        ! calcul de l'integrale
        cpl = 0.0
        do ilat=1,nlat/2
           tmp1=0.
           do ilong=1,nlong
              tmp1  = tmp1 + data(ilong,ilat)
           enddo
           cpl = cpl + xxt(lm10,ilat)*tmp1
        enddo
        
        ! On multiplie par dphi = 2 pi/nlong dans la formule,
        !   et par 4 pi a cause des poids !
        !        cpl = -8 * pi * pi * (r(NG) **3) * cpl /(nlong*sqrt(3.0))
        ! Finalement on redivise par 2
        ! du fait de la nouvelle normalisation des xxt
        cpl = -4 * pi * (r(ir) **3) * cpl /(nlong*sqrt(3.0))
        
        ! dimensionement de la benchmark
        cpl_M_shell(ir) = cpl * Lorentz
        
     endif

     !Termes non-lineaires

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)  &
!$OMP PRIVATE (ilat)  
do iblock=1,2*nblock

  do ilat=latstart(iblock),latstop(iblock)

NL_Vr(:,ilat) = ROTVt(:,ilat)*Vp(:,ilat)-ROTVp(:,ilat)*Vt(:,ilat)-Lorentz*(ROTBt(:,ilat)*Bp(:,ilat)-ROTBp(:,ilat)*Bt(:,ilat))
NL_Vt(:,ilat) = ROTVp(:,ilat)*Vr(:,ilat)-ROTVr(:,ilat)*Vp(:,ilat)-Lorentz*(ROTBp(:,ilat)*Br(:,ilat)-ROTBr(:,ilat)*Bp(:,ilat))
NL_Vp(:,ilat) = ROTVr(:,ilat)*Vt(:,ilat)-ROTVt(:,ilat)*Vr(:,ilat)-Lorentz*(ROTBr(:,ilat)*Bt(:,ilat)-ROTBt(:,ilat)*Br(:,ilat))
NL_Tt(:,ilat) = Vr(:,ilat)*Gr(:,ilat)+Vt(:,ilat)*Gt(:,ilat)+Vp(:,ilat)*Gp(:,ilat)
NL_Br(:,ilat)= Vt(:,ilat)*Bp(:,ilat)-Vp(:,ilat)*Bt(:,ilat)
NL_Bt(:,ilat)= Vp(:,ilat)*Br(:,ilat)-Vr(:,ilat)*Bp(:,ilat)
NL_Bp(:,ilat)= Vr(:,ilat)*Bt(:,ilat)-Vt(:,ilat)*Br(:,ilat)
if (dd_convection .and. .not. use_tracers) then
   NL_C(:,ilat) = Vr(:,ilat)*Gr_c(:,ilat)+Vt(:,ilat)*Gt_c(:,ilat)+Vp(:,ilat)*Gp_c(:,ilat)
end if

! Construction du tableau de vitesses pour les traceurs
if (use_tracers .and. (.not. tracers_noadvection)) then
   do ilong = 1,Nlong
      vel_spat(1,ilong,ilat,ir) = Vr(ilong,ilat)
      vel_spat(2,ilong,ilat,ir) = Vt(ilong,ilat)
      vel_spat(3,ilong,ilat,ir) = Vp(ilong,ilat)
   enddo
end if

enddo
enddo
!$OMP END PARALLEL DO




     !cc   Projection sur r.(rot(NL)) et -r.rot(rot(NL)),
     !cc   pour l'influence non-lineaire sur Vp et Vt :
     !cc            - r.rot(A) =  -L2(Ator)
     !cc            r.rot(rot(A)) = - (1/r) (d/dr) (r L2(Asph))
     !cc                              + (1/r^2) L2(L2(Apol))
     !cc   Remarque : On divise ces formules par l(l+1) car il y a un
     !cc   L2 de l'autre cote de l'equation (du/dt).

     call fourierr_spat_spec2(Data_spat_spec)

     if (dd_convection .and. .not. use_tracers) then
        call fourtf(NL_C,Nphi,-1,Nlat)
     end if
     !cc   r.rot(rot(u x B) =
     !cc   - (1/r) L2(Pvsph) - (d/dr) L2(Pvsph)
     !cc   +(1/r^2) L2(L2(Pvpol))


!$OMP PARALLEL DO SCHEDULE (DYNAMIC,1)
     do iblock=1,maxmblock
        if (iblock.ne.nmblock+1) then
           do m=mstart(iblock),mstop(iblock)
              call Spat_Spec_Pol_shell(ir,NL_Vr,yNLpr(:,ir),m)
              call Spat_Spec_Pol_shell(ir,NL_Vr,yNLpr(:,ir),mstop(2*nmblock)+1-m)
              call Spat_Spec_SpherTor_shell &
                   (ir,NL_Vt,NL_Vp,yNLsr(:,ir),yNLtr(:,ir),m)
              call Spat_Spec_SpherTor_shell &
                   (ir,NL_Vt,NL_Vp,yNLsr(:,ir),yNLtr(:,ir),mstop(2*nmblock)+1-m)
              call Spat_Spec_Pol_shell(ir,NL_Br,yNLtr2(:,ir),m)
              call Spat_Spec_Pol_shell(ir,NL_Br,yNLtr2(:,ir),mstop(2*nmblock)+1-m)
              call Spat_Spec_SpherTor_shell &
                   (ir,NL_Bt,NL_Bp,yNLsr2(:,ir),Adams4(:,ir),m)
              call Spat_Spec_SpherTor_shell &
                   (ir,NL_Bt,NL_Bp,yNLsr2(:,ir),Adams4(:,ir),mstop(2*nmblock)+1-m)
              call Spat_Spec_scal_shell(NL_Tt,yNLTtr(:,ir),m)
              call Spat_Spec_scal_shell(NL_Tt,yNLTtr(:,ir),mstop(2*nmblock)+1-m)
              if (dd_convection .and. .not. use_tracers) then
                 call Spat_Spec_scal_shell(NL_C,yNLCr(:,ir),m)
                 call Spat_Spec_scal_shell(NL_C,yNLCr(:,ir),mstop(2*nmblock)+1-m)
              end if
           enddo
        else
           do m=mstop(2*nmblock)+1,Alpha_max+1
              call Spat_Spec_Pol_shell(ir,NL_Vr,yNLpr(:,ir),m)
              call Spat_Spec_SpherTor_shell &
                   (ir,NL_Vt,NL_Vp,yNLsr(:,ir),yNLtr(:,ir),m)
              call Spat_Spec_Pol_shell(ir,NL_Br,yNLtr2(:,ir),m)
              call Spat_Spec_SpherTor_shell &
                   (ir,NL_Bt,NL_Bp,yNLsr2(:,ir),Adams4(:,ir),m)
              call Spat_Spec_scal_shell(NL_Tt,yNLTtr(:,ir),m)
              if (dd_convection .and. .not. use_tracers) then
                 call Spat_Spec_scal_shell(NL_C,yNLCr(:,ir),m)
              end if
           enddo
        endif
     enddo
!$OMP END PARALLEL DO

  do lm=2,LMmax
     yNLpr(lm,ir)=r(ir)*yNLpr(lm,ir)*ll_1(lm)
     yNLtr2(lm,ir)=r(ir)*yNLtr2(lm,ir)*ll_1(lm)
     yNLsr(lm,ir)=yNLsr(lm,ir)*ll_1(lm)
     yNLtr(lm,ir)=yNLtr(lm,ir)*ll_1(lm)
     yNLsr2(lm,ir)=yNLsr2(lm,ir)*ll_1(lm)
     Adams4(lm,ir)=Adams4(lm,ir)*ll_1(lm)
  enddo
  yNLpr(1,ir)=0.
  yNLtr2(1,ir)=0.
  yNLsr(1,ir)=0.
  yNLtr(1,ir)=0.
  yNLsr2(1,ir)=0.
  Adams4(1,ir)=0.

  enddo ! ir loop

! Exchange phantom points for spheroidals

  call exch1_2d(yNLsr,LMmax,ideb_Vp,ifin_Vp)
  call exch1_2d(yNLsr2,LMmax,ideb_Vp,ifin_Vp)


! Reduce CFL optimal time step

  dt_opt=min(dtr_opt,dth_opt)
  call MPI_ALLREDUCE(dt_opt,tmp1,1,mpi_double_precision,MPI_MIN,COMM_model,ier)
  dt_opt=tmp1

! Reduce power budget

  if (mod(t,modulo)==0) then
     powerbuoy=0.
     powervisc=0.
     powerohm=0.
     vol=0.

    call exch1_2d(powerint,1,ideb_all,ifin_all)

    ideb = ideb_Vp
    ifin = ifin_Vp
    if (rank==0) ideb = ideb_Vp-1
    do ir = ideb, ifin
       powerbuoy=powerbuoy+(r(ir+1)-r(ir)) &
                 *0.5*(r(ir)**2*powerint(0,ir)+r(ir+1)**2*powerint(0,ir+1))
       powervisc=powervisc+(r(ir+1)-r(ir)) &
                 *0.5*(r(ir)**2*powerint(1,ir)+r(ir+1)**2*powerint(1,ir+1))
       powerohm=powerohm+(r(ir+1)-r(ir)) &
                 *0.5*(r(ir)**2*powerint(2,ir)+r(ir+1)**2*powerint(2,ir+1))
       vol=vol+4*pi*(r(ir+1)-r(ir)) &
                 *0.5*(r(ir)**2+r(ir+1)**2)
    enddo


    tmp1=0.
  call MPI_ALLREDUCE(powerbuoy,tmp1,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  powerbuoy=tmp1
  call MPI_ALLREDUCE(powervisc,tmp1,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  powervisc=tmp1
  call MPI_ALLREDUCE(powerohm,tmp1,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  powerohm =tmp1
  call MPI_ALLREDUCE(surfvisc,tmp1,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  surfvisc =tmp1
  call MPI_ALLREDUCE(vol,tmp1,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  vol      =tmp1

  powerbuoy=powerbuoy/vol
  powerohm =powerohm /vol
  powervisc=(powervisc+surfvisc)/vol

  endif

! Compute Adams Bashforth contributions

  do ir = ideb_Vt,ifin_Vt
     Adams1(:,ir) = Adams1(:,ir) - yNLtr(:,ir)
  enddo
  do ir = ideb_Vp,ifin_Vp
     ! dr . = 1/r d/dr r.
        if((ir.ne.NG+1).and.(ir.ne.NR-1))then
       Adams2(:,ir) = Adams2(:,ir) &
          - (dra(ir) * yNLsr(:,ir-1) &
            +drb(ir) * yNLsr(:,ir) &
            +drc(ir) * yNLsr(:,ir+1)) &
          + r_2(ir)*ll(:) * yNLpr(:,ir)
       endif

      
      if(ir.eq.NG+1)then
          i=NG+1
          tmp1=r(i)-r(i+1)
          tmp2=r(i)-r(i+2)
          atmp=( (yNLsr(:,i)-yNLsr(:,i+1))*tmp2-(yNLsr(:,i)-yNLsr(:,i+2))*tmp1 )&
               &/((r(i)**2-r(i+1)**2)*tmp2-(r(i)**2-r(i+2)**2)*tmp1)
          btmp=(yNLsr(:,i)-yNLsr(:,i+1)-atmp*(r(i)**2-r(i+1)**2))/tmp1
          ctmp=yNLsr(:,i)-atmp*(r(i)**2)-btmp*r(i) 
          bound_yNLsr=atmp*((r(NG)-tmp1)**2)+btmp*(r(NG)-tmp1)+ctmp

          Adams2(:,ir) =  Adams2(:,ir) &
             &- (dra(ir) * bound_yNLsr &
             &+drb(ir) * yNLsr(:,ir)   &
             &+drc(ir) * yNLsr(:,ir+1))&
             & + r_2(ir)*ll(:) * yNLpr(:,ir)

     endif
      if(ir.eq.NR-1)then
             i=NR-3
             tmp1=r(i)-r(i+1)
             tmp2=r(i)-r(i+2)
             atmp=( (yNLsr(:,i)-yNLsr(:,i+1))*tmp2-(yNLsr(:,i)-yNLsr(:,i+2))*tmp1 )&
                  &/((r(i)**2-r(i+1)**2)*tmp2-(r(i)**2-r(i+2)**2)*tmp1)
             btmp=(yNLsr(:,i)-yNLsr(:,i+1)-atmp*(r(i)**2-r(i+1)**2))/tmp1
             ctmp=yNLsr(:,i)-atmp*(r(i)**2)-btmp*r(i) 
             bound_yNLsr=atmp*((r(NR)+tmp1)**2)+btmp*(r(NR)+tmp1)+ctmp

          Adams2(:,ir) =  Adams2(:,ir)   &
             &- (dra(ir) * yNLsr(:,ir-1) &
             &+drb(ir) * yNLsr(:,ir)     &
             &+drc(ir) * bound_yNLsr)    &
             & + r_2(ir)*ll(:) * yNLpr(:,ir)
       endif
     
    

     Adams3(:,ir) = Adams3(:,ir) - yNLTtr(:,ir)


     Adams5(:,ir) = &
          -(dra(ir) * yNLsr2(:,ir-1) &
           +drb(ir) * yNLsr2(:,ir) &
           +drc(ir) * yNLsr2(:,ir+1)) &
           + r_2(ir)*ll(:) * yNLtr2(:,ir)

     if (dd_convection .and. .not. use_tracers) then
        Adams6(:,ir) = Adams6(:,ir) - yNLCr(:,ir)
     end if

  enddo

  ! Attention val de Adams 4 en NR ???
! BUGG : if (rank==size-1)  Adams4(:,NR) = 0.0

if (rank==0) then
   if (CondIC) then
     do ir=1,NG
         Adams4(:,ir) = -im*mi(:)*omega*yBpr(:,ir)
      enddo
      do ir=1,NG-1
         Adams5(:,ir) = -im*mi(:)*omega*yBtr(:,ir)
      enddo
      ! Adams5 is dicontinuous accros the ICB
      ! We use an interpolated value
      Adams5(:,NG)=0.5*(Adams5(:,NG-1)+Adams5(:,NG+1))
   else
!      Adams4(:,NG) = 0.0
      Adams5(:,NG) = 0.0
   endif
endif

  Return
End Subroutine NL2_para

