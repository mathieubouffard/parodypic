
!*************************************************************************
!                                                                        *
!       mpi calls                                                        *
!                                                                        *
!*************************************************************************
module tracers_parallel_mod

contains 

subroutine csend (it,buf,nb,node)
  use mpi
  implicit none
  integer,intent(in)::it,nb,node
  real,intent(in)::buf(*)
  integer*4 ier,nb4,node4,it4
  nb4=nb; node4=node; it4=it
  call mpi_send (buf,nb4,MPI_DOUBLE_PRECISION,node4,it4,MPI_COMM_WORLD,ier)
end subroutine csend


subroutine csend_integer (it,buf,nb,node)
  use mpi
  implicit none
  integer,intent(in)::it,nb,node
  integer,intent(in)::buf(*)
  integer*4 ier,nb4,node4,it4
  nb4=nb; node4=node; it4=it
  call mpi_send (buf,nb4,MPI_INTEGER,node4,it4,MPI_COMM_WORLD,ier)
end subroutine csend_integer


!subroutine crecv (it,buf,nb)
!  use mpi
!  use globall
!  implicit none
!  integer,intent(in)::it,buf(*),nb
!  type(geominfo),intent(in)::g
!  integer*4 status(MPI_status_size),ierr,nb4,it4
!  nb4=nb; it4=it
!  call MPI_recv (buf,nb4,MPI_byte,MPI_any_source,it4,MPI_COMM_WORLD,status,ier)
!end subroutine crecv


integer function irecv (it,buf,len)
  use mpi
  implicit none
  integer,intent(in)::it,len
  real,intent(out)::buf(*)
  integer*4 ier,ireq,len4,it4
  len4=len; it4=it
  call MPI_irecv (buf,len4,MPI_DOUBLE_PRECISION,MPI_any_source,it4,MPI_COMM_WORLD,ireq,ier)
  irecv = ireq
end function irecv


integer function irecv_integer (it,buf,len)
  use mpi
  implicit none
  integer,intent(in)::it,len
  integer,intent(out)::buf(*)
  integer*4 ier,ireq,len4,it4
  len4=len; it4=it
  call MPI_irecv (buf,len4,MPI_INTEGER,MPI_any_source,it4,MPI_COMM_WORLD,ireq,ier)
  irecv_integer = ireq
end function irecv_integer


subroutine msgwait (id)
  use mpi
  integer*4 status(MPI_status_size),ier,id4
  id4=id
  call MPI_wait (id4,status,ier)
end subroutine msgwait


!subroutine glor (l,n,g)    ! global logical OR
!  use mpi
!  use globall
!  implicit none
!  integer,intent(in):: n
!  logical,intent(inout):: l(n)
!  type(geominfo),intent(in)::g 
!  logical lwork(n)
!  integer*4 ierr,n4
!  if (g%nnodes==1) return
!  n4=n
!  call MPI_allreduce (l,lwork,n4,MPI_logical,MPI_lor,g%comm_mesh,ierr)
!  l(:) = lwork(:)
!end subroutine glor

end module tracers_parallel_mod
