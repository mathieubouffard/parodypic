  module data_parody_local
  implicit none
  SAVE 
  Complex, allocatable :: dyVpr(:,:)
  Complex, allocatable :: LyVpr(:,:)
  Complex, allocatable :: dyVtr(:,:)
  Complex, allocatable :: LyVtr(:,:)

  Complex, allocatable  :: yNLVtr(:,:)
  Complex, allocatable ::  yNLVpr(:,:)
  Complex, allocatable ::  yNLTtr(:,:)
  Complex, allocatable ::  yNLCr(:,:)

  Complex, allocatable  ::  yVs(:,:)

  Complex, allocatable  ::  Adams1_past(:,:)
  Complex, allocatable  ::  Adams2_past(:,:)
  Complex, allocatable  ::  Adams3_past(:,:)
  Complex, allocatable  ::  Adams4_past(:,:)
  Complex, allocatable  ::  Adams5_past(:,:)
  Complex, allocatable  ::  Adams6_past(:,:)

  Complex ::  Adams1_p1, Adams1_m1, Adams2_p1, Adams2_m1


  Complex, allocatable ::  Transit_Vt(:,:)
  Complex, allocatable ::  Transit_Vp(:,:)
  Complex, allocatable ::  Transit_Tt(:,:)
  Complex, allocatable ::  Transit_Bt(:,:)
  Complex, allocatable ::  Transit_Bp(:,:)
  Complex, allocatable ::  Transit_C(:,:)

  Real,allocatable :: Beta(:),Gamma(:,:)

  !  Coefficients pour le calcul de Q.
  Real,allocatable :: K11(:), K12(:)
  Real,allocatable :: K21(:), K22(:)

  end module data_parody_local
