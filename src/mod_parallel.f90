MODULE mod_parallel

! !DESCRIPTION:
! This modules provides variables for the MPI parallelization
! to be shared between model-related routines. The are variables
! that are used in the model, even without PDAF and additional
! variables that are only used, if data assimialtion with PDAF
! is performed.
! In addition methods to initialize and finalize MPI are provided.
! The initialization routine is only for the model itself, the 
! more complex initialization of communicators for xecution with
! PDAF is peformed in init\_parallel\_pdaf.
!
! !REVISION HISTORY:
! 2004-10 - Lars Nerger - Initial code
! Later revisions - see svn log
!
! !USES:
  IMPLICIT NONE
  SAVE

  INCLUDE 'mpif.h'

! !PUBLIC DATA MEMBERS:
  ! Basic variables for model state integrations
  INTEGER :: COMM_model  ! MPI communicator for model tasks
  INTEGER :: mype_model  ! Number of PEs in COMM_model
  INTEGER :: npes_model  ! PE rank in COMM_model
  PUBLIC  ::  init_parallel_parody
  PUBLIC  ::  exit_parallel_parody
  CONTAINS

  SUBROUTINE init_parallel_parody
  use globall 
  integer :: ier
  call mpi_init(ier)

!#if SUSPENDCHECK
!call suspendinit
!#endif

call mpi_comm_size(mpi_comm_world,size,ier)
call mpi_comm_rank(mpi_comm_world,rank,ier)
!call mpi_comm_size(COMM_model,size,ier)
!call mpi_comm_rank(COMM_model,rank,ier)
!af PDAF
COMM_model=mpi_comm_world
!end af PDAF


! 3 processors at least
if(size<3) then
   print *,'size=',size,' <3'
   call mpi_finalize(ier)
   stop
endif


  END SUBROUTINE init_parallel_parody
  SUBROUTINE exit_parallel_parody
  integer :: ier
  call MPI_finalize(ier)

  END SUBROUTINE exit_parallel_parody

END MODULE mod_parallel

