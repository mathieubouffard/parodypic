!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Contains all the subroutines needed to impose   !!
!!   boundary conditions with tracers.               !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!==============================
module tracers_bcs
!==============================

public :: init_matrices_bcs
public :: cvol_sources
private :: distance

contains
!==============================


subroutine impose_fixedC_tracers_bottom ()
  ! impose a fixed composition at top (cmb)
  ! calls subroutines similar to adjust_tracers and tracers_to_Cf
  ! but which concern only the tracers located
  ! near r(NG) and r(NG+1)

  use tracerstuff
  use globall

  implicit none

  integer i,ier
  real,allocatable :: dc(:,:,:),c(:,:,:)


  allocate(dc(Nlong,Nlat,NG:NG+1))
  allocate(c(Nlong,Nlat,NG:NG+1))

  dc(:,:,NG)   = C_bottom - comp(:,:,NG) 
  
  ! First correction on tracers
  call adjust_tracers_C_bcs (dc,r(NG),r(NG+1))
  
  ! Then go back to the grid, similar to tracers_to_Cf but limited to the same tracers
  call tracers_to_Cf_bcs (NG,NG+1)
  
  ! Final correction:                 A REVOIR !!!!!!!!!!!!!!!!!!!
  dc(:,:,NG) = C_bottom - c(:,:,NG)
  dc(:,:,NG+1) = comp(:,:,NG+1)-c(:,:,NG+1)
  call adjust_tracers_C_bcs (dc,r(NG),r(NG+1))
  
  deallocate(dc,c)

end subroutine impose_fixedC_tracers_bottom
!--------------------------------------------------------


subroutine impose_fixedC_tracers_top ()
  ! impose a fixed composition at top (cmb)
  ! calls subroutines similar to adjust_tracers and tracers_to_Cf
  ! but which concern only the tracers located
  ! near r(NR-1) and r(NR)

  use tracerstuff
  use globall

  implicit none

  integer i,ier
  real,allocatable :: dc(:,:,:),c(:,:,:)


  allocate(dc(Nlong,Nlat,NR-1:NR))
  allocate(c(Nlong,Nlat,NR-1:NR))

  dc(:,:,NR)   = C_bottom - comp(:,:,NR) 
  
  ! First correction on tracers
  call adjust_tracers_C_bcs (dc,r(NR-1),r(NR))
  
  ! Then go back to the grid, similar to tracers_to_Cf but limited to the same tracers
  call tracers_to_Cf_bcs (NR-1,NR)

  !A REVOIR !!!!!!!!!!!
  
  ! Final correction:
  dc(:,:,NR-1) = comp(:,:,NR-1)-c(:,:,NR-1)
  dc(:,:,NR) = C_bottom - c(:,:,NR)
  call adjust_tracers_C_bcs (dc,r(NR-1),r(NR))
  
  deallocate(dc,c)

end subroutine impose_fixedC_tracers_top
!-------------------------------------------------------------


subroutine tracers_to_Cf_bcs (irad_deb,irad_fin)

  use tracerstuff
  use globall
!$ use OMP_LIB

  implicit none

  integer,intent(in) :: irad_deb,irad_fin
  
  integer i,irad0,irad1,ilat0,ilat1,ilong0,ilong1,irad,ilat,ilong
  real n(Nlong,Nlat,irad_deb:irad_fin+1),comp2(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2)
  real cmean,n1
  logical pole
!$ integer :: nb_threads
!$ real,allocatable :: comp_private(:,:,:),n_private(:,:,:)

  comp(:,:,irad_deb:irad_fin) = 0.
  n(:,:,:) = 0.

!$OMP PARALLEL PRIVATE (irad0,irad1,ilat0,ilat1,ilong0,ilong1,pole,comp_private,n_private)
!$ if (.TRUE.) then

!$ nb_threads = omp_get_num_threads ()
!$ allocate(comp_private(Nlong,Nlat,irad_deb:irad_fin),n_private(Nlong,Nlat,irad_deb:irad_fin+1))
!$ comp_private = 0.
!$ n_private = 0.

!$OMP DO SCHEDULE(STATIC,ntr/nb_threads)
!$  do i=1,ntr
!$     if (tra(1,i) .le. r(irad_fin+1) .and. tra(1,i) .ge. r(irad_deb)) then

!&     pole = .false.

!        finds the coordinates of the corners of the volume element 
!        in which the tracer is located

!$     irad0 = closest_point_coord(1,1,i)
!$     irad1 = closest_point_coord(2,1,i)
!$     ilat0 = closest_point_coord(1,2,i)
!$     ilat1 = closest_point_coord(2,2,i)
!$     ilong0 = closest_point_coord(1,3,i)
!$     ilong1 = closest_point_coord(2,3,i)

!$     if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true. 

!         gives the weight each corner receives from the tracer 

!$     if (.not. pole) then

!$        comp_private(ilong0,ilat0,irad0) = comp_private(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat0,irad0) = comp_private(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(COMPPOS,i)
!$        comp_private(ilong0,ilat1,irad0) = comp_private(ilong0,ilat1,irad0) + weight_interp(3,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat1,irad0) = comp_private(ilong1,ilat1,irad0) + weight_interp(4,i) * tra(COMPPOS,i)
!$        comp_private(ilong0,ilat0,irad1) = comp_private(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat0,irad1) = comp_private(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(COMPPOS,i)
!$        comp_private(ilong0,ilat1,irad1) = comp_private(ilong0,ilat1,irad1) + weight_interp(7,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat1,irad1) = comp_private(ilong1,ilat1,irad1) + weight_interp(8,i) * tra(COMPPOS,i)

!$        n_private(ilong0,ilat0,irad0) = n_private(ilong0,ilat0,irad0) + weight_interp(1,i)
!$        n_private(ilong1,ilat0,irad0) = n_private(ilong1,ilat0,irad0) + weight_interp(2,i)
!$        n_private(ilong0,ilat1,irad0) = n_private(ilong0,ilat1,irad0) + weight_interp(3,i)
!$        n_private(ilong1,ilat1,irad0) = n_private(ilong1,ilat1,irad0) + weight_interp(4,i)
!$        n_private(ilong0,ilat0,irad1) = n_private(ilong0,ilat0,irad1) + weight_interp(5,i)
!$        n_private(ilong1,ilat0,irad1) = n_private(ilong1,ilat0,irad1) + weight_interp(6,i)
!$        n_private(ilong0,ilat1,irad1) = n_private(ilong0,ilat1,irad1) + weight_interp(7,i)
!$        n_private(ilong1,ilat1,irad1) = n_private(ilong1,ilat1,irad1) + weight_interp(8,i)
  
!$     else

!$        comp_private(ilong0,ilat0,irad0) = comp_private(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat0,irad0) = comp_private(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(COMPPOS,i)
!$        comp_private(ilong0,ilat0,irad1) = comp_private(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(COMPPOS,i)
!$        comp_private(ilong1,ilat0,irad1) = comp_private(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(COMPPOS,i)

!$        n_private(ilong0,ilat0,irad0) = n_private(ilong0,ilat0,irad0) + weight_interp(1,i)
!$        n_private(ilong1,ilat0,irad0) = n_private(ilong1,ilat0,irad0) + weight_interp(2,i)
!$        n_private(ilong0,ilat0,irad1) = n_private(ilong0,ilat0,irad1) + weight_interp(5,i)
!$        n_private(ilong1,ilat0,irad1) = n_private(ilong1,ilat0,irad1) + weight_interp(6,i)

!$     end if
!$   end if  
!$  end do
!$OMP END DO

!$  do irad = irad0,irad1
!$    do ilat = 1,Nlat
!$        do ilong = 1,Nlong
!$OMP ATOMIC
!$            comp(ilong,ilat,irad) = comp(ilong,ilat,irad) + comp_private(ilong,ilat,irad)
!$OMP ATOMIC
!$            n(ilong,ilat,irad) = n(ilong,ilat,irad) + n_private(ilong,ilat,irad)
!$        end do
!$     end do
!$  end do


!$ deallocate (comp_private,n_private)

!$ else

  do i = 1,ntr
     if (tra(1,i) .le. r(irad_fin+1) .and. tra(1,i) .ge. r(irad_deb)) then

        pole = .false.

        irad0 = closest_point_coord(1,1,i)
        irad1 = closest_point_coord(2,1,i)
        ilat0 = closest_point_coord(1,2,i)
        ilat1 = closest_point_coord(2,2,i)
        ilong0 = closest_point_coord(1,3,i)
        ilong1 = closest_point_coord(2,3,i)

        if (.not. pole) then

           comp(ilong0,ilat0,irad0) = comp(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(COMPPOS,i)     
           comp(ilong1,ilat0,irad0) = comp(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(COMPPOS,i)
           comp(ilong0,ilat1,irad0) = comp(ilong0,ilat1,irad0) + weight_interp(3,i) * tra(COMPPOS,i)
           comp(ilong1,ilat1,irad0) = comp(ilong1,ilat1,irad0) + weight_interp(4,i) * tra(COMPPOS,i)
           if (irad1 .lt. irad_fin+1) then
           comp(ilong0,ilat0,irad1) = comp(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(COMPPOS,i)
           comp(ilong1,ilat0,irad1) = comp(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(COMPPOS,i)
           comp(ilong0,ilat1,irad1) = comp(ilong0,ilat1,irad1) + weight_interp(7,i) * tra(COMPPOS,i)
           comp(ilong1,ilat1,irad1) = comp(ilong1,ilat1,irad1) + weight_interp(8,i) * tra(COMPPOS,i)
        end if
           
           
           n(ilong0,ilat0,irad0) = n(ilong0,ilat0,irad0) + weight_interp(1,i)
           n(ilong1,ilat0,irad0) = n(ilong1,ilat0,irad0) + weight_interp(2,i)   
           n(ilong0,ilat1,irad0) = n(ilong0,ilat1,irad0) + weight_interp(3,i)   
           n(ilong1,ilat1,irad0) = n(ilong1,ilat1,irad0) + weight_interp(4,i)
           if (irad1 .lt. irad_fin+1) then
           n(ilong0,ilat0,irad1) = n(ilong0,ilat0,irad1) + weight_interp(5,i)
           n(ilong1,ilat0,irad1) = n(ilong1,ilat0,irad1) + weight_interp(6,i)
           n(ilong0,ilat1,irad1) = n(ilong0,ilat1,irad1) + weight_interp(7,i) 
           n(ilong1,ilat1,irad1) = n(ilong1,ilat1,irad1) + weight_interp(8,i)
        end if
        else 
        
           comp(ilong0,ilat0,irad0) = comp(ilong0,ilat0,irad0) + weight_interp(1,i) * tra(COMPPOS,i)          
           comp(ilong1,ilat0,irad0) = comp(ilong1,ilat0,irad0) + weight_interp(2,i) * tra(COMPPOS,i)         
           comp(ilong0,ilat0,irad1) = comp(ilong0,ilat0,irad1) + weight_interp(5,i) * tra(COMPPOS,i)
           comp(ilong1,ilat0,irad1) = comp(ilong1,ilat0,irad1) + weight_interp(6,i) * tra(COMPPOS,i)
           
           n(ilong0,ilat0,irad0) = n(ilong0,ilat0,irad0) + weight_interp(1,i)   
           n(ilong1,ilat0,irad0) = n(ilong1,ilat0,irad0) + weight_interp(2,i)   
           n(ilong0,ilat0,irad1) = n(ilong0,ilat0,irad1) + weight_interp(5,i)    
           n(ilong1,ilat0,irad1) = n(ilong1,ilat0,irad1) + weight_interp(6,i)
           
        end if
     
     end if
  end do
!$ end if
!$OMP END PARALLEL

! now convert to continuum field
!!$OMP PARALLEL PRIVATE(ilat,ilong)
!!$OMP DO SCHEDULE(DYNAMIC,1)
  do irad = irad_deb,irad_fin
     do ilat = 1,nlat
        do ilong = 1,nlong

           if (n(ilong,ilat,irad) .ne. 0) then
              comp(ilong,ilat,irad) = comp(ilong,ilat,irad) / n(ilong,ilat,irad)
           end if

         end do
      end do
  end do
!!$OMP END DO
!!$OMP END PARALLEL

  !LE RECOPIAGE DE CE TABLEAU POURRAIT SE FAIRE DANS LA BOUCLE PRECEDENTE
  comp2(:,:,:) = comp(:,:,:)

  ! special treatment for points around which no tracers were found
!$OMP PARALLEL PRIVATE(ilat,ilong,n1,cmean)
!$OMP DO SCHEDULE(DYNAMIC,1)
  do irad = irad_deb,irad_fin
     do ilat = 1,nlat
        do ilong = 1,nlong
           n1 = 0.
           cmean = 0.
           if (n(ilong,ilat,irad) .eq. 0) then
              call extend_Cf_interpolation_bcs(irad,ilat,ilong,n1,n,comp2,cmean)
              comp(ilong,ilat,irad) = cmean / n1 
           end if

         end do
      end do
  end do
!$OMP END DO
!$OMP END PARALLEL


end subroutine tracers_to_Cf_bcs
!--------------------------------------------------------


subroutine extend_Cf_interpolation_bcs(irad0,ilat0,ilong0,n1,n,cwork,cmean)

  use tracerstuff
  use globall

  implicit none

  integer,intent(in):: irad0,ilat0,ilong0
  real,intent(in):: cwork(nlong,nlat,ideb_Ctra-2:ifin_Ctra+2)
  real,intent(in):: n(nlong,nlat,ideb_Ctra-2:ifin_Ctra+2)
  real,intent(inout):: n1,cmean

  real dist,dist1,dist2,dist3,dist4,dist5,dist6
  integer irad,ilat,ilong,npoints,nrad_search,irad_min,irad_max,nlat_search,ilat_min,ilat_max,i

  !take all points in a defined sphere around irad0,ilat0,ilong0 and use them to give a weighted average
  ! weights = 1/dist
  npoints = 0


!  Delimit broadly a smaller portion to search points for the average

 ! now take a contribution of all points in this portion

  irad_min = irad_min_emptyn(irad0)
  irad_max = irad_max_emptyn(irad0)

  ilat_min = ilat_min_emptyn(ilat0,irad0)
  ilat_max = ilat_max_emptyn(ilat0,irad0)


 ! now take a contribution of all points in this portion

  do irad = irad_min,irad_max
     do ilat = ilat_min,ilat_max
        do ilong = 1,Nlong !2*nlong_search+1 
           if (n(ilong,ilat,irad) .ne. 0.) then
              call distance(irad0,ilat0,ilong0,r(irad),colat(ilat),long(ilong),dist)
              if ((dist .lt. dmax_empty) .and. (dist .ne. 0.)) then
                 cmean = cmean + cwork(ilong,ilat,irad)*(1/dist)
                 n1 = n1 + 1/dist
                 npoints = npoints + 1
              end if
           end if
        end do
     end do
  end do

 ! print*,'Total number of points at less than dmax:',npts
 ! print*,'Number of points used for interpolation:',npoints
 ! print*


  if (npoints .eq. 0) then
    stop '- ERROR: GAP OF TRACERS SOMEWHERE, problem in tracers advection or increase Vmax_empty or ntr'
  end if

end subroutine extend_Cf_interpolation_bcs
!-----------------------------------------------------------------------------------



subroutine calc_heat_flux_bottom (heat_flux,hf_mean)

  use tracerstuff
  use globall

  implicit none

  real,intent(out) :: heat_flux(Nlong,Nlat),hf_mean

  integer ilat,ilong,ir
  real Nu_T

  real,allocatable :: temp_bot(:,:,:)

  Real, POINTER, DIMENSION(:,:)    ::  Tt

  Complex yTtr2(0:LMmax+1)
  
  hf_mean = 0.
  
  Tt => Data_spec_spat (:,:,7)

  allocate(temp_bot(Nlong,Nlat,NG:NG+2))
   
  do ir=NG,NG+2
     yTtr2=yTtr(:,ir)
     ! Bottom Control
     if (BottomControl) then
        yTtr2 = yTtr2 + yTtrIC(:,ir)
     endif
!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
     do iblock=1,nblock
        call Spec_Spat_Scal_Shell(yTtr2,Tt,latstart(iblock),latstop(iblock))
     enddo
!$OMP END PARALLEL DO
     call fourierr_spec_spat2(Data_spec_spat)     
     temp_bot(:,:,ir) = Real(Tt(:,:),8)
   end do
  
  !now compute the heat flux with a 2nd or !3rd order scheme
  do ilat = 1,Nlat
     do ilong = 1,Nlong
        heat_flux(ilong,ilat) = (temp_bot(ilong,ilat,NG+1)-temp_bot(ilong,ilat,NG))/(r(NG+1)-r(NG))!cfb4 *(cfb3*temp_bot(ilong,ilat,NG)   + &
                                      ! cfb1*temp_bot(ilong,ilat,NG+1) + &
        ! cfb2*temp_bot(ilong,ilat,NG+2)   )
        hf_mean = hf_mean + heat_flux(ilong,ilat)*sintheta(ilat)*dlat2(ilat)*dphi
     end do
  end do



  !add static profile's flux at ICB
  heat_flux(:,:) = heat_flux(:,:) + (Tstat(NG+1)-Tstat(NG))/(r(NG+1)-r(NG))
  hf_mean = hf_mean/(4.*pi)       + (Tstat(NG+1)-Tstat(NG))/(r(NG+1)-r(NG))

  deallocate(temp_bot)


end subroutine calc_heat_flux_bottom
!--------------------------------------------------------


subroutine calc_coupled_C_bottom_flux_LOCAL ()

  use tracerstuff
  use globall

  implicit none

  integer ilat,ilong
  real heat_flux(Nlong,Nlat),hf_mean

  call calc_heat_flux_bottom (heat_flux,hf_mean)

  do ilat = 1,Nlat
     do ilong = 1,Nlong
         C_flux(ilong,ilat) = Stefan * Lewis * comp(ilong,ilat,NG) * heat_flux(ilong,ilat) !-q_icb)
     end do
  end do


end subroutine calc_coupled_C_bottom_flux_LOCAL
!--------------------------------------------------------


subroutine impose_C_top_flux_on_tracers ()
  ! PAS TESTE POUR L'INSTANT, NE PAS UTILISER!!!
  ! MEME MODELE QUE impose_C_bottom_flux_on_tracers MAIS LES EQUATIONS CORRESPONDANTES
  ! N'ONT PAS ENCORE ETE ECRITES. REPRENDRE CETTE ROUTINE UNE FOIS LES EQUATIONS PROPREMENT ETABLIES
  
  ! Imposes the mass flux boundary condition at the CMB by adding a mass
  ! fraction perturbation
  ! to each tracer located within a distance dist_flux_cmb from CMB. Masses are
  ! added following a
  ! gaussian-like radial function whose integral between r(NR)-dist_flux_cmb and
  ! r(NR)  equals the (local) mass flux.
  ! Because of the gaussian shape, most of the mass is thus added at
  ! dist_flux_cmb/2 from the CMB.
  ! This prevents mass accumulation at the CMB due to non-slip and
  ! non-penetration boundary conditions
  ! for the velocity. 

  ! 3 modes are currently available:

  ! TC_bcs_top=0 => independent boundary conditions
  ! TC_bcs_top=1 => locally coupled boundary conditions: the mass flux is locally
  ! proportional to the heat flux
  ! TC_bcs_top=2 uniformly coupled boundary conditions: only with homogeneous heat flux at
  ! the CMB

  use tracerstuff
  use globall
!$ USE OMP_LIB

  implicit none

  integer i,ilong0,ilong1,ilat0,ilat1
  real dTstat_dr_NG,coeff,C_icb,hf_icb,w1,w2,w3,w4,w,r_ref,sigma_2,hf_mean!,erf5_2
  real heat_flux(Nlong,Nlat)

!  erf5_2 = erf(5./2.)
  r_ref = r(NR)-dist_flux_cmb*0.5
  sigma_2 = (5./dist_flux_cmb)**2

!======================================================================================
  if (TC_bcs_bot == 0) then !independent boundary conditions 

     coeff = -Aspect_ratio*r(NG)**2/Pr_C*2.054 * 5./(sqrt(pi)*dist_flux_cmb*erf(5./2.))*dt!2.*Nu_C*dt/(Pr_C*Aspect_ratio*delta_icb**2)

!$OMP PARALLEL DO SCHEDULE (DYNAMIC,500)
     do i = 1,ntr
        if (tra(1,i) .gt. (r(NR)-dist_flux_cmb)) then
           tra(COMPPOS,i) = tra(COMPPOS,i) + coeff*exp(-(tra(1,i)-r_ref)**2 *sigma_2)/(tra(1,i)**2)!coeff*(r(NG)+delta_icb-tra(1,i))
        end if
     end do
!$OMP END PARALLEL DO

!======================================================================================
  else if (TC_bcs_bot == 1) then !locally coupled boundary conditions 

     coeff =-5.*Stefan/(sqrt(pi)*dist_flux_icb*erf(5./2.))*(Ra_T)/(Coriolis*Pr_T*Ra_C)*dt! A VERIFIER 

     if (rank ==0) call calc_heat_flux_bottom (heat_flux,hf_mean)

     sum_tra_source = 0.

!$OMP PARALLEL PRIVATE(w,w1,w2,w3,w4,ilat0,ilat1,ilong0,ilong1,C_icb,hf_icb)
!$OMP DO SCHEDULE (DYNAMIC,500)
     do i = 1,ntr
        if (tra(1,i) .lt. (r(NG) + dist_flux_icb)) then

           ilat0 = closest_point_coord(1,2,i)

           if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) then

              ilong0 = closest_point_coord(1,3,i)
              ilong1 = closest_point_coord(2,3,i)

              w1 = weight_interp(1,i)
              w2 = weight_interp(2,i)

              hf_icb = w1 * heat_flux(ilong0,ilat0) + &
                       w2 * heat_flux(ilong1,ilat0)

              hf_icb = hf_icb/(w1+w2)

           else

              ilat1 = closest_point_coord(2,2,i)
              ilong0 = closest_point_coord(1,3,i)
              ilong1 = closest_point_coord(2,3,i)

              w1 = weight_interp(1,i)
              w2 = weight_interp(2,i)
              w3 = weight_interp(3,i)
              w4 = weight_interp(4,i)

           ! linear interpolation of the icb composition and heat flux just
           ! below the tracer
              hf_icb = w1 * heat_flux(ilong0,ilat0) + &
                       w2 * heat_flux(ilong1,ilat0) + &
                       w3 * heat_flux(ilong0,ilat1) + &
                       w4 * heat_flux(ilong1,ilat1)

              w = w1 + w2 + w3 + w4

              hf_icb = hf_icb/w

           !add a perturbation to the tracer corresponding to the mass brought
           !by C flux. Gaussian-like profile.
              tra(COMPPOS,i) = tra(COMPPOS,i) + hf_icb*coeff*exp(-(tra(1,i)-r_ref)**2*sigma_2)/tra(1,i)**2!coeff*C_icb*hf_icb*
              sum_tra_source = sum_tra_source + hf_icb*coeff*exp(-(tra(1,i)-r_ref)**2*sigma_2)/tra(1,i)**2
           end if
        end if
     end do
!$OMP END DO
!$OMP END PARALLEL

!=======================================================================================
  else if (TC_bcs_bot == 2) then !coupled and uniform boundary conditions

!     dTstat_dr_NG = Coriolis*Pr_T*((1.-Ra_C/Ra_T/Stefan)/(r(NR)**3-r(NG)**3)*(r(NR)**3*r_2(NG)-r(NG))-r_2(NG))
     call calc_heat_flux_bottom (heat_flux,hf_mean)

     coeff =-5.*Stefan/(sqrt(pi)*dist_flux_icb*erf(5./2.))*(Ra_T)/(Coriolis*Pr_T*Ra_C)*dt! A VERIFIER 
!     coeff = 5./(sqrt(pi)*dist_flux_icb*erf(5./2.)) * hf_mean/dTstat_dr_NG * dt

     sum_tra_source = 0.

!$OMP PARALLEL DO SCHEDULE (DYNAMIC,500)
     do i = 1,ntr
        if (tra(1,i) .lt. (r(NG) + dist_flux_icb)) then

           !add a perturbation to the tracer corresponding to the mass brought
           !by C flux. Gaussian-like profile.
           tra(COMPPOS,i) = tra(COMPPOS,i) + hf_mean*coeff*exp(-(tra(1,i)-r_ref)**2*sigma_2)/tra(1,i)**2!coeff*C_icb*hf_icb* (r(NG)+dist_flux-tra(1,i))
           sum_tra_source = sum_tra_source + hf_mean*coeff*exp(-(tra(1,i)-r_ref)**2*sigma_2)/tra(1,i)**2
        end if
     end do
!$OMP END PARALLEL DO

!======================================================================================
  else
     stop 'ERROR, TC_bcs mode non recognized'

  end if


end subroutine impose_C_top_flux_on_tracers
!--------------------------------------------------------


subroutine impose_C_bottom_flux_on_tracers ()
  ! Imposes the mass flux boundary condition at the ICB by adding a mass fraction perturbation
  ! to each tracer located within a distance dist_flux_icb from ICB. Masses are added following a
  ! gaussian-like radial function whose integral between r(NG) and r(NG)+dist_flux_icb equals the (local) mass flux.
  ! Because of the gaussian shape, most of the mass is thus added at dist_flux_icb/2 from the ICB.
  ! This prevents mass accumulation at the ICB due to non-slip and non-penetration boundary conditions
  ! for the velocity. 
  
  ! 3 modes are currently available:
  
  ! TC_bcs = 0 => independent boundary conditions
  ! TC_bcs = 1 => locally coupled boundary conditions: the mass flux is locally proportional to the heat flux
  ! TC_bcs = 2 => uniformly coupled boundary conditions: only with homogeneous heat flux at the ICB

  use tracerstuff
  use globall
  use mpi
!$ USE OMP_LIB

  implicit none

  integer i,ilong0,ilong1,ilat0,ilat1,ier
  real dTstat_dr_NG,coeff,C_icb,hf_icb,w1,w2,w3,w4,w,r_ref,sigma_2,hf_mean!,erf5_2
  real heat_flux(Nlong,Nlat)

!  erf5_2 = erf(5./2.)
  r_ref = r(NG)+dist_flux_icb*0.5
  sigma_2 = (10./dist_flux_icb)**2
  
!======================================================================================
  if (TC_bcs_bot == 0) then !independent boundary conditions 

     coeff = 10./(sqrt(pi)*erf(10./2.)*dist_flux_icb) * dt!(r(NR)**3-r(NG)**3)/3. * 5./(sqrt(pi)*dist_flux_icb*erf(5./2.))*dt!2.*Nu_C*dt/(Pr_C*Aspect_ratio*delta_icb**2)
     sum_tra_source = 0. 

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE (DYNAMIC,500)
     do i = 1,ntr
        if (tra(1,i) .lt. (r(NG)+dist_flux_icb)) then
           tra(COMPPOS,i) = tra(COMPPOS,i) + coeff*exp(-(tra(1,i)-r_ref)**2 * sigma_2)/(tra(1,i)**2)!coeff*(r(NG)+delta_icb-tra(1,i))
!$OMP ATOMIC
           sum_tra_source = sum_tra_source + coeff*exp(-(tra(1,i)-r_ref)**2 * sigma_2)/(tra(1,i)**2)
        end if
     end do
!$OMP END DO
!$OMP END PARALLEL 

!======================================================================================
  else if (TC_bcs_bot == 1) then !locally coupled boundary conditions 

     coeff = 10.*Stefan/(sqrt(pi)*dist_flux_icb*erf(10./2.))*(Ra_T)/(Coriolis*Pr_T*Ra_C)*dt ! A VERIFIER ET RECALCULER EN FONCTION DE l'ADIMENSIONNEMENT CHOISI
  
     if (rank == 0) call calc_heat_flux_bottom (heat_flux,hf_mean)
     call mpi_bcast (heat_flux,Nlat*Nlong,mpi_double_precision,0,MPI_COMM_WORLD,ier)

     sum_tra_source = 0.

!$OMP PARALLEL PRIVATE(w,w1,w2,w3,w4,ilat0,ilat1,ilong0,ilong1,C_icb,hf_icb)
!$OMP DO SCHEDULE (DYNAMIC,500)
     do i = 1,ntr
        if (tra(1,i) .lt. (r(NG) + dist_flux_icb)) then
           
           ilat0 = closest_point_coord(1,2,i)

           if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) then

              ilong0 = closest_point_coord(1,3,i)
              ilong1 = closest_point_coord(2,3,i)

              w1 = weight_interp(1,i)
              w2 = weight_interp(2,i)

              hf_icb = w1 * heat_flux(ilong0,ilat0) + &
                       w2 * heat_flux(ilong1,ilat0)

              hf_icb = hf_icb/(w1+w2)

           else
              
              ilat1 = closest_point_coord(2,2,i)
              ilong0 = closest_point_coord(1,3,i)
              ilong1 = closest_point_coord(2,3,i)
           
              w1 = weight_interp(1,i)
              w2 = weight_interp(2,i)
              w3 = weight_interp(3,i)
              w4 = weight_interp(4,i)
           
           ! linear interpolation of the icb composition and heat flux just below the tracer           
              hf_icb = w1 * heat_flux(ilong0,ilat0) + &
                       w2 * heat_flux(ilong1,ilat0) + &
                       w3 * heat_flux(ilong0,ilat1) + &
                       w4 * heat_flux(ilong1,ilat1) 
              
              w = w1 + w2 + w3 + w4
           
              hf_icb = hf_icb/w
           
           !add a perturbation to the tracer corresponding to the mass brought by C flux. Gaussian-like profile.
              tra(COMPPOS,i) = tra(COMPPOS,i) + hf_icb*coeff*exp(-(tra(1,i)-r_ref)**2*sigma_2)/tra(1,i)**2
!$OMP ATOMIC
              sum_tra_source = sum_tra_source + hf_icb*coeff*exp(-(tra(1,i)-r_ref)**2*sigma_2)/tra(1,i)**2
           end if
        end if
     end do
!$OMP END DO
!$OMP END PARALLEL

!=======================================================================================
  else if (TC_bcs_bot == 2) then !coupled and uniform boundary conditions
     
!     dTstat_dr_NG = Coriolis*Pr_T*((1.-Ra_C/Ra_T/Stefan)/(r(NR)**3-r(NG)**3)*(r(NR)**3*r_2(NG)-r(NG))-r_2(NG))
     if (rank == 0) call calc_heat_flux_bottom (heat_flux,hf_mean)
     call mpi_bcast (hf_mean,1,mpi_double_precision,0,MPI_COMM_WORLD,ier)

     coeff = 10.*Stefan/(sqrt(pi)*dist_flux_icb*erf(10./2.))*(Ra_T)/(Coriolis*Pr_T*Ra_C)*dt! A VERIFIER ET CALCULER EN FONCTION DE L'ADIMENSIONNEMENT CHOISI

!     coeff = 5./(sqrt(pi)*dist_flux_icb*erf(5./2.)) * hf_mean/dTstat_dr_NG * dt !HF_ICB TO BE COMPUTED ELSEWHERE !!!!

     sum_tra_source = 0.

!$OMP PARALLEL DO SCHEDULE (DYNAMIC,500)
     do i = 1,ntr
        if (tra(1,i) .lt. (r(NG) + dist_flux_icb)) then
           
           !add a perturbation to the tracer corresponding to the mass brought by C flux. Gaussian-like profile.
           tra(COMPPOS,i) = tra(COMPPOS,i) + hf_mean*coeff*exp(-(tra(1,i)-r_ref)**2 *sigma_2)/tra(1,i)**2
!$OMP ATOMIC
           sum_tra_source = sum_tra_source + hf_mean*coeff*exp(-(tra(1,i)-r_ref)**2 *sigma_2)/tra(1,i)**2
        end if
     end do
!$OMP END PARALLEL DO

!======================================================================================
  else
     stop 'ERROR, TC_bcs mode non recognized'
     
  end if
  

end subroutine impose_C_bottom_flux_on_tracers
!--------------------------------------------------------


subroutine diffusion_bottom_bcs ()
! implementation of imposed flux at ICB at Lewis = infinity. The ICB buoyant plumes 
! that inject the light elements into the FOC are modeled by a decreasing diffusion coefficient 
! on a thin layer above ICB

! First, the diffusion is calculated on the grid for proc 0
! Secondly, the diffusive perturbation is added to the tracers close to the ICB.

  use globall
  use tracerstuff
  use data_parody_local

  implicit none

  integer lm,ir,l,m
  real rdeb,rfin
  real data(Nphi,Nlat)
  Complex yCr2(0:LMmax+1)
  real,allocatable:: comp_before_diffusion(:,:,:),comp_after_diffusion(:,:,:), delta_comp(:,:,:)

!IL VA FALLOIR RECREER DES MATRICES JUSTE POUR LES CONDITIONS AUX LIMITES ICI...

!  rdeb = r(NG)
!  rfin = r(ir_botbcs)

!  allocate(comp_before_diffusion(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2))
!  allocate(comp_after_diffusion(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2))
!  allocate(delta_comp(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2))

  !-------------0) PRELIMINARY-------------------
  
  !Extract perturbative part by subtracting static profile
 ! do ir=ideb_Ctra-2,ifin_Ctra+2
 !   comp_before_diffusion(:,:,ir) = comp(:,:,ir)-C_cond(ir)
 ! enddo

  !Spatial=>Spectral
 ! do ir=ideb_all,ifin_all

 !    data(1:Nlong,:) = comp_before_diffusion(:,:,ir)
 !    data(Nlong+1:Nphi,:) = 0.

 !    call fourtf(data,Nphi,-1,Nlat)

 !    do iblock=1,maxmblock
 !      if (iblock.ne.nmblock+1) then
 !         do m=mstart(iblock),mstop(iblock)
 !  	     call Spat_Spec_scal_shell(data,yCr(:,ir),m)
! 	     call Spat_Spec_scal_shell(data,yCr(:,ir),mstop(2*nmblock)+1-m)!!
!	  enddo
!       else
!          do m=mstop(2*nmblock)+1,Alpha_max+1
!             call Spat_Spec_scal_shell(data,yCr(:,ir),m)
!          enddo
!      endif
!    enddo
! enddo

! call exch1_2d(yCr,LMmax,ideb_C,ifin_C) 
 
!-------------1) SOLVE FOR THE DIFFUSION--------------- 

   if (rank==0) then
!      if ((HeatingMode.eq.2).or.(HeatingMode.eq.3)) then
         ! Performing inversion at NG
         do lm = 1, LMmax
            l = li(lm)
            Transit_C(lm,NG) = A6c_bcs(NG)   * yCr(lm,NG+1) &
                 + A6b_bcs(l,NG) * yCr(lm,NG)
         enddo
   !   endif
   endif

   do ir = ideb_Vp, ifin_Vp
      do lm = 1, LMmax
         l = li(lm)
         Transit_C(lm,ir) = A6a_bcs(ir)   * yCr(lm,ir-1) &
              + A6b_bcs(l,ir) * yCr(lm,ir) &
              + A6c_bcs(ir)   * yCr(lm,ir+1)
      enddo
   enddo

   if (rank==size-1) then
   !   if ((HeatingMode.eq.1).or.(HeatingMode.eq.2).or.(HeatingMode.eq.3) &
   !        .or.(HeatingMode.eq.4)) then
         ! Performing inversion at NR
         do lm = 1, LMmax
            l = li(lm)
            Transit_C(lm,NR) = A6a_bcs(NR)   * yCr(lm,NR-1) &
                 + A6b_bcs(l,NR) * yCr(lm,NR)
         enddo
      endif
 !  endif

   if (rank==0) then
 !     if ((HeatingMode.eq.2).or.(HeatingMode.eq.3)) then
         ! Performing inversion at NG
         ir = NG
         do lm = 1, LMmax
            Transit_C(lm,ir) = -1 * ( Transit_C(lm,ir)) &
                 + ((2.0/dt) * yCr(lm,ir))
         enddo
 !     endif
   endif


   do ir = ideb_Vp, ifin_Vp
      do lm = 1, LMmax
         Transit_C(lm,ir) = -1 * ( Transit_C(lm,ir)) &
                + ((2.0/dt) * yCr(lm,ir))
      enddo
   enddo

   if (rank==size-1) then
   !   if ((HeatingMode.eq.1).or.(HeatingMode.eq.2).or.(HeatingMode.eq.3) &
   !      .or. (HeatingMode.eq.4)) then
         ! Performing inversion at NR
         ir = NR
         do lm = 1, LMmax
            Transit_C(lm,ir) = -1 * ( Transit_C(lm,ir)) &
                 + ((2.0/dt) * yCr(lm,ir))
         enddo
   !   endif
   endif


!  [A3]-1 x {Tt}

  call solver_3_para(A6a_2d_bcs,diag6_bcs,A6c_2d_bcs,Transit_C,yCr,m6_l_bcs,m6_u_bcs,1,LMmax,ideb_C,ifin_C)


!----------2) MODIFY THE TRACERS FOR DIFFUSION----------------------

! ATTENTION : LE CAS OU IR_BOTBCS EST DEPASSE OU LE DOMAINE DE PROC 0 NECESSITE L'ECHANGE DE PLAN TAMPON N'A PAS 
  ! ENCORE ETE PREVU...

!  if (rank==0) then
!     print*,'just after diffusion'
!     call calc_Nu_C_bcs ()
!  end if

!  if (rank == 0) then
     !Save exact field after diffusion
 !    call Spec_spat_C (comp_after_diffusion)

!     do ir=ideb_all,ifin_all
        
!        yCr2(:) = yCr(:,ir)

!!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
!        do iblock=1,nblock
!           call Spec_Spat_Scal_shell(yCr2,data,latstart(iblock),latstop(iblock))
!        enddo
!!$OMP END PARALLEL DO
!        call fourtf(data,Nphi,1,Nlat)
!        comp_after_diffusion(:,:,ir) = data(1:Nlong,:)
!     
!     end do
!     print*,'comp before diffusion',comp(1,48,:)
!     print*
!     print*,'comp_before_diffusion',comp_before_diffusion(1,48,:)
!     print*
!     print*,'comp_after_diffusion',comp_after_diffusion(1,48,:) 
     !Compute deltaT_diffusif
!     delta_comp = comp_after_diffusion - comp_before_diffusion
 !    delta_comp(:,:,NG) = 0.
!     print*
!     print*,'delta_comp',delta_comp(1,48,:)
     !Add this to the tracers
!     call adjust_tracers_C_bcs (delta_comp,rdeb,rfin)
!     call tracers_to_Cf_bcs (NG,ir_botbcs)
!     print*,'before smooth, after tra to C',comp(1,48,:)
     !Diffuse tracers
 !    do ir = NG,ir_botbcs
 !       comp(:,:,ir) = C_cond(ir) + comp_after_diffusion(:,:,ir) !exact field after diffusion 
 !    end do
!     comp(:,:,NG)=1. 
 !    call smooth_tracers_C_bcs (NG,ir_botbcs,rdeb,rfin)
 !    print*
 !    print*,'after smooth tracers',comp(1,48,:)
!  end if

 ! if (rank ==0) then
 !    print*,'just after diffusion on tracers'
 !    call calc_nu_C_bcs ()
 ! end if
 ! if (rank ==0) print*
  
!  deallocate(comp_before_diffusion,comp_after_diffusion,delta_comp)


end subroutine diffusion_bottom_bcs
!------------------------------------------------------------------------------------------


subroutine init_matrices_bcs ()

 use globall
 use tracerstuff
 use data_parody_local

 implicit none

 real a,b
 integer ir,i,l,lm

 a = -DeltaT/(r(ir_botbcs)-r(NG))
 b = -a*r(ir_botbcs)

if (rank==0) then
  do ir = NG,ir_botbcs
end do
end if

 !ATTENTION PREVOIR UN MODE INITIALISATION ET UN MODE DE RECALCUL EN CAS DE CHANGEMENT DE dt !!
 allocate (m6_l_bcs(LMmax,ideb_C-2:ifin_C+2),m6_u_bcs(LMmax,ideb_C-2:ifin_C+2))
     
 allocate (A6a_bcs(NG:NR), A6b_bcs(0:Lmax+1,NG:NR), A6c_bcs(NG:NR))
 allocate (A6bp_bcs(NG:NR))
 
 allocate (A6a_2d_bcs(LMmax,ideb_C-2:ifin_C+2)) 
 allocate (diag6_bcs(LMmax,ideb_C-2:ifin_C+2))
 allocate (A6c_2d_bcs(LMmax,ideb_C-2:ifin_C+2))
 !allocate (Transit_C(LMmax+1,ideb_Vp-2:ifin_Vp+2))

 do ir = ir_botbcs+1, NR!-1 JUSTE POUR LE TEST

    A6a_bcs(ir) = 0.!( -0.5*Lva(ir) - (1.0/r(ir))*Gra(ir) ) *DeltaT
        do l = 0, Lmax
        A6b_bcs(l, ir) = ( 1.0/dt )! &
 !            + ( -0.5 * Lvb(ir) &
 !            - (1.0/r(ir)) * Grb(ir) &
 !            + (Real(l*(l+1)) &
 !            /( 2 * (r(ir))**2) ) ) *DeltaT
     enddo
    A6b_bcs(:,ir) = 1.0/dt
    A6bp_bcs(ir)= ( 1.0/dt )! &
 !         + ( -0.5 * Lvb(ir) &
 !         - (1.0/r(ir)) * Grb(ir) ) *DeltaT
    A6c_bcs = 0.!( -0.5*Lvc(ir) - (1.0/r(ir))*Grc(ir) ) *DeltaT*0.001

 end do

 do ir = NG+1,ir_botbcs

     A6a_bcs(ir) =  ( -0.5*Lva(ir) - (1.0/r(ir))*Gra(ir) ) * DeltaT!(a*r(ir)+b) !DeltaC

     do l = 0, Lmax
        A6b_bcs(l, ir) = ( 1.0/dt ) &
             + ( -0.5 * Lvb(ir) &
             - (1.0/r(ir)) * Grb(ir) &
             + (Real(l*(l+1)) &
             /( 2 * (r(ir))**2) ) ) * DeltaT!(a*r(ir)+b) !DeltaC
     enddo
     A6bp_bcs(ir) = ( 1.0/dt ) &
          + ( -0.5 * Lvb(ir) &
          - (1.0/r(ir)) * Grb(ir) ) * DeltaT!(a*r(ir)+b) !DeltaC

     A6c_bcs(ir) = ( -0.5*Lvc(ir) - (1.0/r(ir))*Grc(ir) ) * DeltaT!(a*r(ir)+b) !DeltaC

     
  enddo

!  if ((HeatingMode.eq.2).or.(HeatingMode.eq.3)) then

! Secular Cooling or thermochemical
! No ICB heat flux for perturbation

     if (Aspect_ratio.ne.0.) then
        A6a_bcs(NG) = 0.
        do l = 0, Lmax
           A6b_bcs(l, NG) = ( 1.0/dt ) &
                + ((1.0 / (r(NG+1)-r(NG))**2 ) &
                + (Real(l*(l+1)) /( 2 * (r(NG))**2))) *DeltaT
        enddo
        A6bp_bcs(NG) = ( 1.0/dt ) + (1.0 / (r(NG+1)-r(NG))**2) *DeltaT
        A6c_bcs(NG) =  ( -1.0 / (r(NG+1)-r(NG))**2 ) *DeltaT
     else
        A6a_bcs(NG) = 0.
        do l = 0, Lmax
           A6b_bcs(l, NG) = ( 1.0/dt ) 
        enddo
        A6bp_bcs(NG) = ( 1.0/dt ) + (1.0 / (r(NG+1)-r(NG))**2) *DeltaT
        A6c_bcs(NG) =  0.
     endif
     
!  else

! Other driving modes: ICB temperature is fixed

!     A6a_bcs(NG+1) = 0.0

!  endif

 ! if ((HeatingMode.eq.1).or.(HeatingMode.eq.2).or.(HeatingMode.eq.3) &
 !    .or.(HeatingMode.eq.4)) then
 !    
!! Chemical convection or secular cooling or thermochemical or ICT/OCflux
!! no CMB heat flux for the temperature perturbation!

     A6a_bcs(NR) =  ( -1.0 / (r(NR)-r(NR-1))**2 ) *DeltaT
     do l = 0, Lmax
        A6b_bcs(l, NR) = ( 1.0/dt ) &
             + ((1.0 / (r(NR)-r(NR-1))**2 ) &
             + (Real(l*(l+1)) /( 2 * (r(NR))**2))) *DeltaT
     enddo
     A6bp_bcs(NR) = ( 1.0/dt ) + (1.0 / (r(NR)-r(NR-1))**2) *DeltaT
     A6c_bcs(NR) = 0.0
!  else
! Delta T heating: CMB temperature is fixed
!     A6c_bcs(NR-1)=0.0
!  endif


 do l = 0, Lmax
     if (A6b_bcs(l,NG+1) .eq. 0.) then
        if(rank==0) print *,'Pb A6b_bcs pour l =',l
        if(rank==0) print *, '2.tridiag (TriRes): cf. Num.Rec. p.43'
 !       call MPI_finalize(ier)
        stop
     endif
  enddo


  do i=ideb_all,ifin_all
     A6a_2d_bcs(:,i)=A6a_bcs(i)
     A6c_2d_bcs(:,i)=A6c_bcs(i)
  enddo

  do i=ideb_all,ifin_all
     do lm = 1, LMmax
        diag6_bcs(lm,i)=A6b_bcs(li(lm),i)              
     enddo
  enddo

  call factor_3_para(A6a_2d_bcs,diag6_bcs,A6c_2d_bcs,LMmax,ideb_C,ifin_C,1,LMmax)
  
  call solver_3_init(A6a_2d_bcs,diag6_bcs,A6c_2d_bcs,m6_l_bcs,m6_u_bcs,LMmax,ideb_C,ifin_C,1,LMmax)


end subroutine init_matrices_bcs
!-------------------------------------------------------------------------------

subroutine adjust_tracers_C_bcs (dc,rdeb,rfin)

! for tracer composition treatment, adjusts tracers according to 
! scalar field, interpolated to tracer positions.
! For diffusion calculated in spectral space

  use tracerstuff
  use globall
!$ use OMP_LIB

  implicit none

  real,intent(in) :: rdeb,rfin
  real,intent(in):: dc(nlong,nlat,ideb_Ctra-2:ifin_Ctra+2)

  integer i,irad0,irad1,ilat0,ilat1,ilong0,ilong1
  real rad,theta,phi,dci,n,dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8, &
       w1,w2,w3,w4,w5,w6,w7,w8,dist_rad,dist_lat,dist_long
  logical pole

 real mtime_debut,mtime_fin,ptime_debut,ptime_fin
!$  integer nb_threads

!$OMP PARALLEL  &
!$OMP PRIVATE (irad0,irad1,ilat0,ilat1,ilong0,ilong1) &
!$OMP PRIVATE (dist_rad,dist_lat,dist_long,rad,theta,phi,dci,n,w1,w2,w3,w4,w5,w6,w7,w8)    &
!$OMP PRIVATE (dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8,nb_threads,pole) &
!$OMP FIRSTPRIVATE (r,colat,long,drad1,dlat1)  

!$ nb_threads = omp_get_num_threads ()

!      IF (rank == 0) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!!$          ptime_debut = omp_get_wtime()
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
!            mtime_debut = MPI_WTime ()
!!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!      END IF

!$OMP DO SCHEDULE(DYNAMIC,500) !(STATIC,ntr/nb_threads) 
  do i = 1,ntr

     if (tra(1,i) .ge. rdeb .and. tra(1,i) .le. rfin) then

        pole = .false.
        
        irad0 = closest_point_coord(1,1,i)
        irad1 = closest_point_coord(2,1,i)
        ilat0 = closest_point_coord(1,2,i)
        ilat1 = closest_point_coord(2,2,i)
        ilong0 = closest_point_coord(1,3,i)
        ilong1 = closest_point_coord(2,3,i)
        
        if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true.
        
        if (.not. pole) then
           
           dci  =   weight_interp(1,i) * dc(ilong0,ilat0,irad0) + &
                    weight_interp(2,i) * dc(ilong1,ilat0,irad0) + &
                    weight_interp(3,i) * dc(ilong0,ilat1,irad0) + &
                    weight_interp(4,i) * dc(ilong1,ilat1,irad0) + &
                    weight_interp(5,i) * dc(ilong0,ilat0,irad1) + &
                    weight_interp(6,i) * dc(ilong1,ilat0,irad1) + &
                    weight_interp(7,i) * dc(ilong0,ilat1,irad1) + &
                    weight_interp(8,i) * dc(ilong1,ilat1,irad1)


        else

           rad = tra(1,i)
           theta = tra(2,i)
           phi = tra(3,i)
           
           call distance(irad0,ilat0,ilong0,rad,theta,phi,dist1)
           call distance(irad1,ilat0,ilong0,rad,theta,phi,dist2)
           call distance(irad0,ilat0,ilong1,rad,theta,phi,dist3)
           call distance(irad1,ilat0,ilong1,rad,theta,phi,dist4)
           
           if (dist1 .eq. 0.) dist1 = 1e-10
           if (dist2 .eq. 0.) dist2 = 1e-10
           if (dist3 .eq. 0.) dist3 = 1e-10
           if (dist4 .eq. 0.) dist4 = 1e-10
           
           w1 = 1/dist1
           w2 = 1/dist2
           w3 = 1/dist3
           w4 = 1/dist4
      
           if (mod(nlong,2) .eq. 0) then
              call distance(irad0,ilat0,mod(ilong0+nlong*Mc/2-1,nlong)+1,rad,theta,phi,dist5)
              call distance(irad0,ilat0,mod(ilong0+nlong*Mc/2,nlong)+1,rad,theta,phi,dist6)
              call distance(irad1,ilat0,mod(ilong0+nlong*Mc/2-1,nlong)+1,rad,theta,phi,dist7)
              call distance(irad1,ilat0,mod(ilong0+nlong*Mc/2,nlong)+1,rad,theta,phi,dist8)
              
              if (dist5 .eq. 0.) dist5 = 1e-10
              if (dist6 .eq. 0.) dist6 = 1e-10
              if (dist7 .eq. 0.) dist7 = 1e-10
              if (dist8 .eq. 0.) dist8 = 1e-10
              
              w5 = 1/dist5
              w6 = 1/dist6
              w7 = 1/dist7
              w8 = 1/dist8

           else 
              call distance(irad0,ilat0,mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,rad,theta,phi,dist5)
              call distance(irad1,ilat0,mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,rad,theta,phi,dist6)
              dist5 = 0.5*dist5
              dist6 = 0.5*dist6
              
              if (dist5 .eq. 0.) dist5 = 1e-10
              if (dist6 .eq. 0.) dist6 = 1e-10
              
              w5 = 1/dist5
              w6 = 1/dist6
              
           end if
           
           if (mod(nlong*Mc,2) .eq. 0) then
              
              dci =  w1 * dc(ilong0,ilat0,irad0) +  &
                     w2 * dc(ilong1,ilat0,irad0) +  &
                     w3 * dc(ilong0,ilat0,irad1) +  &
                     w4 * dc(ilong1,ilat0,irad1) +  &
                     w5 * dc(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * dc(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad0)   +  &
                     w7 * dc(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad1) +  &
                     w8 * dc(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad1)   
              
              n = w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8
              
           else  
              
              dci =  w1 * dc(ilong0,ilat0,irad0) +  &
                     w2 * dc(ilong1,ilat0,irad0) +  &
                     w3 * dc(ilong0,ilat0,irad1) +  &
                     w4 * dc(ilong1,ilat0,irad1) +  &
                     w5 * dc(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad0) +  &
                     w6 * dc(mod(ilong0+(nlong*Mc+1)/2,nlong)+1,ilat0,irad1) 
              
              n = w1 + w2 + w3 + w4 + w5 + w6 

           end if
           
           dci = dci/n

        end if


        tra(COMPPOS,i) = tra(COMPPOS,i) + dci

     end if
  end do
!$OMP END DO

!IF (rank == 0) THEN
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          ptime_fin = omp_get_wtime()
!!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele adjust_tracers_T : ', ptime_fin - ptime_debut
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            mtime_fin = MPI_WTime ()
!            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele adjust_tracers_T : ', mtime_fin - mtime_debut
!!$       END IF
!      END IF

!$OMP END PARALLEL

end subroutine adjust_tracers_C_bcs
!-------------------------------------------------------------------------

subroutine smooth_tracers_C_bcs (irad_deb,irad_fin,rdeb,rfin)

  use tracerstuff
  use globall
  use mpi
!$ USE OMP_LIB

  implicit none

  integer,intent(in) :: irad_deb,irad_fin
  real,intent(in) :: rdeb,rfin

  integer i,irad0,irad1,ilat0,ilat1,ilong0,ilong1
  real rad,theta,phi,dist_rad,dist_lat,dist_long,w1,w2,w3,w4,w5,w6,w7,w8,w9,&
       dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8
  real Cold,t_diffi,n,rfac,dCi,Ci

  real,dimension(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2):: comp0,cwork

  logical pole
  real mtime_debut,mtime_fin,ptime_debut,ptime_fin
!$ integer nb_threads

  ! smooth temperatures carried on tracers, similar to Gerya and Yuen (2003)
  ! (i) adjusts them towards an interpolated linear profile with a timescale that depends
  !      on local grid spacing and thermal diffusivity
  ! (ii) calculates grid-scale diffusion that results from this, and corrects for it

!$OMP PARALLEL &
!$OMP PRIVATE (irad0,irad1,ilat0,ilat1,ilong0,ilong1) &
!$OMP PRIVATE (dist_rad,dist_lat,dist_long,n,rad,theta,phi,w1,w2,w3,w4,w5,w6,w7,w8)    &
!$OMP PRIVATE (dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8)  &
!$OMP PRIVATE (pole,nb_threads,Cold,t_diffi,rfac,dCi,Ci) &
!$OMP FIRSTPRIVATE (r,colat,long,drad1,dlat1)

!$ nb_threads = omp_get_num_threads ()

!      IF (rank == 0) THEN
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          WRITE (6,'(3/,A)') 'Execution en parallele hybride : MPI + OpenMP'
!!$          WRITE (6,'(A,I4)') 'Nombre de taches MPI : ', size
!!$          WRITE (6,'(A,I2,3///)') 'Nombre de threads OpenMP par tache MPI : ', nb_threads 
!!$          ptime_debut = omp_get_wtime()
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            WRITE (6,'(3/,A)') 'Execution en parallele simple : MPI'
!            WRITE (6,'(A,I4,3///)') 'Nombre de taches MPI : ', size
!            mtime_debut = MPI_WTime ()
!!$       END IF
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!            WRITE (6,'(A)')
!      END IF

!$OMP DO SCHEDULE(DYNAMIC,500) !(STATIC,ntr/nb_threads)
  do i=1,ntr

     if (tra(1,i) .ge. rdeb .and. tra(1,i) .le. rfin) then

        pole = .false.
        
        rad = tra(1,i)
        theta = tra(2,i)
        phi = tra(3,i)

        irad0 = closest_point_coord(1,1,i)
        irad1 = closest_point_coord(2,1,i)
        ilat0 = closest_point_coord(1,2,i)
        ilat1 = closest_point_coord(2,2,i)
        ilong0 = closest_point_coord(1,3,i)
        ilong1 = closest_point_coord(2,3,i)
        
        if (ilat0 .eq. 1 .or. ilat0 .eq. Nlat) pole = .true.
        
        if (.not. pole) then
           
           Ci   =   weight_interp(1,i) * comp(ilong0,ilat0,irad0) + &
                    weight_interp(2,i) * comp(ilong1,ilat0,irad0) + &
                    weight_interp(3,i) * comp(ilong0,ilat1,irad0) + &
                    weight_interp(4,i) * comp(ilong1,ilat1,irad0) + &
                    weight_interp(5,i) * comp(ilong0,ilat0,irad1) + &
                    weight_interp(6,i) * comp(ilong1,ilat0,irad1) + &
                    weight_interp(7,i) * comp(ilong0,ilat1,irad1) + &
                    weight_interp(8,i) * comp(ilong1,ilat1,irad1)
           
           t_diffi   =   weight_interp(1,i) * t_diff_C(ilong0,ilat0,irad0) + &
                         weight_interp(2,i) * t_diff_C(ilong1,ilat0,irad0) + &
                         weight_interp(3,i) * t_diff_C(ilong0,ilat1,irad0) + &
                         weight_interp(4,i) * t_diff_C(ilong1,ilat1,irad0) + &
                         weight_interp(5,i) * t_diff_C(ilong0,ilat0,irad1) + &
                         weight_interp(6,i) * t_diff_C(ilong1,ilat0,irad1) + &
                         weight_interp(7,i) * t_diff_C(ilong0,ilat1,irad1) + &
                         weight_interp(8,i) * t_diff_C(ilong1,ilat1,irad1)


        else !if pole 

           call distance(irad0,ilat0,ilong0,rad,theta,phi,dist1)
           call distance(irad1,ilat0,ilong0,rad,theta,phi,dist2)
           call distance(irad0,ilat0,ilong1,rad,theta,phi,dist3)
           call distance(irad1,ilat0,ilong1,rad,theta,phi,dist4)
        
           if (dist1 .eq. 0.) dist1 = 1.e-10
           if (dist2 .eq. 0.) dist2 = 1.e-10
           if (dist3 .eq. 0.) dist3 = 1.e-10
           if (dist4 .eq. 0.) dist4 = 1.e-10
        
           w1 = 1./dist1
           w2 = 1./dist2
           w3 = 1./dist3
           w4 = 1./dist4
        
           if (mod(nlong,2) .eq. 0) then
              call distance(irad0,ilat0,mod(ilong0+nlong*Mc/2-1,nlong)+1,rad,theta,phi,dist5)
              call distance(irad0,ilat0,mod(ilong0+nlong*Mc/2,nlong)+1,rad,theta,phi,dist6)
              call distance(irad1,ilat0,mod(ilong0+nlong*Mc/2-1,nlong)+1,rad,theta,phi,dist7)
              call distance(irad1,ilat0,mod(ilong0+nlong*Mc/2,nlong)+1,rad,theta,phi,dist8)
         
              if (dist5 .eq. 0.) dist5 = 1.e-10
              if (dist6 .eq. 0.) dist6 = 1.e-10
              if (dist7 .eq. 0.) dist7 = 1.e-10
              if (dist8 .eq. 0.) dist8 = 1.e-10
              
              w5 = 1./dist5
              w6 = 1./dist6
              w7 = 1./dist7
              w8 = 1./dist8
           
           else 
              call distance(irad0,ilat0,mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,rad,theta,phi,dist5)
              call distance(irad1,ilat0,mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,rad,theta,phi,dist6)
              dist5 = 0.5*dist5
              dist6 = 0.5*dist6
           
              if (dist5 .eq. 0.) dist5 = 1e-10
              if (dist6 .eq. 0.) dist6 = 1e-10
           
              w5 = 1/dist5
              w6 = 1/dist6
              
           end if
           
           if (mod(nlong,2) .eq. 0) then
              
              Ci    =   w1 * comp(ilong0,ilat0,irad0) +  &
                        w2 * comp(ilong1,ilat0,irad0) +  &
                        w3 * comp(ilong0,ilat0,irad1) +  &
                        w4 * comp(ilong1,ilat0,irad1) +  &
                        w5 * comp(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad0) +  &
                        w6 * comp(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad0)   +  &
                        w7 * comp(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad1) +  &
                        w8 * comp(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad1)   

              t_diffi = w1 * t_diff_C(ilong0,ilat0,irad0) +  &
                        w2 * t_diff_C(ilong1,ilat0,irad0) +  &
                        w3 * t_diff_C(ilong0,ilat0,irad1) +  &
                        w4 * t_diff_C(ilong1,ilat0,irad1) +  &
                        w5 * t_diff_C(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad0) +  &
                        w6 * t_diff_C(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad0)   +  &
                        w7 * t_diff_C(mod(ilong0+nlong*Mc/2-1,nlong)+1,ilat0,irad1) +  &
                        w8 * t_diff_C(mod(ilong0+nlong*Mc/2,nlong)+1,ilat0,irad1)  

              n = w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8

           else

              Ci    =   w1 * comp(ilong0,ilat0,irad0) +  &
                        w2 * comp(ilong1,ilat0,irad0) +  &
                        w3 * comp(ilong0,ilat0,irad1) +  &
                        w4 * comp(ilong1,ilat0,irad1) +  &
                        w5 * comp(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad0) +  &
                        w6 * comp(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad1) 

              t_diffi = w1 * t_diff_C(ilong0,ilat0,irad0) +  &
                        w2 * t_diff_C(ilong1,ilat0,irad0) +  &
                        w3 * t_diff_C(ilong0,ilat0,irad1) +  &
                        w4 * t_diff_C(ilong1,ilat0,irad1) +  &
                        w5 * t_diff_C(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad0) +  &
                        w6 * t_diff_C(mod(ilong0+(nlong*Mc+1)/2-1,nlong)+1,ilat0,irad1)
         
              n = w1 + w2 + w3 + w4 + w5 + w6 
              
           end if
           
           Ci = Ci/n                
           t_diffi = t_diffi/n

        end if

        Cold = tra(COMPPOS,i)
        rfac = exp(-numerical_diffusion * dt/t_diffi)           ! fraction of old temperature to keep
        dCi = (Ci-Cold)*rfac
        tra(COMPPOS,i) = Ci - dCi 
     
     end if
  end do
!$OMP END DO

!  IF (rank == 0) THEN
!!$       IF (.TRUE.) THEN
!!$OMP MASTER
!!$          ptime_fin = omp_get_wtime()
!!$          Write (6,'(A,F10.2)') 'Temps ecoule, mixte, region parallele smooth_tracer_T : ', ptime_fin - ptime_debut
!!$OMP END MASTER
!!$OMP BARRIER
!!$       ELSE
!            mtime_fin = MPI_WTime ()
!            Write (6,'(A,F10.2)') 'Temps ecoule, MPI  , region parallele smooth_tracer_T : ', mtime_fin - mtime_debut
!!$       END IF
!      END IF

!$OMP END PARALLEL

  comp0 = comp

  call tracers_to_Cf_bcs(irad_deb,irad_fin)
!  call exchange_ghost_planes_dC(comp)

  Cwork = -(comp-comp0)   ! temperature change due to erroneous diffusion

  !..subtract this.
  call adjust_tracers_C_bcs(Cwork,rdeb,rfin)

  ! NOTE: actually this will not perfectly subtract the
  !  diffusion. Trying testing this. Could even iterate to see if it
  !  eventally does subtract diffusion

 ! print*,rank,'after adjust_tracers_T'
  ! final update of temperature (which ideally would not have changed) USELESS IF DOING ADVECTION RIGHT AFTER
  call tracers_to_Cf_bcs(irad_deb,irad_fin)
!  call exchange_ghost_planes_dC(comp)

  
end subroutine smooth_tracers_C_bcs
!---------------------------------------------------------------------------------------


subroutine cvol_sources ()

! add a term of source/well in volume to reach a steady state when imposing a flux at iCB

  use tracerstuff
  use globall
  use mpi
!$USE OMP_LIB

  implicit none

  integer ier,i
  real dC,sum_tra,sum_tra_tot,sum_tra_tot_dC
  real comp0(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2)

! if (rank == 0) print*,'sum_tra_source =',sum_tra_source
 ! if (TC_bcs == 0) then
 !    dC = -dt
 ! else if (TC_bcs == 1 .or. TC_bcs == 2) then
     call mpi_bcast (sum_tra_source,1,mpi_double_precision,0,MPI_COMM_WORLD,ier)
! print*,rank,'sum_tra_source=',sum_tra_source,sum_tra_source/ntr_tot,sum_tra_source/ntr_tot*ntr

     dC = -sum_tra_source/ntr_tot 
 ! end if
 ! dC = -3.*Nu_C*dt*r(NG)**2/(Aspect_ratio*Pr_C*(r(NR)**3-r(NG)**3))

  ! PLUSIEURS FACON DE CALCULER dC:
  ! 1) ANALYTIQUEMENT A PARTIR DU FLUX DE CHALEUR INTEGRE
  ! 2) EN SOMMANT LES PERTURBATIONS AJOUTEES AUX TRACEURS ET EN DIVISANT PAR ntr (SUPPOSE QUE LES TRACEURS SONT BIEN REPARTIS DE MANIERE HOMOGENE)
  ! TESTER LES DEUX ET COMPARER ?
  ! AVANTAGE DE 2): LA SOMME DES INFOS CONTENUS PAR LES TRACEURS RESTE CONSTANTE (moins les corrections...).
  ! OU ALORS CALCUL SUR LA GRILLE ?

  
! first step : add deltaC to all tracers
!!$OMP PARALLEL PRIVATE ()
!!$OMP DO SCHEDULE(DYNAMIC,500)
  do i = 1,ntr
     tra(COMPPOS,i) = tra(COMPPOS,i) + dC
  end do
!!$OMP END DO
!!$OMP END PARALLEL

!  print*,rank,'dC * ntr',dC*ntr

  ! store previous composition
!  comp0(:,:,:) = comp(:,:,:)
  
  ! compute new comp field
!  call tracers_to_Cf ()

  ! compute the difference (should be exactly deltaC if tracers where evenly dispersed) and compare to dC
!  comp0(:,:,:) = dC - (comp(:,:,:) - comp0(:,:,:)) 

  ! correct for the error :
!  call adjust_tracers_C (comp0)

  ! final composition update
!  call tracers_to_Cf ()


!CHECK IT'S ACTUALLY WORKING
  sum_tra = 0.
  sum_tra_tot = 0.

  sum_tra = sum(tra(COMPPOS,1:ntr))
!  print*,rank,'sum_tra',sum_tra
  call mpi_reduce (sum_tra,sum_tra_tot,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,ier)
!  call mpi_reduce (dC*ntr,sum_tra_tot_dC,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,ier)
  if (rank == 0) then
     print*,'SUM TRA =',sum_tra_tot
  endif

end subroutine cvol_sources
!---------------------------------------------------------------------------------


subroutine distance(irad0,ilat0,ilong0,rad,theta,phi,dist)

  use tracerstuff
  use globall

  implicit none

  integer,intent(in):: irad0,ilat0,ilong0
  real,intent(in):: rad,theta,phi
  real,intent(out):: dist

  real d2_1,d2_2,d2_3

! calculates distance (dist) between two points (irad0,ilat0,ilong0) and (irad,ilat,ilong) 
! in spherical coordinates

  d2_1 = (rad*cos(phi)*sin(theta) - r(irad0)*cosphi(ilong0)*sintheta(ilat0))**2
  d2_2 = (rad*sin(phi)*sin(theta) - r(irad0)*sinphi(ilong0)*sintheta(ilat0))**2
  d2_3 = (rad*cos(theta) - r(irad0)*costheta(ilat0))**2

  dist = sqrt(d2_1 + d2_2 + d2_3)


end subroutine distance
!-----------------------------------------------------------------------------------

subroutine calc_Nu_C_bcs ()

  use globall
  use tracerstuff
 
  implicit none

  integer ilat,ilong,ir
  real NuC
  real,allocatable :: comp_bot(:,:,:)
  real data(Nphi,Nlat)
  Complex yCr2(0:LMmax+1)


  allocate(comp_bot(Nlong,Nlat,NG:NG+2))

  NuC = 0.

  do ir=NG,NG+2

     yCr2(:) = yCr(:,ir)

     do iblock=1,nblock
        call Spec_Spat_Scal_shell(yCr2,data,latstart(iblock),latstop(iblock))
     enddo
     call fourtf(data,Nphi,1,Nlat)
     comp_bot(:,:,ir) = data(1:Nlong,:)! + C_cond(ir)
     
  end do

  !now compute the heat flux with a 3rd order scheme
  do ilat = 1,Nlat
     do ilong = 1,Nlong
        NuC = NuC + icb_dS(ilong,ilat) *cfb4 *(cfb3*comp_bot(ilong,ilat,NG)   + &
                                               cfb1*comp_bot(ilong,ilat,NG+1) + &
                                               cfb2*comp_bot(ilong,ilat,NG+2)   )
     end do
  end do
  
  NuC = -Aspect_ratio * NuC/surf_icb !(4*pi*r(NG)**2)

  print*,'-------------------------'
  print*,'Nu_C =',NuC
  print*,'-------------------------'

  deallocate(comp_bot)

end subroutine calc_Nu_C_bcs
!------------------------------------------------------------

end module tracers_bcs
