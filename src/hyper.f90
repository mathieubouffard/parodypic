!==========================
  module hyper
!==========================
  use globall
  use data_hyper 
  use mod_parallel
  implicit none
  public :: create_hyperdiffusivities
  private
  contains
!===========================================
  subroutine create_hyperdiffusivities
!
  integer :: l 
  integer :: ier
!
  allocate(hyperdiffU(0:lmax))
  allocate(hyperdiffB(1:lmax))
  allocate(hyperdiffT(0:lmax))
!
  if (rank==0) call readin_hyperfile
! Broadcast info 
  call mpi_bcast(lcutU,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(expU,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(aU,1,mpi_double_precision,0,COMM_model,ier)
!
  call mpi_bcast(lcutT,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(expT,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(aT,1,mpi_double_precision,0,COMM_model,ier)
!
  call mpi_bcast(lcutB,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(expB,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(aB,1,mpi_double_precision,0,COMM_model,ier)
!
!
  do l = 0, lmax  
   if (l<lcutU) then 
     hyperdiffU(l) = 1.
   else
     hyperdiffU(l) = 1. + aU*real((l-lcutU))**expU
   end if
   if (l<lcutT) then 
     hyperdiffT(l) = 1.
   else
     hyperdiffT(l) = 1. + aT*real((l-lcutT))**expT
   end if
   if (l>0  .AND.  l< lcutB) then  
     hyperdiffB(l) = 1.
   elseif (l>=lcutB) then 
     hyperdiffB(l) = 1. + aB*real((l-lcutB))**expB
   end if
  end do
!
  allocate(CLe_hyp(0:lmax))
  allocate(CLi_hyp(0:lmax))
!
  allocate(A1a_hyp(Lmax+1,NG:NR))
  allocate(A1c_hyp(Lmax+1,NG:NR))
  allocate(A3a_hyp(0:Lmax+1,NG:NR))
  allocate(A3bp_hyp(0:Lmax+1,NG:NR))
  allocate(A3c_hyp(0:Lmax+1,NG:NR))
  allocate(A4a_hyp(Lmax+1,NR))
  allocate(A4c_hyp(Lmax+1,NR))
  allocate(A5a_hyp(Lmax+1,NR))
  allocate(A5c_hyp(Lmax+1,NR))
!

  if (rank==0) write (6,*) ' MAXVAL(hyperdiffU) ', MAXVAL(hyperdiffU) 
  if (rank==0) write (6,*) ' MINVAL(hyperdiffU) ', MINVAL(hyperdiffU) 
  if (rank==0) write (6,*) ' MAXVAL(hyperdiffT) ', MAXVAL(hyperdiffT) 
  if (rank==0) write (6,*) ' MINVAL(hyperdiffT) ', MINVAL(hyperdiffT) 
  if (rank==0) write (6,*) ' MAXVAL(hyperdiffB) ', MAXVAL(hyperdiffB) 
  if (rank==0) write (6,*) ' MINVAL(hyperdiffB) ', MINVAL(hyperdiffB) 

  end subroutine create_hyperdiffusivities 
!===========================================
!
!===========================================
  subroutine readin_hyperfile
!
  open(5,file='./inparam_hyper',STATUS="OLD",POSITION="REWIND")
   read(5,*) lcutU
   read(5,*) lcutT
   read(5,*) lcutB
   read(5,*) expU
   read(5,*) expT
   read(5,*) expB
   read(5,*) aU
   read(5,*) aT
   read(5,*) aB
  close(5)
  end subroutine readin_hyperfile
!==========================================

!
!
!============================
  end module hyper
!=============================
