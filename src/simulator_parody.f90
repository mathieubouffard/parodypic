!==========================
  module simulator_parody
!==========================
  implicit none
  public :: fw_parody
  public :: initial_condition_parody 
  contains
  subroutine fw_parody(nsteps)
  use globall
  use tracers
  use tracerstuff
  use tracers_bcs
  use mod_parallel
  use data_parody_local 
  use data_parody_timing 
  use data_hyper 
  use matrices_parody
  integer, intent(IN) :: nsteps
  integer :: i,l,m,ir,lm
  integer :: it
  integer :: ier
! Adams Interpolation
  Complex atmp, btmp, ctmp
  Real  ::  tmp1, tmp2, tmp3
  real  :: AB1,AB2, dt_past
  real stop_code
  external stop_code
  real print_graph
  external print_graph
  integer toresub
  real data(Nphi,Nlat)
  Complex yCr2(0:LMmax+1) 
  real tor10
  integer :: version
  character*16 :: num
  character*32 :: sortie
  real,allocatable:: temp_before_diffusion(:,:,:),temp_after_diffusion(:,:,:), delta_temp(:,:,:)
  real,allocatable:: comp_before_diffusion(:,:,:),comp_after_diffusion(:,:,:), delta_comp(:,:,:),comp_f(:,:,:)
  real :: t0
  real mtime_deb,mtime_fin,mtime
  version=io_version
  !---------------
  t0 = 0.
  !---------------

  if (temperature_tracers) then
     allocate(temp_before_diffusion(Nlong,Nlat,ideb_Ttra-2:ifin_Ttra+2))
     allocate(temp_after_diffusion(Nlong,Nlat,ideb_Ttra-2:ifin_Ttra+2))
     allocate(delta_temp(Nlong,Nlat,ideb_Ttra-2:ifin_Ttra+2))
  end if

  if (composition_tracers) then
     if (diffusion_composition) then
        allocate(comp_before_diffusion(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2))
        allocate(comp_after_diffusion(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2))
     end if
     allocate(delta_comp(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2))
  end if

  if (dd_convection .and. .not. use_tracers) then
     allocate(comp_f(Nlong,Nlat,max(ideb_C-2,NG):min(ifin_C+2,NR)))
  end if

  TIME_INTEGRATION:do it = 1, nsteps

     if (rank ==0) then
        print*
        print*,'================================================================================='
        print*,'                            TIME STEP:',it
        print*,'---------------------------------------------------------------------------------'
        mtime_deb = MPI_Wtime ()
     end if
     
     call quick_fix_para

! Catch various signals sent to the code
#ifndef XLF
        call signal(30,stop_code,-1) 
        call signal(40,print_graph,-1)
#else
        call signal(30,stop_code)
        call signal(40,print_graph)
#endif

        if (stopcode==1) print *,'processor ',rank,' received signal 30'
        call MPI_ALLREDUCE(stopcode,lm,1,mpi_integer,MPI_SUM,COMM_model,ier)

        if (lm.gt.0) then

           call quick_fix_para
           call ecriture
           call graphic_ave

           call output_std_end
           
           if(rank==0) print *
           if(rank==0) print *,'Thank you for using PARODY!'
           
           if(rank==0) then
              open (22, file='log.'//trim(runid), STATUS = 'old', position='append')
              write(22,*) 
              write(22,*) 'Thank you for using PARODY!'
              close(22)
           endif

           call MPI_finalize(ier)
           stop
        endif

        if (printgraph==1) print *,'processor ',rank,' received signal 40'
        call MPI_ALLREDUCE(printgraph,lm,1,mpi_integer,MPI_SUM,COMM_model,ier)
        
        if (lm.gt.0) then
           printgraph=0
           l_printgraph=.true.
        endif

     !     Copie pour croissance

     yBpr_o(:) = yBpr(:,ifin_Bp)
     yBtr_o(:) = yBtr(:,ifin_Bp)
     yVpr_o(:) = yVpr(:,ifin_Vp)
     yVtr_o(:) = yVtr(:,ifin_Vp)

     !**        Copie des Adams1, Adams2, Adams3 precedants


     Adams1_past = Adams1
     Adams2_past = Adams2
     Adams3_past = Adams3
     Adams4_past = Adams4
     Adams5_past = Adams5
     if (dd_convection .and. .not. use_tracers) Adams6_past = Adams6


  if (CondIC) then
     ! Rotation de la graine
     Couple_old = Couple
     Omega_old  = Omega
  else
     Couple=0.
     Omega=0.
  endif

!    Echange des points fantomes


  call exch2_2d(yVpr,LMmax,ideb_Vp,ifin_Vp)
  call exch1_2d(yVtr,LMmax,ideb_Vt,ifin_Vt)
  call exch1_2d(yTtr,LMmax,ideb_Tt,ifin_Tt)
  call exch1_2d(yBpr,LMmax,ideb_Bp,ifin_Bp)
  call exch1_2d(yBtr,LMmax,ideb_Bt,ifin_Bt)

  if (dd_convection) then
     call exch1_2d(yCr,LMmax,ideb_C,ifin_C)
  end if


!**        Calcul des gradients

! ------------- gradient yVtr => dyVtr ------------------
! ------------- gradient yVpr => dyVpr ------------------
! ------------- Laplace  yVpr => LyVpr ------------------


  do ir=ideb_all, ifin_all
     if((ir.ne.NG).and.(ir.ne.NR))then
        do lm = 2, LMmax 
           dyVtr(lm, ir) = Gra(ir) * yVtr(lm, ir-1) &
                + Grb(ir) * yVtr(lm, ir) &
                + Grc(ir) * yVtr(lm, ir+1)
           
           
           dyVpr(lm, ir) = Gra(ir) * yVpr(lm, ir-1) &
                + Grb(ir) * yVpr(lm, ir) &
                + Grc(ir) * yVpr(lm, ir+1)

           LyVpr(lm, ir) = La(ir) * yVpr(lm, ir-1) &
                + ( Lb(ir)  - ll(lm)*r_2(ir) ) &
                * yVpr(lm, ir) &
                + Lc(ir) * yVpr(lm, ir+1)
           
        enddo
     endif
     
!Interpolation of the derivated parameter at boundaries
     if((ir.eq.NG).or.(ir.eq.NR))then
        if(ir.eq.NG)then
           i=NG 
        else
           i=NR-4
        endif
        tmp1=r(i+1)-r(i+2)
        tmp2=r(i+1)-r(i+3)
        do lm = 2, LMmax
           !Need dyVpr(NG) and dyVpr(NR) for Adams1
           atmp=( (dyVpr(lm,i+1)-dyVpr(lm,i+2))*tmp2&
                & -(dyVpr(lm,i+1)-dyVpr(lm,i+3))*tmp1 )&
                &/((r(i+1)**2-r(i+2)**2)*tmp2-(r(i+1)**2-r(i+3)**2)*tmp1)
           btmp=(dyVpr(lm,i+1)-dyVpr(lm,i+2)-atmp*(r(i+1)**2-r(i+2)**2))/tmp1
           ctmp=dyVpr(lm,i+1)-atmp*(r(i+1)**2)-btmp*r(i+1)
           if (ir.eq.NG) dyVpr(lm,i)=atmp*(r(i)**2)+btmp*r(i)+ctmp
           if (ir.eq.NR) dyVpr(lm,i+4)=atmp*(r(i+4)**2)+btmp*r(i+4)+ctmp
        enddo 
     endif
  enddo

     ! -----------------------------------------------------
     !      Calcul des nouveaux Adams1, Adams2, Adams3

      do ir = ideb_Vt,ifin_Vt
        do lm = 2, LMmax
           l = li(lm)
           m = mi(lm)
           Adams1_m1 = - (K11(lm) *  yVpr(lm-1,ir) * r_1(ir)) &
                - (K12(lm) * dyVpr(lm-1,ir))

           Adams1_p1 = - (K21(lm) *  yVpr(lm+1,ir) * r_1(ir)) &
                - (K22(lm) * dyVpr(lm+1,ir))
           if ((l.eq.m).or.(l.eq.1)) then
              Adams1_m1 = (0.0,0.0)
           endif
           if (l.eq.Lmax) then
              Adams1_p1 = (0.0,0.0)
           endif

           Adams1(lm,ir) = (m * Im * yVtr(lm,ir)) &
                + Adams1_m1 + Adams1_p1

           Adams1(lm,ir) = 2.*Adams1(lm,ir) * ll_1(lm) * Coriolis
        enddo
     enddo

     do ir = ideb_Vp, ifin_Vp
        do lm = 2, LMmax
           l = li(lm)
           m = mi(lm)
           Adams2_m1 = (K11(lm) *  yVtr(lm-1,ir) * r_1(ir)) &
                + (K12(lm) * dyVtr(lm-1,ir))

           Adams2_p1 = (K21(lm) *  yVtr(lm+1,ir) * r_1(ir)) &
                + (K22(lm) * dyVtr(lm+1,ir))


           if ((l.eq.m).or.(l.eq.1)) Adams2_m1 = (0.0,0.0)
           if (l.eq.Lmax)            Adams2_p1 = (0.0,0.0)

           Adams2(lm,ir) = (m * Im * LyVpr(lm,ir)) &
                + Adams2_m1 + Adams2_p1

           Adams2(lm,ir) = 2.*Adams2(lm,ir) * ll_1(lm) * Coriolis

           !           Adams2(lm,ir) = Adams2(lm,ir) - Buoyancy_T/r(NR) * yTtr(lm,ir)!
           Adams2(lm,ir) = Adams2(lm,ir) - (Buoyancy_T/r(NR) * yTtr(lm,ir))
           if (dd_convection) then
              Adams2(lm,ir) = Adams2(lm,ir) - Buoyancy_C/r(NR) * yCr(lm,ir) !COMPOSITIONAL BUOYANCY
           end if
! Mantle Control
           if (MantleControl) then
           Adams2(lm,ir) = Adams2(lm,ir) &
           - (Buoyancy_T/r(NR)) * yTTrMC(lm,ir)
           endif

           if (BottomControl) then
           Adams2(lm,ir) = Adams2(lm,ir) &
           - (Buoyancy_T/r(NR)) * yTtrIC(lm,ir)
           endif 

        enddo
     enddo


     do ir=ideb_Vp, ifin_Vp

        do lm = 1, LMmax

           Adams3(lm,ir) = ll(lm) * yVpr(lm,ir)

           if (HeatingMode.eq.5) then
! Boundary conditions for Enceladus
! Adams3      = -1/r dTs/dr * yVpr *l(l+1) (Thesis Dormy p.129)              
              Adams3(lm,ir) = (r(NG)**2)*Adams3(lm,ir)*r_3(ir)
           end if

           if (HeatingMode.eq.4) then
! IC T/ OC flux boundary condition
! Adams3      = -1/r dTs/dr * yVpr *l(l+1) (Thesis Dormy p.129)
              Adams3(lm,ir) = (r(NR)**2)*Adams3(lm,ir)*r_3(ir)
           endif

           if (HeatingMode.eq.3) then
! Thermochemical convection
! Adams3      = -1/r dTs/dr * yVpr *l(l+1) (Thesis Dormy p.129)
              Adams3(lm,ir) = -(2.*Pch-1)/(r(NR)**3-r(NG)**3)*Adams3(lm,ir) &
              -(r(NG)**3*(1.-Pch)-r(NR)**3*Pch)/(r(NR)**3-r(NG)**3)*Adams3(lm,ir) * r_3(ir)
           endif

           if (HeatingMode.eq.2) then
! Secular Cooling
! Adams3      = -1/r dTs/dr * yVpr *l(l+1) (Thesis Dormy p.129)
              Adams3(lm,ir) = (r(ir)**3-r(NG)**3)/3.*Adams3(lm,ir) * r_3(ir)
           endif

           if (HeatingMode.eq.1) then
! Chemical convection static profile
! Adams3      = -1/r dTs/dr * yVpr *l(l+1) (Thesis Dormy p.129)
              Adams3(lm,ir) = (r(NR)**3-r(ir)**3)/3.*Adams3(lm,ir) * r_3(ir)
           endif

           if (HeatingMode.eq.0) then
! Delta T static profile
              Adams3(lm,ir) = Aspect_ratio/(1-Aspect_ratio)**2 * Adams3(lm,ir) * r_3(ir)
            !  Adams3(lm,ir) = r(NG)*r(NR)*Adams3(lm,ir) * r_3(ir)
           endif
 
        enddo
     enddo

          if (dd_convection .and. .not. use_tracers) then !composition advection treated like temperature, not by tracers

        do ir=ideb_Vp, ifin_Vp

           do lm = 1, LMmax
              
              Adams6(lm,ir) = ll(lm) * yVpr(lm,ir)
              
              if (ChemicalMode.eq.4) then
! IC T/ OC flux boundary condition
! Adams3      = -1/r dTs/dr * yVpr *l(l+1) (Thesis Dormy p.129)
                 Adams6(lm,ir) = (r(NR)**2)*Adams6(lm,ir)*r_3(ir)
              endif

              if (ChemicalMode.eq.3) then
! Thermochemical convection
! Adams3      = -1/r dTs/dr * yVpr *l(l+1) (Thesis Dormy p.129)
                 Adams6(lm,ir) = -(2.*Pch-1)/(r(NR)**3-r(NG)**3)*Adams6(lm,ir) &
                      -(r(NG)**3*(1.-Pch)-r(NR)**3*Pch)/(r(NR)**3-r(NG)**3)*Adams6(lm,ir) * r_3(ir)
              endif

              if (ChemicalMode.eq.2) then
! Secular Cooling
! Adams3      = -1/r dTs/dr * yVpr *l(l+1) (Thesis Dormy p.129)
                 Adams6(lm,ir) = (r(ir)**3-r(NG)**3)/3.*Adams6(lm,ir) * r_3(ir)
              endif

              if (ChemicalMode.eq.1) then
! Chemical convection static profile
! Adams3      = -1/r dTs/dr * yVpr *l(l+1) (Thesis Dormy p.129)
                 Adams6(lm,ir) = (r(NR)**3-r(ir)**3)/3.*Adams6(lm,ir) * r_3(ir)
              endif

              if (ChemicalMode.eq.0) then
! Delta T static profile
                 Adams6(lm,ir) = Aspect_ratio/(1-Aspect_ratio)**2 * Adams6(lm,ir) * r_3(ir)
            !  Adams6(lm,ir) = r(NG)*r(NR)*Adams6(lm,ir) * r_3(ir)
              endif
 
           enddo
        enddo

     end if

!   Non-linear terms

     if (non_lin) call NL2_para

! Here seems a good point for suspendcheck

!#if SUSPENDCHECK
!     call suspendcheck
!#endif

! Adaptative timestepping
     dt_past=dt   

     if (non_lin) then
        if (Adaptative) then
           call check_new_dt
        else
           if (t.eq.1) then
      if (rank==0) then
      write (6,'("setting time step to fixed value ",d16.8)') dtfixed
      open(22,file='log.'//trim(runid),status='old',position='append')
      write (22,'("setting time step to fixed value ",d16.8)') dtfixed
      close(22)
      endif
      new_dt=.true.
      dt=dtfixed
           endif
        endif
     endif

     adaptive: if (new_dt) then 
      call define_fw_matrices_parody 
     end if adaptive

     time = time+dt

     if ((version.eq.99).and.(t.eq.1)) then
        AB2=0.
        AB1=1.
     else
! If dt is unchanged:  AB1=3/2, AB2=-1/2
        AB2=-0.5*(dt/dt_past)
        AB1=1.-AB2
     endif

! Viscous torque
if ((rank==0).and.CondIC.and.(NOSLIPIC)) then

     tmp1= 3./2. * ( (yVtr(lm10,NG+1) /r(NG+1)) &
          - (yVtr(lm10,NG)   /r(NG)) ) &
          / (r(NG+1)-r(NG)) &
          - 1./2. * ( (yVtr(lm10,NG+2) /r(NG+2)) &
          - (yVtr(lm10,NG+1) /r(NG+1)) ) &
          / (r(NG+2)-r(NG+1))

     tmp1 = r(NG) * tmp1

     cpl_V = 8 * pi * (r(NG) **3) * tmp1 / sqrt(3.)
else
   cpl_V=0.0
endif


!    if (rank==0) then
!       mtime_fin = MPI_Wtime()
!       print*,'TIME STEP DURATION FIRST PORTION:',mtime_fin-mtime_deb
!    end if
     !**       Integration temporelle


if (Navier_Stokes .and. .not. hyperdiff) then

     ! --------------------------------------------------------------------
     ! Eq. 1 : yVtr
     ! ----------


     ! ----------
     !  [B1] x {yVtr}


     do ir = ideb_Vt, ifin_Vt
        do lm = 2, LMmax
           Transit_Vt(lm,ir) =  A1a(ir) * yVtr(lm, ir-1) &
                + A1b(li(lm),ir) * yVtr(lm, ir)   &
                + A1c(ir)        * yVtr(lm, ir+1)

        enddo
     enddo


!     do ir = NG+1, NR-1
     do ir = ideb_Vt, ifin_Vt
        do lm = 2, LMmax
           Transit_Vt(lm,ir) = -1 * Transit_Vt(lm,ir) &
                + ((2.0 / dt) * yVtr(lm, ir))
        enddo
     enddo


 if ((rank==0).and.Couette) then
     Transit_Vt(lm10,NG+1)=Transit_Vt(lm10,NG+1) - yVtr(lm10,NG)*CLi
 endif

 if ((rank==0).and.CondIC.and.NOSLIPOC) then


     ! Rotation de la graine...
!!!! on utilise :
!!!!      CLi =  -0.5 * Lva(NG+1) - (1.0/r(NG+1)) * Gra(NG+1)
     !
     !        Transit(indx(1,0),NG+1) = Transit(indx(1,0),NG+1)
     !     &        + EPSILON * Ek * (-2.0 * CLi)


     Transit_Vt(lm10,NG+1)=Transit_Vt(lm10,NG+1) - yVtr(lm10,ng)*CLi

     ! Sur la graine (on calcule omega et yVtr en t+dt)

     Couple = 15.*(Cpl_v+Cpl_m-gammatau*omega)/(8.*pi*r(ng)**5)

     omega= omega+dt*(AB1*Couple+AB2*Couple_old)

     ! On avance yVtr en NG avant le reste pour garantir la
     ! continuite de la vitesse a t+dt a la graine.

     yVtr(lm10,NG)=omega*r(ng)/sqrt(3.)

     Transit_Vt(lm10,NG+1)=Transit_Vt(lm10,NG+1) - yVtr(lm10,ng)*CLi

!else
!   omega=0.0
!   yVtr(lm10,NG)=0.0
endif



     !  {Vt} = {Vt} + ( 3/2 {Adams1(t)} - 1/2 {Adams1(t-1)} )

     do ir = ideb_Vt, ifin_Vt
        do lm = 2, LMmax
           Transit_Vt(lm,ir) = Transit_Vt(lm,ir) &
                + (AB1 * Adams1(lm, ir)) &
                + (AB2 * Adams1_past(lm, ir))
        enddo
     enddo


     !  [A1]-1 x {Vt_c}


     call solver_3_para(A1a_2d,diag1,A1c_2d,Transit_Vt,yVtr,m1_l,m1_u,    &
  & 	2,LMmax,ideb_Vt,ifin_Vt)


! Remove solid-body rotation if stress-free
     if ((.not.NOSLIPIC).and.(.not.NOSLIPOC).and.(.not.CondIC)) then
     call angular_para
     tor10=Angmom/Integral
     do ir=ideb_all,ifin_all
        yVtr(lm10,ir)=yVtr(lm10,ir)-r(ir)*tor10
     enddo
     endif



     ! --------------------------------------------------------------------
     ! Eq. 2 : yVpr
     ! ----------

     !  [B2] x {yVpr}

      do ir= ideb_Vp,ifin_Vp
        do lm = 2, LMmax
           Transit_Vp(lm,ir) = B2a(lm,ir) * yVpr(lm,ir-2) &
                + B2b(lm,ir) * yVpr(lm,ir-1)           &
                + B2c(lm,ir) * yVpr(lm,ir)             &
                + B2d(lm,ir) * yVpr(lm,ir+1)           &
                + B2e(lm,ir) * yVpr(lm,ir+2)
        enddo
     enddo

      do ir= ideb_Vp,ifin_Vp
        do lm = 2, LMmax
           yVpr(lm,ir) = Transit_Vp(lm,ir)
        enddo
     enddo

 
     !  {Vp} = {Vp} + ( 3/2 {Adams2(t)} - 1/2 {Adams2(t-1)} )

      do ir= ideb_Vp,ifin_Vp
        do lm = 2, LMmax
           yVpr(lm,ir) = yVpr(lm,ir) &
                + (AB1 * Adams2(lm,ir)) &
                + (AB2 * Adams2_past(lm,ir))
        enddo
     enddo


     call solver_5_para(A2U_big,m51_l,m52_l,m51_u,m52_u,yVpr,LMmax,ideb_Vp,ifin_Vp)

endif ! (Navier_Stokes)


if (Navier_Stokes .and. hyperdiff) then

     ! --------------------------------------------------------------------
     ! Eq. 1 : yVtr
     ! ----------


     ! ----------
     !  [B1] x {yVtr}


     do ir = ideb_Vt, ifin_Vt
        do lm = 2, LMmax
           Transit_Vt(lm,ir) =  A1a_hyp(li(lm),ir) * yVtr(lm, ir-1) &
                + A1b(li(lm),ir) * yVtr(lm, ir)   &
                + A1c_hyp(li(lm),ir)        * yVtr(lm, ir+1)

        enddo
     enddo


!     do ir = NG+1, NR-1
     do ir = ideb_Vt, ifin_Vt
        do lm = 2, LMmax
           Transit_Vt(lm,ir) = -1 * Transit_Vt(lm,ir) &
                + ((2.0 / dt) * yVtr(lm, ir))
        enddo
     enddo


 if ((rank==0).and.Couette) then
     Transit_Vt(lm10,NG+1)=Transit_Vt(lm10,NG+1) - yVtr(lm10,NG)*CLi
 endif

 if ((rank==0).and.CondIC.and.NOSLIPOC) then


     ! Rotation de la graine...
!!!! on utilise :
!!!!      CLi =  -0.5 * Lva(NG+1) - (1.0/r(NG+1)) * Gra(NG+1)
     !
     !        Transit(indx(1,0),NG+1) = Transit(indx(1,0),NG+1)
     !     &        + EPSILON * Ek * (-2.0 * CLi)


     Transit_Vt(lm10,NG+1)=Transit_Vt(lm10,NG+1) - yVtr(lm10,ng)*CLi

     ! Sur la graine (on calcule omega et yVtr en t+dt)

     Couple = 15.*(Cpl_v+Cpl_m-gammatau*omega)/(8.*pi*r(ng)**5)

     omega= omega+dt*(AB1*Couple+AB2*Couple_old)

     ! On avance yVtr en NG avant le reste pour garantir la
     ! continuite de la vitesse a t+dt a la graine.

     yVtr(lm10,NG)=omega*r(ng)/sqrt(3.)

     Transit_Vt(lm10,NG+1)=Transit_Vt(lm10,NG+1) - yVtr(lm10,ng)*CLi

!else
!   omega=0.0
!   yVtr(lm10,NG)=0.0
endif



     !  {Vt} = {Vt} + ( 3/2 {Adams1(t)} - 1/2 {Adams1(t-1)} )

     do ir = ideb_Vt, ifin_Vt
        do lm = 2, LMmax
           Transit_Vt(lm,ir) = Transit_Vt(lm,ir) &
                + (AB1 * Adams1(lm, ir)) &
                + (AB2 * Adams1_past(lm, ir))
        enddo
     enddo


     !  [A1]-1 x {Vt_c}


     call solver_3_para(A1a_2d,diag1,A1c_2d,Transit_Vt,yVtr,m1_l,m1_u,    &
  & 	2,LMmax,ideb_Vt,ifin_Vt)


! Remove solid-body rotation if stress-free
     if ((.not.NOSLIPIC).and.(.not.NOSLIPOC).and.(.not.CondIC)) then
     call angular_para
     tor10=Angmom/Integral
     do ir=ideb_all,ifin_all
        yVtr(lm10,ir)=yVtr(lm10,ir)-r(ir)*tor10
     enddo
     endif



     ! --------------------------------------------------------------------
     ! Eq. 2 : yVpr
     ! ----------

     !  [B2] x {yVpr}

      do ir= ideb_Vp,ifin_Vp
        do lm = 2, LMmax
           Transit_Vp(lm,ir) = B2a(lm,ir) * yVpr(lm,ir-2) &
                + B2b(lm,ir) * yVpr(lm,ir-1)           &
                + B2c(lm,ir) * yVpr(lm,ir)             &
                + B2d(lm,ir) * yVpr(lm,ir+1)           &
                + B2e(lm,ir) * yVpr(lm,ir+2)
        enddo
     enddo

      do ir= ideb_Vp,ifin_Vp
        do lm = 2, LMmax
           yVpr(lm,ir) = Transit_Vp(lm,ir)
        enddo
     enddo

 
     !  {Vp} = {Vp} + ( 3/2 {Adams2(t)} - 1/2 {Adams2(t-1)} )

      do ir= ideb_Vp,ifin_Vp
        do lm = 2, LMmax
           yVpr(lm,ir) = yVpr(lm,ir) &
                + (AB1 * Adams2(lm,ir)) &
                + (AB2 * Adams2_past(lm,ir))
        enddo
     enddo


     call solver_5_para(A2U_big,m51_l,m52_l,m51_u,m52_u,yVpr,LMmax,ideb_Vp,ifin_Vp)

endif ! (Navier_Stokes)


!-------------------------------------DIFFUSION TRACERS-----------------------------------
!Save the field in spatial space
!call Spec_spat_T ()
if (temperature_tracers) then
   temp_before_diffusion = temp - temp_stat !variable part of the temperature
   call Spat_spec_T (temp_before_diffusion)
   call exch1_2d(yTtr,LMmax,ideb_Tt,ifin_Tt) !exchange ghost planes
end if
!------------------------------------------------------------------------------------------


if (Heat_equation .and. .not. hyperdiff) then

     ! --------------------------------------------------------------------
     ! Eq. 3 : yTtr
     ! ----------

     !  [B3] x {Tt}


   if (rank==0) then
      if ((HeatingMode.eq.2).or.(HeatingMode.eq.3).or.(HeatingMode.eq.5)) then
         ! Performing inversion at NG
         do lm = 1, LMmax
            l = li(lm)
            Transit_Tt(lm,NG) = A3c(NG)   * yTtr(lm,NG+1) &
                 + A3b(l,NG) * yTtr(lm,NG)
         enddo
      endif
   endif

   do ir = ideb_Vp, ifin_Vp
      do lm = 1, LMmax
         l = li(lm)
         Transit_Tt(lm,ir) = A3a(ir)   * yTtr(lm,ir-1) &
              + A3b(l,ir) * yTtr(lm,ir) &
              + A3c(ir)   * yTtr(lm,ir+1)
      enddo
   enddo


   if (rank==size-1) then
      if ((HeatingMode.eq.1).or.(HeatingMode.eq.2).or.(HeatingMode.eq.3) &
           .or.(HeatingMode.eq.4)) then
         ! Performing inversion at NR
         do lm = 1, LMmax
            l = li(lm)
            Transit_Tt(lm,NR) = A3a(NR)   * yTtr(lm,NR-1) &
                 + A3b(l,NR) * yTtr(lm,NR)
         enddo
      endif
   endif

   if (rank==0) then
      if ((HeatingMode.eq.2).or.(HeatingMode.eq.3).or.(HeatingMode.eq.5)) then
         ! Performing inversion at NG
         ir = NG
         do lm = 1, LMmax
            Transit_Tt(lm,ir) = -1 * ( Transit_Tt(lm,ir)) &
                 + ((2.0/dt) * yTtr(lm,ir))
         enddo
      endif
   endif


   do ir = ideb_Vp, ifin_Vp
      do lm = 1, LMmax
         Transit_Tt(lm,ir) = -1 * ( Transit_Tt(lm,ir)) &
                + ((2.0/dt) * yTtr(lm,ir))
      enddo
   enddo

   if (rank==size-1) then
      if ((HeatingMode.eq.1).or.(HeatingMode.eq.2).or.(HeatingMode.eq.3) &
         .or. (HeatingMode.eq.4)) then
         ! Performing inversion at NR
         ir = NR
         do lm = 1, LMmax
            Transit_Tt(lm,ir) = -1 * ( Transit_Tt(lm,ir)) &
                 + ((2.0/dt) * yTtr(lm,ir))
         enddo
      endif
   endif

     !  {Tt} = {Tt} + ( 3/2 {Adams3(t)} - 1/2 {Adams3(t-1)} )

     
! No temperature advection in NG or NR
! This is ok in rigid boundary conditions
! but clearly a bug in stress free velocity conditions

   if (.not. temperature_tracers) then
      do ir = ideb_Vp, ifin_Vp
        do lm = 1, LMmax
           Transit_Tt(lm,ir) = Transit_Tt(lm,ir) &
                + (AB1 * Adams3(lm, ir)) &
                + (AB2 * Adams3_past(lm, ir))
        enddo
     enddo
  end if


     !  [A3]-1 x {Tt}

     call solver_3_para(A3a_2d,diag3,A3c_2d,Transit_Tt,yTtr,m3_l,m3_u,    &
  & 	1,LMmax,ideb_Tt,ifin_Tt)

  if ((Aspect_ratio.eq.0.).and.(rank.eq.0)) yTtr(1,NG)=yTtr(1,NG+1)

endif !(Heat_equation)
!alex
if (Heat_equation .and. hyperdiff) then

     ! --------------------------------------------------------------------
     ! Eq. 3 : yTtr
     ! ----------

     !  [B3] x {Tt}


   if (rank==0) then
      if ((HeatingMode.eq.2).or.(HeatingMode.eq.3).or.(HeatingMode.eq.5)) then
         ! Performing inversion at NG
         do lm = 1, LMmax
            l = li(lm)
            Transit_Tt(lm,NG) = A3c_hyp(l,NG)   * yTtr(lm,NG+1) &
                 + A3b(l,NG) * yTtr(lm,NG)
         enddo
      endif
   endif

   do ir = ideb_Vp, ifin_Vp
      do lm = 1, LMmax
         l = li(lm)
         Transit_Tt(lm,ir) = A3a_hyp(l,ir)   * yTtr(lm,ir-1) &
              + A3b(l,ir) * yTtr(lm,ir) &
              + A3c_hyp(l,ir)   * yTtr(lm,ir+1)
      enddo
   enddo


   if (rank==size-1) then
      if ((HeatingMode.eq.1).or.(HeatingMode.eq.2).or.(HeatingMode.eq.3) &
           .or.(HeatingMode.eq.4)) then
         ! Performing inversion at NR
         do lm = 1, LMmax
            l = li(lm)
            Transit_Tt(lm,NR) = A3a_hyp(l,NR)   * yTtr(lm,NR-1) &
                 + A3b(l,NR) * yTtr(lm,NR)
         enddo
      endif
   endif

   if (rank==0) then
      if ((HeatingMode.eq.2).or.(HeatingMode.eq.3).or.(HeatingMode.eq.5)) then
         ! Performing inversion at NG
         ir = NG
         do lm = 1, LMmax
            Transit_Tt(lm,ir) = -1 * ( Transit_Tt(lm,ir)) &
                 + ((2.0/dt) * yTtr(lm,ir))
         enddo
      endif
   endif


   do ir = ideb_Vp, ifin_Vp
      do lm = 1, LMmax
         Transit_Tt(lm,ir) = -1 * ( Transit_Tt(lm,ir)) &
                + ((2.0/dt) * yTtr(lm,ir))
      enddo
   enddo

   if (rank==size-1) then
      if ((HeatingMode.eq.1).or.(HeatingMode.eq.2).or.(HeatingMode.eq.3) &
         .or. (HeatingMode.eq.4)) then
         ! Performing inversion at NR
         ir = NR
         do lm = 1, LMmax
            Transit_Tt(lm,ir) = -1 * ( Transit_Tt(lm,ir)) &
                 + ((2.0/dt) * yTtr(lm,ir))
         enddo
      endif
   endif

     !  {Tt} = {Tt} + ( 3/2 {Adams3(t)} - 1/2 {Adams3(t-1)} )

     
! No temperature advection in NG or NR
! This is ok in rigid boundary conditions
! but clearly a bug in stress free velocity conditions

   if (.not. temperature_tracers) then
      do ir = ideb_Vp, ifin_Vp
        do lm = 1, LMmax
           Transit_Tt(lm,ir) = Transit_Tt(lm,ir) &
                + (AB1 * Adams3(lm, ir)) &
                + (AB2 * Adams3_past(lm, ir))
        enddo
     enddo
  end if

     !  [A3]-1 x {Tt}

     call solver_3_para(A3a_2d,diag3,A3c_2d,Transit_Tt,yTtr,m3_l,m3_u,    &
  & 	1,LMmax,ideb_Tt,ifin_Tt)

  if ((Aspect_ratio.eq.0.).and.(rank.eq.0)) yTtr(1,NG)=yTtr(1,NG+1)

endif !(Heat_equation)

!    if (rank==0) then
!       mtime = MPI_Wtime()
!       print*,'TIME STEP DURATION PORTION 2:',mtime-mtime_fin
!    end if

!-----------------------DIFFUSION TRACERS-----------------------------------------

if (temperature_tracers) then
   !Save exact field after diffusion
   call Spec_spat_T (temp_after_diffusion)
   call exchange_ghost_planes_dT (temp_after_diffusion)
   
   !Compute deltaT_diffusif
   delta_temp = temp_after_diffusion - temp_before_diffusion

   ! Inforce boundary conditions
   if (HeatingMode.eq.0 .or. HeatingMode.eq.1 .or. HeatingMode.eq.4) then ! Tt fixed on the ICB
      if (rank == 0) delta_temp(:,:,NG) = 0.
   end if
   if (HeatingMode.eq.0) then
      if (rank == size-1) delta_temp(:,:,NR) = 0.
   end if

   !Add this to the tracers
   call adjust_tracers_T (delta_temp)
   
   !Smooth tracers
   temp = temp_stat+temp_after_diffusion !exact field after diffusion 
   
   ! Reinforce boundary conditions
   if (HeatingMode.eq.0 .or. HeatingMode.eq.1 .or. HeatingMode.eq.4) then ! Tt fixed on the ICB
      if (rank == 0) temp(:,:,NG) = 1.
   end if
   if (HeatingMode.eq.0) then
      if (rank == size-1) temp(:,:,NR) = 0.
   end if
   
   call smooth_tracer_T ()

end if


!    if (rank==0) then
!       mtime_fin = MPI_Wtime()
!       print*,'TIME STEP DURATION PORTION 3:',mtime_fin-mtime
!    end if

if (Induction .and. .not. hyperdiff) then
     ! --------------------------------------------------------------------
     ! Eq. 4 : yBpr
     ! ----------

     !    produit [B4] x yBpr

     
     do ir=ideb_Bp, ifin_Bp
        do lm = 2, LMmax
           l = li(lm)
           Transit_Bp(lm,ir) = A4a(ir)   * yBpr(lm,ir-1) &
                                   + A4b(l,ir) * yBpr(lm,ir) &
                                   + A4c(ir)   * yBpr(lm,ir+1)
           Transit_Bp(lm,ir) = -1. * ( Transit_Bp(lm,ir)) &
                                   + ((2.0/dt) * yBpr(lm,ir))
        enddo
     enddo

     do ir=ideb_Bp, ifin_Bp
        do lm = 2, LMmax
           Transit_Bp(lm,ir) = Transit_Bp(lm,ir) &
                + (AB1 * Adams4(lm, ir)) &
                + (AB2 * Adams4_past(lm, ir))
        enddo
     enddo


     call solver_3_para(A4a_2d,diag4,A4c_2d,Transit_Bp,yBpr,m4_l,m4_u,    &
  & 	2,LMmax,ideb_Bp,ifin_Bp)


     ! --------------------------------------------------------------------
     ! Eq. 5 : yBtr
     ! ----------


     !    produit B5 x yBtr

      do ir=ideb_Bt,ifin_Bt
        do lm = 2, LMmax
           l = li(lm)
           Transit_Bt(lm,ir) = A5a(ir)   * yBtr(lm,ir-1) &
                                   + A5b(l,ir) * yBtr(lm,ir) &
                                   + A5c(ir)   * yBtr(lm,ir+1)
           Transit_Bt(lm,ir) = -1 * ( Transit_Bt(lm,ir)) &
                                   + ((2.0/dt) * yBtr(lm,ir))
        enddo
     enddo

      do ir=ideb_Bt,ifin_Bt
        do lm = 2, LMmax
           Transit_Bt(lm,ir) = Transit_Bt(lm,ir) &
                                   + (AB1 * Adams5(lm, ir)) &
                                   + (AB2 * Adams5_past(lm, ir))
        enddo
     enddo

     call solver_3_para(A5a_2d,diag5,A5c_2d,Transit_Bt,yBtr,m5_l,m5_u,    &
  & 	2,LMmax,ideb_Bt,ifin_Bt)

endif !(Induction) 

if (Induction .and. hyperdiff) then
     ! --------------------------------------------------------------------
     ! Eq. 4 : yBpr
     ! ----------

     !    produit [B4] x yBpr

     
     do ir=ideb_Bp, ifin_Bp
        do lm = 2, LMmax
           l = li(lm)
           Transit_Bp(lm,ir) = A4a_hyp(l,ir)   * yBpr(lm,ir-1) &
                                   + A4b(l,ir) * yBpr(lm,ir) &
                                   + A4c_hyp(l,ir)   * yBpr(lm,ir+1)
           Transit_Bp(lm,ir) = -1. * ( Transit_Bp(lm,ir)) &
                                   + ((2.0/dt) * yBpr(lm,ir))
        enddo
     enddo

     do ir=ideb_Bp, ifin_Bp
        do lm = 2, LMmax
           Transit_Bp(lm,ir) = Transit_Bp(lm,ir) &
                + (AB1 * Adams4(lm, ir)) &
                + (AB2 * Adams4_past(lm, ir))
        enddo
     enddo


     call solver_3_para(A4a_2d,diag4,A4c_2d,Transit_Bp,yBpr,m4_l,m4_u,    &
  & 	2,LMmax,ideb_Bp,ifin_Bp)


     ! --------------------------------------------------------------------
     ! Eq. 5 : yBtr
     ! ----------


     !    produit B5 x yBtr

      do ir=ideb_Bt,ifin_Bt
        do lm = 2, LMmax
           l = li(lm)
           Transit_Bt(lm,ir) = A5a_hyp(l,ir)   * yBtr(lm,ir-1) &
                                   + A5b(l,ir) * yBtr(lm,ir) &
                                   + A5c_hyp(l,ir)   * yBtr(lm,ir+1)
           Transit_Bt(lm,ir) = -1 * ( Transit_Bt(lm,ir)) &
                                   + ((2.0/dt) * yBtr(lm,ir))
        enddo
     enddo

      do ir=ideb_Bt,ifin_Bt
        do lm = 2, LMmax
           Transit_Bt(lm,ir) = Transit_Bt(lm,ir) &
                                   + (AB1 * Adams5(lm, ir)) &
                                   + (AB2 * Adams5_past(lm, ir))
        enddo
     enddo

     call solver_3_para(A5a_2d,diag5,A5c_2d,Transit_Bt,yBtr,m5_l,m5_u,    &
  & 	2,LMmax,ideb_Bt,ifin_Bt)

endif !(Induction) 
!alex

!----------------------------DIFFUSION COMPOSITION TRACERS---------------------------
if (composition_tracers .and. diffusion_composition) then
   do ir = ideb_Ctra-2,ifin_Ctra+2
      comp_before_diffusion(:,:,ir) = comp(:,:,ir) - C_cond(ir) !get variable part
   end do

   call Spat_Spec_C(comp_before_diffusion)
   call exch1_2d(yCr,LMmax,ideb_C,ifin_C) 

end if
!------------------------------------------------------------------------------------

if (diffusion_composition) then

     ! --------------------------------------------------------------------
     ! Eq. 6 : yCtr
     ! ----------

     !  [B6] x {C}


   if (rank==0) then
      if ((ChemicalMode.eq.2).or.(ChemicalMode.eq.3)) then
         ! Performing inversion at NG
         do lm = 1, LMmax
            l = li(lm)
            Transit_C(lm,NG) = A6c(NG)   * yCr(lm,NG+1) &
                 + A6b(l,NG) * yCr(lm,NG)
         enddo
      endif
   endif

   do ir = ideb_Vp, ifin_Vp
      do lm = 1, LMmax
         l = li(lm)
         Transit_C(lm,ir) = A6a(ir)   * yCr(lm,ir-1) &
              + A6b(l,ir) * yCr(lm,ir) &
              + A6c(ir)   * yCr(lm,ir+1)
      enddo
   enddo


   if (rank==size-1) then
      if ((ChemicalMode.eq.1).or.(ChemicalMode.eq.2).or.(ChemicalMode.eq.3) &
           .or.(ChemicalMode.eq.4)) then
         ! Performing inversion at NR
         do lm = 1, LMmax
            l = li(lm)
            Transit_C(lm,NR) = A6a(NR)   * yCr(lm,NR-1) &
                 + A6b(l,NR) * yCr(lm,NR)
         enddo
      endif
   endif

   if (rank==0) then
      if ((ChemicalMode.eq.2).or.(ChemicalMode.eq.3)) then
         ! Performing inversion at NG
         ir = NG
         do lm = 1, LMmax
            Transit_C(lm,ir) = -1 * ( Transit_C(lm,ir)) &
                 + ((2.0/dt) * yCr(lm,ir))
         enddo
      endif
   endif


   do ir = ideb_Vp, ifin_Vp
      do lm = 1, LMmax
         Transit_C(lm,ir) = -1 * ( Transit_C(lm,ir)) &
                + ((2.0/dt) * yCr(lm,ir))
      enddo
   enddo

   if (rank==size-1) then
      if ((ChemicalMode.eq.1).or.(ChemicalMode.eq.2).or.(ChemicalMode.eq.3) &
         .or. (ChemicalMode.eq.4)) then
         ! Performing inversion at NR
         ir = NR
         do lm = 1, LMmax
            Transit_C(lm,ir) = -1 * ( Transit_C(lm,ir)) &
                 + ((2.0/dt) * yCr(lm,ir))
         enddo
      endif
   endif

     !  {Tt} = {Tt} + ( 3/2 {Adams3(t)} - 1/2 {Adams3(t-1)} )

     
! No temperature advection in NG or NR
! This is ok in rigid boundary conditions
! but clearly a bug in stress free velocity conditions

 if (.not. composition_tracers) then
      do ir = ideb_Vp, ifin_Vp
        do lm = 1, LMmax
           Transit_C(lm,ir) = Transit_C(lm,ir) &
                + (AB1 * Adams6(lm, ir)) &
                + (AB2 * Adams6_past(lm, ir))
        enddo
     enddo
  end if


     !  [A3]-1 x {Tt}

     call solver_3_para(A6a_2d,diag6,A6c_2d,Transit_C,yCr,m6_l,m6_u,    &
  & 	1,LMmax,ideb_C,ifin_C)

 ! if ((Aspect_ratio.eq.0.).and.(rank.eq.0)) yTtr(1,NG)=yTtr(1,NG+1)

  else

     if (composition_tracers) call diffusion_bottom_bcs ()
     
endif !(diffusion_composition)


if (composition_tracers .and. diffusion_composition) then

   if (rank==0) then
      print*,'before tracers'
      call calc_Nu_C ()
   end if
   
   !Save field after diffusion
   call Spec_spat_C (comp_after_diffusion)
   call exchange_ghost_planes_dC (comp_after_diffusion)

!if(mod(t,modulo2) ==0) then
!   if     (time.le.9.99999) then
!      write(sortie,'("Comp_after_diffusion",f7.5,".dat")') time
!   elseif (time.le.99.9999) then
!      write(sortie,'("Comp_after_diffusion",f7.4,".dat")') time
!   elseif (time.le.999.999) then
!      write(sortie,'("Comp_after_diffusion",f7.3,".dat")') time
!   elseif (time.le.9999.99) then
!      write(sortie,'("Comp_after_diffusion",f7.2,".dat")') time
!   else
!      write(sortie,'("Comp_after_diffusion",f7.1,".dat")') time
!   endif
   
!   call write_files_real(comp_after_diffusion(:,:,ideb_all:ifin_all),sortie,ideb_all,ifin_all)
!end if
   
   !Compute deltaT_diffusif
   delta_comp = comp_after_diffusion - comp_before_diffusion

   ! Inforce boundary conditions
   if (rank == 0) delta_comp(:,:,NG) = 0.
   if (rank == size-1) delta_comp(:,:,NR) = 0.
   
   !Add this to the tracers
   call adjust_tracers_C (delta_comp)
   
   !Diffuse tracers
   do ir = ideb_Ctra-2,ifin_Ctra+2
      comp(:,:,ir) = C_cond(ir) + comp_after_diffusion(:,:,ir) !exact field after diffusion 
   end do

   ! Reinforce boundary conditions
   if (rank == 0) comp(:,:,NG) = 1.
   if (rank == size-1) comp(:,:,NR) = 0.
   
   call smooth_tracer_C ()

   if (rank==0) then
      call calc_Nu_C ()
   end if
   
end if

if (dd_convection .and. .not. use_tracers .and. mod(t,100) ==0) then

  do ir=ideb_all,ifin_all
     yCr2(:) = yCr(:,ir)
!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
     do iblock=1,nblock
        call Spec_Spat_Scal_shell(yCr2,data,latstart(iblock),latstop(iblock))
     enddo
!$OMP END PARALLEL DO
     call fourtf(data,Nphi,1,Nlat)
     comp_f(:,:,ir) = data(1:Nlong,:)
  end do    

      if     (time.le.9.99999) then
      write(sortie,'("Comp",f7.5,".dat")') time
   elseif (time.le.99.9999) then
      write(sortie,'("Comp",f7.4,".dat")') time
   elseif (time.le.999.999) then
      write(sortie,'("Comp",f7.3,".dat")') time
   elseif (time.le.9999.99) then
      write(sortie,'("Comp",f7.2,".dat")') time
   else
      write(sortie,'("Comp",f7.1,".dat")') time
   endif  
      
   call write_files_real(comp_f(:,:,ideb_all:ifin_all),sortie,ideb_all,ifin_all)    

endif
!UNE FOIS QUE CA MARCHE, FAIRE QUELQUE CHOSE DE PROPRE POUR LES ROUTINES: PASSER LE BON TABLEAU EN ARGUMENT PLUTOT
!QUE DE FAIRE DES RECOPIAGES PREALABLES INUTILES

!---------------------------------------------------------------------------------
if (composition_tracers .and. .not. diffusion_composition) then
   if (rank == 0) call impose_C_bottom_flux_on_tracers ()
!   if (rank == size-1) call impose_C_top_flux_on_tracers ()
!end if
      
      ! Volumic sources to compensate injected flux
   call cvol_sources ()
!   end if
end if


!    if (rank==0) then
!       mtime = MPI_Wtime()
!       print*,'TIME STEP DURATION PORTION 4:',mtime-mtime_fin
!    end if
!---------------------------------TRACERS ADVECTION--------------------------------------
if (use_tracers .and. .not. tracers_noadvection) then
   !first exchange ghost planes
   call exchange_ghost_planes_V (vel_spat,1,Nlat)
   
   if(.not.tracers_interp2ord) then
      !then convert velocity from spherical to cartesian coordinates around poles
      call change_spherical_cartesian (ideb_veltra-2,ifin_veltra+2,1,ilat_N,vel_spat,vel_cart1,Psc_N)
      call change_spherical_cartesian (ideb_veltra-2,ifin_veltra+2,ilat_S,Nlat,vel_spat,vel_cart2,Psc_S)
   else
      call change_spherical_cartesian_poles ()
   end if

!   if (rank==0) then
!       mtime_fin = MPI_Wtime()
!       print*,'TIME STEP DURATION PORTION 5-1:',mtime_fin-mtime
!    end if
   
!   if (rank == 0) print*,'advecting tracers, time step n:',t
   !advect tracers, check boundaries, exchange of tracers
   call advect_tracers (ideb_veltra-2,ifin_veltra+2)
   
   !updates the weights for the trilinear interpolations
   call calc_weights_interpolation () 
   
   !update the field on the grid and in spectral space
   if (temperature_tracers) then
      if (grid_fusion) then
         call tracers_to_grid_special (temp,ideb_Ttra,ifin_Ttra,TEMPPOS)
      else
         call tracers_to_T ()
      end if
      call exchange_ghost_planes_dT (temp)

 !     if (rank==0) then
 !      mtime = MPI_Wtime()
 !      print*,'TIME STEP DURATION PORTION 5-2:',mtime-mtime_fin
 !   end if
      
      ! Reinforce boundary conditions
      if (HeatingMode.eq.0 .or. HeatingMode.eq.1 .or. HeatingMode.eq.4) then ! Tt fixed on the ICB
         if (rank == 0) temp(:,:,NG) = 1.
      end if
      if (HeatingMode.eq.0) then
         if (rank == size-1) temp(:,:,NR) = 0.
      end if
      
      delta_temp = temp-temp_stat
      call Spat_spec_T (delta_temp)
   end if

!   if (rank==0) then
!       mtime_fin = MPI_Wtime()
!       print*,'TIME STEP DURATION PORTION 5-3:',mtime_fin-mtime
!    end if
   
   if (composition_tracers) then
        if (grid_fusion) then
           call tracers_to_grid_special (comp,ideb_Ctra,ifin_Ctra,COMPPOS)
        else
           call tracers_to_Cf ()
        end if

      call exchange_ghost_planes_dC (comp)
      
      
      ! Reinforce boundary conditions
!         if (rank == 0) comp(:,:,NG) = 1.
!         if (rank == size-1) comp(:,:,NR) = 0.
      
      do ir = ideb_Ctra-2,ifin_Ctra+2
         delta_comp(:,:,ir) = comp(:,:,ir)-C_cond(ir)
         !      comp_before_diffusion(:,:,ir) = comp(:,:,ir)-C_cond(ir)
      end do
      call Spat_Spec_C (delta_comp) !comp_before_diffusion)
    
      call calc_thickness_layer ()

      if ((mod(t,Modulo)==0).and.track_tracers) call write_tracked_tracers ()
   
      if(mod(t,Modulo_comp) == 0) then
        
         call write_equatorial_tracers ()
 
         if     (time.le.9.99999) then
            write(sortie,'("Comp",f7.5,".dat")') time
         elseif (time.le.99.9999) then
            write(sortie,'("Comp",f7.4,".dat")') time
         elseif (time.le.999.999) then
            write(sortie,'("Comp",f7.3,".dat")') time
         elseif (time.le.9999.99) then
            write(sortie,'("Comp",f7.2,".dat")') time
         else
            write(sortie,'("Comp",f7.1,".dat")') time
         endif
         
         call write_files_real(comp(:,:,ideb_all:ifin_all),sortie,ideb_all,ifin_all)
         
         !   if     (time.le.9.99999) then
         !      write(sortie,'("Comp_var",f7.5,".dat")') time
         !   elseif (time.le.99.9999) then
         !      write(sortie,'("Comp_var",f7.4,".dat")') time
         !   elseif (time.le.999.999) then
         !      write(sortie,'("Comp_var",f7.3,".dat")') time
         !   elseif (time.le.9999.99) then
         !      write(sortie,'("Comp_var",f7.2,".dat")') time
         !   else
         !      write(sortie,'("Comp_var",f7.1,".dat")') time
         !   endif
         !   
         !   call write_files_real(delta_comp(:,:,ideb_all:ifin_all),sortie,ideb_all,ifin_all)
      end if
      
   end if

   if(mod(t,modulo3) == 0) then
      
      call tra_density ()

!            if (rank==0) then
!       mtime = MPI_Wtime()
!       print*,'TIME STEP DURATION PORTION 5-4-1:',mtime-mtime_fin
!    end if
   
      if     (time.le.9.99999) then
         write(sortie,'("Dens",f7.5,".dat")') time
      elseif (time.le.99.9999) then
         write(sortie,'("Dens",f7.4,".dat")') time
      elseif (time.le.999.999) then
         write(sortie,'("Dens",f7.3,".dat")') time
      elseif (time.le.9999.99) then
         write(sortie,'("Dens",f7.2,".dat")') time
      else
         write(sortie,'("Dens",f7.1,".dat")') time
      endif
      
      call write_files_real(Dens_tra(:,:,ideb_all:ifin_all),sortie,ideb_all,ifin_all)

      if     (time.le.9.99999) then
         write(sortie,'("N_per_cell",f7.5,".dat")') time
      elseif (time.le.99.9999) then
         write(sortie,'("N_per_cell",f7.4,".dat")') time
      elseif (time.le.999.999) then
         write(sortie,'("N_per_cell",f7.3,".dat")') time
      elseif (time.le.9999.99) then
         write(sortie,'("N_per_cell",f7.2,".dat")') time
      else
         write(sortie,'("N_per_cell",f7.1,".dat")') time
      endif
      
      call write_files_integer(tra_per_cell(:,:,ideb_all:ifin_all),sortie,ideb_all,ifin_all)
   end if

!      if (rank==0) then
!       mtime_fin = MPI_Wtime()
!       print*,'TIME STEP DURATION PORTION 5-4-2:',mtime_fin-mtime
!    end if
   
end if
   !-----------------------------------------------------------------------------------------

  t=t+1 
!
!  OUTPUTS
!


! ASCII output
     if (Mod(t,modulo) .eq. 0) then 
       call quick_fix_para
        call energie_para
        call angular_para
        if(rank==0) call echo_std(6)
        if(rank==0) then
           open (22, file='log.'//trim(runid), STATUS = 'old', position='append')
           call echo_std(22)
           close(22)
        endif
        call output_std
     endif

! Check for the need to resubmit at modulo times

     if (Mod(t,modulo) .eq. 0) then
        toresub=0
        call cpu_time(actualtime)
        if (Resub.and.((actualtime-time1)/(3600.*Maxtime).gt.0.99)) toresub=1
        call MPI_ALLREDUCE(toresub,lm,1,mpi_integer,MPI_SUM,COMM_model,ier)

        if (lm.gt.0) then
           
            if (rank==0) then
               open(22, file='log.'//trim(runid), STATUS = 'old', position='append')
               write(22,*) 
               write(22,*) 'Job time limit reached, resubmitting'
               write(22,*)
               close(22)
           
               write(6,*) 
               write(6,*) 'Job time limit reached, resubmitting'
               write(6,*)
            endif

            call quick_fix_para
            call ecriture
            call graphic_ave
           
            call output_std_end
            
            if(rank==0) print *
            if(rank==0) print *,'Thank you for using PARODY!'
            
            if(rank==0) then
               open (22, file='log.'//trim(runid), STATUS = 'old', position='append')
               write(22,*) 
               write(22,*) 'Thank you for using PARODY!'
               close(22)
            endif
            
            if (rank==0) call system(trim(ResubCommand))
            
            call MPI_finalize(ier)
            stop

         endif
      endif

! cpu time if Iter_fin

      if (t.eq.Iter_fin) call cpu_time(time2)

! restart file output
     if ((Mod(t,modulo2) .eq. 0).or.(t.eq.Iter_fin)) then
        call quick_fix_para
        call ecriture
        if (use_tracers) call ecriture_tracers
     endif

! graphical and average graphical output
    if  ((mod(t,modulo3).eq.0).or.(l_printgraph).or.(t.eq.Iter_fin)) then 
       call graphic
       call graphic_ave
    endif

    if  ((mod(t,modulo4).eq.0).or.(l_printgraph).or.(t.eq.Iter_fin)) then 
       call graphic_surface
    endif

    if (rank==0) then
       mtime_fin = MPI_Wtime()
 !      print*,'TIME STEP DURATION, PORTION 5-5:',mtime_fin-mtime
       print*,'TIME STEP DURATION:',mtime_fin-mtime_deb
       print*,'================================================================================='
       print*
    end if
    
  enddo TIME_INTEGRATION  !! >> END OF MAIN LOOP
  end subroutine fw_parody
  subroutine initial_condition_parody
  use globall
  use tracerstuff
  use data_parody_local
  integer :: ier
  integer :: indx
  integer :: ir
  integer :: l,m,lm
  complex :: MCcoeff,ICcoeff
  real :: amh,bmh
  integer :: nh,ih
  integer :: version
  Real  ::  x

! Init. de yBtr, yBpr, yTtr


  time = 0.0
  yBtr(:,:)=cmplx(0.,0.)
  yBpr(:,:)=cmplx(0.,0.)
  yVtr(:,:)=cmplx(0.,0.)
  yVpr(:,:)=cmplx(0.,0.)
  yTtr(:,:)=cmplx(0.,0.)
  if (dd_convection .and. .not. use_tracers) then
     yCr(:,:)=cmplx(0.,0.)
  end if
  Couple=0.0
  Couple_old=0.0
  omega_old=0.0
  omega=0.0
  Adams1(:,:)=cmplx(0.,0.)
  Adams2(:,:)=cmplx(0.,0.)    
  Adams3(:,:)=cmplx(0.,0.)
  Adams4(:,:)=cmplx(0.,0.)
  Adams5(:,:)=cmplx(0.,0.)
  if (dd_convection .and. .not. use_tracers) then
     Adams6(:,:)=cmplx(0.,0.)
  end if
  
  NOT_A_RESTART:if (Init) then

! Initial dipole field scaled by amp_b

      lm=indx(2,0)
      do ir=ideb_all,ifin_all
         yBtr(lm,ir)= amp_b * 10.*    sin(pi*(r(ir)-r(NG)))/(3.*sqrt(5.))
      enddo
      lm=indx(1,0)
      do ir=ideb_all,ifin_all
         yBpr(lm,ir)= amp_b *  (-5./16./sqrt(3.)) &
              *(6.*r(ir)**2-8.*r(NR)*r(ir)+2.*r(NG)**4/r(ir)**2)
      enddo

      if ((Aspect_ratio.eq.0.).and.(rank==0)) yBpr(:,NG)=0.

! Some field in the inner core in case of conducting inner core

      if ((CondIC).and.(rank==0)) then
        do ir=1,NG
           yBpr(lm,ir)= r(ir)/r(ng)*yBpr(lm,ng)
        enddo
      endif

! Initial temperature perturbation

      !FOR THE BENCHMARK OF BREUER ET AL.----------------------
!      yTtr(:,:) = 0.
!      l=5
!      m=4
!      lm=indx(l,m)
!      do ir = ideb_Tt,ifin_Tt
!         x= 2*r(ir)- r(ng) - r(nr)
!           yTtr(lm,ir)=0.1*(1.-3.*x**2+3.*x**4-x**6) !Breuer et al 2010
!      end do

!      if (dd_convection .and. .not. use_tracers) then
!         yCr(:,:) = 0.
!         l=5
!         m=4
!         lm=indx(l,m)
!         do ir = ideb_C,ifin_C
!            x= 2*r(ir)- r(ng) - r(nr)
!            yCr(lm,ir)=0.1*(1.-3.*x**2+3.*x**4-x**6)
!         end do
!      end if
       !---------------------------------------------------------

      Temperature: if (init_t.eq.1) then

         do lm = 1,LMmax
            do ir = ideb_Tt,ifin_Tt
               x= 2*r(ir)- r(ng) - r(nr)
               yTtr(lm, ir)=amp_t*(1.-3.*x**2+3.*x**4-x**6)/10./sqrt(2*pi)      
            enddo
         enddo
         
      else
         
         l=int(init_t/100)
         m=init_t-100*l
         
         lm=indx(l,m)
         do ir = ideb_Tt,ifin_Tt
            x= 2*r(ir)- r(ng) - r(nr)
            yTtr(lm, ir)=amp_t*(1.-3.*x**2+3.*x**4-x**6)/10./sqrt(2*pi)      
         enddo
         
      endif Temperature


! Angular velocity is fixed at the inner core
     TCouette: if (Couette) then
        if (rank==0) then
           lm=indx(1,0)
           yVtr(lm,ng) = ForcingU
        endif
     endif TCouette

!First guess for the time step

     dt=0.1/Coriolis

     adams1(:,:) = cmplx(0.,0.)
     adams2(:,:) = cmplx(0.,0.)
     adams3(:,:) = cmplx(0.,0.)
     adams4(:,:) = cmplx(0.,0.)
     adams5(:,:) = cmplx(0.,0.)
     if (dd_convection .and. .not. use_tracers) then
        adams6(:,:) = cmplx(0.,0.)
     end if

  else ! Restart CASE

     open(UNIT = 22, FILE = TRIM(entree), STATUS = 'old', &
          FORM='unformatted')

     read (22) version
     io_version=version

!     if (version==1) then
!        call lecture_v1
!        dt=0.1/Coriolis
!     endif

     if (version==2) call lecture_v2

     if (version==3) call lecture

     if (version==99) call lecture_matlab
        
  endif NOT_A_RESTART

! Fixed time step if linear convection onset is studied

  if (.not.(non_lin)) dt=0.05/Coriolis

  !** Les scalaires s'annulent au centre pour B
  if (rank==0) then
     yBpr(:,0) =0.
     yBtr(:,0) =0.
  endif

! Initialisation of Mantle-Control static T field

 yTtrMC=0.

 if (MantleControl) then

    if ((HeatingMode.ne.1).and.(HeatingMode.ne.4).and.(HeatingMode.ne.3)) then 
       if (rank==0) print *,'Use Driving Mode=1,3 or 4 with Mantle Control'
       call MPI_finalize(ier)
           stop
        endif

    if (ControlType.eq.1) then

    open(3,file='MantleControl',status='old')
    read(3,*) nh
    do ih=1,nh
       read(3,*) l,m,MCcoeff
       lm=indx(l,m)

if (HeatingMode.eq.3) then
       amh=1./(l*(r(NR)**(l-1)-r(NG)**(2*l+1)*r(NR)**(-l-2)))
       bmh=1./((l+1)*(r(NR)**(l-1)*r(NG)**(-2*l-1)-r(NR)**(-l-2)))
else
       amh= 1./((l+1)*r(NG)**(2*l+1)*r(NR)**(-l-2)+l*r(NR)**(l-1))
       bmh=-1./(l*r(NG)**(-2*l-1)*r(NR)**(l-1)+(l+1)*r(NR)**(-l-2))
endif

       do ir=ideb_Tt,ifin_Tt
          yTtrMC(lm,ir)=qstar*MCcoeff &
                       *(amh*r(ir)**l+bmh*r(ir)**(-l-1))
          yTtrMC_r(lm,ir)=yTtrMC(lm,ir)*r_1(ir)
          ydTdrMC(lm,ir)=qstar*MCcoeff &
                       *(amh*l*r(ir)**(l-1)-bmh*(l+1)*r(ir)**(-l-2))
       enddo
    enddo
    close(3)

    else

       l=int(ControlType/100)
       m=Controltype-100*l

       lm=indx(l,m)

if (HeatingMode.eq.3) then
       amh=1./(l*(r(NR)**(l-1)-r(NG)**(2*l+1)*r(NR)**(-l-2)))
       bmh=1./((l+1)*(r(NR)**(l-1)*r(NG)**(-2*l-1)-r(NR)**(-l-2)))
else
       amh= 1./((l+1)*r(NG)**(2*l+1)*r(NR)**(-l-2)+l*r(NR)**(l-1))
       bmh=-1./(l*r(NG)**(-2*l-1)*r(NR)**(l-1)+(l+1)*r(NR)**(-l-2))
endif
       
       do ir=ideb_Tt,ifin_Tt
          yTtrMC(lm,ir)=qstar &
                       *(amh*r(ir)**l+bmh*r(ir)**(-l-1))
          yTtrMC_r(lm,ir)=yTtrMC(lm,ir)*r_1(ir)
          ydTdrMC(lm,ir)=qstar &
                       *(amh*l*r(ir)**(l-1)-bmh*(l+1)*r(ir)**(-l-2))
       enddo

    endif
 endif

 if (BottomControl) then

    if (HeatingMode.ne.5) then 
       if (rank==0) print *,'Use Driving Mode=5 with Bottom Control'
       call MPI_finalize(ier)
           stop
        endif

    if (ControlTypeBottom.eq.1) then

    open(3,file='BottomControl',status='old')
    read(3,*) nh
    do ih=1,nh
       read(3,*) l,m,ICcoeff
       lm=indx(l,m)

       amh= 1./((l+1)*r(NR)**(2*l+1)*r(NG)**(-l-2)+l*r(NG)**(l-1))
       bmh=-1./(l*r(NR)**(-2*l-1)*r(NG)**(l-1)+(l+1)*r(NG)**(-l-2))   

       do ir=ideb_Tt,ifin_Tt
          yTtrIC(lm,ir)=qstar_IC*ICcoeff &
                       *(amh*r(ir)**l+bmh*r(ir)**(-l-1))
          yTtrIC_r(lm,ir)=yTtrIC(lm,ir)*r_1(ir)
          ydTdrIC(lm,ir)=qstar_IC*ICcoeff &
                       *(amh*l*r(ir)**(l-1)-bmh*(l+1)*r(ir)**(-l-2))
       enddo
    enddo
    close(3)

    else

       l=int(ControlTypeBottom/100)
       m=ControltypeBottom-100*l

       lm=indx(l,m)

       amh= 1./((l+1)*r(NR)**(2*l+1)*r(NG)**(-l-2)+l*r(NG)**(l-1))
       bmh=-1./(l*r(NR)**(-2*l-1)*r(NG)**(l-1)+(l+1)*r(NG)**(-l-2))   
       
       do ir=ideb_Tt,ifin_Tt
          yTtrIC(lm,ir)=qstar_IC &
                       *(amh*r(ir)**l+bmh*r(ir)**(-l-1))
          yTtrIC_r(lm,ir)=yTtrIC(lm,ir)*r_1(ir)
          ydTdrIC(lm,ir)=qstar_IC &
                       *(amh*l*r(ir)**(l-1)-bmh*(l+1)*r(ir)**(-l-2))
       enddo

    endif
 endif

  end subroutine initial_condition_parody
!==============================
  end module simulator_parody
!==============================
