!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                       !!
!!  Version JA-2.3 of Feb 2012                                           !!
!!                                                                       !!
!!  This contains the standard output subroutines                        !!
!!                                                                       !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Subroutine output_std_param(id)
  use globall
  use mod_parallel
  Implicit none


  integer id,ier,l,m

  ! Echo ecran.
  write (id,*) '-----------------------------------'
  write (id,*) '-----------------------------------'
  write (id,*) ' P A R O D Y'
  write (id,*) ' Version JA-2.3   (Feb  2012)'
  write (id,*)
  write (id,*)
  write (id,*) 'RUNid              = ', trim(RUNid)
  write (id,*)
  write (id,*) 'Parallel run, with size = ', size
  write (id,*)

  write (id,*) 'Time'
  write (id,*) 'Iter_fin           = ', Iter_fin
  write (id,*) 'modulo ASCII out   = ', modulo
  write (id,*) 'modulo data file   = ', modulo2
  write (id,*) 'modulo graph out   = ', modulo3
  write (id,*) 'modulo surface graph out   = ', modulo4
  write (id,*) 'Adaptative time stepping ? ', Adaptative
  if (.not.Adaptative) then
     write(id,*) 'Fixed time step = ',dtfixed
  endif

  write (id,*)
  write (id,*) 'NG                 = ', NG
  write (id,*) 'NR                 = ', NR
  write (id,*) 'Ratio1             = ', Ratio1
  write (id,*) 'Ratio2             = ', Ratio2
  write (id,*) 'Aspect_ratio       = ', Aspect_ratio
  write (id,*) 'Grille             = ', grille
  if (Grille) then
     write (id,*) '  Radius is normalized such that d=1'
  else
     write (id,*) '  Radius is normalized such that r_o=1'
  endif

  write (id,*) 'Nlong              = ', Nlong
  write (id,*) 'Nlat               = ', Nlat
  write (id,*) 'Lmin               = ', Lmin
  write (id,*) 'Lmax               = ', Lmax
  write (id,*) 'Mmin               = ', Mmin
  write (id,*) 'Mmax               = ', Mmax
  write (id,*) 'Mc                 = ', Mc
  write (id,*) 'Aliasing           = ', Aliasing
  write (id,*)

  write (id,*) 'Navier-Stokes      = ', Navier_Stokes
  if (.not.Navier_Stokes) then
     write (id,*) 'NOT SOLVING FOR THE NAVIER-STOKES EQUATIONS'
  else
     write (id,*) 'DeltaU             = ', DeltaU
     write (id,*) 'Coriolis           = ', Coriolis
     write (id,*) 'Buoyancy           = ', Buoyancy
     write (id,*) 'Lorentz            = ', Lorentz
     write (id,*) 'ForcingU           = ', ForcingU
     write (id,*) 'Ekman              = ', 1./Coriolis
  endif

  write (id,*) 'Heat equation      = ', Heat_equation
  if (.not.Heat_equation) then
     write (id,*) 'NOT SOLVING FOR THE HEAT EQUATION'
  else
     write (id,*) 'DeltaT             = ', DeltaT
     write (id,*) 'ForcingT           = ', ForcingT
     write (id,*) 'Rayleigh           = ', Buoyancy/Coriolis
     write (id,*) 'Prandtl            = ', 1./DeltaT
  endif

  write (id,*) 'Induction          = ', Induction
  if (.not.Induction) then
     write (id,*) 'NOT SOLVING FOR THE INDUCTION EQUATION'
  else
     write (id,*) 'DeltaB             = ', DeltaB     
     write (id,*) 'ForcingB           = ', ForcingB
     write (id,*) 'Magnetic Prandtl   = ', 1./DeltaB
  endif

  write (id,*) 'non_lin            = ', non_lin
  if ((non_lin).and.(Mmin.ne.0)) then
     write (id,*) 'A non-linear computation requires Mmin=0'
     write (id,*) 'You are using Mmin=',Mmin
     call MPI_finalize(ier)
     stop
  endif

  if (Couette) then
     write (id,*) 'SOLVING FOR COUETTE PROBLEM'
  endif

  write (id,*) 'No-slip O.C.       = ', NOSLIPOC
if (NOSLIPOC) then
   write (id,*) 'No Slip BC at outer boundary'
else
   write (id,*) 'Stress-Free BC at outer boundary'
endif

  write (id,*) 'No-slip I.C.       = ', NOSLIPIC

if (NOSLIPIC) then
   write (id,*) 'No Slip BC at inner boundary'
else
   write (id,*) 'Stress-Free BC at inner boundary'
endif


  write (id,*) 'Conducting I.C.    = ', CondIC
  if (CondIC) write (id,*) 'Gravitational coupling constant = ',gammatau
  write (id,*) 'Heating Mode       = ', HeatingMode

  if (HeatingMode.eq.0) write(id,*) 'Delta T Heating'
  if (HeatingMode.eq.1) write(id,*) 'Chemical convection'
  if (HeatingMode.eq.2) write(id,*) 'Secular Cooling'
  if (HeatingMode.eq.3) then
     write(id,*) 'Thermochemical driving'
     write(id,*) 'with ',Pch*100., '% Chemical convection'
  endif
  if (HeatingMode.eq.4) write(id,*) 'ICB fixed T, CMB fixed flux'
  if (HeatingMode.eq.5) write(id,*) 'CMB fixed T, ICB fixed flux (Enceladus mode)'
  IF (HeatingMode.ge.6) then
     write (id,*) 'Cas non prevu !'
     call MPI_finalize(ier)
     stop
  endif
  if (MantleControl) then
     write(id,*) 'Heteoregenous OC heat flux'
     if (ControlType.eq.1) then
        write (id,*) 'Heat flux pattern from file MantleControl'
     else
        l=int(ControlType/100)
        m=Controltype-100*l
        write (id,*) 'Single Harmonic heterogeneous OC heat flux'
        write (id,*) 'l=',l,', m= ',m
     endif
     write (id,*) 'qstar= ',qstar
  endif
  if (BottomControl) then
     write(id,*) 'Heteoregenous IC heat flux'
     if (ControlType.eq.1) then
        write (id,*) 'Heat flux pattern from file BottomControl'
     else
        l=int(ControlType/100)
        m=Controltype-100*l
        write (id,*) 'Single Harmonic heterogeneous IC heat flux'
        write (id,*) 'l=',l,', m= ',m
     endif
     write (id,*) 'qstar= ',qstar_IC
  endif

  write (id,*) 'Init               = ', Init
  if (Init) then
     write (id,*) 'Starting simulation from SCRATCH with'
     write (id,*) 'amp_b= ',amp_b
     if (init_t.eq.1) then
     write (id,*) 'All T modes are initialized with'
     write (id,*) 'amp_t= ',amp_t
     else     
        l=int(init_t/100)
        m=init_t-100*l
     write (id,*) 'Initializing only mode'
     write (id,*) 'l=',l,', m= ',m
     write (id,*) 'with amp_t= ',amp_t
     endif
  else
     write (id,*) 'RESTARTING from data file ', Entree
  endif

  write (id,*)

  if (Resub) then
     write(id,*) 'Resubmission is scheduled after ',Maxtime,' hours'
     write(id,*) 'with command ',trim(ResubCommand)
  endif

  write (id,*)

  if ((rem.ne.0).and.(rem.lt.size/2).and.(rank==0)) then
     write (id,*) '*** PARALLEL WARNING ***'
     write (id,'(A,i4,A,i5,A)') 'You are using ',size,' processors with ',   &
          &  NR-NG+1,' points in the fluid'
     write (id,'(A,i5,A)') 'You could use ',(axis2_standard_length+1)*size,  &
          &                ' points instead, at no extra cost...'
     write (id,'(A,i5,A,i4)') 'For example, you could set NR=',              &
          &  (axis2_standard_length+1)*size+NG-1,                           &
          &  ' while keeping NG=', NG
     write (id,*) ' '
  endif

End Subroutine output_std_param


Subroutine output_std_init
  use globall
  use mod_parallel
  Implicit none
  Real tmp
  Integer ir, ier,ideb,ifin


  spVl_ave=0.
  spBl_ave=0.
  spBm_ave=0.
  spVm_ave=0.

  yVpr_ave=0.
  yVtr_ave=0.
  yTtr_ave=0.
  yBtr_ave=0.
  yBpr_ave=0.

  cpl_M_shell_ave=0.

! Compute an integral which is useful for angular momentum conservation

  Integral=0.
  ideb = ideb_Vp
  ifin = ifin_Vp
  if (rank==0) ideb = ideb_Vp-1

  do ir=ideb,ifin
       Integral=Integral+(r(ir+1)-r(ir))*0.5*(r(ir)**4+r(ir+1)**4)
  enddo
 
    tmp=Integral
  call MPI_ALLREDUCE(tmp,Integral,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  
  Integral=Integral*8*pi*sqrt(3.)/3

End Subroutine output_std_init


Subroutine echo_std(id)
  use globall
  Implicit none

  Integer :: id,tmp

  tmp=Iter_fin/20
  if (tmp==0) tmp=1

  if (mod(t,tmp)==0) write(id,'(i3,'' % Finished at time step '',i10)') floor(100.0*t/Iter_fin),t
  
end Subroutine echo_std
          
Subroutine output_std
  use globall
  use mod_parallel
  Implicit none
  integer :: lm,indx,l,m,ier,ir,irank
  Real Nu_i,Nu_e,Nu_c,dipcolat

  Real tmp1,tmp2
  Integer mmV,mmB,mmT,lmV,lmB,lmT
  Real dlV,dlB

  character*30 sortie

! What is the modulo number ?

mn=real(t/modulo)

! output growth rates

        if (.not.(non_lin)) then

CroissP(:)=(yVpr(:,ifin_Vp)-yVpr_o(:)) / &
                      (yVpr(:,ifin_Vp) * (dt))
CroissT(:)=(yVpr(:,ifin_Vt)-yVtr_o(:)) / &
                      (yVtr(:,ifin_Vp) * (dt))

CroissPA(:)=(abs(yVpr(:,ifin_Vp))-abs(yVpr_o(:))) / & 
                      (abs(yVpr(:,ifin_Vp)) * (dt))
CroissTA(:)=(abs(yVtr(:,ifin_Vp))-abs(yVtr_o(:))) / &
                      (abs(yVtr(:,ifin_Vp)) * (dt))

        if (rank==size/2) then

           open(3,file='growth_sym.'//trim(RUNid),form='formatted',position='append')
            do m = Mmin, Mmax, Mc
               l = max(m,1)
               lm = Indx(l,m)
               if (m.eq.0) then
               write (3,'(d16.8)',advance="no") CroissTA(lm)
               else
               write (3,'(d16.8)',advance="no") CroissPA(lm)
               endif
            enddo
            close(3)

            
           open(3,file='growth_asym.'//trim(RUNid),form='formatted',position='append')
            do m = Mmin, Mmax, Mc
               l = max(m,1)
               lm = Indx(l+1,m)
               if (m.eq.0) then
               write (3,'(d16.8)',advance="no") CroissTA(lm)
               else
               write (3,'(d16.8)',advance="no") CroissPA(lm)
               endif
            enddo
            close(3)

           open(3,file='puls_sym.'//trim(RUNid),form='formatted',position='append')
            do m = Mmin, Mmax, Mc
               l = max(m,1)
               lm = Indx(l,m)
               if (m.eq.0) then
               write (3,'(d16.8)',advance="no") aimag(CroissT(lm))
               else
               write (3,'(d16.8)',advance="no") aimag(CroissP(lm))
               endif
            enddo
            close(3)

            
           open(3,file='puls_asym.'//trim(RUNid),form='formatted',position='append')
            do m = Mmin, Mmax, Mc
               l = max(m,1)
               lm = Indx(l+1,m)
               if (m.eq.0) then
               write (3,'(d16.8)',advance="no") aimag(CroissT(lm))
               else
               write (3,'(d16.8)',advance="no") aimag(CroissP(lm))
               endif
            enddo
            close(3)
         endif

  endif


! Output Energies

  if (rank==0) then
     if (Navier_Stokes) then
        open (3,file='e_kin.'//trim(RUNid),form='formatted',position='append')
        write(3,'(1p,d20.12,5d16.8)') time, energVp+energVt,energVp,energVt,energVpa,energVta
        close(3)
     endif

     if (Induction) then
        open (4,file='e_mag.'//trim(RUNid),form='formatted',position='append')
        write(4,'(1p,d20.12,5d16.8)') time, energBp+energBt,energBp,energBt,energBpa,energBta 
        close(4)
     endif
  endif

! Output inner core file when CondIC
  if ((rank==0).and.CondIC) then
     open(4,file='innercore.'//trim(RUNid),form='formatted',position='append')
     write(4,'(1p,d20.12,9d16.8)') time, energBip+energBit,energBip,energBit, omega, &
     Cpl_v, Cpl_M,-gammatau*omega, AngMom, 8./15.*pi*r(NG)**5*omega
     close(4)
  endif

! Output spectra (with their cumulative sum)

  if (rank==0) then
     
     open (4,file='spec_l.'//trim(RUNid),form='formatted',status='unknown')
     spVl_ave=((mn-1)*spVl_ave+spVl)/mn
     spBl_ave=((mn-1)*spBl_ave+spBl)/mn
     
     do l=0,lmax
        if ((Navier_Stokes).and.(Induction)) then
           write(4,'(1p,i3,4d16.8)') l,spVl(l),spVl_ave(l),spBl(l),spBl_ave(l)
        elseif (Navier_Stokes) then
           write(4,'(1p,i3,2d16.8)') l,spVl(l),spVl_ave(l)
        elseif (Induction)  then
           write(4,'(1p,i3,2d16.8)') l,spBl(l),spBl_ave(l)
        endif
     enddo
     close(4)

     open (4,file='spec_m.'//trim(RUNid),form='formatted',status='unknown')
     spVm_ave=((mn-1)*spVm_ave+spVm)/mn
     spBm_ave=((mn-1)*spBm_ave+spBm)/mn

     do m=0,Mmax
        if ((Navier_Stokes).and.(Induction)) then
           write(4,'(1p,i3,4d16.8)') m,spVm(m),spVm_ave(m),spBm(m),spBm_ave(m)
        elseif (Navier_Stokes) then
           write(4,'(1p,i3,2d16.8)') m,spVm(m),spVm_ave(m)
        elseif (Induction)  then 
           write(4,'(1p,i3,2d16.8)') m,spBm(m),spBm_ave(m)
        endif
     enddo
     close(4)

  endif

! Average magnetic torque in the shell

if (condIC) then

  if     (startime.le.9.99999) then
     write(sortie,'("MT=",f7.5,".")') startime
  elseif (startime.le.99.9999) then
     write(sortie,'("MT=",f7.4,".")') startime
  elseif (startime.le.999.999) then
     write(sortie,'("MT=",f7.3,".")') startime
  elseif (startime.le.9999.99) then
     write(sortie,'("MT=",f7.2,".")') startime
  else 
     write(sortie,'("MT=",f7.1,".")') startime
  endif

   cpl_M_shell_ave=((mn-1)*cpl_M_shell_ave+cpl_M_shell)/mn

if (rank==0) then
   open (4,file=trim(sortie)//trim(RUNid),form='formatted',status='unknown')

   do ir=ideb_all,ifin_all
      write(4,*) r(ir),cpl_M_shell_ave(ir)
   enddo
   close(4)
endif

  call mpi_barrier(COMM_model,ier)
#ifndef XLF
   call sleep(1)
#endif


do irank=1,size-1
   if (rank==irank) then
      open (4,file=trim(sortie)//trim(RUNid),position='append',form='formatted')
      do ir=ideb_all,ifin_all
         write(4,*) r(ir),cpl_M_shell_ave(ir)
      enddo
      close(4)
   endif

   call mpi_barrier(COMM_model,ier)
#ifndef XLF
   call sleep(1)
#endif

enddo

endif

! Output Nusselt numbers

! if (dd_convection .and. rank==0) Nu_C=1-yCr(1,NG+1)/(r(NG+1)-r(NG))*r(NG)/r(NR)

if (Heat_equation) then

  if (rank==0) then
     if (HeatingMode.eq.0) then
        Nu_i=1-yTtr(1,NG+1)/(r(NG+1)-r(NG))*r(NG)/r(NR)
     endif

     if (HeatingMode.eq.1) then
        Nu_i=r(NG)/r(NR)* (  &
             (r(NR)**3-r(NG)**3)/r(NG)**2/3. &    
             -yTtr(1,NG+1)/(r(NG+1)-r(NG))  )
     endif

     if (HeatingMode.eq.2) Nu_i=0.
     if (HeatingMode.eq.3) Nu_i=r(NG)/r(NR)*Pch/r(NG)**2
     if (HeatingMode.eq.4) then
        Nu_i=1-yTtr(1,NG+1)/(r(NG+1)-r(NG))*r(NG)**2/r(NR)**2
     endif

  endif

  if (rank==size-1) then
     if (HeatingMode.eq.0) then
        Nu_e=1+yTtr(1,NR-1)/(r(NR)-r(NR-1))*r(NR)/r(NG)
     endif

     if (HeatingMode.eq.1) Nu_e=0.

     if (HeatingMode.eq.2) then
        Nu_e=r(NR)/r(NG)* (  &
             (r(NR)**3-r(NG)**3)/r(NR)**2/3.)
     endif
     if (HeatingMode.eq.3) Nu_e=r(NR)/r(NG)*(1.-Pch)/r(NR)**2
     if (HeatingMode.eq.4) Nu_e=1.

  endif

  call mpi_bcast(Nu_e,1,mpi_double_precision,size-1,COMM_model,ier)

  if (rank==0) then
     open (4,file='Nuss.'//trim(RUNid),form='formatted',position='append')
     write(4,'(1p,d20.12,3d16.8)') time, Nu_i,Nu_e
     close(4)
  endif
endif

! CMB field and dipole information

if (Induction) then

   if (rank==size-1) then
      open (4, FILE='dipole.'//trim(RUNid),FORM='formatted',position='append')
      dipcolat=(180./pi)*atan(abs(yBpr(lm11, NR))/real(yBpr(lm10, NR)))
      if (dipcolat.le.0.) dipcolat=dipcolat+180.
      write(4,'(1p,d20.12,6d16.8)') time,dipcolat,real(yBpr(lm10,NR)), &
           abs(yBpr(lm11,NR)), sqrt(EnergBcmb), &
           sqrt(EnergBdipcmb),sqrt(EnergBcmb12)
      close(4)

      open (4, FILE='compliance.'//trim(RUNid),FORM='formatted',position='append')
      write(4,'(1p,d20.12,3d16.8)') time, AD/NAD, SYM/ASYM, ZON/NZON
      close(4)

   endif

endif

! Power budget

if (rank==0) then
      open(4, FILE='power.'//trim(RUNid),FORM='formatted',position='append')
      If (Navier_Stokes.and.Heat_equation) then
         if (Induction) then
            write(4,'(1p,d20.12,3d16.8)') time, powerbuoy, powervisc, powerohm
         else
            write(4,'(1p,d20.12,2d16.8)') time, powerbuoy, powervisc
         endif
      endif
      close(4)
   endif

!  Average fields
if (Navier_Stokes) then
yVpr_ave=((mn-1)*yVpr_ave+yVpr)/mn
yVtr_ave=((mn-1)*yVtr_ave+yVtr)/mn
endif

if (Induction) then
yBpr_ave=((mn-1)*yBpr_ave+yBpr)/mn
yBtr_ave=((mn-1)*yBtr_ave+yBtr)/mn
endif

if (Heat_Equation) then
yTtr_ave=((mn-1)*yTtr_ave+yTtr)/mn
endif

!  Length scales

lmV=0
lmB=0
lmT=0
mmV=0
mmB=0
mmT=0


if (Navier_Stokes) then
tmp1=0.
tmp2=0.
do l=0,lmax
   tmp2=tmp2+l*spVl(l)
   tmp1=tmp1+spVl(l)
enddo
lmV=nint(tmp2/tmp1)

tmp2=0.
do m=0,Mmax
tmp2=tmp2+spVm(m)
if (tmp2.lt.tmp1/2.) mmV=m
enddo
mmV=mmV+1
endif

if (Induction) then
tmp1=0.
tmp2=0.
do l=0,lmax
   tmp2=tmp2+l*spBl(l)
   tmp1=tmp1+spBl(l)
enddo
lmB=nint(tmp2/tmp1)

tmp2=0.
do m=0,Mmax
tmp2=tmp2+spBm(m)
if (tmp2.lt.tmp1/2.) mmB=m
enddo
mmB=mmB+1
endif

if (Heat_Equation) then
tmp1=0.
tmp2=0.
do l=0,lmax
   tmp2=tmp2+l*spTl(l)
   tmp1=tmp1+spTl(l)
enddo
lmT=nint(tmp2/tmp1)

tmp2=0.
do m=0,Mmax
tmp2=tmp2+spTm(m)
if (tmp2.lt.tmp1/2.) mmT=m
enddo
mmT=mmT+1
endif

dlV=sqrt(2.*(EnergVp+EnergVt)/powervisc)
dlB=sqrt(2.*(EnergBp+EnergBt)/powerohm*deltaB)

if (rank==0) then
open(4, FILE='scales.'//trim(RUNid),FORM='formatted',position='append')
write(4,'(1p,d20.12,6i4,2d16.8)') time,lmV,lmB,lmT,mmV,mmB,mmT,dlV,dlB
close(4)
endif

end Subroutine output_std

Subroutine output_std_end
  use globall
  Implicit none
End Subroutine output_std_end


Subroutine energie_para

! Compute KE, ME, spectra and temperature spectra

  Use globall
  use mod_parallel

  Implicit NONE


  Real    cost,dphy,vol,volg, tmp
  Integer ir, ilong, ilat, lm, l, m, ier,ideb,ifin

  real EVp,EVt,EBp,EBt,ETt

  Complex, DIMENSION(0:LMmax+1,ideb_Vp-2:ifin_Vp+2)    :: yVs
  Complex, DIMENSION(0:LMmax+1,ideb_Bp-2:ifin_Bp+2)    :: yBs


  call exch1_2d(yVpr,LMmax,ideb_Vp,ifin_Vp)
  call exch1_2d(yBpr,LMmax,ideb_Bp,ifin_Bp)
  call exch1_2d(yVtr,LMmax,ideb_Vt,ifin_Vt)
  call exch1_2d(yBtr,LMmax,ideb_Bt,ifin_Bt)

  if (rank==0) then
     yVs(:,NG) = 0.0
     yBs(:,0)  = 0.0
  endif

  do ir=ideb_Vp,ifin_Vp
     yVs(:,ir) = dra(ir)*yVpr(:,ir-1) &
               + drb(ir)*yVpr(:,ir)   &
               + drc(ir)*yVpr(:,ir+1)
  enddo

! astuce pour ne pas calculer NR (pas propre...)
  do ir=ideb_Bp,ifin_Vp
     yBs(:,ir) = dra(ir)*yBpr(:,ir-1) &
               + drb(ir)*yBpr(:,ir)   &
               + drc(ir)*yBpr(:,ir+1)
  enddo

  if ((rank==0).and.(InsulIC).and.(Aspect_ratio.ne.0)) then
     ir=NG
     do lm=1,LMmax
        l=li(lm)
        yBs(lm,ir) = real(l+1)*yBpr(lm,ir)/r(ir)
     enddo
  endif

  if (rank==size-1) then
     ir=NR
     yVs(:,ir) = 0.0
     do lm=1,LMmax
        l=li(lm)
        yBs(lm,ir)=-(l*yBpr(lm,ir))/r(ir)
     enddo
  endif

  call exch1_2d(yVs,LMmax,ideb_Vp,ifin_Vp)
  call exch1_2d(yBs,LMmax,ideb_Bp,ifin_Bp)


  energVP  = 0.0
  energVT  = 0.0

  energVpa = 0.0
  energVta = 0.0

  energBiP = 0.0
  energBiT = 0.0

  energBP  = 0.0
  energBT  = 0.0

  energBpa = 0.0
  energBta=  0.0

  spVm     = 0.0
  spBm     = 0.0
  spVl     = 0.0
  spBl     = 0.0
  spTm     = 0.0
  spTl     = 0.0

  volg = 0.0
  vol = 0.0

  if (rank==0.and.CondIC) then
  do ir = 1, NG-1
     do lm=2,LMmax 
     energBiP = energBiP+ ll(lm)*(r(ir+1)-r(ir))               &
        * ( (ll(lm)                                          &
        * 0.5*(abs(yBpr(lm,ir+1))**2 + abs(yBpr(lm,ir))**2)) &
        + 0.5*(r(ir+1)**2*abs(yBs(lm,ir+1))**2 + r(ir)**2*abs(yBs(lm,ir))**2))

     energBiT = energBiT+ ll(lm)*(r(ir+1)-r(ir))           &
               * 0.5*(r(ir+1)**2*abs(yBtr(lm,ir+1))**2 + &
                 r(ir)**2*abs(yBtr(lm,ir))**2)
     enddo
     volg = volg+4*pi*0.5*(r(ir+1)**2+r(ir)**2)*(r(ir+1)-r(ir))
  enddo
  endif

! NG belongs to the fluid
  ideb = ideb_Vp
  ifin = ifin_Vp
  if (rank==0) ideb = ideb_Vp-1
  do ir = ideb, ifin
     do lm=2,LMmax 

     EVp = ll(lm)*(r(ir+1)-r(ir))                               &
        * ( (ll(lm) * 0.5*(abs(yVpr(lm,ir+1))**2 + abs(yVpr(lm,ir))**2))     &
        + 0.5*(r(ir+1)**2*abs(yVs(lm,ir+1))**2 + r(ir)**2*abs(yVs(lm,ir))**2))

     EVt = ll(lm)*(r(ir+1)-r(ir))             &
               * 0.5*(r(ir+1)**2*abs(yVtr(lm,ir+1))**2 +   &
                      r(ir)**2*abs(yVtr(lm,ir))**2)

     EBp = ll(lm)*(r(ir+1)-r(ir))               &
        * ( (ll(lm)                                          &
        * 0.5*(abs(yBpr(lm,ir+1))**2 + abs(yBpr(lm,ir))**2)) &
        + 0.5*(r(ir+1)**2*abs(yBs(lm,ir+1))**2 + r(ir)**2*abs(yBs(lm,ir))**2))

     EBt = ll(lm)*(r(ir+1)-r(ir))           &
               * 0.5*(r(ir+1)**2*abs(yBtr(lm,ir+1))**2 + &
                 r(ir)**2*abs(yBtr(lm,ir))**2)

     EnergVp=EnergVp+EVp
     EnergVt=EnergVt+EVt
     EnergBp=EnergBp+EBp
     EnergBt=EnergBt+EBt

     l=li(lm)
     m=mi(lm)

     if (m.eq.0) then
     EnergVpa=EnergVpa+EVp
     EnergVta=EnergVta+EVt
     EnergBpa=EnergBpa+EBp
     EnergBta=EnergBta+EBt
     endif

     ETt=ll(lm)*(r(ir+1)-r(ir))           &
          * 0.5*(r(ir+1)**2*abs(yTtr(lm,ir+1))**2 + &
          r(ir)**2*abs(yTtr(lm,ir))**2)

! Mantle control
     if (MantleControl) then
     ETt=ETt+ll(lm)*(r(ir+1)-r(ir))           &
          * 0.5*(r(ir+1)**2*abs(yTtrMC(lm,ir+1))**2 + &
          r(ir)**2*abs(yTtrMC(lm,ir))**2)
     endif

! Bottom Control
    if (BottomControl) then
    ETt=ETt+ll(lm)*(r(ir+1)-r(ir))           &
          * 0.5*(r(ir+1)**2*abs(yTtrIC(lm,ir+1))**2 + &
          r(ir)**2*abs(yTtrIC(lm,ir))**2)
    endif

     spVm(m)=spVm(m)+EVp+EVt
     spVl(l)=spVl(l)+EVp+EVt
     spBm(m)=spBm(m)+EBp+EBt
     spBl(l)=spBl(l)+EBp+EBt
     spTm(m)=spTm(m)+ETt
     spTl(l)=spTl(l)+ETt
     
     enddo
     vol = vol+4*pi*0.5*(r(ir+1)**2+r(ir)**2)*(r(ir+1)-r(ir))
  enddo

  tmp=vol
  call MPI_ALLREDUCE(tmp,vol,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=energVT
  call MPI_ALLREDUCE(tmp,energVT,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=energVP
  call MPI_ALLREDUCE(tmp,energVP,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=energBT
  call MPI_ALLREDUCE(tmp,energBT,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=energBP
  call MPI_ALLREDUCE(tmp,energBP,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=energVPa
  call MPI_ALLREDUCE(tmp,energVPa,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=energVta
  call MPI_ALLREDUCE(tmp,energVta,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=energBpa
  call MPI_ALLREDUCE(tmp,energBpa,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=energBta
  call MPI_ALLREDUCE(tmp,energBta,1,mpi_double_precision,MPI_SUM,COMM_model,ier)

  do m=Mmin,Mmax,Mc
  tmp=spVm(m)
  call MPI_ALLREDUCE(tmp,spVm(m),1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=spBm(m)
  call MPI_ALLREDUCE(tmp,spBm(m),1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=spTm(m)
  call MPI_ALLREDUCE(tmp,spTm(m),1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  enddo

  do l=0,Lmax
  tmp=spVl(l)
  call MPI_ALLREDUCE(tmp,spVl(l),1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=spBl(l)
  call MPI_ALLREDUCE(tmp,spBl(l),1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  tmp=spTl(l)
  call MPI_ALLREDUCE(tmp,spTl(l),1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  enddo

if ((rank==0).and.(CondIC)) then  
  energBiT=energBiT*2*pi/volg*Lorentz
  energBiP=energBiP*2*pi/volg*Lorentz
endif

  energVT=energVT*2*pi/vol
  energVP=energVP*2*pi/vol
  energBT=energBT*2*pi/vol*Lorentz
  energBP=energBP*2*pi/vol*Lorentz
  energVTa=energVTa*2*pi/vol
  energVPa=energVPa*2*pi/vol
  energBTa=energBTa*2*pi/vol*Lorentz
  energBPa=energBPa*2*pi/vol*Lorentz
  spVm=spVm*2*pi/vol
  spVl=spVl*2*pi/vol
  spBm=spBm*2*pi/vol*Lorentz
  spBl=spBl*2*pi/vol*Lorentz
  spTm=spTm*2*pi/vol
  spTl=spTl*2*pi/vol

! surface integrals for rms B values at CMB

if (rank==size-1) then

  energBcmb =0.0
  energBcmb12=0.0
  energBdipcmb=0.0

  AD=0.
  NAD=0.
  SYM=0.
  ASYM=0.
  ZON=0.
  NZON=0.

  do lm=2,LMmax
     
     EBp = ll(lm) &
          * ( ll(lm) * abs(yBpr(lm,NR))**2     &
          + r(NR)**2*abs(yBs(lm,NR))**2 )
     
     EBt = ll(lm) * r(NR)**2*abs(yBtr(lm,NR))**2
     
     l=li(lm)
     m=mi(lm)
     
     if (l.eq.1) EnergBdipcmb=EnergBdipcmb+EBp+EBt

     EnergBcmb=EnergBcmb+EBp+EBt

     if (l.lt.12) EnergBcmb12=EnergBcmb12+EBp+EBt

     if ((l.eq.1).and.(m.eq.0)) AD=EBp

     if (l.lt.8) NAD=NAD+EBp

     if ((l.gt.2).and.(l.lt.8)) then
     if (mod(l+m,2).eq.1) then 
        SYM=SYM+EBp
     else
        ASYM=ASYM+EBp
     endif
     endif

     if ((l.gt.2).and.(l.lt.8)) then 
        if (m.eq.0) then
           ZON=ZON+EBp
        else 
           NZON=NZON+EBp
        endif
        endif
  enddo

  vol=4*pi*r(NR)**2
  EnergBcmb=2*EnergBcmb*2*pi/vol
  EnergBdipcmb=2*EnergBdipcmb*2*pi/vol
  EnergBcmb12=2*EnergBcmb12*2*pi/vol

endif

  Return
End Subroutine energie_para


Subroutine angular_para
Use globall
  use mod_parallel

  Implicit NONE


  Real    tmp
  Integer ir, lm, ier,ideb,ifin


  call exch1_2d(yVtr,LMmax,ideb_Vt,ifin_Vt)

  AngMom=0.

  ideb = ideb_Vp
  ifin = ifin_Vp
  if (rank==0) ideb = ideb_Vp-1

  do ir=ideb,ifin
     AngMom=AngMom+(r(ir+1)-r(ir))*0.5*(r(ir)**3*yVtr(lm10,ir)+r(ir+1)**3*yVtr(lm10,ir+1))
  enddo
 
  tmp=AngMom
  call MPI_ALLREDUCE(tmp,AngMom,1,mpi_double_precision,MPI_SUM,COMM_model,ier)
  
  AngMom=AngMom*8*pi*sqrt(3.)/3

End Subroutine angular_para


Subroutine Graphic

  !------------------------------------------------------------------------*
  !    Graphical output                                                        *
  !------------------------------------------------------------------------*

  use globall
  use mod_parallel 
  Implicit None


  !**        Variables locales

  Complex yVs(0:LMmax+1,ideb_Vp-2:ifin_Vp+2)
  Complex yBs(0:LMmax+1,ideb_Bp-2:ifin_Bp+2)
  Complex CBp(0:LMmax+1),CVp(0:LMmax+1)

! Arrays to be allocated in case of graphic output

  Real, POINTER, DIMENSION(:,:)    ::  Vr,Vt,Vp
  Real, POINTER, DIMENSION(:,:)    ::  Br,Bt,Bp
  Real, POINTER, DIMENSION(:,:)    ::  Tt

  Integer Nilat,indx,ideb,ifin,ier

  Integer ir, i, l, m, lm, ilat, ilong,isigne

  integer irank
  character*30 :: sortie
  integer :: version
  real tmp1,tmp2

! Mantle control
  Complex yTtr2(0:LMmax+1)

  Vr => Data_spec_spat(:,:,1)
  Vt => Data_spec_spat(:,:,2)
  Vp => Data_spec_spat(:,:,3)
  Br => Data_spec_spat(:,:,4)
  Bt => Data_spec_spat(:,:,5)
  Bp => Data_spec_spat(:,:,6)
  Tt => Data_spec_spat(:,:,7)

!CC Calcul des Spheroidaux


if (rank==0) then
  !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC Graine
  ir = NG

if (Aspect_ratio.eq.0.) then
   do lm=1,lmmax
      l=li(lm)
      if (l.eq.1) then
         CBp(lm)=yBpr(lm,ir+1)/r(ir+1)
         CVp(lm)=yVpr(lm,ir+1)/r(ir+1)
      else
         CBp(lm)=0.
         CVp(lm)=0.
      endif
   enddo

else

if (CondIC) then
  yBs(:,ir) = dra(ir)*yBpr(:,ir-1) &
       + drb(ir)*yBpr(:,ir) &
       + drc(ir)*yBpr(:,ir+1)
else
  do lm=1,LMmax
     l=li(lm)

     yBs(lm,ir)=real(l+1)*yBpr(lm,ir)/r(ir)

  enddo
endif

tmp1=r(ir+1)-r(ir)
tmp2=r(ir+2)-r(ir) 
if (NOSLIPIC) then
   yVtr(:,ir-1)=yVtr(:,ir+1)-2*(tmp2**2*yVtr(:,ir+1)-tmp1**2*yVtr(:,ir+2))&
        &/(tmp2**2-tmp1*tmp2)
   yVs(:,ir)=0
else
   yVtr(:,ir-1)=yVtr(:,ir+1)*(r(ir)-tmp1)/(r(ir)+tmp1)
   yVs(:,ir)=(yVpr(:,ir+1)-yVpr(:,ir-1))/(2*tmp1)
endif

endif
endif

  !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC NG+1 -> NR-1
  do ir=ideb_Vp,ifin_Vp
     yVs(:,ir) = dra(ir)*yVpr(:,ir-1) &
          + drb(ir)*yVpr(:,ir) &
          + drc(ir)*yVpr(:,ir+1)

     !     dr . = 1/r d/dr r.
     yBs(:,ir) = dra(ir)*yBpr(:,ir-1) &
          + drb(ir)*yBpr(:,ir) &
          + drc(ir)*yBpr(:,ir+1)
  enddo

  if (rank==size-1) then
     !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC NR
     ir=NR
     
     do lm=1,LMmax
        l=li(lm)
        
        yBs(lm,ir)=-(real(l)*yBpr(lm,ir))/r(ir)
        
     enddo
    
     tmp1=r(ir)-r(ir-1)
     tmp2=r(ir)-r(ir-2) 
     
     if (NOSLIPOC)then
        yVtr(:,ir+1)=yVtr(:,ir-1)-2*(tmp2**2*yVtr(:,ir-1) &
             & -tmp1**2*yVtr(:,ir-2))  &
             & /(tmp2**2-tmp1*tmp2)
        yVs(:,ir)=0.
     else
        yVtr(:,ir+1)=2*tmp1/r(ir)*yVtr(:,ir)+yVtr(:,ir-1)
        yVpr(:,ir+1)=-yVpr(:,ir-1)
        yVs(:,ir)=(yVpr(:,ir+1)-yVpr(:,ir-1))/(2*tmp1)
        
     endif

  endif


!!!!!!!! Graphic file name
  if     (time.le.9.99999) then
     write(sortie,'("Gt=",f7.5,".")') time
  elseif (time.le.99.9999) then
     write(sortie,'("Gt=",f7.4,".")') time
  elseif (time.le.999.999) then
     write(sortie,'("Gt=",f7.3,".")') time
  elseif (time.le.9999.99) then
     write(sortie,'("Gt=",f7.2,".")') time
  else
     write(sortie,'("Gt=",f7.1,".")') time
  endif

!!! Header
  version=3
 
  if (rank==0)  then
     open (300,file=trim(sortie)//trim(runid),status='unknown',form='unformatted')

! version number
     write(300) real(version,4)
     
! physical parameters
     write(300) real(time,4),real(DeltaU,4),real(Coriolis,4), &
     real(Lorentz,4),real(Buoyancy,4),real(ForcingU,4),       &
     real(DeltaT,4),real(ForcingT,4),real(DeltaB,4),real(ForcingB,4)

! grid parameters
     write(300) real(NR-NG+1,4),real(Nlat,4),real(Nlong,4),real(Mc,4)

! grid
     write(300) (real(r(ir),4),ir=NG,NR)
     write(300) (real(colat(ilat),4),ilat=1,Nlat)
     close(300)
  endif

  do irank=0,size-1
  if (rank==irank) then

  open (300,file=trim(sortie)//trim(runid),position='append',form='unformatted')
  do ir=ideb_all,ifin_all

     if ((ir.eq.NG).and.(Aspect_ratio.eq.0)) then

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
        do iblock=1,nblock
           call Spec_Spat_PolTor_NoCore &
                (ir,CVp, &
                Vr,Vt,Vp,latstart(iblock),latstop(iblock))
           call Spec_Spat_PolTor_NoCore &
                (ir,CBp, &
                Br,Bt,Bp,latstart(iblock),latstop(iblock))
        enddo
!$OMP END PARALLEL DO

     else

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
        do iblock=1,nblock
           call Spec_Spat_PolTor_shell &
                (ir,yVpr(:,ir),yVtr(:,ir),yVs(:,ir), &
                Vr,Vt,Vp,latstart(iblock),latstop(iblock))
           call Spec_Spat_PolTor_shell &
                (ir,yBpr(:,ir),yBtr(:,ir),yBs(:,ir), &
                Br,Bt,Bp,latstart(iblock),latstop(iblock))
        enddo
!$OMP END PARALLEL DO

     endif

     yTtr2=yTtr(:,ir)
     ! Mantle Control
     if (MantleControl) yTtr2=yTtr2+yTtrMC(:,ir)
     ! Bottom Control
     if (BottomControl) yTtr2=yTtr2+yTtrIC(:,ir)

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
do iblock=1,nblock
     call Spec_Spat_Scal_Shell(yTtr2,Tt,latstart(iblock),latstop(iblock))
enddo
!$OMP END PARALLEL DO

     call fourierr_spec_spat2(Data_spec_spat)

! Add static temperature profile

     if (HeatingMode.eq.0) then
        !Tt = Tt - Aspect_ratio/(1-Aspect_ratio)+Aspect_ratio/(1-Aspect_ratio)**2*r_1(ir)
        Tt=Tt+r(NG)*r(NR)/r(ir)+1-r(NR)
     endif

     if (HeatingMode.eq.1) then
        Tt=Tt+(r(ir)**2-r(NG)**2)/6.+r(NR)**3/3.*(1/r(ir)-1/r(NG))
     endif

     if (HeatingMode.eq.2) then
        Tt=Tt-r(ir)**2/6-r(NG)**3/(3.*r(ir))+r(NG)**2/2.
     endif

     if (HeatingMode.eq.3) then
        if (Aspect_ratio.eq.0) then
        Tt=Tt+3*(2.*Pch-1)/(r(NR)**3-r(NG)**3)*r(ir)**2/6 
        else
        Tt=Tt+3*(2.*Pch-1)/(r(NR)**3-r(NG)**3)*r(ir)**2/6   &
           - (r(NG)**3*(1.-Pch)-r(NR)**3*Pch)/(r(NR)**3-r(NG)**3)/r(ir)
     endif
     endif

     if (HeatingMode.eq.4) then
        Tt=Tt+(1./r(ir)-1./r(NG))*r(NR)**2
     endif

     if (HeatingMode.eq.5) then
        Tt=Tt+(1./r(ir)-1./r(NR))*r(NG)**2
     end if

! Velocity at ICB or CMB

     do ilat=1,nlat
        write(300) (real(Vr(ilong,ilat),4),ilong=1,Nlong)
        write(300) (real(Vt(ilong,ilat),4),ilong=1,Nlong)
        write(300) (real(Vp(ilong,ilat),4),ilong=1,Nlong)
        write(300) (real(Br(ilong,ilat),4),ilong=1,Nlong)
        write(300) (real(Bt(ilong,ilat),4),ilong=1,Nlong)
        write(300) (real(Bp(ilong,ilat),4),ilong=1,Nlong)
        write(300) (real(Tt(ilong,ilat),4),ilong=1,Nlong)
     enddo

  enddo
     close(300)
  endif
  call mpi_barrier(COMM_model,ier)
#ifndef XLF
  call sleep(1)
#endif
enddo

l_printgraph=.false.
if (rank==0) then 
print *,'Graphical file written at time=',time
open(22,file='log.'//trim(RUNid),status='old',position='append')
write(22,*) 'Graphical file written at time=',time
close(22)
endif

  Return
End Subroutine Graphic




Subroutine Graphic_surface

  !------------------------------------------------------------------------*
  !    Graphical output                                                        *
  !------------------------------------------------------------------------*

  use globall
  use mod_parallel
  Implicit None


  !**        Variables locales

  Complex yVs(0:LMmax+1,ideb_Vp-2:ifin_Vp+2)
  Complex yBs(0:LMmax+1,ideb_Bp-2:ifin_Bp+2)

! Arrays to be allocated in case of graphic output

  Real, POINTER, DIMENSION(:,:)    ::  Vr,Vt,Vp
  Real, POINTER, DIMENSION(:,:)    ::  Br,Bt,Bp
  Real, POINTER, DIMENSION(:,:)    ::  Tt

  Integer Nilat,indx,ideb,ifin,ier

  Integer ir, i, l, m, lm, ilat, ilong,isigne

  integer irank
  character*30 :: sortie
  integer :: version
  real tmp1,tmp2

! Mantle control
  Complex yTtr2(0:LMmax+1)

  Vr => Data_spec_spat(:,:,1)
  Vt => Data_spec_spat(:,:,2)
  Vp => Data_spec_spat(:,:,3)
  Br => Data_spec_spat(:,:,4)
  Bt => Data_spec_spat(:,:,5)
  Bp => Data_spec_spat(:,:,6)
  Tt => Data_spec_spat(:,:,7)

!CC Calcul des Spheroidaux


if (rank==0) then
  !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC Graine
  ir = NG
if (CondIC) then
  yBs(:,ir) = dra(ir)*yBpr(:,ir-1) &
       + drb(ir)*yBpr(:,ir) &
       + drc(ir)*yBpr(:,ir+1)
else
  do lm=1,LMmax
     l=li(lm)

     yBs(lm,ir)=real(l+1)*yBpr(lm,ir)/r(ir)

  enddo
endif

tmp1=r(ir+1)-r(ir)
tmp2=r(ir+2)-r(ir) 
if (NOSLIPIC) then
   yVtr(:,ir-1)=yVtr(:,ir+1)-2*(tmp2**2*yVtr(:,ir+1)-tmp1**2*yVtr(:,ir+2))&
        &/(tmp2**2-tmp1*tmp2)
   yVs(:,ir)=0
else
   yVtr(:,ir-1)=yVtr(:,ir+1)*(r(ir)-tmp1)/(r(ir)+tmp1)
   yVs(:,ir)=(yVpr(:,ir+1)-yVpr(:,ir-1))/(2*tmp1)
endif

endif

  !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC NG+1 -> NR-1
  do ir=ideb_Vp,ifin_Vp
     yVs(:,ir) = dra(ir)*yVpr(:,ir-1) &
          + drb(ir)*yVpr(:,ir) &
          + drc(ir)*yVpr(:,ir+1)

     !     dr . = 1/r d/dr r.
     yBs(:,ir) = dra(ir)*yBpr(:,ir-1) &
          + drb(ir)*yBpr(:,ir) &
          + drc(ir)*yBpr(:,ir+1)
  enddo

  if (rank==size-1) then
     !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC NR
     ir=NR
     
     do lm=1,LMmax
        l=li(lm)
        
        yBs(lm,ir)=-(real(l)*yBpr(lm,ir))/r(ir)
        
     enddo
    
     tmp1=r(ir)-r(ir-1)
     tmp2=r(ir)-r(ir-2) 
     
     if (NOSLIPOC)then
        yVtr(:,ir+1)=yVtr(:,ir-1)-2*(tmp2**2*yVtr(:,ir-1) &
             & -tmp1**2*yVtr(:,ir-2))  &
             & /(tmp2**2-tmp1*tmp2)
        yVs(:,ir)=0.
     else
        yVtr(:,ir+1)=2*tmp1/r(ir)*yVtr(:,ir)+yVtr(:,ir-1)
        yVpr(:,ir+1)=-yVpr(:,ir-1)
        yVs(:,ir)=(yVpr(:,ir+1)-yVpr(:,ir-1))/(2*tmp1)
        
     endif

  endif


!!!!!!!! Graphic file name
  if     (time.le.9.99999) then
     write(sortie,'("St=",f7.5,".")') time
  elseif (time.le.99.9999) then
     write(sortie,'("St=",f7.4,".")') time
  elseif (time.le.999.999) then
     write(sortie,'("St=",f7.3,".")') time
  elseif (time.le.9999.99) then
     write(sortie,'("St=",f7.2,".")') time
  else
     write(sortie,'("St=",f7.1,".")') time
  endif

!!! Header
  version=3
 
  if (rank==0)  then
     open (300,file=trim(sortie)//trim(runid),status='unknown',form='unformatted')

! version number
     write(300) real(version,4)
     
! physical parameters
     write(300) real(time,4),real(dt,4),real(DeltaU,4),real(Coriolis,4), &
     real(Lorentz,4),real(Buoyancy,4),real(ForcingU,4),       &
     real(DeltaT,4),real(ForcingT,4),real(DeltaB,4),real(ForcingB,4)

! grid parameters
     write(300) real(NR-NG+1,4),real(Nlat,4),real(Nlong,4),real(Mc,4)

! grid
     write(300) (real(r(ir),4),ir=NG,NR)
     write(300) (real(colat(ilat),4),ilat=1,Nlat)
     close(300)
  endif

  call mpi_barrier(COMM_model,ier)

  do irank=0,size-1
     if (rank==irank) then

     open (300,file=trim(sortie)//trim(runid),position='append',form='unformatted')

  do ir=ideb_all,ifin_all

     if (ir.eq.nr-8) then

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
     do iblock=1,nblock
        call Spec_Spat_PolTor_shell &
             (ir,yVpr(:,ir),yVtr(:,ir),yVs(:,ir), &
             Vr,Vt,Vp,latstart(iblock),latstop(iblock))
     enddo
!$OMP END PARALLEL DO
     call fourierr_spec_spat2(Data_spec_spat)

     do ilat=1,nlat
        write(300) (real(Vt(ilong,ilat),4),ilong=1,Nlong)
        write(300) (real(Vp(ilong,ilat),4),ilong=1,Nlong)
     enddo

  endif


    if (ir.eq.nr) then
     
!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
     do iblock=1,nblock
        call Spec_Spat_PolTor_shell &
             (ir,yBpr(:,ir),yBtr(:,ir),yBs(:,ir), &
             Br,Bt,Bp,latstart(iblock),latstop(iblock))
        call Spec_Spat_PolTor_shell &
          (ir,(yBpr(:,ir)-yBpr_o(:)),yBtr(:,ir),yBs(:,ir), &
          Bt,Bp,Bp,latstart(iblock),latstop(iblock))
     enddo
!$OMP END PARALLEL DO
     call fourierr_spec_spat2(Data_spec_spat)

     do ilat=1,nlat
        write(300) (real(Br(ilong,ilat),4),ilong=1,Nlong)
        write(300) (real(Bt(ilong,ilat),4),ilong=1,Nlong)
     enddo

  endif


enddo

close(300)

endif

  call mpi_barrier(COMM_model,ier)
#ifndef XLF
  call sleep(1)
#endif
enddo


l_printgraph=.false.
if (rank==0) then 
print *,'Surface file written at time=',time
open(22,file='log.'//trim(RUNid),status='old',position='append')
write(22,*) 'Surface file written at time=',time
close(22)
endif

  Return
End Subroutine Graphic_surface



Subroutine Graphic_ave

! Graphical output of average fields

  use globall
  use mod_parallel
  Implicit None


  !**        Variables locales

  Complex yVs(0:LMmax+1,ideb_Vp-2:ifin_Vp+2)
  Complex yBs(0:LMmax+1,ideb_Bp-2:ifin_Bp+2)
  Complex CBp(0:LMmax+1),CVp(0:LMmax+1)


! Arrays to be allocated in case of graphic output

  Real, POINTER, DIMENSION(:,:)    ::  Vr,Vt,Vp
  Real, POINTER, DIMENSION(:,:)    ::  Br,Bt,Bp
  Real, POINTER, DIMENSION(:,:)    ::  Tt

  Integer Nilat,indx,ideb,ifin,ier

  Integer ir, i, l, m, lm, ilat, ilong,isigne

  integer irank
  integer :: version
  real tmp1,tmp2
  character*30 sortie

! Mantle control
  Complex yTtr2(0:LMmax+1)

  Vr => Data_spec_spat(:,:,1)
  Vt => Data_spec_spat(:,:,2)
  Vp => Data_spec_spat(:,:,3)
  Br => Data_spec_spat(:,:,4)
  Bt => Data_spec_spat(:,:,5)
  Bp => Data_spec_spat(:,:,6)
  Tt => Data_spec_spat(:,:,7)

!CC Calcul des Spheroidaux


if (rank==0) then
  !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC Graine
  ir = NG

if (Aspect_ratio.eq.0.) then
   do lm=1,lmmax
      l=li(lm)
      if (l.eq.1) then
         CBp(lm)=yBpr_ave(lm,ir+1)/r(ir+1)
         CVp(lm)=yVpr_ave(lm,ir+1)/r(ir+1)
      else
         CBp(lm)=0.
         CVp(lm)=0.
      endif
   enddo

else

if (CondIC) then
  yBs(:,ir) = dra(ir)*yBpr_ave(:,ir-1) &
       + drb(ir)*yBpr_ave(:,ir) &
       + drc(ir)*yBpr_ave(:,ir+1)
else
  do lm=1,LMmax
     l=li(lm)

     yBs(lm,ir)=real(l+1)*yBpr_ave(lm,ir)/r(ir)

  enddo
endif

tmp1=r(ir+1)-r(ir)
tmp2=r(ir+2)-r(ir) 
if (NOSLIPIC) then
   yVtr_ave(:,ir-1)=yVtr_ave(:,ir+1)-2*(tmp2**2*yVtr_ave(:,ir+1)-tmp1**2*yVtr_ave(:,ir+2))&
        &/(tmp2**2-tmp1*tmp2)
   yVs(:,ir)=0
else
   yVtr_ave(:,ir-1)=yVtr_ave(:,ir+1)*(r(ir)-tmp1)/(r(ir)+tmp1)
   yVs(:,ir)=(yVpr_ave(:,ir+1)-yVpr_ave(:,ir-1))/(2*tmp1)
endif

endif
endif

  !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC NG+1 -> NR-1
  do ir=ideb_Vp,ifin_Vp
     yVs(:,ir) = dra(ir)*yVpr_ave(:,ir-1) &
          + drb(ir)*yVpr_ave(:,ir) &
          + drc(ir)*yVpr_ave(:,ir+1)

     !     dr . = 1/r d/dr r.
     yBs(:,ir) = dra(ir)*yBpr_ave(:,ir-1) &
          + drb(ir)*yBpr_ave(:,ir) &
          + drc(ir)*yBpr_ave(:,ir+1)
  enddo

  if (rank==size-1) then
     !CCCCCCCCCCCCCCCCCCCCCCCCCCCCC NR
     ir=NR
     
     do lm=1,LMmax
        l=li(lm)
        
        yBs(lm,ir)=-(real(l)*yBpr_ave(lm,ir))/r(ir)
        
     enddo
    
     tmp1=r(ir)-r(ir-1)
     tmp2=r(ir)-r(ir-2) 
     
     if (NOSLIPOC)then
        yVtr_ave(:,ir+1)=yVtr_ave(:,ir-1)-2*(tmp2**2*yVtr_ave(:,ir-1) &
             & -tmp1**2*yVtr_ave(:,ir-2))  &
             & /(tmp2**2-tmp1*tmp2)
        yVs(:,ir)=0.
     else
        yVtr_ave(:,ir+1)=2*tmp1/r(ir)*yVtr_ave(:,ir)+yVtr_ave(:,ir-1)
        yVpr_ave(:,ir+1)=-yVpr_ave(:,ir-1)
        yVs(:,ir)=(yVpr_ave(:,ir+1)-yVpr_ave(:,ir-1))/(2*tmp1)
        
     endif

  endif

! Average graphical filename
  if     (startime.le.9.99999) then
     write(sortie,'("Gave=",f7.5,".")') startime
  elseif (startime.le.99.9999) then
     write(sortie,'("Gave=",f7.4,".")') startime
  elseif (startime.le.999.999) then
     write(sortie,'("Gave=",f7.3,".")') startime
  elseif (startime.le.9999.99) then
     write(sortie,'("Gave=",f7.2,".")') startime
  else 
     write(sortie,'("Gave=",f7.1,".")') startime
  endif

  if (rank==0)  then
     open (600,file=trim(sortie)//trim(runid),status='unknown',form='unformatted')

! version number
     write(600) real(version,4)
     
! physical parameters
! For G_ave file time is replaced by number of averaged fields
     write(600) real(mn,4),real(DeltaU,4),real(Coriolis,4), &
     real(Lorentz,4),real(Buoyancy,4),real(ForcingU,4),       &
     real(DeltaT,4),real(ForcingT,4),real(DeltaB,4),real(ForcingB,4)

! grid parameters
     write(600) real(NR-NG+1,4),real(Nlat,4),real(Nlong,4),real(Mc,4)

! grid
     write(600) (real(r(ir),4),ir=NG,NR)
     write(600) (real(colat(ilat),4),ilat=1,Nlat)
     close(600)
  endif

  do irank=0,size-1
  if (rank==irank) then

  open (600,file=trim(sortie)//trim(runid),position='append',form='unformatted')
  do ir=ideb_all,ifin_all

     if ((ir.eq.NG).and.(Aspect_ratio.eq.0)) then

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
        do iblock=1,nblock
           call Spec_Spat_PolTor_NoCore &
                (ir,CVp, &
                Vr,Vt,Vp,latstart(iblock),latstop(iblock))
           call Spec_Spat_PolTor_NoCore &
                (ir,CBp, &
                Br,Bt,Bp,latstart(iblock),latstop(iblock))
        enddo
!$OMP END PARALLEL DO

     else

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
        do iblock=1,nblock
           call Spec_Spat_PolTor_shell &
                (ir,yVpr_ave(:,ir),yVtr_ave(:,ir),yVs(:,ir), &
                Vr,Vt,Vp,latstart(iblock),latstop(iblock))
           call Spec_Spat_PolTor_shell &
                (ir,yBpr_ave(:,ir),yBtr_ave(:,ir),yBs(:,ir), &
                Br,Bt,Bp,latstart(iblock),latstop(iblock))
        enddo
!$OMP END PARALLEL DO

     endif

     yTtr2=yTtr_ave(:,ir)
     ! Mantle Control
     if (MantleControl) yTtr2=yTtr2+yTtrMC(:,ir)

!$OMP PARALLEL DO SCHEDULE(DYNAMIC,1)
do iblock=1,nblock
     call Spec_Spat_Scal_Shell(yTtr2,Tt,latstart(iblock),latstop(iblock))
enddo
!$OMP END PARALLEL DO

     call fourierr_spec_spat2(Data_spec_spat)

! Add static temperature profile

     if (HeatingMode.eq.0) then
     !   Tt = Tt - Aspect_ratio/(1-Aspect_ratio)+Aspect_ratio/(1-Aspect_ratio)**2*r_1(ir)
        Tt=Tt+r(NG)*r(NR)/r(ir)+1-r(NR)
     endif

     if (HeatingMode.eq.1) then
        Tt=Tt+(r(ir)**2-r(NG)**2)/6.+r(NR)**3/3.*(1/r(ir)-1/r(NG))
     endif

     if (HeatingMode.eq.2) then
        Tt=Tt-r(ir)**2/6-r(NG)**3/(3.*r(ir))+r(NG)**2/2.
     endif

     if (HeatingMode.eq.3) then
        if (Aspect_ratio.eq.0) then
        Tt=Tt+3*(2.*Pch-1)/(r(NR)**3-r(NG)**3)*r(ir)**2/6 
        else
        Tt=Tt+3*(2.*Pch-1)/(r(NR)**3-r(NG)**3)*r(ir)**2/6   &
           - (r(NG)**3*(1.-Pch)-r(NR)**3*Pch)/(r(NR)**3-r(NG)**3)/r(ir)
     endif
     endif

     if (HeatingMode.eq.4) then
        Tt=Tt+(1./r(ir)-1./r(NG))*r(NR)**2
     endif


! Velocity at ICB or CMB

     do ilat=1,nlat
        write(600) (real(Vr(ilong,ilat),4),ilong=1,Nlong)
        write(600) (real(Vt(ilong,ilat),4),ilong=1,Nlong)
        write(600) (real(Vp(ilong,ilat),4),ilong=1,Nlong)
        write(600) (real(Br(ilong,ilat),4),ilong=1,Nlong)
        write(600) (real(Bt(ilong,ilat),4),ilong=1,Nlong)
        write(600) (real(Bp(ilong,ilat),4),ilong=1,Nlong)
        write(600) (real(Tt(ilong,ilat),4),ilong=1,Nlong)
     enddo

     enddo
     close(600)
  endif
  call mpi_barrier(COMM_model,ier)
#ifndef XLF
  call sleep(1)
#endif
enddo

l_printgraph=.false.
if (rank==0) then 
print *,'Average Graphical file written at time=',time
open(22,file='log.'//trim(RUNid),status='old',position='append')
write(22,*) 'Average Graphical file written at time=',time
close(22)
endif

  Return
End Subroutine Graphic_ave
