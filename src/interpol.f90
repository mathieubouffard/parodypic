!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                       !!
!!  Version JA-2.3 of Feb  2012                                          !!
!!                                                                       !!
!!  This is an interpolation routine                                     !!
!!                                                                       !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine polint(XA,YA,X,Y,DY)
real    :: XA(4),X
Complex :: YA(4),Y,DY

real :: YAR(4),YAI(4), YR, YI,DYR,DYI

YAR=real(YA)
YAI=imag(YA)

call polint1(XA,YAR,4,X,YR,DYR)
call polint1(XA,YAI,4,X,YI,DYI)

Y=cmplx(YR,YI)
DY=cmplx(DYR,DYI)

end subroutine polint


SUBROUTINE POLINT1(XA,YA,N,X,Y,DY)
  PARAMETER (NMAX=10) 
  DIMENSION XA(N),YA(N),C(NMAX),D(NMAX)
  NS=1
  DIF=ABS(X-XA(1))
  DO I=1,N 
     DIFT=ABS(X-XA(I))
     IF (DIFT.LT.DIF) THEN
        NS=I
        DIF=DIFT
     ENDIF
     C(I)=YA(I)
     D(I)=YA(I)
  ENDDO
  Y=YA(NS)
  NS=NS-1
  DO M=1,N-1
     DO I=1,N-M
        HO=XA(I)-X
        HP=XA(I+M)-X
        W=C(I+1)-D(I)
        DEN=HO-HP
        IF(DEN.EQ.0.)PAUSE
        DEN=W/DEN
        D(I)=HP*DEN
        C(I)=HO*DEN
     ENDDO
     IF (2*NS.LT.N-M)THEN
        DY=C(NS+1)
     ELSE
        DY=D(NS)
        NS=NS-1
     ENDIF
     Y=Y+DY
  ENDDO
  RETURN
END SUBROUTINE POLINT1
