
!===============================
  module initialisation_parody
!===============================
  implicit none
  public :: init_parody
  private :: init_param
  private :: init_compute_pbsize
  private :: init_allocate_global_arrays
  private :: init_allocate_local_arrays
  private :: init_param_output_on_screen_and_log_file
  private :: init_parody_radial_DD
  private :: init_parody_thetaphi
  private :: init_parody_radial_arrays
  private :: init_parody_pointers_and_check_dimensions
  private :: init_parody_coriolis_mat
  private
  contains 
!
  subroutine init_parody
! This is the driver routine for the initialization of the parody model 
  use globall
  use data_hyper
  use mpi

!----------------------
  integer :: ier
  integer :: indx
  integer :: i,j,k
  real timer
  character*30:: sortie
!
!----------------------------------------------------------------
! Input parameters from 'Par' file
!----------------------------------------------------------------
call init_param()
! Compute problem size
call init_compute_pbsize()
! Parameters output on screen and log file.
 call init_param_output_on_screen_and_log_file()
! Radial domain decomposition
 call init_parody_radial_DD()
!!!!!!!!!!!!!! Allocate of common arrays
call init_allocate_global_arrays()
!!!!!!!!!!!!!! Allocate of local arrays
call init_allocate_local_arrays()

call init_Tstat()

!alex
hyperdiff = .false. 
!if (hyperdiff) call create_hyperdiffusivities 

! IniT

call FFTinit()

! Init adaptative time-stepping
! values for Courant and Alfven factors
cf2=2.5**2     ! Courant factor
af2=1.0**2     ! Alfven factor
!
call init_parody_thetaphi()
! Constants initialisation

Im = (0.0,1.0)
lm10=indx(1,0)
lm11=indx(1,1)

if(rank==0) then 
   ! Validations 
   if (delta.ne.max(1,Mmin)) then 
      print *,'delta = ',delta
      print *,'Mmin  = ',Mmin
      call MPI_finalize(ier)
      stop 
   endif
endif

!------------------------------------------------------------------------*
! Initialisations                                                        *
!------------------------------------------------------------------------*

if(rank==0) then
#ifndef DEBUG
#else
  print *,' Initialisation :'
  print *,' ----------------'
#endif
endif

call init_parody_radial_arrays()

  ! Astuce : pour le calcul N.L., on decale le spectre de la FFT avec
  ! un modulo donne par Ms (=Mc). Si on met Ms a 1, l'espace physique
  ! devient le "vrai" espace physique (ce qui n'empeche pas d'avoir Mc<>1
  ! pour un stockage en Indx efficace).

  Ms = Mc

!!!  Init des H.S.

  call initHS()

 !**        Init des pointeurs et validation des dimensions
  
  call init_parody_pointers_and_check_dimensions()


  call init_parody_coriolis_mat ! The Q matrix 



  end subroutine init_parody
  subroutine init_allocate_global_arrays
    use globall
    use tracerstuff
  !!!!!!!!!!!!!! Allocate of common arrays

allocate (r(0:NR),r_1(NR), r_2(NR), r_3(NR),r_4(NR),dr2(NG:NR),dh2(NG:NR))
allocate (Gra(NR), Grb(NR), Grc(NR))
allocate (Lva(NR), Lvb(NR), Lvc(NR))
allocate (La(NR),  Lb(NR),  Lc(NR))
allocate (dra(NR), drb(NR), drc(NR))

allocate (Ylm(LMmax,Nlat/2),dtYlm(LMmax,Nlat/2),dpYlm(LMmax,Nlat/2))
allocate (xxy(LMmax,Nlat/2),xxt(LMmax,Nlat/2),xxp(LMmax,Nlat/2))
allocate (trigsf(3*(Nlong)/2+1))
allocate (NOIKIstart(Mmin:Mmax), NOIKIstop(Mmin:Mmax))
allocate (lmodd(Mmin:Mmax))
allocate (NOIKIstart2(1:2*(Mmax/Mc)+1), NOIKIstop2(1:2*(Mmax/Mc)+1))
allocate (lmodd2(2*(Mmax/Mc)+1))
allocate (ll(0:LMmax+1),ll_1(2:LMmax))
allocate (racin(Nlat),Poids(Nlat),sintheta(Nlat),colat(nlat))
allocate (li(0:LMmax+1),mi(0:LMmax+1))

allocate (yVpr(0:LMmax+1, ideb_Vp-2:ifin_Vp+2))
allocate (yVtr(0:LMmax+1, ideb_Vt-2:ifin_Vt+2))
allocate (yTtr(0:LMmax+1, ideb_Tt-2:ifin_Tt+2))
allocate (yBtr(0:LMmax+1, ideb_Bt-2:ifin_Bt+2))
allocate (yBpr(0:LMmax+1, ideb_Bp-2:ifin_Bp+2))
if (dd_convection .and. .not. use_tracers) then
   allocate (yCr(0:LMmax+1, ideb_C-2:ifin_C+2))
end if
   
allocate (yTTrMC(0:LMmax+1, ideb_Tt-2:ifin_Tt+2))
allocate (yTtrMC_r(0:LMmax+1, ideb_Tt-2:ifin_Tt+2))
allocate (ydTdrMC(0:LMmax+1, ideb_Tt-2:ifin_Tt+2))

allocate (yTTrIC(0:LMmax+1, ideb_Tt-2:ifin_Tt+2))
allocate (yTtrIC_r(0:LMmax+1, ideb_Tt-2:ifin_Tt+2))
allocate (ydTdrIC(0:LMmax+1, ideb_Tt-2:ifin_Tt+2))

allocate (yVpr_ave(0:LMmax+1, ideb_Vp-2:ifin_Vp+2))
allocate (yVtr_ave(0:LMmax+1, ideb_Vt-2:ifin_Vt+2))
allocate (yTtr_ave(0:LMmax+1, ideb_Tt-2:ifin_Tt+2))
allocate (yBtr_ave(0:LMmax+1, ideb_Bt-2:ifin_Bt+2))
allocate (yBpr_ave(0:LMmax+1, ideb_Bp-2:ifin_Bp+2))

allocate (Data_spec_spat(Nphi, Nlat, 15),Data_spat_spec(Nphi, Nlat,  7))

allocate (spVm(0:Mmax),spVl(0:Lmax),spBm(0:Mmax),spBl(0:Lmax))
allocate (spVm_ave(0:Mmax),spVl_ave(0:Lmax),spBm_ave(0:Mmax),spBl_ave(0:Lmax))
allocate (spTm(0:Mmax),spTl(0:Lmax))

allocate(CroissP(0:LMmax+1),CroissT(0:LMmax+1),CroissPA(0:LMmax+1),CroissTA(0:LMmax+1))


  end subroutine init_allocate_global_arrays
!
  subroutine init_allocate_local_arrays
    use globall
    use tracerstuff
  use data_parody_local
allocate (dyVpr(0:LMmax+1, ideb_Vp-2:ifin_Vp+2),LyVpr(LMmax, ideb_Vp-2:ifin_Vp+2))
allocate (dyVtr(0:LMmax+1, ideb_Vt-2:ifin_Vt+2),LyVtr(LMmax, ideb_Vt-2:ifin_Vt+2))

allocate (yNLVtr(LMmax, ideb_Vt-2:ifin_Vt+2))
allocate (yNLVpr(LMmax, ideb_Vp-2:ifin_Vp+2))
allocate (yNLTtr(LMmax, ideb_Tt-2:ifin_Tt+2))
if (dd_convection .and. .not. use_tracers) then
   allocate (yNLCr(LMmax, ideb_C-2:ifin_C+2))
end if

allocate (yVs(0:LMmax+1, ideb_Vp-2:ifin_Vp+2))

allocate (Adams1(0:LMmax+1, ideb_Vt-2:ifin_Vt+2))
allocate (Adams2(0:LMmax+1, ideb_Vp-2:ifin_Vp+2))
allocate (Adams3(0:LMmax+1, ideb_Tt-2:ifin_Tt+2))
allocate (Adams4(0:LMmax+1, ideb_Bp-2:ifin_Bp+2))
allocate (Adams5(0:LMmax+1, ideb_Bt-2:ifin_Bt+2))
if (dd_convection .and. .not. use_tracers) then
   allocate (Adams6(0:LMmax+1, ideb_C-2:ifin_C+2))
end if

allocate (Adams1_past(0:LMmax+1, ideb_Vt-2:ifin_Vt+2))
allocate (Adams2_past(0:LMmax+1, ideb_Vp-2:ifin_Vp+2))
allocate (Adams3_past(0:LMmax+1, ideb_Tt-2:ifin_Tt+2))
allocate (Adams4_past(0:LMmax+1, ideb_Bp-2:ifin_Bp+2))
allocate (Adams5_past(0:LMmax+1, ideb_Bt-2:ifin_Bt+2))
if (dd_convection .and. .not. use_tracers) then
   allocate (Adams6_past(0:LMmax+1, ideb_C-2:ifin_C+2))
end if

allocate (m1_l(LMmax,ideb_Vt-2:ifin_Vt+2),    m1_u(LMmax,ideb_Vt-2:ifin_Vt+2))
allocate (m51_l(LMmax,ideb_Vp-2:ifin_Vp+2),  m51_u(LMmax,ideb_Vp-2:ifin_Vp+2))
allocate (m52_l(LMmax,ideb_Vp-2:ifin_Vp+2),  m52_u(LMmax,ideb_Vp-2:ifin_Vp+2))
allocate (m3_l(LMmax,ideb_Tt-2:ifin_Tt+2),    m3_u(LMmax,ideb_Tt-2:ifin_Tt+2))
allocate (m4_l(LMmax,ideb_Bp-2:ifin_Bp+2),m4_u(LMmax,ideb_Bp-2:ifin_Bp+2))
allocate (m5_l(LMmax,ideb_Bt-2:ifin_Bt+2),m5_u(LMmax,ideb_Bt-2:ifin_Bt+2))
if (dd_convection .and. .not. use_tracers) then
   allocate (m6_l(LMmax,ideb_C-2:ifin_C+2),    m6_u(LMmax,ideb_C-2:ifin_C+2))
end if

allocate (K11(LMmax), K12(LMmax))
allocate (K21(LMmax), K22(LMmax))
allocate (yBpr_o(0:LMmax+1),yBtr_o(0:LMmax+1))
allocate (yVpr_o(0:LMmax+1),yVtr_o(0:LMmax+1))

allocate (Beta(LMmax+1), Gamma(LMmax+1, NR))
allocate (A1a(NG:NR), A1b(Lmax+1,NG:NR), A1c(NG:NR))
allocate (A3a(NG:NR), A3b(0:Lmax+1,NG:NR), A3c(NG:NR))
allocate (A4a(NR), A4b(Lmax+1,NR), A4c(NR))
allocate (A5a(NR), A5b(Lmax+1,NR), A5c(NR))
if (dd_convection .and. .not. use_tracers) then
   allocate (A6a(NG:NR), A6b(0:Lmax+1,NG:NR), A6c(NG:NR))
end if

allocate (A2LU(Lmax+1,NG:NR,5), A2L(Lmax+1,NG:NR,2))
allocate (B2(Lmax+1,NG:NR,5))
allocate (A3bp(NG:NR))
if (dd_convection .and. .not. use_tracers) then
   allocate (A6bp(NG:NR))
end if

allocate (A1a_2d(LMmax,ideb_Vt-2:ifin_Vt+2))
allocate (diag1(LMmax,ideb_Vt-2:ifin_Vt+2))
allocate (A1c_2d(LMmax,ideb_Vt-2:ifin_Vt+2))

allocate (A2U_big(LMmax,ideb_Vp-2:ifin_Vp+2,5))

allocate (B2a(LMmax+1,NG:NR),B2b(LMmax+1,NG:NR),B2c(LMmax+1,NG:NR))
allocate (B2d(LMmax+1,NG:NR),B2e(LMmax+1,NG:NR))

allocate (A3a_2d(LMmax,ideb_Tt-2:ifin_Tt+2))
allocate (diag3(LMmax,ideb_Tt-2:ifin_Tt+2))
allocate (A3c_2d(LMmax,ideb_Tt-2:ifin_Tt+2))

allocate (A4a_2d(LMmax,ideb_Bp-2:ifin_Bp+2))
allocate (diag4(LMmax,ideb_Bp-2:ifin_Bp+2))
allocate (A4c_2d(LMmax,ideb_Bp-2:ifin_Bp+2))

allocate (A5a_2d(LMmax,ideb_Bt-2:ifin_Bt+2))
allocate (diag5(LMmax,ideb_Bt-2:ifin_Bt+2))
allocate (A5c_2d(LMmax,ideb_Bt-2:ifin_Bt+2))

if (dd_convection .and. .not. use_tracers) then
   allocate (A6a_2d(LMmax,ideb_C-2:ifin_C+2))
   allocate (diag6(LMmax,ideb_C-2:ifin_C+2))
   allocate (A6c_2d(LMmax,ideb_C-2:ifin_C+2))
end if

allocate (Transit_Vt(LMmax+1, ideb_Vt-2:ifin_Vt+2))
allocate (Transit_Tt(LMmax+1, ideb_Tt-2:ifin_Tt+2))
allocate (Transit_Bt(LMmax+1, ideb_Bt-2:ifin_Bt+2))
allocate (Transit_Bp(LMmax+1, ideb_Bp-2:ifin_Bp+2))
allocate (Transit_Vp(LMmax+1, ideb_Vp-2:ifin_Vp+2))
if (dd_convection .and. .not. use_tracers) then
   allocate (Transit_C(LMmax+1, ideb_C-2:ifin_C+2))
end if

allocate (cpl_M_shell(NG:NR),cpl_M_shell_ave(NG:NR))


  end subroutine init_allocate_local_arrays


  subroutine init_compute_pbsize
  use globall
  integer :: i,j,k,i0,j0,k0
  real :: dist, dist_min
  Lmin=0

if (Mmin==0) then
   delta=1
else
   delta = Mmin
endif

Alpha_min=Mmin/Mc
Alpha_max=Mmax/Mc

LMmax=(Alpha_max-Alpha_min)*(Lmax+1)                              &
                              -(((Alpha_max*(Alpha_max-1))        &
                              -(Alpha_min*(Alpha_min-1)))/2) * Mc &
                              + Lmax - delta                      &
                              - (Alpha_max-Alpha_min)*Mc          &
                              + 1+1

Nlat=   int( (Lmax+1)*30./Aliasing/2)*2
Nlong = 2*((Mmax/Mc))*30./Aliasing

!Compute optimal Nlong in powers of 2 and 3
dist_min=1000.
do i=0,10
   do j=0,1
      do k=0,1
         dist=real((2**i)*(3**j)*(5**k)-Nlong)
         if ((dist.lt.dist_min).and.(dist.ge.0)) then
            dist_min=dist
            i0=i
            j0=j
            k0=k
         endif
      enddo
   enddo
enddo

Nlong=(2**i0)*(3**j0)*(5**k0)

Nphi=Nlong+2

if ((.not.CondIC).and.(NG.ne.1)) then
   if (rank==0) print *,'WARNING: NG is set to 1, since CondIC=FALSE'
   NG=1
   NR=NF
endif

if (Aspect_ratio.eq.0.) then
NG=0
condIC=.false.
NOSLIPIC=.true.
endif

InsulIC=.not.CondIC

axis2_standard_length=(NR-NG+1)/(size)
rem=(NR-NG+1)-axis2_standard_length*size

  
  end subroutine init_compute_pbsize 
! 
  subroutine init_param
  use globall
  use tracerstuff
  use mod_parallel
  integer :: ier
  if (rank==0) call input_std 
! Broadcast to all nodes 
  call mpi_bcast(RUNid,20,mpi_character,0,COMM_model,ier)
  call mpi_bcast(Iter_fin,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(modulo,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(modulo2,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(modulo3,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(modulo4,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(Adaptative,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(dtfixed,1,mpi_double_precision,0,COMM_model,ier)

  call mpi_bcast(NF,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(NR,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(NG,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(Ratio1,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Ratio2,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Aspect_ratio,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(grille,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(Lmax,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(Mmin,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(Mmax,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(Mc,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(Aliasing,1,mpi_double_precision,0,COMM_model,ier)

  call mpi_bcast(Navier_Stokes,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(DeltaU,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Coriolis,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Buoyancy,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Lorentz,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(ForcingU,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Heat_equation,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(DeltaT,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(ForcingT,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Induction,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(DeltaB,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(ForcingB,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(non_lin,1,mpi_logical,0,COMM_model,ier)

  call mpi_bcast(dd_convection,1,mpi_logical,0,COMM_model,ier)

  call mpi_bcast(Couette,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(NOSLIPOC,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(NOSLIPIC,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(CondIC,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(gammatau,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(HeatingMode,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(Pch,1,mpi_double_precision,0,COMM_model,ier)

  call mpi_bcast(Init,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(Entree,20,mpi_character,0,COMM_model,ier)
  call mpi_bcast(amp_b,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(init_t,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(amp_t,1,mpi_double_precision,0,COMM_model,ier)

  call mpi_bcast(MantleControl,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(BottomControl,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(ControlType,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(ControlTypeBottom,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(qstar,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(qstar_IC,1,mpi_double_precision,0,COMM_model,ier)

  call mpi_bcast(use_tracers,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(ntr_tot,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(initial_distribution,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(rebalance_domains,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(Entree_tra,20,mpi_character,0,COMM_model,ier)
  call mpi_bcast(Vmax_empty,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(tracers_noadvection,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(theta_poles,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(tracers_advord,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(tracers_interp2ord,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(temperature_tracers,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(composition_tracers,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(numerical_diffusion,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(diffusion_composition,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(volumic_sources,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(init_comp_mode,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(lc_init,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(mc_init,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(Ra_T,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Ra_C,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Pr_T,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Pr_C,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Buoyancy_C,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Buoyancy_T,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(comp_source,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(DeltaC,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(dist_flux_icb,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(dist_flux_cmb,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(Reordering,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(Modulo_reord,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(grid_fusion,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(triquadratic_interpolation,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(Buoyancy_ratio,1,mpi_double_precision,0,COMM_model,ier)
  call mpi_bcast(ntracked,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(track_tracers,1,mpi_logical,0,COMM_model,ier)
  call mpi_bcast(Modulo_comp,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(TC_bcs_bot,1,mpi_integer,0,COMM_model,ier)
  call mpi_bcast(measure_time,1,mpi_logical,0,COMM_model,ier)
    

  end subroutine init_param
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                       !!
!!  Version JA-2.3 of Feb  2012                                          !!
!!                                                                       !!
!!  This contains the standard input preprocessors                       !!
!!                                                                       !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Subroutine input_std

  use globall
  use tracerstuff
  Implicit none
  Integer    :: i
  Character  :: PROBLEM *20
  Character  :: label *15
  Real       :: Ek,Ra,Pr,Pm

  dd_convection = .false.

  open (UNIT = 3, FILE = 'Par', STATUS = 'old')

  ! Formats
1 format (a,f10.5)
2 format (a,i7)
3 format (f14.9,' ',f16.9)
4 format (i6,' iterations, soit ',f5.1,' %, yBpr_c ~ ',f25.20)
5 format (a,i4,a,i4)
6 format (a,a)
7 format ('m=',i4,' l=',i4,' ',f16.7,f16.7,'    l=',i4,f16.7,f16.7)
8 format (a,l1)
9 format (a,i10)

  ! Lecture des 6 premieres lignes (entete)
  do i = 1,6
     read (3,*)
  end do

  ! Lecture des parametres
  read (3,6) label, RUNid
  read (3,*)
  read (3,*)
  read (3,2) label, Iter_fin
  read (3,2) label, modulo
  read (3,2) label, modulo2
  read (3,2) label, modulo3
  read (3,2) label, modulo4
  read (3,2) label, Modulo_comp
  read (3,8) label, Adaptative
  read (3,1) label, dtfixed
  read (3,*)
  read (3,*)
  read (3,2) label, NF
  read (3,2) label, NG
  NR=NF+NG-1
  read (3,1) label, Ratio1
  read (3,1) label, Ratio2
  read (3,1) label, Aspect_ratio
  read (3,8) label, grille
  read (3,2) label, Lmax
  read (3,2) label, Mmin
  read (3,2) label, Mmax
  read (3,2) label, Mc
  read (3,1) label, Aliasing
  read (3,*)
  read (3,*)
  read (3,6) label, PROBLEM

if (trim(PROBLEM)=="RAW") then
  print *,'PROBLEM IS OF THE RAW TYPE'
  read (3,8) label, Navier_Stokes
  print *,Navier_Stokes
  read (3,1) label, DeltaU
  read (3,1) label, Coriolis
  read (3,1) label, Buoyancy
  read (3,1) label, Lorentz
  read (3,1) label, ForcingU
  read (3,8) label, Heat_equation
  read (3,1) label, DeltaT
  read (3,1) label, ForcingT
  read (3,8) label, Induction
  read (3,1) label, DeltaB
  read (3,1) label, ForcingB
  read (3,8) label, non_lin

elseif (trim(PROBLEM)=="DYNAMO") then
  print *,'PROBLEM IS OF THE DYNAMO TYPE'
  read (3,1) label, Ek
  read (3,1) label, Ra
  read (3,1) label, Pr
  read (3,1) label, Pm
  Navier_Stokes =.true.
  Heat_equation =.true.
  Induction     =.true.
  non_lin       =.true.
  DeltaU        =1.
  Coriolis      =1./Ek
  Buoyancy      =Ra/Ek
  Lorentz       =1./(Ek*Pm)
  ForcingU      =0.  
  DeltaT        =1./Pr  
  forcingT      =0.  
  DeltaB        =1./Pm 
  ForcingB      =0.
  dd_convection = .false.

  elseif (trim(PROBLEM)=="PURETHERMAL") then
  print *,'PROBLEM IS PURE THERMAL CONVECTION TYPE'
  read (3,1) label, Ek
  read (3,1) label, Ra
  read (3,1) label, Pr
  Navier_Stokes =.true.
  Heat_equation =.true.
  Induction     =.false.
  non_lin       =.true.
  DeltaU        =1.
  Coriolis      =1./Ek
  Buoyancy_T    =Ra/Ek
  Lorentz       =0.
  ForcingU      =0.  
  DeltaT        =1./Pr  
  forcingT      =0.  
  DeltaB        =0. 
  ForcingB      =0.
  dd_convection = .false.

elseif (trim(PROBLEM)=="DDCONVECTION") then
  dd_convection = .true.
  read (3,8) label, Induction
  read (3,1) label, Ek
  read (3,1) label, Ra
  read (3,1) label, Pr_T
  read (3,1) label, Pr_C
  read (3,1) label, Pm
  read (3,1) label, Buoyancy_ratio 
  if (Induction) then
     print*,'PROBLEM IS MAGNETIC DOUBLE DIFFUSIVE CONVECTION'
  else
     print*,'PROBLEM IS NON-MAGNETIC DOUBLE DIFFUSIVE CONVECTION'
     Ra_C = Ra/(1.+0.5*Buoyancy_ratio)!Buoyancy_ratio*Ra 
     Ra_T = 0.5*Ra_C*Buoyancy_ratio  !(1.-Buoyancy_ratio)*Ra ! (1-delta_TC)
     Navier_Stokes =.true.
     Heat_equation =.true.
     non_lin       =.true.
     DeltaU        =1.
     Coriolis      =1./Ek
     Buoyancy_T    =Ra_T
     Buoyancy_C    =Ra_C
     Lorentz       =0.
     ForcingU      =0.  
     DeltaT        =1./Pr_T 
     DeltaC        =1./Pr_C
     forcingT      =0.  
     DeltaB        =0.
     ForcingB      =0.  
  end if
 
elseif (trim(PROBLEM)=="ONSET") then
  print *,'PROBLEM IS LINEAR CONVECTION ONSET'
  read (3,1) label, Ek
  read (3,1) label, Ra
  read (3,1) label, Pr
  read (3,1) label, Pm
  Navier_Stokes =.true.
  Heat_equation =.true.
  Induction     =.true.
  non_lin       =.false.
  DeltaU        =1.
  Coriolis      =1./Ek
  Buoyancy      =Ra/Ek
  Lorentz       =0.
  ForcingU      =0.  
  DeltaT        =1./Pr  
  forcingT      =0.  
  DeltaB        =0. 
  ForcingB      =0.  

elseif (trim(PROBLEM)=="COUETTE") then
  print *,'PROBLEM IS OF THE COUETTE TYPE'

else 
   print *,'PROBLEM IS UNDEFINED'
endif
  read (3,*)
  read (3,*)
  read (3,8) label, Couette
  read (3,8) label, NOSLIPOC
  read (3,8) label, NOSLIPIC
  read (3,8) label, CondIC
  read (3,1) label, gammatau
  read (3,2) label, HeatingMode
  read (3,2) label, ChemicalMode
  read (3,1) label, Pch
  read (3,8) label, MantleControl
  read (3,8) label, BottomControl
  read (3,2) label, ControlType
  read (3,2) label, ControlTypeBottom
  read (3,1) label, qstar
  read (3,1) label, qstar_IC
  read (3,2) label, TC_bcs_bot
  read (3,1) label, dist_flux_icb
  read (3,1) label, dist_flux_cmb
  read (3,*)
  read (3,*)
  read (3,8) label, Init
  read (3,6) label, Entree
  read (3,1) label, amp_b
  read (3,2) label, init_t
  read (3,1) label, amp_t

  read (3,*)
  read (3,*)
  read (3,8) label, Resub
  read (3,1) label, Maxtime
  read (3,6) label, ResubCommand
  read (3,*)
  read (3,*)

  read (3,8) label, use_tracers
  read (3,9) label, ntr_tot
  read (3,2) label, initial_distribution
  read (3,8) label, rebalance_domains
  read (3,8) label, grid_fusion
  read (3,8) label, Reordering
  read (3,2) label, Modulo_reord
  read (3,6) label, Entree_tra
  read (3,1) label, Vmax_empty
  read (3,8) label, tracers_noadvection
  read (3,1) label, theta_poles
  read (3,2) label, tracers_advord
  read (3,8) label, tracers_interp2ord ! NOM A CHANGER POUR EVITER LA CONFUSION AVEC TRIQUADRATIC
  read (3,8) label, triquadratic_interpolation
  read (3,8) label, temperature_tracers
  read (3,8) label, composition_tracers
  read (3,8) label, diffusion_composition
  read (3,1) label, numerical_diffusion
  read (3,8) label, track_tracers
  read (3,2) label, ntracked
  read (3,8) label, measure_time
  close (3)

  if (.not. use_tracers) then
     composition_tracers = .false.
     temperature_tracers = .false.
  end if

  if (Aspect_ratio .le. 0.1) then
     theta_poles = 0.51*pi !theta_poles > 0.5 => cartesian everywhere POURRAIT ETRE MIEUX FAUT
     print*,'Aspect_ratio < 0.1, switching to full cartesian advection'
     if (Aspect_ratio == 0.) then
        if (.not. grid_fusion) then
           grid_fusion = .true.  
           print*,'Setting grid_fusion = .true. since aspect ratio equals zero'
        end if             
     end if 
     if (.not. grid_fusion) print*,'GRID_FUSION mode is HIGHLY RECOMMENDED for low Aspect ratios...'
  end if

end Subroutine input_std
subroutine init_param_output_on_screen_and_log_file
use globall
integer :: ier
! Parameters output on screen and log file.
if(rank==0) call output_std_param(6)

if(rank==0) then
   open (22, file='log.'//trim(Runid), STATUS = 'unknown', position='append')
   call output_std_param(22)
   close(22)
endif

#ifndef DEBUG

#else
        if (rank==0) print *,'TOTAL NUMBER OF POINTS=',NR-NG+1
        if (rank==0) print *,'STANDARD SIZE=',axis2_standard_length
        if (rank==0) print *,'REM=',rem
        if (rank==0) print *,axis2_standard_length,'x',size,'=',axis2_standard_length*size 
        if (rank==0) print *
#endif


        if (axis2_standard_length.lt.4) then
           write (6,*) '*** PARALLEL PROBLEM ***'
           write (6,'(A,i4,A,i4,A)') 'Processor ',rank,' has ',axis2_standard_length,' points in radius.'
           write (6,'(A)') 'It is required to maintain at least 4 points per process,'
           write (6,'(A)') 'you should use less processors, or increase the radial resolution.'
           call MPI_finalize(ier)
           stop
        endif

end subroutine init_param_output_on_screen_and_log_file

subroutine init_parody_radial_DD
use globall
use mod_parallel
use tracerstuff
use tracers
integer :: ier,ir,i
integer ::status(mpi_status_size)


if(rank==0) then
   ideb_all=NG
   ifin_all=NG+axis2_standard_length-1
else if(rank.gt.size-1-rem)then
   ideb_all=NG
   ifin_all=NG+axis2_standard_length
else
   ideb_all=NG
   ifin_all=NG+axis2_standard_length-1
endif

if (rank.le.size-rem) then
   ideb_all=ideb_all+rank*axis2_standard_length
   ifin_all=ifin_all+rank*axis2_standard_length
else
   ideb_all=ideb_all+(size-rem)*axis2_standard_length
   ifin_all=ifin_all+(size-rem)*axis2_standard_length
   ideb_all=ideb_all+(rank-(size-rem))*(axis2_standard_length+1)
   ifin_all=ifin_all+(rank-(size-rem))*(axis2_standard_length+1)
endif
if (use_tracers .and. rebalance_domains) then
   !rebalance the domains to have equal number of tracers per node
   call balance_domains_for_tracers ()
end if

ideb_allB=ideb_all
ifin_allB=ifin_all
if (rank==0) ideb_allB=1

ideb_Vt=ideb_all
ifin_Vt=ifin_all
if (NOSLIPIC) then
   if(rank==0) ideb_Vt=NG+1
else
   if(rank==0) ideb_Vt=NG
endif
if (NOSLIPOC) then
   if (rank==size-1) ifin_Vt=ifin_Vt-1
else
   if (rank==size-1) ifin_Vt=ifin_Vt
endif


! Velocity
ideb_Vp=ideb_all
ifin_Vp=ifin_all
if(rank==0) ideb_Vp=NG+1
if (rank==size-1) ifin_Vp=ifin_Vp-1


ideb_Tt=ideb_all
ifin_Tt=ifin_all

if (HeatingMode.eq.0) then
! Tt fixed on the ICB
   if(rank==0) ideb_Tt=NG+1
! Tt fixed on the CMB
   if (rank==size-1) ifin_Tt=ifin_Tt-1   
endif

if (HeatingMode.eq.1) then
! Chemical convection: Tt fixed on the ICB
   if(rank==0) ideb_Tt=NG+1
! Chemical convection: flux CMB is computed (and set to 0)
   if (rank==size-1) ifin_Tt=ifin_all
endif

if (HeatingMode.eq.2) then
! Secular Cooling: flux ICB is computed (and set to 0)
   if(rank==0) ideb_Tt=NG
! Secular Cooling: flux CMB is computed (and set to 0)
   if (rank==size-1) ifin_Tt=ifin_all
endif

if (HeatingMode.eq.3) then
! Thermochemical: flux ICB is computed (and set to 0)
   if (rank==0) ideb_Tt=NG
! Thermochemical: flux CMB is computed (and set to 0)
   if (rank==size-1) ifin_Tt=ifin_all
endif

if (HeatingMode.eq.4) then
! Tt fixed on the ICB
   if (rank==0) ideb_Tt=NG+1
! flux CMB is computed (and set to 0)
   if (rank==size-1) ifin_Tt=ifin_all
endif

if (HeatingMode.eq.5) then
! flux ICB is computed (and set to 0) --- Mode created for Enceladus
   if (rank==0) ideb_Tt=NG
! Tt fixed on the CMB --- Mode created for Enceladus
if (rank==size-1) ifin_Tt=ifin_Tt-1
endif


if (dd_convection .and. .not. use_tracers) then
   ideb_C=ideb_all
   ifin_C=ifin_all

   if (ChemicalMode.eq.0) then
      ! Tt fixed on the ICB
      if(rank==0) ideb_C=NG+1
   endif
   
   if (ChemicalMode.eq.1) then
      ! Chemical convection: Tt fixed on the ICB
      if(rank==0) ideb_C=NG+1
   endif
   
   if (ChemicalMode.eq.2) then
      ! Secular Cooling: flux ICB is computed (and set to 0)
      if(rank==0) ideb_C=NG
   endif
   
   if (ChemicalMode.eq.3) then
      ! Thermochemical: flux ICB is computed (and set to 0)
      if (rank==0) ideb_C=NG
   endif
   
   if (ChemicalMode.eq.4) then
      ! Tt fixed on the ICB
      if (rank==0) ideb_C=NG+1
   endif
   
   if (ChemicalMode.eq.0) then
      ! Tt fixed on the CMB
      if (rank==size-1) ifin_C=ifin_C-1
   endif
   
   if (ChemicalMode.eq.1) then
      ! Chemical convection: flux CMB is computed (and set to 0)
      if (rank==size-1) ifin_C=ifin_all
   endif
   
   if (ChemicalMode.eq.2) then
      ! Secular Cooling: flux CMB is computed (and set to 0)
      if (rank==size-1) ifin_C=ifin_all
   endif
   
   if (ChemicalMode.eq.3) then
      ! Thermochemical: flux CMB is computed (and set to 0)
      if (rank==size-1) ifin_C=ifin_all
   endif
   
   if (ChemicalMode.eq.4) then
      ! flux CMB is computed (and set to 0)
      if (rank==size-1) ifin_C=ifin_all
   endif
   
end if


! B toroidal
ideb_Bt=ideb_allB
ifin_Bt=ifin_allB
if ((rank==0).and.InsulIC.and.(Aspect_ratio.ne.0.)) ideb_Bt=ideb_Bt+1
if (rank==size-1) ifin_Bt=ifin_Bt-1

! B poloidal
ideb_Bp=ideb_allB
ifin_Bp=ifin_allB


#ifndef DEBUG

#else
do i=0,size-1
   if (rank==i) then
110   format('le processeur ',i3,' a ',i4,' points fluides et ',i4,' points
magnetiques')
      write(6,110) rank, ifin_all-ideb_all+1, ifin_allB-ideb_allB+1
   endif
   call mpi_barrier(COMM_model,ier)
enddo



do i=0,size-1
   if (rank==i) then
      write(6,*) rank, ideb_all, ifin_all
   endif
   call mpi_barrier(COMM_model,ier)
enddo
#endif

allocate (nb_rad(size-1))

do ir=1,size-1
   if (rank==0) then
      call MPI_Recv(nb_rad(ir),1,mpi_integer,ir,0,COMM_model,status,ier)
   endif
   if (rank==ir) then
      call MPI_Send(ifin_all-ideb_all+1,1,mpi_integer,0,0,COMM_model,status,ier)
   endif
enddo

end subroutine init_parody_radial_DD  

subroutine init_parody_radial_arrays
use globall
use tracerstuff
integer :: i,ir
integer :: l,m
integer :: ier
real :: tmp1, tmp2

  !       Initialisation de "r".

  !  Ratio1 = nb intervals reguliers / nb intervals decroissants.
  !  Ratio2 = dx_min / dx_max.
  !  tmp1 : dx local.
  !  tmp2 : coefficient eta de decroissance geometrique.
  !  l    : nb inter irreg
  !  m    : nb inter reguliers


  if (Ratio2.eq.1.0) then
     if(rank==0) print *,'Grille reguliere'
     tmp1 = (1.-Aspect_ratio)/real(NR-NG)
     r(nr) = 1.0
     do i = NR-1, NG,-1
        r(i) = r(i+1) - tmp1
     enddo

#ifndef DEBUG

#else
     if(rank==0) print *,'r(NR)=',r(NR)
     if(rank==0) print *,'le dx =',tmp1
     if(rank==0) print *,'et Aspect_ratio =',r(NG)
#endif

  else

     r(NG) = Aspect_ratio

     l = int( real(NR-NG) / (2.*(1. + Ratio1)))

     Ratio1 = real(NR-NG)/ real(2*l) - 1.

#ifndef DEBUG
#else
     if (rank==0) print *,'Ratio1 est modifie : Ratio1 = ',Ratio1
#endif


     if (Aspect_ratio.ne.0.) then

     m = NR - NG - 2*l

     tmp2 = exp(alog(Ratio2)/real(l))

     tmp1 = 1.
     do i = 1, l
        tmp1 = tmp1 * tmp2
     enddo
     tmp1 = (1. - Aspect_ratio) / (real(m) + 2. * tmp2 * &
          ((1.-tmp1)/(1.-tmp2)))

     else

     m = NR - NG - l
     tmp2 = exp(alog(Ratio2)/real(l))

     tmp1 = 1.
     do i = 1, l
        tmp1 = tmp1 * tmp2
     enddo
     tmp1 = (1. - Aspect_ratio) / (real(m) + tmp2 * &
          ((1.-tmp1)/(1.-tmp2)))
     endif

#ifndef DEBUG
#else
     if(rank==0) print *,'Le dx max vaut :',tmp1
#endif

     do i = 1, l
        tmp1 = tmp1 * real(tmp2)
     enddo

#ifndef DEBUG
#else
     if(rank==0) print *,'Le dx min vaut :',tmp1
#endif

     if (Aspect_ratio.ne.0.) then

     do i = 1, l
        r(NG+i) = r(NG+i-1) + tmp1
        tmp1 = tmp1 / tmp2
     enddo
     do i = 1, m
        r(NG+l+i) = r(NG+l+i-1) + tmp1
     enddo

     do i = 1, l
        tmp1 = tmp1 * tmp2
        r(NG+l+m+i) = r(NG+l+m+i-1) + tmp1
     enddo

     else

     do i = 1, l
     tmp1 = tmp1 / tmp2
     enddo
     do i = 1, m
        r(NG+i) = r(NG+i-1) + tmp1
     enddo
     do i = 1, l
        tmp1 = tmp1 * tmp2
        r(NG+m+i) = r(NG+m+i-1) + tmp1
     enddo
     endif

     if (r(NR) .ne. 1.0) then

#ifndef DEBUG
#else
        if(rank==0) print *
        if(rank==0) print *, 'On a r(NR) <>    1,'
        if(rank==0) print *, '     r(NR) = ',r(NR)
#endif
        if (abs(r(NR) - 1.0).gt. tmp1) then
           if(rank==0) print *,'PB RAYON externe!!!'
           call MPI_finalize(ier)
           stop
        else
#ifndef DEBUG
#else
           if(rank==0) print *,'On le met a 1.0'
#endif
           r(NR)=1.0
        endif
     endif


     ! maillage graine serie arithmetique...

     if (NG.gt.1) then
        tmp2 = 2.0/real(NG*(NG-1)) * (Aspect_ratio - real(NG)*tmp1)

        do i = NG-1, 0 , -1
           r(i) = r(i+1) - tmp1
           tmp1 = tmp1 + tmp2
        enddo

        if (r(0) .ne. 0.) then
           if(rank==0) print *
           if(rank==0) print *, 'On a r(0)  <>    0.,'
           if(rank==0) print *, '     r(0)  = ',r(0)
           if (abs(r(0)).gt. tmp1) then
              if(rank==0) print *,'PB RAYON centre!!!'
              call MPI_finalize(ier)
              stop
           else
              if(rank==0) print *,'On le met a 0.0'
              r(0)=0.0
           endif
        endif



     endif

  endif

  ! Modif de la grille pour comparaison avec Zhang et Jones,
  ! ou Sun et al. (i.e. d = r_e-r_i = 1 <==> Tchebychev).

  if (Grille) then
     do ir = 0, NR
        r(ir) = r(ir) / (Real(1.0) - Aspect_ratio)
     enddo
  endif


! dr2 and dh2 are used for adaptative time stepping only
! hence their rough values

  dr2(NG)=(r(NG+1)-r(NG))**2
  dh2(NG)=r(NG)**2/(lmax*(lmax+1))
  do ir=NG+1,NR-1
     dr2(ir)=min(r(ir+1)-r(ir),r(ir)-r(ir-1))**2
     dh2(ir)=r(ir)**2/(lmax*(lmax+1))
  enddo
  dr2(NR)=(r(NR)-r(NR-1))**2
  dh2(NR)=r(NR)**2/(lmax*(lmax+1))

! repertoire de sortie

  if (rank==0) then

  open (2, file='rayon.'//trim(runid), STATUS = 'unknown')
  do ir=1,nr
     write (2,*) real(ir), r(ir), r(ir)-r(ir-1)
  enddo
  close(2)
#ifndef DEBUG
#else
  print*,"Le rayon R(NR) vaut", r(NR)
#endif
  endif


!!! Calcul des inverses, inverses au carre, et inverses au cube
!!! des rayons.

  do ir = 1, NR
     r_1(ir) = 1.0/r(ir)
     r_2(ir) = r_1(ir)/r(ir)
     r_3(ir) = r_2(ir)/r(ir)
     r_4(ir) = r_3(ir)/r(ir)
  enddo

  !**        Initialisation du gradient et du Laplacien (a passer
  !**        dans la boucle si la taille de la graine varie pendant
  !**        la simulation).

    do ir = 1, NR

     !  tmp1 = Delta1
     !  tmp2 = Delta2
     tmp1 = r(ir) - r(ir-1)
     if (ir.ne.NR)then
     tmp2 = r(ir+1) - r(ir)
     else
     tmp2 = r(ir) - r(ir-1)
     endif

     !! Gr . = d/dr .
     Gra(ir) = -1.0 * tmp2 / (tmp1 * (tmp1+tmp2))
     Grb(ir) = (tmp2 - tmp1)     / (tmp1 * tmp2)
     Grc(ir) =  tmp1             / (tmp2 * (tmp1+tmp2))

     !! Dr . = 1/r d/dr r.

     Dra(ir) = Gra(ir)
     Drb(ir) = 1./r(ir) + Grb(ir)
     Drc(ir) = Grc(ir)

     !! Lv . = d2/dr2 .
     Lva(ir) =  2.0 / (tmp1 * (tmp1+tmp2))
     Lvb(ir) = -2.0 / (tmp1 * tmp2)
     Lvc(ir) =  2.0 / (tmp2 * (tmp1+tmp2))

  enddo


  !** C.L. pour A2.

  ir = NG+1
  CLi = DeltaU*( -0.5 * Lva(ir) - (1.0/r(ir)) * Gra(ir))

  ir = NR-1
  CLe = DeltaU*( -0.5 * Lvc(ir) - (1.0/r(ir)) * Grc(ir))

  Gre = Grc(ir)

  do ir = 1, NR

     La(ir) = Lva(ir) + (2.0/r(ir)) * Gra(ir)

     Lb(ir) = Lvb(ir) + (2.0/r(ir)) * Grb(ir)

     Lc(ir) = Lvc(ir) + (2.0/r(ir)) * Grc(ir)

  enddo


end subroutine init_parody_radial_arrays

subroutine init_parody_thetaphi
use globall
integer :: i,j,k,i0,j0,k0
integer :: ier
real :: dist_min
! Theta Loop blocking

!Your IDEAL_BLOCK is machine dependant!!
!It defines the level of vectorisation
!and is also useful for OMP parallel stuff

!Here is the ideal block for Fourier blocking
IDEAL_BLOCK=(284/(Nlong+16)+1)*8

! Init blocking 15 Fourier transforms spectral to spatial

BLOCK15=min(15*Nlat,IDEAL_BLOCK)
Nblock15=(15*Nlat)/BLOCK15

if (Nblock15.eq.1) then
   BLOCK15=15*Nlat
   latrem=0
else
   latrem=15*Nlat-nblock15*block15
   do while (latrem.gt.nblock15)
      block15=block15-2
      Nblock15=(15*Nlat)/BLOCK15
      latrem=15*Nlat-nblock15*block15
   enddo
endif

allocate(start15(nblock15+1),stop15(nblock15))
start15(1)=1
do iblock=1,nblock15
   stop15(iblock) = start15(iblock)+block15-1
   if (iblock.le.latrem/2) then
      stop15(iblock)=stop15(iblock)+2
   endif
   start15(iblock+1) = stop15(iblock)+1
enddo

if (stop15(Nblock15).ne.15*Nlat) then
   print *,'Problem Blocking Fourier 15'
   call mpi_finalize(ier)
   stop
endif

#ifndef DEBUG
#else
do iblock=1,nblock15
if (rank==0) print *,'iblock15', iblock,
start15(iblock),stop15(iblock),stop15(iblock)-start15(iblock)+1
enddo
#endif

! Init blocking 7 Fourier transforms spatial to spectral
BLOCK7=min(7*Nlat,IDEAL_BLOCK)
Nblock7=(7*Nlat)/BLOCK7

If (Nblock7.eq.1) then
   block7=7*Nlat
   latrem=0
else
   latrem=7*Nlat-nblock7*block7
   do while (latrem.gt.nblock7)
      block7=block7-2
      Nblock7=(7*Nlat)/BLOCK7
      latrem=7*Nlat-nblock7*block7
   enddo
endif

allocate(start7(nblock7+1),stop7(nblock7))
start7(1)=1
do iblock=1,nblock7
   stop7(iblock) = start7(iblock)+block7-1
   if (iblock.le.latrem/2) then
      stop7(iblock)=stop7(iblock)+2
   endif
   start7(iblock+1) = stop7(iblock)+1
enddo

if (stop7(Nblock7).ne.7*Nlat) then
   print *,'Problem Blocking Fourier 7'
   call mpi_finalize(ier)
   stop
endif

#ifndef DEBUG
#else
do iblock=1,nblock7
if (rank==0) print *,'iblock7', iblock,
start7(iblock),stop7(iblock),stop7(iblock)-start7(iblock)+1
enddo
#endif

! Init blocking legendre transforms
! Most importantly here the nblock must be a multiple of the # of threads
dist_min=1000
do i=0,3
do j=0,3
nblock=8*(2**i)*(3**j)
BLOCK1=(NLAT/2)/nblock
latrem=Nlat/2-nblock*block1
if ((latrem.lt.dist_min).and.(latrem.ge.0).and.(block1.le.10)) then
dist_min=latrem
i0=i
j0=j
endif
enddo
enddo
nblock=8*(2**i0)*(3**j0)
BLOCK1=(NLAT/2)/nblock
latrem=Nlat/2-nblock*block1

allocate(latstart(2*nblock),latstop(2*nblock))
latstart(1)=1
do iblock=1,nblock
   latstop(iblock) = latstart(iblock)+block1-1
   if (iblock.le.latrem) then
      latstop(iblock)=latstop(iblock)+1
   endif
   latstart(iblock+1) = latstop(iblock)+1
enddo
do iblock=nblock+1,2*nblock
   latstart(iblock)=latstart(iblock-nblock)+Nlat/2
   latstop(iblock)=latstop(iblock-nblock)+Nlat/2
enddo

if (latstop(Nblock).ne.Nlat/2) then
   print *,'Problem Blocking Legendre'
   call mpi_finalize(ier)
   stop
endif

#ifndef DEBUG
#else
do iblock=1,nblock
if (rank==0) print *,'iblock', iblock,
latstart(iblock),latstop(iblock),latstop(iblock)-latstart(iblock)+1
enddo
#endif

! Init blocking in m for spat_spec
BLOCKM=4
nmblock=floor(real(Alpha_max-Alpha_min+1)/real(2*BLOCKM))
if (2*nmblock*blockm.eq.(Alpha_max-Alpha_min+1)) then
maxmblock=nmblock
else
maxmblock=nmblock+1
endif

allocate(mstart(0:2*nmblock),mstop(0:2*nmblock))

mstart(1)=Alpha_min+1
do iblock=1,nmblock
   mstop(iblock)=mstart(iblock)+BLOCKM-1
   mstart(iblock+1)=mstop(iblock)+1
enddo

do iblock=nmblock+1,2*nmblock
   mstart(iblock)=mstart(iblock-nmblock)+mstop(nmblock)
   mstop(iblock)=mstop(iblock-nmblock)+mstop(nmblock)
enddo

if (nmblock.eq.0) mstop(2*nmblock)=Alpha_min

#ifndef DEBUG
#else
do iblock=1,2*nmblock
if (rank==0) print *,'iblock m', iblock,
mstart(iblock),mstop(iblock),mstop(iblock)-mstart(iblock)+1
enddo
#endif

end subroutine init_parody_thetaphi

subroutine init_parody_pointers_and_check_dimensions
use globall
integer :: ier
integer :: lm,l,m
integer :: Indx

 !**        Init des pointeurs et validation des dimensions


  if (delta.ne.max(Mmin,1)) then
     if(rank==0) print *,'delta n''est pas a jour dans in_HarmSpher'
     if(rank==0) print *,'delta=',delta,'  Mmin=',Mmin
     if(rank==0) print *,'on devrait avoir delta=',max(Mmin,1)
     call MPI_finalize(ier)
     stop
  endif

  if (Lmin.ge.Lmax) then
     if(rank==0) print *,'Il est prevu que Lmin < Lmax'
     call MPI_finalize(ier)
     stop
  endif

  if (Mmin.gt.Mmax) then
     if(rank==0) print *,'Il est prevu que Mmin =< Mmax'
     call MPI_finalize(ier)
     stop
  endif

  if (Lmin.gt.Mmin) then
     if(rank==0) print *,'Il est prevu que Lmin =< Mmin'
     call MPI_finalize(ier)
     stop
  endif

  if (Mmax.gt.Lmax) then
     if(rank==0) print *,'Il est prevu que Mmax =< Lmax'
     call MPI_finalize(ier)
     stop
  endif

  do lm = 1, LMmax
     li(lm) = 99999999
  enddo

  if (Indx(Lmax,Mmax).ne.LMmax) then
     if(rank==0) print *,'Lmax =',Lmax
     if(rank==0) print *,'Indx(Lmax,Mmax) =',Indx(Lmax,Mmax)
     if(rank==0) print *,'LMmax=',LMmax
     if(rank==0) print *,'Mc   =',Mc
     if(rank==0) print *,'Indx(Lmax,Mmax) <> LMmax'
     if(rank==0) print *,'---> PROBLEM'
     call MPI_finalize(ier)
     stop
  endif

  if ((Mmax/Mc)*Mc.ne.Mmax) then
     if(rank==0) print *,'Mmax =',Mmax
     if(rank==0) print *,'Mc   =',Mc
     if(rank==0) print *,'They must be multiples...'
     call MPI_finalize(ier)
     stop
  endif

  li(1)=0
  mi(1)=0
  ll(1)=0

  do l = 1, Lmax
     do m = Mmin, min(l,Mmax), Mc

        lm  = Indx(l,m)

        if (li(lm).ne.99999999) then
           if(rank==0) print *,'Probleme avec Indx :'
           if(rank==0) print *,'  On utilise deux fois lm=',lm
           if(rank==0) print *,'  1ere fois : l= ',li(lm),', m= ',mi(lm)
           if(rank==0) print *,'  2eme fois : l= ',l,', m= ',m
           call MPI_finalize(ier)
           stop
        endif

        li(lm) = l
        mi(lm) = m

        ll(lm)   = real(l*(l+1))
        ll_1(lm) = 1.0/ll(lm)

     enddo
  enddo


end subroutine init_parody_pointers_and_check_dimensions
subroutine init_parody_coriolis_mat
use globall
use data_parody_local
integer :: l,m,lm
integer :: indx
  !**       Initialisation de K11, K12, K21, K22
  !**      (calcul de l'operateur Q pour Coriolis)

  do l = 1, Lmax
     do m = Mmin, min(l,Mmax), Mc

        lm = Indx(l,m)

        K11(lm) = Real((l+1)*(l-1)*(l-1))      &
             * Sqrt( Real((l-m)*(l+m))         &
             / Real((2*l-1)*(2*l+1)) )

        K12(lm) = Real(-1*(l-1)*(l+1))         &
             * Sqrt( Real((l-m)*(l+m))         &
             / Real((2*l-1)*(2*l+1)) )

        K21(lm) = ( Real((-1*l)*(l+2)*(l+2)) ) &
             * Sqrt( Real((l+1+m)*(l+1-m))     &
             / Real((2*l+3)*(2*l+1)) )

        K22(lm) = ( Real((-1*l)*(l+2)) )       &
             * Sqrt( Real((l+1+m)*(l+1-m))     &
             / Real((2*l+3)*(2*l+1)) )

     enddo
  enddo

end subroutine init_parody_coriolis_mat


subroutine init_Tstat ()

  use globall

  implicit none

  integer ir

  allocate(Tstat(NG:NR))

  if (HeatingMode.eq.0) then
     do ir = NG,NR
        Tstat(ir) = r(NG)*r(NR)/r(ir)+1-r(NR)
     end do
  endif
  
  if (HeatingMode.eq.1) then
     do ir = NG,NR
        Tstat(ir) = (r(ir)**2-r(NG)**2)/6.+r(NR)**3/3.*(1/r(ir)-1/r(NG))
     end do
  endif
  
  if (HeatingMode.eq.2) then
     do ir = NG,NR
        Tstat(ir) = -r(ir)**2/6-r(NG)**3/(3.*r(ir))+r(NG)**2/2.
     end do
  endif
  
  if (HeatingMode.eq.3) then
     do ir = NG,NR
        if (Aspect_ratio.eq.0) then
           Tstat(ir)=3*(2.*Pch-1)/(r(NR)**3-r(NG)**3)*r(ir)**2/6 
        else
           Tstat(ir)=3*(2.*Pch-1)/(r(NR)**3-r(NG)**3)*r(ir)**2/6   &
                - (r(NG)**3*(1.-Pch)-r(NR)**3*Pch)/(r(NR)**3-r(NG)**3)/r(ir)
        endif
     end do
  endif
  
  if (HeatingMode.eq.4) then
     do ir = NG,NR
        Tstat(ir)=(1./r(ir)-1./r(NG))*r(NR)**2
     end do
  endif

  if (HeatingMode.eq.5) then
     do ir = NG,NR
        Tstat(ir)=(1./r(ir)-1./r(NR))*r(NG)**2
     end do
  endif

end subroutine init_Tstat

!===================================
  end module initialisation_parody
!===================================
