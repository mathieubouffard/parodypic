subroutine exchange_tracers ()

  ! adapted from the same routine in StagYY (P. Tackley)

  use globall
  use tracerstuff
  use tracers_parallel_mod
  use mpi
!$ use OMP_LIB

  implicit none

  real wk(ntracervar,maxcomtr)
  logical explus, exminus, too_many
  integer i,ipass,nnm,nnp,ntsend,ntrecv,ntrl0,i1,ii,ier
  integer,parameter::it0p1=600,it0p2=1200,it0m1=1800,it0m2=2400
!$ integer :: ntsend_p,i_wk,nb_threads
!$ real,allocatable :: wk_p(:,:)
  real mp_time1,mp_time2

!     ..detect which tracers have crossed local domain walls and send
!     them
!       to the appropriate node

  too_many = .false.

  nnp = rank + 1
  nnm = rank - 1

  if (rank == rank_time .and. measure_time) mp_time1 = MPI_wtime()

  do ipass = 0,1
     ! 1st pass - odd# nodes ->, even <-
     ! 2nd pass - odd# nodes <-, even ->

     explus = ((.not.rank==size-1) .and. mod(rank+ipass,2)==1)
     exminus= ((.not.rank==0)    .and. mod(rank+ipass,2)==0)

     if (explus) then

!!$OMP PARALLEL PRIVATE(ntsend_p,wk_p)
!!$ nb_threads = omp_get_num_threads ()
!!$ allocate(wk_p(ntracervar,maxcomtr))

!!$ if (.true.) then

!!$ ntsend = 0
!!$ ntsend_p = 0
!!$ i_wk = 0
!!$OMP DO SCHEDULE(STATIC,ntr/nb_threads)
!!$    do i = 1,ntr 
!!$       if (tra(1,i)>rfin_tra .and. tra(1,i)/=BLANK) then 
!!$          ntsend_p = ntsend_p + 1
!!$          wk_p(:,ntsend_p) = tra(:,i)
!!$          tra(1,i) = BLANK
!!$          if(ntsend_p>maxcomtr) then
!!$             print*,'WARNING: tracer com wk full'
!!$             too_many = .true. ; ntsend=maxcomtr
!!$           end if
!!$        end if
!!$     end do
!!$OMP END DO!

!!$OMP CRITICAL
!!$    if (i_wk+ntsend_p>maxcomtr) then
!!$       print*,'WARNING: tracer com wk full'
!!$       too_many = .true. ; ntsend=maxcomtr
!!$    end if
!!$    wk(:,i_wk+1:i_wk+ntsend_p) = wk_p(:,1:ntsend_p)
!!$    i_wk = i_wk+ntsend_p
!!$    ntsend = ntsend+ntsend_p
!!$OMP END CRITICAL

!!$ deallocate(wk_p)

!!$ else

        ntsend = 0
        do i = 1,ntr
           if (tra(1,i)>rfin_tra .and. tra(1,i)/=BLANK) then
              ntsend = ntsend + 1
              wk(:,ntsend) = tra(:,i)
              tra(1,i) = BLANK
              if(ntsend>maxcomtr) then
                 print*,'WARNING: tracer com wk full'
                 too_many = .true. ; ntsend=maxcomtr
              end if
           end if
        end do

!!$ end if
!!$OMP END PARALLEL

        call mpi_irecv (ntrecv,1,MPI_INTEGER,nnp,it0m1+nnp,MPI_COMM_WORLD,i1,ier)
        call mpi_send (ntsend,1,MPI_INTEGER,nnp,it0p1+rank,MPI_COMM_WORLD,ier)
!        i1 = irecv (it0m1+nnp,ntrecv,1)
!        call csend (it0p1+rank,ntsend,1,nnp)
        call msgwait (i1)
        if (mod(t,modulo2) == 0) print*,'proc',rank,'received',ntrecv,'and sent',ntsend
        if (ntr+ntrecv>ntr_max) then
           print*,'WARNING: too many tracers/node after communication'
           too_many = .true.          ! wait until end to crash
           ntr = ntr_max - ntrecv  ! out synchronously
        end if
        i1 = irecv (it0m2+nnp,tra(1,ntr+1),ntracervar*ntrecv)
        call csend (it0p2+rank,wk,ntracervar*ntsend,nnp)
        call msgwait (i1)
        ntr = ntr + ntrecv
        if (ntr>ntr_max) print*,'WARNING: too many tracers/node after communication'

     end if

     if (exminus) then

!!$OMP PARALLEL PRIVATE(ntsend_p,wk_p)
!!$ nb_threads = omp_get_num_threads ()
!!$ allocate(wk_p(ntracervar,maxcomtr))

!!$      if (.true.) then

!!$      ntsend = 0
!!$      ntsend_p = 0
!!$      i_wk = 0
!!$OMP DO SCHEDULE(STATIC,ntr/nb_threads)
!!$        do i = 1,ntr
!!$          if (tra(1,i)<rdeb_tra .and. tra(1,i)/=BLANK) then
!!$              ntsend_p = ntsend_p + 1
!!$              wk_p(:,ntsend_p) = tra(:,i)
!!$              tra(1,i) = BLANK
!!$              if(ntsend_p>maxcomtr) then
!!$                 print*,'WARNING: tracer com wk full'
!!$                 too_many = .true. ; ntsend=maxcomtr
!!$              end if
!!$           end if
!!$        end do
!!$OMP END DO

!!$OMP CRITICAL
!!$    if (i_wk+ntsend_p>maxcomtr) then
!!$       print*,'WARNING: tracer com wk full'
!!$       too_many = .true. ; ntsend=maxcomtr
!!$    end if
!!$    wk(:,i_wk+1:i_wk+ntsend_p) = wk_p(:,1:ntsend_p)
!!$    i_wk = i_wk+ntsend_p
!!$    ntsend = ntsend+ntsend_p
!!$OMP END CRITICAL

!!$ deallocate(wk_p)

!!$      else

        ntsend = 0
        do i = 1,ntr
           if (tra(1,i)<rdeb_tra .and. tra(1,i)/=BLANK) then
              ntsend = ntsend + 1
              wk(:,ntsend) = tra(:,i)
              tra(1,i) = BLANK
              if(ntsend>maxcomtr) then
                 print*,'WARNING: tracer com wk full'
                 too_many = .true. ; ntsend=maxcomtr
              end if
           end if
        end do

!!$      end if
!!$OMP END PARALLEL

        call mpi_irecv (ntrecv,1,MPI_INTEGER,nnm,it0p1+nnm,MPI_COMM_WORLD,ii,ier)
        call mpi_send (ntsend,1,MPI_INTEGER,nnm,it0m1+rank,MPI_COMM_WORLD,ier)
!        ii = irecv (it0p1+nnm,ntrecv,1)
!        call csend (it0m1+rank,ntsend,1,nnm)
        call msgwait (ii)
        if (mod(t,modulo2) == 0) print*,'proc',rank,'received',ntrecv,'and sent',ntsend
        if (ntr+ntrecv>ntr_max) then
           print*,'WARNING: too many tracers/node after communication'
           too_many = .true.          ! wait until end to crash
           ntr = ntr_max - ntrecv  ! out synchronously
        end if
        ii = irecv (it0p2+nnm,tra(1,ntr+1),ntracervar*ntrecv)
        call csend (it0m2+rank,wk,ntracervar*ntsend,nnm)
        call msgwait (ii)
        ntr = ntr + ntrecv
        if (ntr>ntr_max) print*,'WARNING: too many tracers in communication'

     end if

!     call mpi_barrier (MPI_COMM_WORLD,ier)

  end do

  if (rank == rank_time .and. measure_time) then
     mp_time2 = MPI_wtime()
     print*,'Time spent in MPI COMMUNICATIONS:',mp_time2-mp_time1
  end if
  call consolidate_tracers ()
  if (rank == rank_time .and. measure_time) then
     mp_time1 = MPI_wtime ()
     print*,'Time spent in CONSOLIDATE_TRACERS:',mp_time1-mp_time2
  end if

  !.......  ...wrap up and finish
!  call glor (too_many,1,gl)
  if (too_many) stop 'Tracer storage full after or during communication'!

!  call check_ntrl()


end subroutine exchange_tracers
!------------------------------------------------------------------------------


subroutine consolidate_tracers ()

!     ...consolidate tracer storage - move tracers down to fill gaps in array

  use tracerstuff

  implicit none

  integer i

  i = 1
  do while (i<=ntr)
     do while (tra(1,max(ntr,1))==BLANK .and. ntr>0)  ! ntr valid tracer?
        ntr = ntr - 1
     end do
     do while (tra(1,i)/=BLANK .and. i<=ntr) ! look for blank tracer
        i = i + 1
     end do
     if (i<ntr) then
        tra(:,i) = tra(:,ntr)
        ntr = ntr - 1
     end if
  end do


end subroutine consolidate_tracers
!-----------------------------------------------------------------------------


subroutine exchange_tracked_tracers ()

  use globall
  use tracerstuff
  use mpi

  implicit none

  integer :: i,ier
  real,dimension(4,ntracked) :: wk

  do i = 1,ntracked
     if (tracked_tracers(1,i) .ge. 0.) then ! not a BLANK=-999.
        wk(:,i) = tracked_tracers(:,i)
     else
        wk(:,i) = 0.
     end if
  end do

  tracked_tracers(:,:) = 0.

  ! Rebuild the whole array of tracked tracers on each MPI proc
  call mpi_allreduce (wk,tracked_tracers,4*ntracked,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,ier)  

!  if (rank == 1) print*,'after reduce',tracked_tracers

! Then check which particles belong to the MPI proc, BLANK the others
  do i = 1,ntracked
     if (tracked_tracers(1,i) > rfin_tra .or. tracked_tracers(1,i) < rdeb_tra) then
       tracked_tracers(:,i) = BLANK
     end if
  end do
 
end subroutine exchange_tracked_tracers
!-----------------------------------------------------------------------------

subroutine exchange_ghost_planes (field,n,ideb,ifin)

! communicate the ghost planes between processors
! 3 radial layers to share in total !

!  use tracerstuff
  use globall
  use tracers_parallel_mod
  use mpi

  implicit none

  integer,intent(in) :: ideb,ifin
  real,intent(inout),dimension(Nlong,Nlat,ideb-2:ifin+2) :: field,n

  integer nb,itm,itp,id,rank_dest,ier
  real,dimension(Nlong,Nlat,3):: fwork1,fwork2

  nb = Nlat*Nlong*3
  itm = 100
  itp = 200

  fwork1(:,:,:) = 0.
  fwork2(:,:,:) = 0.

  ! first, communicate the bottom ghost plane
  rank_dest = rank-1
  if (rank_dest == -1) rank_dest = MPI_PROC_NULL
  if (rank == size-1) then
     call csend (itm,field(:,:,ideb-1),nb,rank_dest) !proc size-1 does not receive anything
  else
     id = irecv (itm,fwork1,nb)
     call csend (itm,field(:,:,ideb-1),nb,rank_dest)
     call msgwait (id)
  end if

  !  !then communicate the upper ghost plane
  rank_dest = rank+1
  if (rank_dest == size) rank_dest = MPI_PROC_NULL

  if (rank == 0) then
     call csend (itp,field(:,:,ifin-1),nb,rank_dest) !proc 0 does not receive anything
  else
     id = irecv (itp,fwork2,nb)
     call csend (itp,field(:,:,ifin-1),nb,rank_dest)
     call msgwait (id)
  end if

  field(:,:,ifin:ifin+2) = field(:,:,ifin:ifin+2) + fwork1(:,:,:)
  field(:,:,ideb-2:ideb) = field(:,:,ideb-2:ideb) + fwork2(:,:,:)


  !* Now same thing for the weights !!

  ! first, communicate the bottom ghost plane
  rank_dest = rank-1
  if (rank_dest == -1) rank_dest = MPI_PROC_NULL
  if (rank == size-1) then
     call csend (itm,n(:,:,ideb-1),nb,rank_dest) !proc size-1 does not receive anything
  else
     id = irecv (itm,fwork1,nb)
     call csend (itm,n(:,:,ideb-1),nb,rank_dest)
     call msgwait (id)
  end if

  !  !then communicate the upper ghost plane
  rank_dest = rank+1
  if (rank_dest == size) rank_dest = MPI_PROC_NULL

  if (rank == 0) then
     call csend (itp,n(:,:,ifin-1),nb,rank_dest) !proc 0 does not receive anything
  else
     id = irecv (itp,fwork2,nb)
     call csend (itp,n(:,:,ifin-1),nb,rank_dest)
     call msgwait (id)
  end if

  n(:,:,ifin:ifin+2) = n(:,:,ifin:ifin+2) + fwork1(:,:,:)
  n(:,:,ideb-2:ideb) = n(:,:,ideb-2:ideb) + fwork2(:,:,:)

end subroutine exchange_ghost_planes
!-------------------------------------------------------------------------------


subroutine exchange_ghost_planes_dC (dc)

! communicate the ghost planes between processors
! 3 radial layers to share in total !

  use tracerstuff
  use globall
  use tracers_parallel_mod
  use mpi

  implicit none

  real,intent(inout):: dc(Nlong,Nlat,ideb_Ctra-2:ifin_Ctra+2)

  integer nb,itm,itp,id,rank_dest,ier

  nb = Nlat*Nlong*2
  itm = 100
  itp = 200

  ! first, communicate the bottom ghost plane
  rank_dest = rank-1
  if (rank_dest == -1) rank_dest = MPI_PROC_NULL
  if (rank == size-1) then
     call csend (itm,dc(1,1,ideb_Ctra),nb,rank_dest) !proc size-1 does not receive anything
  else
     id = irecv (itm,dc(1,1,ifin_Ctra+1),nb)
     call csend (itm,dc(1,1,ideb_Ctra),nb,rank_dest)
     call msgwait (id)
  end if

  !  !then communicate the upper ghost plane
  rank_dest = rank+1
  if (rank_dest == size) rank_dest = MPI_PROC_NULL

  if (rank == 0) then
     call csend (itp,dc(1,1,ifin_Ctra-1),nb,rank_dest) !proc 0 does not receive anything
  else
     id = irecv (itp,dc(1,1,ideb_Ctra-2),nb)
     call csend (itp,dc(1,1,ifin_Ctra-1),nb,rank_dest)
     call msgwait (id)
  end if

end subroutine exchange_ghost_planes_dC
!-------------------------------------------------------------------------------


subroutine exchange_ghost_planes_T (n)

! communicate the ghost planes between processors

  use globall,only : Nlat,Nlong,rank,size
  use tracerstuff
  use tracers_parallel_mod
  use mpi

  implicit none

  real,intent(inout):: n(Nlong,Nlat,ideb_Ttra-2:ifin_Ttra+2)

  integer nb,itm,itp,id,rank_dest,ier
  real,dimension(Nlong,Nlat,3):: twork1,twork2

  nb = Nlat*Nlong*3
  itm = 100
  itp = 200

  twork1(:,:,:) = 0.
  twork2(:,:,:) = 0.

  ! first, communicate the bottom ghost plane
  rank_dest = rank-1
  if (rank_dest == -1) rank_dest = MPI_PROC_NULL
  if (rank == size-1) then
     call csend (itm,temp(:,:,ideb_Ttra-1),nb,rank_dest) !proc size-1 does not receive anything
  else
     id = irecv (itm,twork1,nb)
     call csend (itm,temp(:,:,ideb_Ttra-1),nb,rank_dest)
     call msgwait (id)
  end if

  !  !then communicate the upper ghost plane
  rank_dest = rank+1
  if (rank_dest == size) rank_dest = MPI_PROC_NULL

  if (rank == 0) then
     call csend (itp,temp(:,:,ifin_Ttra-1),nb,rank_dest) !proc 0 does not receive anything
  else
     id = irecv (itp,twork2,nb)
     call csend (itp,temp(:,:,ifin_Ttra-1),nb,rank_dest)
     call msgwait (id)
  end if

  temp(:,:,ifin_Ttra:ifin_Ttra+2) = temp(:,:,ifin_Ttra:ifin_Ttra+2) + twork1(:,:,:)
  temp(:,:,ideb_Ttra-2:ideb_Ttra) = temp(:,:,ideb_Ttra-2:ideb_Ttra) + twork2(:,:,:)


!* Now same thing for the weights !!

  ! first, communicate the bottom ghost plane
  rank_dest = rank-1
  if (rank_dest == -1) rank_dest = MPI_PROC_NULL
  if (rank == size-1) then
     call csend (itm,n(:,:,ideb_Ttra-1),nb,rank_dest) !proc size-1 does not receive anything
  else
     id = irecv (itm,twork1,nb)
     call csend (itm,n(:,:,ideb_Ttra-1),nb,rank_dest)
     call msgwait (id)
  end if

  !  !then communicate the upper ghost plane
  rank_dest = rank+1
  if (rank_dest == size) rank_dest = MPI_PROC_NULL

  if (rank == 0) then
     call csend (itp,n(:,:,ifin_Ttra-1),nb,rank_dest) !proc 0 does not receive anything
  else
     id = irecv (itp,twork2,nb)
     call csend (itp,n(:,:,ifin_Ttra-1),nb,rank_dest)
     call msgwait (id)
  end if

  n(:,:,ifin_Ttra:ifin_Ttra+2) = n(:,:,ifin_Ttra:ifin_Ttra+2) + twork1(:,:,:)
  n(:,:,ideb_Ttra-2:ideb_Ttra) = n(:,:,ideb_Ttra-2:ideb_Ttra) + twork2(:,:,:)

end subroutine exchange_ghost_planes_T
!-------------------------------------------------------------------------------


subroutine exchange_ghost_planes_dT (dt)

! communicate the ghost planes between processors
! 3 radial layers to share in total !

  use tracerstuff
  use globall,only : Nlat,Nlong,size,rank
  use tracers_parallel_mod
  use mpi

  implicit none

  real,intent(inout):: dt(Nlong,Nlat,ideb_Ttra-2:ifin_Ttra+2)

  integer nb,itm,itp,id,rank_dest,ier

  nb = Nlat*Nlong*2
  itm = 100
  itp = 200

  ! first, communicate the bottom ghost plane
  rank_dest = rank-1
  if (rank_dest == -1) rank_dest = MPI_PROC_NULL
  if (rank == size-1) then
     call csend (itm,dt(1,1,ideb_Ttra),nb,rank_dest) !proc size-1 does not receive anything
  else
     id = irecv (itm,dt(1,1,ifin_Ttra+1),nb)
     call csend (itm,dt(1,1,ideb_Ttra),nb,rank_dest)
     call msgwait (id)
  end if

  !  !then communicate the upper ghost plane
  rank_dest = rank+1
  if (rank_dest == size) rank_dest = MPI_PROC_NULL

  if (rank == 0) then
     call csend (itp,dt(1,1,ifin_Ttra-1),nb,rank_dest) !proc 0 does not receive anything
  else
     id = irecv (itp,dt(1,1,ideb_Ttra-2),nb)
     call csend (itp,dt(1,1,ifin_Ttra-1),nb,rank_dest)
     call msgwait (id)
  end if


end subroutine exchange_ghost_planes_dT
!-------------------------------------------------------------------------------


subroutine exchange_ghost_planes_V (vel,ilat_deb,ilat_fin)

! communicate the ghost planes between processors

  use globall
  use tracers_parallel_mod
  use tracerstuff
  use mpi

  implicit none

  integer,intent(in):: ilat_deb,ilat_fin
  real,intent(inout):: vel(3,Nlong,ilat_deb:ilat_fin,ideb_veltra-2:ifin_veltra+2)

  integer nb,i,itm,itp,id,rank_dest


  nb = 3*(ilat_fin-ilat_deb+1)*Nlong*2
  itm = 100
  itp = 200

!  do i=1,3

  ! first, communicate the bottom ghost plane
     rank_dest = rank-1
     if (rank_dest == -1) rank_dest = MPI_PROC_NULL
     if (rank == size-1) then
        call csend (itm,vel(1,1,1,ideb_all),nb,rank_dest) !proc size-1 does not receive anything
     else
        id = irecv (itm,vel(1,1,1,ifin_all+1),nb)
        call csend (itm,vel(1,1,1,ideb_all),nb,rank_dest)
        call msgwait (id)
     end if

     !  !then communicate the upper ghost plane
     rank_dest = rank+1
     if (rank_dest == size) rank_dest = MPI_PROC_NULL

     if (rank == 0) then
        call csend (itp,vel(1,1,1,ifin_all-1),nb,rank_dest) !proc 0 does not receive anything
     else
        id = irecv (itp,vel(1,1,1,ideb_all-2),nb)
        call csend (itp,vel(1,1,1,ifin_all-1),nb,rank_dest)
        call msgwait (id)
     end if

!  end do

end subroutine exchange_ghost_planes_V
!-------------------------------------------------------------------------------


subroutine exchange_ghost_planes_dir (dir,ilat_deb,ilat_fin)

! communicate the ghost planes between processors

  use globall
  use tracers_parallel_mod
  use tracerstuff
  use mpi

  implicit none

  integer,intent(in):: ilat_deb,ilat_fin
  integer,intent(inout):: dir(3,Nlong,ilat_deb:ilat_fin,ideb_veltra-2:ifin_veltra+2)

  integer nb,i,itm,itp,id,rank_dest


  nb = 3*(ilat_fin-ilat_deb+1)*Nlong*2
  itm = 100
  itp = 200

  ! first, communicate the bottom ghost plane
  rank_dest = rank-1
  if (rank_dest == -1) rank_dest = MPI_PROC_NULL
  if (rank == size-1) then
     call csend_integer (itm,dir(1,1,1,ideb_all),nb,rank_dest) !proc size-1 does not receive anything
  else
     id = irecv_integer (itm,dir(1,1,1,ifin_all+1),nb)
     call csend_integer (itm,dir(1,1,1,ideb_all),nb,rank_dest)
     call msgwait (id)
  end if

  !  !then communicate the upper ghost plane
  rank_dest = rank+1
  if (rank_dest == size) rank_dest = MPI_PROC_NULL

  if (rank == 0) then
     call csend_integer (itp,dir(1,1,1,ifin_all-1),nb,rank_dest) !proc 0 does not receive anything
  else
     id = irecv_integer (itp,dir(1,1,1,ideb_all-2),nb)
     call csend_integer (itp,dir(1,1,1,ifin_all-1),nb,rank_dest)
     call msgwait (id)
  end if


end subroutine exchange_ghost_planes_dir
!-------------------------------------------------------------------------------
