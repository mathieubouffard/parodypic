!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                       !!
!!  Version JA-2.3 of Feb  2012                                          !!
!!                                                                       !!
!!  This contains the fourier transform routines                         !!
!!                                                                       !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine Fourierr_spec_spat2(data)

 Use globall

 Implicit None

integer i
complex data(nphi/2,nlat*15) 

!$OMP PARALLEL DO SCHEDULE (DYNAMIC,1)
 do iblock=1,nblock15
    call fourtf(data(:,start15(iblock):stop15(iblock)),Nphi,1,stop15(iblock)-start15(iblock)+1)
 enddo
!$OMP END PARALLEL DO

 continue
end subroutine Fourierr_spec_spat2

subroutine Fourierr_spat_spec2(data)

Use globall

Implicit None

integer i
complex data(nphi/2,nlat*7) 

!$OMP PARALLEL DO SCHEDULE (DYNAMIC,1)
do iblock=1,nblock7
   call fourtf(data(:,start7(iblock):stop7(iblock)),Nphi,-1,stop7(iblock)-start7(iblock)+1)
enddo
!$OMP END PARALLEL DO

! MOVED TO GAUSS WEIGHT
! data=data*pi

! MOVED TO LEGENDRE TRANSFORM
! data=conjg(data)
! data(2:Nphi/2,:)=2.0*data(2:Nphi/2,:)

! WARNING NOT DONE BY PRESENT FFT
! data(1,:)=cmplx(real(data(1,:)),0.0)

continue
end subroutine Fourierr_spat_spec2


Subroutine quick_fix_para
  
  Use globall
  
  Implicit NONE
  
  integer lm,ir

! TRES MAUVAIS EN PERFS. 
! A OPTIMISER
  do lm=1,LMmax
     if (mi(lm)==0) then
        do ir=ideb_Vp-2,ifin_Vp+2
           yVpr(lm,ir)=cmplx(real(yVpr(lm,ir)),0.0)
        enddo
        do ir=ideb_Vt-2,ifin_Vt+2
           yVtr(lm,ir)=cmplx(real(yVtr(lm,ir)),0.0)
        enddo
        do ir=ideb_Bp-2,ifin_Bp+2         
           yBpr(lm,ir)=cmplx(real(yBpr(lm,ir)),0.0)
        enddo
        do ir=ideb_Bt-2,ifin_Bt+2
           yBtr(lm,ir)=cmplx(real(yBtr(lm,ir)),0.0)
        enddo
        do ir=ideb_Tt-2,ifin_Tt+2
           yTtr(lm,ir)=cmplx(real(yTtr(lm,ir)),0.0)
        enddo
     endif
  enddo
  Return
End Subroutine quick_fix_para

subroutine fftinit

  !
  !  Initializes auxiliary arrays for Fourier transforms
  !  (self-supplied version)
  !
  use globall

  implicit none

  !-- input/output:

  !-- local:
  integer k,kk,n_phi_max

  ! Modif Aubert&Dormy...
  n_phi_max = Nlong

  !-- end of declaration
  !--------------------------------------------------------------------

  call fax(ifaxf,n_phi_max)
  k=ifaxf(1)
  if((k .lt. 1) .or. (ifaxf(k+1) .gt. 5)) stop '17'
  if (rank==0) print *
  if (rank==0) print *,'FFT init'
  if (rank==0) write(6,'(/'' Fourier factors= '',10I2/)') &
       (ifaxf(kk),kk=2,k+1)
  if (rank==0) print *
  if (rank==0) print *
  call fftrig(trigsf,n_phi_max)
  return
end subroutine fftinit
!
!  **************        NEW SUBROUTINE        *************************
!
subroutine fax(ifax,n)
  !
  !     calculates factorisation for Fourier transform
  !     ifax: (output) Number of factors in ifax(1), list of factors in
  !                    following elements
  !     n: (input) size of transform
  !     called in fftinit
  !
  implicit none

  !-- input:
  integer n

  !-- output:
  integer ifax(*)

  !-- local:
  integer nn,k,inc,ii,i,istop,l,nfax,item

  !-- end of declaration
  !-----------------------------------------------------------------------

  nn=n/2
  k=1
  !
  !     test for factors of 4
  !
20 if (mod(nn,4).ne.0) go to 30
  k=k+1
  ifax(k)=4
  nn=nn/4
  if (nn.eq.1) go to 80
  go to 20
  !
  !     test for extra factor of 2
  !
30 if (mod(nn,2).ne.0) go to 40
  k=k+1
  ifax(k)=2
  nn=nn/2
  if (nn.eq.1) go to 80
  !
  !     test for factors of 3
  !
40 if (mod(nn,3).ne.0) go to 50
  k=k+1
  ifax(k)=3
  nn=nn/3
  if (nn.eq.1) go to 80
  go to 40
  !
  !     now find remaining factors
  !
50 l=5
  inc=2
  !
  !     inc alternately takes on values 2 and 4
  !
60 if (mod(nn,l).ne.0) go to 70
  k=k+1
  ifax(k)=l
  nn=nn/l
  if (nn.eq.1) go to 80
  go to 60
  !
70 l=l+inc
  inc=6-inc
  go to 60
  !
80 ifax(1)=k-1
  !     ifax(1) contains number of factors
  nfax=ifax(1)
  !
  !     sort factors into ascending order
  !
  if (nfax.eq.1) return
  !
  do ii=2,nfax
     istop=nfax+2-ii
     do  i=2,istop
        if (ifax(i+1).lt.ifax(i)) then
           item=ifax(i)
           ifax(i)=ifax(i+1)
           ifax(i+1)=item
        endif
     enddo
  enddo
  !
  return
end subroutine fax
!
!  **************        NEW SUBROUTINE        *************************
!
subroutine fftrig(trigs,n)
  !
  !     called in fftinit
  !
  implicit none

  !-- input:
  integer n

  !-- output:
  real trigs(*)

  !-- local:
  integer nn,i,nh,l
  real pi,angle,del

  !-- end of declaration
  !----------------------------------------------------------------------

  pi=4.*atan(1.)
  nn=n/2
  del=(pi+pi)/real(nn)
  !
  do  i=1,n,2
     angle=0.5*real(i-1)*del
     trigs(i)=cos(angle)
     trigs(i+1)=sin(angle)
  enddo
  !
  del=0.5*del
  nh=(nn+1)/2
  l=nh+nh
  !
  do i=1,l,2
     angle=0.5*real(i-1)*del
     trigs(n+i)=cos(angle)
     trigs(n+i+1)=sin(angle)
  end do
  !
  return
end subroutine fftrig

!  ** This self-supplied FFT is a portable version to be used on other
!  ** machines than IBM RISC 6000. This file contains also the subroutines
!  ** called by fourtf: fft99a, fft99b, wpass2, wpass3, wpass4 and wpass5
!
subroutine fourtf(a,ld_a,isign,nsize)
  !
  !
  !     called in amhd, upcont, spherictf
  !
  !
  ! purpose      perform a number of simultaneous real/half-complex
  !              periodic fast fourier transforms or corresponding inverse
  !              transforms, using ordinary spatial order of
  !              gridpoint values.  given a set
  !              of real data vectors, the package returns a set of
  !              "half-complex" fourier coefficient vectors, or vice
  !              versa.  the length of the transforms must be an even
  !              number that has no other factors except possibly powers
  !              of 2, 3, and 5.  this version of fft991 is
  !              optimized for use on the cray-1.
  !
  ! argument     a(n_theta_max*(n_phi_max+2))
  ! dimensions
  !
  !
  ! on input     a
  !               an array of length n_theta_max*(n_phi_max+2) containing the input data
  !               or coefficient vectors.  this array is overwritten by
  !               the results.
  !
  !              n_phi_max (from param)
  !               the length of each transform (see definition of
  !               transforms, below).
  !
  !              n_theta_max (from param)
  !               the number of transforms to be done simultaneously.
  !
  !              isign
  !               = +1 for a transform from fourier coefficients to
  !                    gridpoint values.
  !               = -1 for a transform from gridpoint values to fourier
  !                    coefficients.
  !
  ! on output    a
  !               if isign = +1, and n_theta_max coefficient vectors are supplied
  !               each containing the sequence
  !
  !               a(0),b(0),a(1),b(1),...,a(n_phi_max/2),b(n_phi_max/2)  (n_phi_max+2 values)
  !
  !               then the result consists of n_theta_max data vectors each
  !               containing the corresponding n_phi_max+2 gridpoint values
  !
  !               for fft991, x(0), x(1), x(2),...,x(n_phi_max-1),0,0.
  !                    (n_phi_max+2) real values with x(n)=x(n_phi_max+1)=0
  !
  !               when isign = +1, the transform is defined by
  !                 x(j)=sum(k=0,...,n_phi_max-1)(c(k)*exp(2*i*j*k*pi/n_phi_max))
  !                 where c(k)=a(k)+i*b(k) and c(n_phi_max-k)=a(k)-i*b(k)
  !                 and i=sqrt (-1)
  !                    for k=0,...,n_phi_max/2    i.e., (n_phi_max/2+1) complex values
  !                    with c(0) = c(n_phi_max) = a(0) and c(n_phi_max/2)=a(n_phi_max/2)=0
  !
  !               if isign = -1, and n_theta_max data vectors are supplied each
  !               containing a sequence of gridpoint values x(j) as
  !               defined above, then the result consists of n_theta_max vectors
  !               each containing the corresponding fourier cofficients
  !               a(k), b(k), 0 .le. k .le n_phi_max/2.
  !
  !               when isign = -1, the inverse transform is defined by
  !                 c(k)=(1/n_phi_max)*sum(j=0,...,n_phi_max-1)(x(j)*exp(-2*i*j*k*pi/n_phi_max))
  !                 where c(k)=a(k)+i*b(k) and i=sqrt(-1)
  !                 for k=0,...,n/2
  !
  !               a call with isign=+1 followed by a call with isign=-1
  !               (or vice versa) returns the original data.
  !
  !               note the fact that the gridpoint values x(j) are real
  !               implies that b(0)=b(n_phi_max/2)=0.  for a call with isign=+1,
  !               it is not actually necessary to supply these zeros.
  !               note starting from grid with x(n)=x(n+1)=0
  !               then transforming to spectral (sign=-1)
  !               then c(n_phi_max/2)=a(n_phi_max/2) is not necessarily 0
  !               unless there is no aliasing.
  !-----------------------------------------------------------------------
  use globall
  implicit none

  !-- input:

  integer ld_a         ! leading dimension of a

  integer nrp

  integer n_phi_max

  real a(ld_a,*)
  integer isign,nsize

  !-- output: transformed a(nrp,*)

  !-- local:
  logical tofro
  integer nfax,nodd,njap1,njah
  integer i,i0,i1,i2,ia,ic,id,j
  integer laa,k,iffc
  !      real wrk(nrp,nfs)   ! work array of at least the same size as a


  real wrk(ld_a,nsize)

  !-- end of declaration
  !----------------------------------------------------------------------
  !
  nrp=ld_a
  n_phi_max=ld_a-2


  nfax=ifaxf(1)
  nodd=mod(nfax,2)
  njap1=n_phi_max+1
  njah=n_phi_max/2
  !
  i0=0
  i1=i0+1
  i2=nsize
  !
  if (isign.eq.+1) then   ! Preprocessing
     call fft99a(a(1,i1),wrk,trigsf,nrp,nsize)
     tofro=.false.
     go to 40
  endif
  !
  tofro=.true.


  if(nodd.eq.0) then
     do i=1,nsize,2
        ia=i+1
        ic=i0+i
        id=ic+1
        do j=1,n_phi_max
           wrk(j,i)=a(j,ic)
           wrk(j,ia)=a(j,id)
        enddo  ! j
     enddo    ! ic
     tofro=.false.
  endif
  !
  !     complex transform
  !
40 laa=1
  do k=1,nfax
     iffc=ifaxf(k+1)
     if (tofro) then
        if(iffc.eq.2) then
           call wpass2(a(1,i1),a(2,i1),wrk,wrk(2,1),trigsf, &
                nrp,laa,nsize)
        else if(iffc.eq.3) then
           call wpass3(a(1,i1),a(2,i1),wrk,wrk(2,1),trigsf, &
                nrp,laa,nsize)
        else if(iffc.eq.4) then
           call wpass4(a(1,i1),a(2,i1),wrk,wrk(2,1),trigsf, &
                nrp,laa,nsize)
        else if(iffc.eq.5) then
           call wpass5(a(1,i1),a(2,i1),wrk,wrk(2,1),trigsf, &
                nrp,laa,nsize)
        endif
     else
        if(iffc.eq.2) then
           call wpass2(wrk,wrk(2,1),a(1,i1),a(2,i1),trigsf, &
                nrp,laa,nsize)
        else if(iffc.eq.3) then
           call wpass3(wrk,wrk(2,1),a(1,i1),a(2,i1),trigsf, &
                nrp,laa,nsize)
        else if(iffc.eq.4) then
           call wpass4(wrk,wrk(2,1),a(1,i1),a(2,i1),trigsf, &
                nrp,laa,nsize)
        else if(iffc.eq.5) then
           call wpass5(wrk,wrk(2,1),a(1,i1),a(2,i1),trigsf, &
                nrp,laa,nsize)
        endif
     endif
     laa=laa*iffc
     tofro=.not. tofro
  enddo  ! k
  !
  if (isign.eq.-1) then
     call fft99b(wrk,a(1,i1),trigsf,nrp,nsize)
     return
  endif
  !


  if (nodd.eq.0) then
     do i=1,nsize,2
        ia=i+1
        ic=i0+i
        id=ic+1
        do j=1,n_phi_max
           a(j,ic)=wrk(j,i)
           a(j,id)=wrk(j,ia)
        enddo
     enddo
  endif
  !
  do ic=i1,i2  ! Fill zeros into 2 extra elements
     a(njap1,ic)=0.0
     a(nrp,ic)=0.0
  enddo
  !
  return
end subroutine fourtf
!
!  **************        NEW SUBROUTINE        *************************
!
subroutine fft99a(a,work,trigs,nrp,nsize)
  !
  implicit none

  !-- input/output:
  integer nrp,nsize
  real a(*),work(*),trigs(*)

  !-- local:
  integer nja,njah,njap1
  integer ic,ia0,ib0,ia,ib,k,kk,kkmax
  real c,s

  !-- end of declaration
  !------------------------------------------------------------------------
  !
  !     preprocessing step (isign=+1)
  !     (spectral to gridpoint transform)
  !
  !     called in fourtf
  !
  nja=nrp-2
  njah=nja/2
  njap1=nja+1
  kkmax=njah/2
  !
  do ic=1,nsize
     ia0=(ic-1)*nrp
     ib0=ic*nrp
     ia=ia0+1
     ib=ib0-1
     work(ia)=a(ia)+a(ib)
     work(ia+1)=a(ia)-a(ib)
     k=1
     do kk=2,kkmax
        k=k+2
        c=trigs(nja+k)
        s=trigs(njap1+k)
        ia=ia0+k
        ib=ib0-k
        work(ia)=(a(ia)+a(ib))- &
             (s*(a(ia)-a(ib))+c*(a(ia+1)+a(ib+1)))
        work(ib)=(a(ia)+a(ib))+ &
             (s*(a(ia)-a(ib))+c*(a(ia+1)+a(ib+1)))
        work(ia+1)=(c*(a(ia)-a(ib))-s*(a(ia+1)+a(ib+1)))+ &
             (a(ia+1)-a(ib+1))
        work(ib+1)=(c*(a(ia)-a(ib))-s*(a(ia+1)+a(ib+1)))- &
             (a(ia+1)-a(ib+1))
     enddo
     ia=ia0+njah+1
     work(ia)=2.0*a(ia)
     work(ia+1)=-2.0*a(ia+1)
  enddo
  !
  return
end subroutine fft99a
!
!  **************        NEW SUBROUTINE        *************************
subroutine fft99b(work,a,trigsf,nrp,nsize)
  !-----------------------------------------------------------------------
  !
  !     postprocessing step (isign=-1)
  !     (gridpoint to spectral transform)
  !
  !     called in fourtf
  !
  !-----------------------------------------------------------------------

  implicit none

  !-- input/output:
  integer nrp,nsize
  real work(*),a(*),trigsf(*)

  !-- local:
  integer nja,njah,njap1,kkmax,k,kk
  integer ia,ib,ic,ia0,ib0
  real scal1,scal2,s,c

  !-- end of declaration
  !-----------------------------------------------------------------------
  !
  !     postprocessing step (isign=-1)
  !     (gridpoint to spectral transform)
  !
  !     called in fourtf
  !
  nja=nrp-2
  njah=nja/2
  njap1=nja+1
  kkmax=njah/2
  scal1=1./real(nja)
  scal2=0.5*scal1
  !
  do ic=1,nsize
     ia0=(ic-1)*nrp
     ib0=ic*nrp
     ia=ia0+1
     ib=ib0-1
     a(ia)=scal1*(work(ia)+work(ia+1))
     a(ib)=scal1*(work(ia)-work(ia+1))
     k=1
     do kk=2,kkmax
        k=k+2
        c=trigsf(nja+k)
        s=trigsf(nja+k+1)
        ia=ia0+k
        ib=ib0-k
        a(ia)=scal2*((work(ia)+work(ib)) &
             +(c*(work(ia+1)+work(ib+1))+s*(work(ia)-work(ib))))
        a(ib)=scal2*((work(ia)+work(ib)) &
             -(c*(work(ia+1)+work(ib+1))+s*(work(ia)-work(ib))))
        a(ia+1)=scal2*((c*(work(ia)-work(ib)) &
             -s*(work(ia+1)+work(ib+1)))+(work(ib+1)-work(ia+1)))
        a(ib+1)=scal2*((c*(work(ia)-work(ib)) &
             -s*(work(ia+1)+work(ib+1)))-(work(ib+1)-work(ia+1)))
     enddo ! k
     ia=ia0+njah+1
     a(ia)=scal1*work(ia)
     a(ia+1)=-scal1*work(ia+1)
  enddo   ! ic
  return
end subroutine fft99b
!
!  **************        NEW SUBROUTINE        *************************
subroutine wpass2(a,b,c,d,trigs,nrp,laa,nsize)
  !-----------------------------------------------------------------------
  !
  !     called in fourtf
  !     reduction for factor 2
  !
  !     if(laa.ne.1) stop 'call to wpass2 with laa .ne. 1'
  !
  !-----------------------------------------------------------------------

  implicit none

  !-- input/ouput:
  integer nrp,laa,nsize
  real a(*),b(*),c(*),d(*),trigs(*)

  !-- local:
  integer i,j,ijk,iadd,in,n
  real c1,s1,an,bn

  !-- end of declaration
  !-----------------------------------------------------------------------

  n=(nrp-2)/2
  !
  do ijk=1,nsize
     iadd=(ijk-1)*nrp - 1
     do in=2,n,2
        i=iadd+in
        j=i+in-2
        c1=trigs(in-1)
        s1=trigs(in)
        an=a(i)-a(n+i)
        bn=b(i)-b(n+i)
        c(j)=a(i)+a(n+i)
        d(j)=b(i)+b(n+i)
        c(2+j)=c1*an-s1*bn
        d(2+j)=s1*an+c1*bn
     enddo
  enddo
  !
  return
end subroutine wpass2
!
!  **************        NEW SUBROUTINE        *************************
subroutine wpass3(a,b,c,d,trigs,nrp,laa,nsize)
  !-----------------------------------------------------------------------
  !
  !     called in fourtf
  !
  !-----------------------------------------------------------------------

  implicit none

  !-- input/output:
  integer nrp,laa,nsize
  real a(*),b(*),c(*),d(*),trigs(*)

  !-- local
  integer n,m,iink,jink,jump,ib,jb,ic,jc
  integer ims,ijk,iadd,l,kc,kk,i,j,im

  integer mdim
  parameter(mdim=180)
  integer iindex(mdim),jindex(mdim)
  real c1(mdim),c2(mdim)
  real s1(mdim),s2(mdim)

  real sin60
  data sin60/0.866025403784437/

  !-- end of declaration
  !------------------------------------------------------------------------
  !
  n=(nrp-2)/2
  m=n/3
  iink=m*2
  jink=laa*2
  jump=2*jink
  !
  !     coding for factor 3
  !
50 ib=iink
  jb=jink
  ic=ib+iink
  jc=jb+jink
  !
  ims=1
  if(laa.lt.m.and.laa.lt.16) go to 65
  do ijk=1,nsize
     iadd=(ijk-1)*nrp-1
     do l=1,laa
        i=l*2+iadd
        c(  i)=a(  i)+(a(ib+i)+a(ic+i))
        d(  i)=b(  i)+(b(ib+i)+b(ic+i))
        c(jb+i)=(a(  i)-0.5*(a(ib+i)+a(ic+i))) &
             -(sin60*(b(ib+i)-b(ic+i)))
        c(jc+i)=(a(  i)-0.5*(a(ib+i)+a(ic+i))) &
             +(sin60*(b(ib+i)-b(ic+i)))
        d(jb+i)=(b(  i)-0.5*(b(ib+i)+b(ic+i))) &
             +(sin60*(a(ib+i)-a(ic+i)))
        d(jc+i)=(b(  i)-0.5*(b(ib+i)+b(ic+i))) &
             -(sin60*(a(ib+i)-a(ic+i)))
     enddo
  enddo
  !
  if (laa.eq.m) return
  ims=laa+1
  !
65 do im=ims,m
     kc=(im-1)/laa
     kk=kc*laa
     c1(im)=trigs(2*kk+1)
     s1(im)=trigs(2*kk+2)
     c2(im)=trigs(4*kk+1)
     s2(im)=trigs(4*kk+2)
     iindex(im)=im*2
     jindex(im)=im*2+jump*kc
  enddo
  !
  do ijk=1,nsize
     iadd=(ijk-1)*nrp - 1
     do im=ims,m
        i= iindex(im)+iadd
        j= jindex(im)+iadd
        c(  j)=a(  i)+(a(ib+i)+a(ic+i))
        d(  j)=b(  i)+(b(ib+i)+b(ic+i))
        c(jb+j)= &
             c1(im)*((a(  i)-0.5*(a(ib+i)+a(ic+i))) &
             -(sin60*(b(ib+i)-b(ic+i)))) &
             -s1(im)*((b(  i)-0.5*(b(ib+i)+b(ic+i))) &
             +(sin60*(a(ib+i)-a(ic+i))))
        d(jb+j)= &
             s1(im)*((a(  i)-0.5*(a(ib+i)+a(ic+i))) &
             -(sin60*(b(ib+i)-b(ic+i)))) &
             +c1(im)*((b(  i)-0.5*(b(ib+i)+b(ic+i))) &
             +(sin60*(a(ib+i)-a(ic+i))))
        c(jc+j)= &
             c2(im)*((a(  i)-0.5*(a(ib+i)+a(ic+i))) &
             +(sin60*(b(ib+i)-b(ic+i)))) &
             -s2(im)*((b(  i)-0.5*(b(ib+i)+b(ic+i))) &
             -(sin60*(a(ib+i)-a(ic+i))))
        d(jc+j)= &
             s2(im)*((a(  i)-0.5*(a(ib+i)+a(ic+i))) &
             +(sin60*(b(ib+i)-b(ic+i)))) &
             +c2(im)*((b(  i)-0.5*(b(ib+i)+b(ic+i))) &
             -(sin60*(a(ib+i)-a(ic+i))))
     enddo
  enddo
  !
  return
end subroutine wpass3
!
!  **************        NEW SUBROUTINE        *************************
!

subroutine wpass4(a,b,c,d,trigs,nrp,laa,nsize)
  !-----------------------------------------------------------------------
  !
  !     called in fourtf
  !     reduction for factor 4
  !
  !-----------------------------------------------------------------------

  implicit none

  !-- input/output:
  integer nrp,laa,nsize
  real a(*),b(*),c(*),d(*),trigs(*)

  !-- local:
  integer n,m,iink,jink,jump,ib,jb,ic,jc,id,jd
  integer ims,ijk,iadd,l,kc,kk,i,j,im
  real abdm,aacm,aac,abd
  real bbdm,bacm,bbd,bac

  integer mdim
  parameter(mdim=135)
  integer jindex(mdim)
  real c1(mdim),c2(mdim),c3(mdim)
  real s1(mdim),s2(mdim),s3(mdim)

  !-- end of declaration
  !------------------------------------------------------------------------

  n=(nrp-2)/2
  m=n/4
  iink=m*2
  jink=laa*2
  jump=3*jink
  !
  ib=iink
  jb=jink
  ic=ib+iink
  jc=jb+jink
  id=ic+iink
  jd=jc+jink
  !
  ims=1
  if(laa.lt.m.and.laa.lt.64) go to 105
  do ijk=1,nsize
     iadd=(ijk-1)*nrp-1
     do l=1,laa
        i=l*2+iadd
        aac=a(i)+a(ic+i)
        abd=a(ib+i)+a(id+i)
        bac=b(i)+b(ic+i)
        bbd=b(ib+i)+b(id+i)
        aacm=a(i)-a(ic+i)
        abdm=a(ib+i)-a(id+i)
        bacm=b(i)-b(ic+i)
        bbdm=b(ib+i)-b(id+i)
        c(   i)=aac+abd
        c(jc+i)=aac-abd
        d(   i)=bac+bbd
        d(jc+i)=bac-bbd
        c(jb+i)=aacm-bbdm
        c(jd+i)=aacm+bbdm
        d(jb+i)=bacm+abdm
        d(jd+i)=bacm-abdm
     enddo
  enddo

  if (laa.eq.m) return
  ims=laa+1
  !
105 do im=ims,m
     kc=(im-1)/laa
     kk=kc*laa
     c1(im)=trigs(2*kk+1)
     s1(im)=trigs(2*kk+2)
     c2(im)=trigs(4*kk+1)
     s2(im)=trigs(4*kk+2)
     c3(im)=trigs(6*kk+1)
     s3(im)=trigs(6*kk+2)
     jindex(im)=im*2+jump*kc
  enddo
  !
  do ijk=1,nsize
     iadd=(ijk-1)*nrp - 1
     i=iadd+ims+ims-2
     do im=ims,m
        i=i+2
        j= jindex(im)+iadd
        aac=a(i)+a(ic+i)
        abd=a(ib+i)+a(id+i)
        bac=b(i)+b(ic+i)
        bbd=b(ib+i)+b(id+i)
        aacm=a(i)-a(ic+i)
        abdm=a(ib+i)-a(id+i)
        bacm=b(i)-b(ic+i)
        bbdm=b(ib+i)-b(id+i)
        c(   j)=aac+abd
        d(   j)=bac+bbd
        c(jc+j)=c2(im)*(aac-abd)-s2(im)*(bac-bbd)
        d(jc+j)=s2(im)*(aac-abd)+c2(im)*(bac-bbd)
        c(jb+j)=c1(im)*(aacm-bbdm)-s1(im)*(bacm+abdm)
        d(jb+j)=s1(im)*(aacm-bbdm)+c1(im)*(bacm+abdm)
        c(jd+j)=c3(im)*(aacm+bbdm)-s3(im)*(bacm-abdm)
        d(jd+j)=s3(im)*(aacm+bbdm)+c3(im)*(bacm-abdm)
     enddo
  enddo
  return
  !
end subroutine wpass4
!
!  **************        NEW SUBROUTINE        *************************
subroutine wpass5(a,b,c,d,trigs,nrp,laa,nsize)
  !-----------------------------------------------------------------------
  !
  !     called in fourtf
  !     reduction for factor 5
  !
  !-----------------------------------------------------------------------

  implicit none

  !-- input/output:
  integer nrp,laa,nsize
  real a(*),b(*),c(*),d(*),trigs(*)

  !-- local:
  integer n,m,iink,jink,jump
  integer ib,jb,ic,jc,id,jd,ie,je
  integer ims,ijk,iadd,l,kc,kk,i,j,im

  integer mdim
  parameter(mdim=108)
  integer iindex(mdim),jindex(mdim)
  real c1(mdim),c2(mdim),c3(mdim),c4(mdim)
  real s1(mdim),s2(mdim),s3(mdim),s4(mdim)

  real sin36,cos36,sin72,cos72
  data sin36/0.587785252292473/,cos36/0.809016994374947/, &
       sin72/0.951056516295154/,cos72/0.309016994374947/

  !-- end of declaration
  !------------------------------------------------------------------------

  n=(nrp-2)/2
  m=n/5
  iink=m*2
  jink=laa*2
  jump=4*jink
  !
  ib= iink
  jb= jink
  ic=ib+iink
  jc=jb+jink
  id=ic+iink
  jd=jc+jink
  ie=id+iink
  je=jd+jink
  !
  ims=1
  if(laa.lt.m.and.laa.lt.16) go to 145
  do ijk=1,nsize
     iadd=(ijk-1)*nrp-1
     do l=1,laa
        i=l*2+iadd
        c( i)=a(i)+(a(ib+i)+a(ie+i))+(a(ic+i)+a(id+i))
        d( i)=b(i)+(b(ib+i)+b(ie+i))+(b(ic+i)+b(id+i))
        c(jb+i)=(a(i)+cos72*(a(ib+i)+a(ie+i)) &
             -cos36*(a(ic+i)+a(id+i))) &
             -(sin72*(b(ib+i)-b(ie+i))+sin36*(b(ic+i)-b(id+i)))
        c(je+i)=(a(i)+cos72*(a(ib+i)+a(ie+i)) &
             -cos36*(a(ic+i)+a(id+i))) &
             +(sin72*(b(ib+i)-b(ie+i))+sin36*(b(ic+i)-b(id+i)))
        d(jb+i)=(b(i)+cos72*(b(ib+i)+b(ie+i)) &
             -cos36*(b(ic+i)+b(id+i))) &
             +(sin72*(a(ib+i)-a(ie+i))+sin36*(a(ic+i)-a(id+i)))
        d(je+i)=(b(i)+cos72*(b(ib+i)+b(ie+i)) &
             -cos36*(b(ic+i)+b(id+i))) &
             -(sin72*(a(ib+i)-a(ie+i))+sin36*(a(ic+i)-a(id+i)))
        c(jc+i)=(a(i)-cos36*(a(ib+i)+a(ie+i)) &
             +cos72*(a(ic+i)+a(id+i))) &
             -(sin36*(b(ib+i)-b(ie+i))-sin72*(b(ic+i)-b(id+i)))
        c(jd+i)=(a(i)-cos36*(a(ib+i)+a(ie+i)) &
             +cos72*(a(ic+i)+a(id+i))) &
             +(sin36*(b(ib+i)-b(ie+i))-sin72*(b(ic+i)-b(id+i)))
        d(jc+i)=(b(i)-cos36*(b(ib+i)+b(ie+i)) &
             +cos72*(b(ic+i)+b(id+i))) &
             +(sin36*(a(ib+i)-a(ie+i))-sin72*(a(ic+i)-a(id+i)))
        d(jd+i)=(b(i)-cos36*(b(ib+i)+b(ie+i)) &
             +cos72*(b(ic+i)+b(id+i))) &
             -(sin36*(a(ib+i)-a(ie+i))-sin72*(a(ic+i)-a(id+i)))
     enddo
  enddo
  !
  if (laa.eq.m) return
  ims=laa+1
  !
145 do im=ims,m
     kc=(im-1)/laa
     kk=kc*laa
     c1(im)=trigs(2*kk+1)
     s1(im)=trigs(2*kk+2)
     c2(im)=trigs(4*kk+1)
     s2(im)=trigs(4*kk+2)
     c3(im)=trigs(6*kk+1)
     s3(im)=trigs(6*kk+2)
     c4(im)=trigs(8*kk+1)
     s4(im)=trigs(8*kk+2)
     iindex(im)=im*2
     jindex(im)=im*2+jump*kc
  enddo
  !
  do ijk=1,nsize
     iadd=(ijk-1)*nrp - 1
     do im=ims,m
        i= iindex(im)+iadd
        j= jindex(im)+iadd
        c( j)=a(i)+(a(ib+i)+a(ie+i))+(a(ic+i)+a(id+i))
        d( j)=b(i)+(b(ib+i)+b(ie+i))+(b(ic+i)+b(id+i))
        c(jb+j)= &
             c1(im)*((a(i)+cos72*(a(ib+i)+a(ie+i)) &
             -cos36*(a(ic+i)+a(id+i))) &
             -(sin72*(b(ib+i)-b(ie+i))+sin36*(b(ic+i)-b(id+i)))) &
             -s1(im)*((b(i)+cos72*(b(ib+i)+b(ie+i)) &
             -cos36*(b(ic+i)+b(id+i))) &
             +(sin72*(a(ib+i)-a(ie+i))+sin36*(a(ic+i)-a(id+i))))
        d(jb+j)= &
             s1(im)*((a(i)+cos72*(a(ib+i)+a(ie+i)) &
             -cos36*(a(ic+i)+a(id+i))) &
             -(sin72*(b(ib+i)-b(ie+i))+sin36*(b(ic+i)-b(id+i)))) &
             +c1(im)*((b(i)+cos72*(b(ib+i)+b(ie+i)) &
             -cos36*(b(ic+i)+b(id+i))) &
             +(sin72*(a(ib+i)-a(ie+i))+sin36*(a(ic+i)-a(id+i))))
        c(je+j)= &
             c4(im)*((a(i)+cos72*(a(ib+i)+a(ie+i)) &
             -cos36*(a(ic+i)+a(id+i))) &
             +(sin72*(b(ib+i)-b(ie+i))+sin36*(b(ic+i)-b(id+i)))) &
             -s4(im)*((b(i)+cos72*(b(ib+i)+b(ie+i)) &
             -cos36*(b(ic+i)+b(id+i))) &
             -(sin72*(a(ib+i)-a(ie+i))+sin36*(a(ic+i)-a(id+i))))
        d(je+j)= &
             s4(im)*((a(i)+cos72*(a(ib+i)+a(ie+i)) &
             -cos36*(a(ic+i)+a(id+i))) &
             +(sin72*(b(ib+i)-b(ie+i))+sin36*(b(ic+i)-b(id+i)))) &
             +c4(im)*((b(i)+cos72*(b(ib+i)+b(ie+i)) &
             -cos36*(b(ic+i)+b(id+i))) &
             -(sin72*(a(ib+i)-a(ie+i))+sin36*(a(ic+i)-a(id+i))))
        c(jc+j)= &
             c2(im)*((a(i)-cos36*(a(ib+i)+a(ie+i)) &
             +cos72*(a(ic+i)+a(id+i))) &
             -(sin36*(b(ib+i)-b(ie+i))-sin72*(b(ic+i)-b(id+i)))) &
             -s2(im)*((b(i)-cos36*(b(ib+i)+b(ie+i)) &
             +cos72*(b(ic+i)+b(id+i))) &
             +(sin36*(a(ib+i)-a(ie+i))-sin72*(a(ic+i)-a(id+i))))
        d(jc+j)= &
             s2(im)*((a(i)-cos36*(a(ib+i)+a(ie+i)) &
             +cos72*(a(ic+i)+a(id+i))) &
             -(sin36*(b(ib+i)-b(ie+i))-sin72*(b(ic+i)-b(id+i)))) &
             +c2(im)*((b(i)-cos36*(b(ib+i)+b(ie+i)) &
             +cos72*(b(ic+i)+b(id+i))) &
             +(sin36*(a(ib+i)-a(ie+i))-sin72*(a(ic+i)-a(id+i))))
        c(jd+j)= &
             c3(im)*((a(i)-cos36*(a(ib+i)+a(ie+i)) &
             +cos72*(a(ic+i)+a(id+i))) &
             +(sin36*(b(ib+i)-b(ie+i))-sin72*(b(ic+i)-b(id+i)))) &
             -s3(im)*((b(i)-cos36*(b(ib+i) &
             +b(ie+i))+cos72*(b(ic+i)+b(id+i))) &
             -(sin36*(a(ib+i)-a(ie+i))-sin72*(a(ic+i)-a(id+i))))
        d(jd+j)= &
             s3(im)*((a(i)-cos36*(a(ib+i)+a(ie+i)) &
             +cos72*(a(ic+i)+a(id+i))) &
             +(sin36*(b(ib+i)-b(ie+i))-sin72*(b(ic+i)-b(id+i)))) &
             +c3(im)*((b(i)-cos36*(b(ib+i)+b(ie+i)) &
             +cos72*(b(ic+i)+b(id+i))) &
             -(sin36*(a(ib+i)-a(ie+i))-sin72*(a(ic+i)-a(id+i))))
     enddo
  enddo
  !
  return
end subroutine wpass5
